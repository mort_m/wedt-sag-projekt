package pl.edu.pw.ii.entityminer;

import gate.util.GateException;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: ralpher
 * Date: 03.08.13
 * Time: 23:01
 * To change this template use File | Settings | File Templates.
 */
public class MainGate {

    public static void main(String args[]) throws GateException, IOException {
        AbstractApplicationContext applicationContext = new ClassPathXmlApplicationContext(
                "META-INF/spring/module-context.xml");
        GateHandler gate = applicationContext.getBean("gateHandler", GateHandler.class);
        gate.handleRequest();
        applicationContext.registerShutdownHook();
    }
}
