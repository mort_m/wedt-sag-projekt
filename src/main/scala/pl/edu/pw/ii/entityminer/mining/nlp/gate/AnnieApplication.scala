package pl.edu.pw.ii.entityminer.mining.nlp.gate

import org.apache.log4j.Logger
import java.util.concurrent.atomic.AtomicInteger
import gate._
import java.lang.String
import java.io.{File, PrintWriter}
import gate.creole.{SerialAnalyserController, ANNIEConstants, ResourceInstantiationException, ExecutionException}
import javax.annotation.{PostConstruct, PreDestroy}
import gate.util.Out
import gate.util.persistence.PersistenceManager
import java.util
import pl.edu.pw.ii.entityminer.GateHandler

/**
 * Created by ralpher on 12/14/13.
 */
class AnnieApplication {
  private final val JAPE_PATH_PARAM = "jape_path"
  private final val RESOURCES_PATH_PARAM = "resources_path"

  private final val log: Logger = Logger.getLogger(classOf[GateHandler])
  /**
   * Atomic counter that we use to obtain a unique ID for each handler
   * instance.
   */
  private val nextId: AtomicInteger = new AtomicInteger(1)
  /**
   * The ID of this handler instance.
   */
  private var handlerId: Int = 0
  /**
   * The application that will be run.
   */
  private var application: CorpusController = null
  /**
   * A corpus that will be used to hold the document being processed.
   */
  private var corpus: Corpus = null
  private val confParams: util.Map[String, String] = new util.HashMap[String, String]
  confParams.put(JAPE_PATH_PARAM, "file:./src/main/jape/")
  confParams.put(RESOURCES_PATH_PARAM, "./src/main/resources/")

  /**
   * Set the application that will be run over the documents.
   */
  def setApplication(application: CorpusController) {
    this.application = application
  }

  /**
   * Create the corpus. The PostConstruct annotation means that this
   * method will be called by spring once the handler object has been
   * constructed and its properties (i.e. the application) have been
   * set.
   */
  @PostConstruct def init {
    handlerId = nextId.getAndIncrement
    log.info("init() for GateHandler " + handlerId)
    corpus = Factory.newCorpus("webapp corpus")
    application.setCorpus(corpus)


    Out.prln("Initialising GATE...")
//    Gate.init
    val pluginsHome: File = new File(Gate.getGateHome, "plugins")
    Gate.getCreoleRegister.registerDirectories(new File(pluginsHome, "ANNIE").toURI.toURL)
    Out.prln("...GATE initialised")
    Out.prln("Initialising ANNIE...")
    var annieController = PersistenceManager.loadObjectFromFile(new File(new File(Gate.getPluginsHome, ANNIEConstants.PLUGIN_DIR), ANNIEConstants.DEFAULT_FILE)).asInstanceOf[SerialAnalyserController]
    Out.prln("...ANNIE loaded")
    Out.prln("ANNIE processing resources pipeline:")
    annieController = PResWrapper.loadChunker(annieController)
    annieController.add(4, PResWrapper.loadTransducter(confParams.get(JAPE_PATH_PARAM) + "fontTags.jape"))
    annieController.add(5, PResWrapper.loadTransducter(confParams.get(JAPE_PATH_PARAM) + "classes.jape"))
    annieController.add(PResWrapper.loadTransducter(confParams.get(JAPE_PATH_PARAM) + "nsw.jape"))
    annieController.add(PResWrapper.loadTransducter(confParams.get(JAPE_PATH_PARAM) + "cleanEmbedded.jape"))
    val anniePRs: util.Collection[_] = annieController.getPRs
    val anniePRsA: Array[ProcessingResource] = anniePRs.toArray(new Array[ProcessingResource](0))
    var i: Int = 0
    while (i < anniePRs.size) {
      Out.prln(i + ". PR is " + anniePRsA(i).getName)
      i += 1
    }
  }

  /**
   * Clean-up method. The PreDestroy annotation means that Spring will
   * call the method when the object is no longer required.
   */
  @PreDestroy def cleanup {
    log.info("cleanup() for GateHandler " + handlerId)
    Factory.deleteResource(corpus)
    Factory.deleteResource(application)
  }

  /**
   * Handle a request.
   */
  def handleRequest {
    log.info("Handler " + handlerId + " handling request")
    val text: String = "jakis przykladowy tekst"
    val mime: String = "text/plain"
    var delay: Int = 0
    val delayParam: String = "5"
    if (delayParam != null) {
      try {
        delay = Integer.parseInt(delayParam)
      }
      catch {
        case e: NumberFormatException => {
          log.warn("Failed to parse delay value " + delayParam + ", ignored", e)
        }
      }
    }
    var doc: Document = null
    try {
      log.debug("Creating document")
      doc = Factory.createResource("gate.corpora.DocumentImpl", Utils.featureMap("stringContent", text, "mimeType", mime)).asInstanceOf[Document]
    }
    catch {
      case e: ResourceInstantiationException => {
        failureMessage("Could not create GATE document for input text", e)
        return
      }
    }
    try {
      corpus.add(doc)
      log.info("Executing application")
      application.execute
      try {
        Thread.sleep(delay * 1000)
      }
      catch {
        case e: InterruptedException => {
          Thread.currentThread.interrupt
        }
      }
      log.info("Application completed \n\n")
      successMessage(doc)
    }
    catch {
      case e: ExecutionException => {
        failureMessage("Error occurred which executing GATE application", e)
      }
    }
    finally {
      corpus.clear
      log.info("Deleting document")
      Factory.deleteResource(doc)
    }
  }

  /**
   * Render the document's features in an HTML table.
   */
  private def successMessage(doc: Document) {
    val w: PrintWriter = new PrintWriter(System.out)
    w.println("<html>")
    w.println("<head>")
    w.println("<title>Results - GATE handler " + handlerId + "</title>")
    w.println("</head>")
    w.println("<body>")
    w.println("<h1>Document features: GATE handler " + handlerId + "</h1>")
    w.println("<table border='1'>")
    w.println("<tr><td><b>Name</b></td><td><b>Value</b></td></tr>")
    import scala.collection.JavaConversions._
    for (entry <- doc.getFeatures.entrySet) {
      log.info("Document Features: " + entry.getKey + " value: " + entry.getValue)
      w.println("<tr><td>" + entry.getKey + "</td><td>" + entry.getValue + "</td></tr>")
    }
    w.println("</table>")
    w.println("Anotationsets: " + doc.getAnnotations.get("SectionReference").toString)
    w.println("</body>")
    w.println("</html>")
    w.close
  }

  /**
   * Simple error handler - you would obviously use something more
   * sophisticated in a real application.
   */
  private def failureMessage(message: String, e: Exception) {
    val w: PrintWriter = new PrintWriter(System.out)
    w.println("<html>")
    w.println("<head>")
    w.println("<title>Error - GATE handler " + handlerId + "</title>")
    w.println("</head>")
    w.println("<body>")
    w.println("<h1>Error in GATE handler " + handlerId + "</h1>")
    w.println("<p>" + message + "</p>")
    if (e != null) {
      w.println("<pre>")
      e.printStackTrace(w)
      w.println("</pre>")
    }
    w.println("</body>")
    w.println("</html>")
    w.close
  }
}
