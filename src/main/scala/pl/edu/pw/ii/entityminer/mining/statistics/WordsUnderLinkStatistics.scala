package pl.edu.pw.ii.entityminer.mining.statistics

import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData

import scala.collection.mutable
import org.jsoup.nodes.Document
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import org.springframework.beans.factory.annotation.Autowired
import pl.edu.pw.ii.entityminer.mining.nlp.WebPageTokenizer
import pl.edu.pw.ii.entityminer.crawler.HtmlConsts

/**
 * Created by Raphael Hazan on 3/29/2014.
 */
class WordsUnderLinkStatistics(val labelToYesAndNo: mutable.Map[String, (Int, Int)]) extends DataStatistics {
  override val CODE: String = "WordsUnderLinkStatistics"

  override def accumulate(other: DataStatistics): Unit = {
    for ((label, (linkNo, notLinkNo)) <- other.asInstanceOf[WordsUnderLinkStatistics].labelToYesAndNo) {
      labelToYesAndNo.get(label) match {
        case None =>
          labelToYesAndNo put (label, (linkNo, notLinkNo))
        case Some((thisLinkNo, thisNotLinkNo)) =>
          labelToYesAndNo put (label, (thisLinkNo + linkNo, thisNotLinkNo + notLinkNo))
      }
    }
  }
}

class WordsUnderLinkStatisticsComputer extends StatisticsComputer {

  @Autowired
  private var webPageTokenizer: WebPageTokenizer = _

  override def computeStatistics(conferenceData: ConferenceData, document: Document, manifestEntry: (String, (Set[String], String)), documentPath: String): DataStatistics = {
    val labelToWordUnderLinkOrNot = mutable.Map[String, (Int, Int)]()
    val tokens = webPageTokenizer.tokenize(document)
    for (token <- tokens) {
      if (ConferenceHtmlAnnotation.ALL_ANNOTATIONS.contains(token.parentName)) {
        var parent = token.node.parent().parent()
        var isHighlighted = false
        while (HtmlConsts.TextHighlighters.contains(parent.nodeName())) {
          isHighlighted = true
          parent = parent.parent()
        }
        if (parent.nodeName() == HtmlConsts.Anchor) {
          if (isHighlighted) {
            println("ANCHOR HIGHLIGHTED: " + token.text + " in document " + documentPath + " " + manifestEntry._2._2)
          }
          labelToWordUnderLinkOrNot.get(token.parentName) match {
            case None => labelToWordUnderLinkOrNot put (token.parentName, (1, 0))
            case Some((yes, no)) => labelToWordUnderLinkOrNot put (token.parentName, (yes + 1, no))
          }
        } else {
          labelToWordUnderLinkOrNot.get(token.parentName) match {
            case None => labelToWordUnderLinkOrNot put (token.parentName, (0, 1))
            case Some((yes, no)) => labelToWordUnderLinkOrNot put (token.parentName, (yes, no + 1))
          }
        }
      }
    }
    new WordsUnderLinkStatistics(labelToWordUnderLinkOrNot)
  }
}