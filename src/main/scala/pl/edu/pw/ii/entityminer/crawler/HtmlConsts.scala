package pl.edu.pw.ii.entityminer.crawler

/**
 * Created by Raphael Hazan on 3/29/2014.
 */
object HtmlConsts {
  val TextHighlighters = Set("font", "strong", "b", "u")
  val LayoutElementRegexes = Set("p", "h\\d", "title", "ul")
  val Anchor = "a"
  val Title = "title"
  val BreakLine = "br"
  val Li = "li"
  val Tr = "tr"
}
