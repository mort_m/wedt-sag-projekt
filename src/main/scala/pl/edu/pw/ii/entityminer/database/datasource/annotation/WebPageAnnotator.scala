package pl.edu.pw.ii.entityminer.database.datasource.annotation

import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryCollector
import pl.edu.pw.ii.entityminer.database.datasource.wikicfp.WikicfpDatasource
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import org.jsoup.nodes.Document

/**
 * Created with IntelliJ IDEA.
 * User: ralpher
 * Date: 7/13/13
 * Time: 6:20 PM
 * To change this template use File | Settings | File Templates.
 */
trait WebPageAnnotator {
  def annotateConferencesWithLabels(sourceDocDir: String, destinyDocDir: String, totalPages: Int): Unit
  def annotatePage(document: Document, conferenceData: ConferenceData): Boolean
}
