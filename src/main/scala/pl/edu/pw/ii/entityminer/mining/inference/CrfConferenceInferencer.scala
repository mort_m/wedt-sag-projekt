package pl.edu.pw.ii.entityminer.mining.inference

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryCollector
import pl.edu.pw.ii.entityminer.mining.accuracy.FactorieAccuracyChecker
import pl.edu.pw.ii.entityminer.mining.factorie.TreeLabelCrfModel.TokenFeaturesDomain
import pl.edu.pw.ii.entityminer.mining.factorie.tools.FactorieModelSaver
import pl.edu.pw.ii.entityminer.mining.token.feature.FeatureCreator
import pl.edu.pw.ii.entityminer.mining.factorie.{LinearCrf, FactorieInferencer, DataSetReader}
import cc.factorie.model.Parameters
import cc.factorie.TemplateModel
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import cc.factorie.app.nlp
import cc.factorie.app.nlp.Token
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.LabeledTreeNerTag
import pl.edu.pw.ii.entityminer.mining.extract._
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import scala.Some
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import scala.Some
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import scala.Some

/**
 * Created by Raphael Hazan on 9/10/2014.
 */
@Service
class CrfConferenceInferencer extends ConferenceInferencer {

  val modelFilePath = "factorie_linear_crf"

  @Autowired
  private var pageDirectoryCollector: PageDirectoryCollector = _

  @Autowired
  private var featureCreator: FeatureCreator = _

  @Autowired
  private var modelSaver: FactorieModelSaver = _

  @Autowired
  private var dataSetReader: DataSetReader = _

  @Autowired
  private var inferencer: FactorieInferencer = _

  private var model: TemplateModel with Parameters = _

  private val entityChoosers =
    List(
      new AbbreviationEntityChooser,
      new NameEntityChooser,
      new WhenEntityChooser,
      new WhereEntityChooser,
      new FinalVersionEntityChooser,
      new NotificationEntityChooser,
      new SubmissionEntityChooser
    )

  @Autowired
  private var accuracyChecker: FactorieAccuracyChecker = _

  override def inference(mainPageUri: String): ConferenceData = {

    val initialData = new ConferenceData
    initialData.uri = mainPageUri

    pageDirectoryCollector.collectPages(Set(initialData), CrfConferenceInferencer.HtmlDir)
    pageDirectoryCollector.addIndexToManifestIfNotAdded(CrfConferenceInferencer.HtmlDir, 1000)
    featureCreator.createFeaturesAndSave(CrfConferenceInferencer.HtmlDir, 1000, CrfConferenceInferencer.FeaturesDir, Conf.SINGLE_ENTITIES)
    if (Conf.IOB_MODE) featureCreator.convertFeaturesToIOB(CrfConferenceInferencer.FeaturesDir, CrfConferenceInferencer.FeaturesIob2Dir, 1000)
    if (Conf.STOP_WORDS_MODE || Conf.STEM) featureCreator.findAndSaveStopWords(featureStorage, CrfConferenceInferencer.FeaturesStopWordDir)

    if (model == null) {
      model = new LinearCrf
      modelSaver.deserialize(CrfConferenceInferencer.WorkingDir + modelFilePath, model)
    }
    TokenFeaturesDomain.freeze()

    val doc = dataSetReader.readDataSestForLinearCrf(finalFeatureStorage, 0, 1000, 0)._2.head

    inferencer.inference(doc, model, "")

    accuracyChecker.checkAccuracy(model, Seq(doc))

    val inferredOccur = findAllOccurs(doc, thisCategoryFunction)

    val result = new ConferenceData()
    for (label <- ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.valuesNoIob) {

      inferredOccur.get(label) match {
        case None =>
        case Some(inferredSeqs) =>
          entityChoosers.filter(_.operatingLabel == label).head.chooseBestEntity(inferredSeqs, model, label) match {
            case None =>
            case Some(information) =>
              result.setInformationByLabel(label, information)
          }
      }
    }
    result
  }

  private def featureStorage =
    if (Conf.IOB_MODE) CrfConferenceInferencer.FeaturesIob2Dir else CrfConferenceInferencer.FeaturesDir

  private def finalFeatureStorage = {
    if (Conf.STOP_WORDS_MODE || Conf.STEM)
      CrfConferenceInferencer.FeaturesStopWordDir
    else
      featureStorage
  }

  private def thisCategoryFunction: (Token) => String = {
    _.attr[LabeledTreeNerTag].categoryValue
  }

  private def findAllOccurs(document: nlp.Document, categoryValueFunction: Token => String) = {
    val result = scala.collection.mutable.Map[String, List[Seq[Token]]]()
    var isSpecialSeq = false
    var currentLabel = ""
    var currentSeq: Seq[nlp.Token] = Nil
    for (token <- document.tokens) {
      val labelNoIob = ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.convertNoIop(categoryValueFunction(token))
      if (labelNoIob != ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP(ConferenceHtmlAnnotation.OTHER_LABEL)) {
        isSpecialSeq = true

        if (currentLabel != labelNoIob && currentLabel.nonEmpty) {
          result put(currentLabel, result.getOrElse(currentLabel, Nil) :+ currentSeq)
          currentSeq = Nil
        }
        currentLabel = labelNoIob

        currentSeq = currentSeq :+ token
      } else if (isSpecialSeq == true) {
        result put(currentLabel, result.getOrElse(currentLabel, Nil) :+ currentSeq)
        isSpecialSeq = false
        currentLabel = ""
        currentSeq = Nil
      }
    }
    if (isSpecialSeq == true) {
      result put(currentLabel, result.getOrElse(currentLabel, Nil) :+ currentSeq)
    }
    result
  }
}

object CrfConferenceInferencer {
  val WorkingDir = "data/workstation/"
  val HtmlDir = WorkingDir + "html"
  val FeaturesDir = WorkingDir + "featurestorage"
  val FeaturesIob2Dir = WorkingDir + "featurestorage_iob"
  val FeaturesStopWordDir = WorkingDir + "featurestorage_stop"
}
