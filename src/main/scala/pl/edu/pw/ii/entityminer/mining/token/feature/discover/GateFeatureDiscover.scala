package pl.edu.pw.ii.entityminer.mining.token.feature.discover

import gate._
import javax.annotation.{PostConstruct, PreDestroy}
import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import scala.collection.JavaConversions._
import gate.creole.ExecutionException
import gate.Factory
/**
 * Created by Raphael Hazan on 3/5/14.
 */
class GateFeatureDiscover extends FeatureDiscover {

  var application: CorpusController = _

  def getApplication() = application

  def setApplication(application: CorpusController) = this.application = application

  /**
   * A corpus that will be used to hold the document being processed.
   */
  private var corpus: Corpus = _

  /**
   * Create the corpus. The PostConstruct annotation means that this
   * method will be called by spring once the handler object has been
   * constructed and its properties (i.e. the application) have been
   * set.
   */
  @PostConstruct def init {
    corpus = Factory.newCorpus("webapp corpus")
    application.setCorpus(corpus)
  }

  override def discoverFeature(entity: List[WebToken]): List[String] = {
    try {
      val headToken = entity.head.sentence.head.head
      if (headToken.gateText.isEmpty) {
        val mime: String = "text/plain"
        println(headToken.sentenceIntoText)
        println(Utils.featureMap("stringContent", headToken.sentenceIntoText, "mimeType", mime))
       // println(Factory.)
        val resource  = Factory.createResource("gate.corpora.DocumentImpl", Utils.featureMap("stringContent", headToken.sentenceIntoText, "mimeType", mime))
        if(resource!=null) {
          val doc = Factory.createResource("gate.corpora.DocumentImpl", Utils.featureMap("stringContent", headToken.sentenceIntoText, "mimeType", mime)).asInstanceOf[Document]
          corpus.clear()
          corpus.add(doc)
          application.execute
          entity.head.sentence.flatten.foreach(_.gateText = doc)
        }
      }

      var result: List[String] = Nil
      val allAnnots = entity.head.gateText.get.getAnnotations

      allAnnots.get(entity.head.sentenceTextPos).iterator().foreach { annotation =>
        if (annotation.getFeatures.get("majorType") != null) {
          if (annotation.getFeatures.get("majorType").toString == "location") {
            result = List(Conf.PREFIX_FEAT_LOCATION + annotation.getFeatures.get("majorType").toString,
              Conf.PREFIX_FEAT_LOCATION + annotation.getFeatures.get("minorType").toString)
          } else if (annotation.getFeatures.get("majorType").toString == "year") {
            result = List(Conf.PREFIX_FEAT_YEAR + "true")
          }
        }
      }
      result
    } catch {
      case e: ExecutionException =>
        println("[GateFeatureDiscover.discoverFeature] ExecutionException on " + entity)
        Nil
    }
  }

  /**
   * Clean-up method. The PreDestroy annotation means that Spring will
   * call the method when the object is no longer required.
   */
  @PreDestroy def cleanup {
    Factory.deleteResource(corpus)
    Factory.deleteResource(application)
  }

  override def discoverFeature(webToken: WebToken): List[String] = discoverFeature(List(webToken))

  def divideToEntities(tokens: List[WebToken]) = {
    val text = tokens.map(_.originalText.trim).foldLeft("")(_ + " " + _)
    val mime: String = "text/plain"
    val doc = Factory.createResource("gate.corpora.DocumentImpl", Utils.featureMap("stringContent", text, "mimeType", mime)).asInstanceOf[Document]
    tokens.foreach(_.gateText = doc)

    corpus.clear()
    corpus.add(doc)
    application.execute

    val allAnnots = doc.getAnnotations.get("Token")
    for (annot <- allAnnots.toList) {
      println(annot.toString)
      println(doc.getContent.getContent(annot.getStartNode.getOffset, annot.getEndNode.getOffset))
    }
    //not good performance therefore I'll write it by myself
  }

  override def getPrefixes(): Set[String] = Set(Conf.PREFIX_FEAT_LOCATION, Conf.PREFIX_FEAT_YEAR)
}
