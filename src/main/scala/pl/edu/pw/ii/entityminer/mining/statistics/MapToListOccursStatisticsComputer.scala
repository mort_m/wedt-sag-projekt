package pl.edu.pw.ii.entityminer.mining.statistics

/**
 * Created by Raphael Hazan on 5/11/2014.
 */


// not used
trait MapToListOccursStatisticsComputer extends StatisticsComputer {
  def accumulate[A <: MapToListOccursDataStatistics](updatedStats: A, stats2: A): Unit = {
    for ((label, stats) <- stats2.asInstanceOf[MapToListOccursDataStatistics].labelToStat) {
      if (updatedStats.labelToStat.contains(label)) {
        for ((sequence, occurNo) <- stats) {
          if (updatedStats.labelToStat(label).contains(sequence)) {
            updatedStats.labelToStat(label).put(sequence, updatedStats.labelToStat(label)(sequence) + occurNo)
          } else {
            updatedStats.labelToStat(label).put(sequence, occurNo)
          }
        }
      } else {
        updatedStats.labelToStat.put(label, stats)
      }
    }
  }
}
