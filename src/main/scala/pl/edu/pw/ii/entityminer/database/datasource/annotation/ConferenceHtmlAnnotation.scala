package pl.edu.pw.ii.entityminer.database.datasource.annotation

import pl.edu.pw.ii.entityminer.Conf

/**
 * Created by Raphael Hazan on 1/9/14.
 */
object ConferenceHtmlAnnotation {
  val IOB_PREFIX = "B_"

  val NAME = "CNAME"
  val WHEN = "WHEN"
  val WHERE = "WHER"
  val SUBMISSION = "SUBM"
  val NOTIFICATION = "NOTF"
  val FINAL_VERSION = "FINV"
  val ABBREVIATION = "ABBRE"
  //IOB version
  val B_NAME = IOB_PREFIX.toLowerCase + NAME
  val B_WHEN = IOB_PREFIX.toLowerCase + WHEN
  val B_WHERE = IOB_PREFIX.toLowerCase + WHERE
  val B_SUBMISSION = IOB_PREFIX.toLowerCase + SUBMISSION
  val B_NOTIFICATION = IOB_PREFIX.toLowerCase + NOTIFICATION
  val B_FINAL_VERSION = IOB_PREFIX.toLowerCase + FINAL_VERSION
  val B_ABBREVIATION = IOB_PREFIX.toLowerCase + ABBREVIATION
  /// labels for parent nodes
  val PARAGRAPH = "PARA"
  val INFOMATIONAL_PARAGRAPH = "INPA"
  val DATE_PARAGRAPH = "DTPA"
  val OTHER_LABEL = "DIFF"

  object ANNO_TO_LABEL_MAP {
    def areLabelsSame(label1: String, label2: String) = {
      val label1Normalized = if (label1.toUpperCase().startsWith(IOB_PREFIX)) label1.drop(IOB_PREFIX.size) else label1
      val label2Normalized = if (label2.toUpperCase().startsWith(IOB_PREFIX)) label2.drop(IOB_PREFIX.size) else label2
      label1Normalized == label2Normalized
    }

    def convertNoIop(label: String) = if (label.toUpperCase.startsWith(IOB_PREFIX)) label.drop(IOB_PREFIX.size) else label

    def apply(key: String) = internMap(key)

    def get(key: String) = internMap.get(key.toUpperCase)

    def getOrElse(key: String, default: String) = internMap.getOrElse(key, default)

    def getNoIob(key: String) = internMap.get(if (key.toUpperCase.startsWith(IOB_PREFIX)) key.drop(IOB_PREFIX.size) else key)

    def values = internMap.values.toList.sorted

    def allValues = Map(
      NAME -> "CNAME",
      WHEN -> "WHEN",
      WHERE -> "WHER",
      ABBREVIATION -> "ABBRE",
      SUBMISSION -> "SUBM",
      NOTIFICATION -> "NOTF",
      FINAL_VERSION -> "FINV",
      OTHER_LABEL -> "DIFF").values

    def valuesNoIob = standardMap.values

    private val iobMap = Map(
      B_NAME -> (IOB_PREFIX + NAME),
      B_WHEN -> (IOB_PREFIX + WHEN),
      B_WHERE -> (IOB_PREFIX + WHERE),
      B_SUBMISSION -> (IOB_PREFIX + SUBMISSION),
      B_NOTIFICATION -> (IOB_PREFIX + NOTIFICATION),
      B_FINAL_VERSION -> (IOB_PREFIX + FINAL_VERSION),
      B_ABBREVIATION -> (IOB_PREFIX + ABBREVIATION))

    private val paragraphMap = Map(
      PARAGRAPH -> "PARA",
      INFOMATIONAL_PARAGRAPH -> "INPA",
      DATE_PARAGRAPH -> "DTPA")

    private val standardMap = //if (!Conf.EXTRACT_DATES) {
      Map(
        NAME -> "CNAME",
        WHEN -> "WHEN",
        WHERE -> "WHER",
        ABBREVIATION -> "ABBRE",
//        OTHER_LABEL -> "DIFF"
//      )
//    } else {
//      Map(WHEN -> "WHEN",
        SUBMISSION -> "SUBM",
        NOTIFICATION -> "NOTF",
        FINAL_VERSION -> "FINV",
        OTHER_LABEL -> "DIFF")
//    }

    private val internMap = if (Conf.IOB_MODE) standardMap ++ iobMap else standardMap
  }

  val ALL_PARENT_ANNOTS = Set(PARAGRAPH, INFOMATIONAL_PARAGRAPH, DATE_PARAGRAPH)
  val ALL_ANNOTATIONS = Set(NAME, WHEN, WHERE, SUBMISSION, NOTIFICATION, FINAL_VERSION, ABBREVIATION) ++ this.ALL_PARENT_ANNOTS
}
