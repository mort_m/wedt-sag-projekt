package pl.edu.pw.ii.entityminer.mining.factorie.sample

import cc.factorie.app.nlp.load.Load
import cc.factorie.util.FastLogging
import cc.factorie.app.nlp._
import scala.collection.mutable.{ListBuffer, ArrayBuffer}
import pl.edu.pw.ii.entityminer.mining.factorie.TreeTag
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.LabeledTreeNerTag

/**
 * Created with IntelliJ IDEA.
 * User: ralpher
 * Date: 12/6/13
 * Time: 11:05 PM
 * To change this template use File | Settings | File Templates.
 */
object ExampleDocumentLoader extends Load with FastLogging {
  val conllToPennMap = Map("\"" -> "''", "(" -> "-LRB-", ")" -> "-RRB-", "NN|SYM" -> "NN")

  val trainDataDigits = List("a/1/0 b/2/-1 c/2/-2 d/3/-3 a/1/0",
    "a/1/0 b/2/-1 c/3/0 d/3/0",
    "b/2/0 a/1/0 c/3/0 d/3/0",
    "a/1/0 c/2/-1 b/2/0",
    "d/3/0 b/1/0 c/3/0 b/2/0",
    "a/1/0 b/2/0 c/3/0 d/3/0",
    "c/3/0 a/1/0 b/2/0 c/3/0")
  val trainData = List("a/NAME/0 b/WHEN/-1 c/WHEN/-2 d/WHER/-3 a/NAME/0",
    "a/NAME/0 b/WHEN/-1 c/WHER/0 d/WHER/0",
    "b/WHEN/0 a/NAME/0 c/WHER/0 d/WHER/0",
    "a/NAME/0 c/WHEN/-1 b/WHEN/0",
    "d/WHER/0 b/NAME/0 c/WHER/0 b/WHEN/0",
    "a/NAME/0 b/WHEN/0 c/WHER/0 d/WHER/0",
    "c/WHER/0 a/NAME/0 b/WHEN/0 c/WHER/0")

  val testDataDigits = List("a/1/0 b/2/-1 c/2/-2")
  val testData = List("a/NAME/0 b/WHEN/-1 cace/WHEN/-2")

  def fromSource(): Seq[Document] = fromSource(null)

  def fromSource(source: io.Source): Seq[Document] = {
    def newDocument(name: String): Document = {
      val document = new Document("").setName(name)
      document.annotators(classOf[Token]) = UnknownDocumentAnnotator.getClass // register that we have token boundaries
      document.annotators(classOf[Sentence]) = UnknownDocumentAnnotator.getClass // register that we have sentence boundaries
      document.annotators(classOf[pos.PennPosTag]) = UnknownDocumentAnnotator.getClass // register that we have POS tags
      document
    }

    val documents = new ArrayBuffer[Document]
    val tokens = new ListBuffer[Token]()
    (trainData ++ testData).foreach {
      sample =>
        var document = newDocument("treecrf-test-data-" + documents.length)
        documents += document
        val sentence = new Sentence(document)
        tokens.clear()
        sample.split("\\s+").foreach {
          tuple =>
            val fields = tuple.split("/")
            val word = fields(0)
            val partOfSpeech = "NNP"
            val ner = fields(1)
            if (sentence.length > 0) document.appendString(" ")
            val token = new Token(sentence, word)
            token.attr += new LabeledTreeNerTag(token, ner)
            token.attr += new cc.factorie.app.nlp.pos.PennPosTag(token, partOfSpeech)
            val parent: Token = if (fields(2) == "0") null
            else {
              val result = tokens(tokens.size + fields(2).toInt)
              result.attr[TreeTag].children += token
              result
            }
            token.attr += TreeTag(parent, new ListBuffer[Token]())
            tokens += token
        }
    }
    logger.info("Loaded " + documents.length + " documents with " + documents.map(_.sentences.size).sum + " sentences with " + documents.map(_.tokens.size).sum + " tokens total")
    documents
  }

}