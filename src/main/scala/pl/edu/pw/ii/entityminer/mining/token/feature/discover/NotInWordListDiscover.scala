package pl.edu.pw.ii.entityminer.mining.token.feature.discover

import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import scala.collection.mutable.ListBuffer
import pl.edu.pw.ii.entityminer.mining.nlp.WordListDictionary

/**
 * Created by Raphael Hazan on 2/26/14.
 */
class NotInWordListDiscover(featureBuffer: ListBuffer[String]) extends FeatureDiscover {
  override def discoverFeature(entity: List[WebToken]): List[String] = discoverFeaturesLocallyAndSum(entity)

  override def getPrefixes(): Set[String] = Set(Conf.PREFIX_FEAT_DICT)

  override def discoverFeature(webToken: WebToken): List[String] = {
    if ( webToken.text.trim.toLowerCase.count(x => !x.isLetter) == 0 &&
       ! WordListDictionary.wordList.contains(webToken.text.trim.toLowerCase)) {
      List(NotInWordListDiscover.LABEL_FEAT + "false")
    } else {
      Nil
    }
  }
}

object NotInWordListDiscover {
  val LABEL_FEAT = Conf.PREFIX_FEAT_DICT
}