package pl.edu.pw.ii.entityminer.util

import java.io._
import java.net.{ConnectException, SocketTimeoutException}
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult

import com.gargoylesoftware.htmlunit.html.HtmlPage
import org.apache.commons.io.{FileUtils, IOUtils}
import org.apache.http.ConnectionClosedException
import org.apache.http.client.ClientProtocolException
import org.w3c.dom.Document

import scala.Serializable
import scala.io.Source
import scala.xml.XML

/**
 * Created by IntelliJ IDEA.
 * User: ralpher
 * Date: 3/19/11
 * Time: 6:22 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Klasa opakowujaca operacje na plikach.
 */
class FileHelper(file: File) {

  /**
   * Dopisuje do pliku.
   * @param text tekst do dopisania.
   */
  def append(text: String): Unit = {
    val fw = new FileWriter(file, true)
    try {
      fw.write(text)
    }
    finally {
      fw.close
    }
  }

  def write(text: String): Unit = {
    val fw = new FileWriter(file, false)
    try {
      fw.write(text)
    }
    finally {
      fw.close
    }
  }

  def writeList(listToWrite: List[String]): Unit = {
    val fw = new FileWriter(file)
    try {
      listToWrite.foreach(s => fw.write(s + "\n"))
    }
    finally {
      fw.close
    }
  }

  /**
   * Dla każdej linii w pliku uruchamia podaną procedure.
   */
  def foreachLine(proc: String => Unit): Unit = {
    val br = new BufferedReader(new FileReader(file))
    try {
      while (br.ready) proc(br.readLine)
    }
    finally {
      br.close
    }
  }

  /**
   * Kasuje opakowany plik.
   */
  def deleteAll: Unit = {
    def deleteFile(dfile: File): Unit = {
      if (dfile.isDirectory) {
        val subfiles = dfile.listFiles
        if (subfiles != null) {
          subfiles.foreach {
            f => deleteFile(f)
          }
        }
      }
      dfile.delete
    }

    deleteFile(file)
  }
}

/**
 * Pomocnik do operacji na plikach.
 */
object FileHelper {
  import pl.edu.pw.ii.entityminer.crawler.dom.DomParser
  def getFileWriter(filePath: String) = {
    new BufferedWriter(new FileWriter(filePath))
  }

  def appendFile(filePath: String, content: String) = {
    val writer = new BufferedWriter(new FileWriter(filePath, true))
    writer.write(content)
    writer.close()
  }

  def getFileReader(filePath: String) = {
    new BufferedReader(new FileReader(filePath))
  }

  def writeStreamToFile(in: InputStream, outFile: String) {
    if (in == null) return
    var out: FileOutputStream = null
    try {
      out = new FileOutputStream(new File(outFile))
      IOUtils.copy(in, out)
    } finally {
      IOUtils.closeQuietly(in)
      IOUtils.closeQuietly(out)
    }
  }

  def saveBinary[T <: Serializable](obj: T)(filename: String) = {
    val fileOut = new FileOutputStream(filename);
    val out = new ObjectOutputStream(fileOut);
    out.writeObject(obj);
    out.close();
    fileOut.close();
  }

  def getResource(filename: String) = {
    val is = getResourceStream(filename)
    Source.fromInputStream(is, "UTF-8")
  }

  def getResourceAsXml(filename: String) = {
    println("filehelper filename: " + filename)
    val fileXml = XML.load(this.getClass.getResourceAsStream(filename))
    assert(fileXml.isInstanceOf[scala.xml.Elem])
    fileXml
  }

  def getResourceStream(filename: String) = {
    val res = if (filename.startsWith("/")) {
      filename
    }
    else {
      "/" + filename
    }
    this.getClass.getResourceAsStream(res)
  }

  def saveDocumentOnDisk(mainPage: Document, filePath: String) {
    val tFactory =
      TransformerFactory.newInstance()
    val transformer = tFactory.newTransformer()
    val source = new DOMSource(mainPage)
    val result = new StreamResult(new File(filePath))
    transformer.transform(source, result)
  }

  def saveDocumentOnDisk(mainPage: org.jsoup.nodes.Document, filePath: String) {
    val input = new File(filePath)
    val writer = new PrintWriter(input, "UTF-8")
    writer.write(mainPage.html())
    writer.flush()
    writer.close()
  }

  def fileExists(filePath: String) = {
    (new File(filePath)).exists()
  }

  def saveHtmlPage(page: HtmlPage, filename: String): Unit = {
    val file2 = new File(filename)
    if (file2.exists()) {
      file2.delete()
    }
    try {
      page.save(file2)
    } catch {
      case _: ClientProtocolException | _: ConnectionClosedException | _: ConnectException | _: SocketTimeoutException |
           _: IOException | _: NullPointerException | _: IllegalArgumentException | _:OutOfMemoryError=>
        var htmlContent = page.getBody.asXml()
        val pw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file2), "UTF-8"))
        val pars = new DomParser
        htmlContent = pars.cleanBody(htmlContent)
        pw.write(htmlContent)
        pw.close()
    }
  }

  def readFileToString(filePath: String) = {
    val source = scala.io.Source.fromFile(filePath, "UTF-8")
    println(filePath)
    val result = source.mkString
    source.close()
    result
  }

  def deleteDirectory(directory: String) = {
    FileUtils.deleteDirectory(new File(directory))
  }


  def copyFolder(src: File, dest: File) {
    if (src.isDirectory()) {

      if (!dest.exists()) {
        dest.mkdir()
        println("Directory copied from "
          + src + "  to " + dest)
      }

      val files = src.list()

      for (file <- files) {
        val srcFile = new File(src, file)
        val  destFile = new File(dest, file)
        copyFolder(srcFile, destFile)
      }

    } else {
      val in = new FileInputStream(src)
      val out = new FileOutputStream(dest)

      val buffer = new Array[Byte](1024)

      var length = in.read(buffer)
      while (length > 0) {
        out.write(buffer, 0, length)
        length = in.read(buffer)
      }

      in.close()
      out.close()
//      println("File copied from " + src + " to " + dest)
    }
  }

  def readFileToLines(filePath: String) = {
    val source = scala.io.Source.fromFile(filePath, "utf-8")
    val lines = source.getLines().toList
    source.close()
    lines
  }

  def writeLinesToFile(lines: List[String], filePath: String) = {
    val writer = FileHelper.getFileWriter(filePath)
    lines.foreach { line =>
      writer.write(line)
      writer.newLine()
    }
    writer.close()
  }
}