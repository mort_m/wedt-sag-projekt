package pl.edu.pw.ii.entityminer.database.datasource.annotation

import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.mining.token.feature.StopWords
import org.jsoup.nodes.TextNode
import scala.collection.mutable.ListBuffer
import pl.edu.pw.ii.entityminer.database.datasource.wikicfp.WikicfpConsts

/**
 * Created by Raphael Hazan on 1/13/14.
 */
trait TokenMatcherWithGap extends AnnotateStrategy {
  val maxGapBetweenNextKeywords = 2

  /**
   *
   * @param tokens
   * @param seq
   * @return (occurence tokens, rest tokens not analyzed yet)
   */
  def findFirstOccurence(tokens: List[WebToken], seq: List[String]): (List[WebToken], List[WebToken]) = {
    findFirstOccurence(tokens, seq, 0, Nil)
  }

  private def findFirstOccurence(tokens: List[WebToken], seq: List[String], currentGap: Int,
                                 seqPart: List[WebToken]): (List[WebToken], List[WebToken]) = {
    tokens match {
      case Nil =>
        (Nil, Nil)
      case token :: rest if token.text == seq.head =>
        if (seq.size > 1) {
          val result = findFirstOccurence(rest, seq.tail, maxGapBetweenNextKeywords, seqPart :+ token)
          if (result._1.isEmpty) {
            findFirstOccurence(rest, seq, 0, Nil)
          } else {
            result
          }
        } else {
          (seqPart :+ token, rest)
        }
      case token :: rest if currentGap > 0 =>
        findFirstOccurence(rest, seq, currentGap - 1, seqPart)
      case token :: rest if currentGap == 0 =>
        if (seqPart.isEmpty) {
          findFirstOccurence(rest, seq, 0, Nil)
        } else {
          (Nil, Nil)
        }
    }
  }
}
