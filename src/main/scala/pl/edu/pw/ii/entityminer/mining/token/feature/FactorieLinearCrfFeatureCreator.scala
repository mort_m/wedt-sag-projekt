package pl.edu.pw.ii.entityminer.mining.token.feature

import cc.factorie.app.nlp.Document
import pl.edu.pw.ii.entityminer.mining.token.feature.discover.{StopWordsRemoveDiscover, WordFeatureDiscover}
import pl.edu.pw.ii.entityminer.util.FileHelper
import pl.edu.pw.ii.entityminer.database.datasource.wikicfp.WikicfpDatasource
import org.springframework.beans.factory.annotation.{Autowired, Qualifier}
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryCollector
import pl.edu.pw.ii.entityminer.Conf
import org.springframework.stereotype.Service
import java.io.{File, FileNotFoundException}
import java.util.concurrent.Executors

import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation

import scala.collection.mutable.ListBuffer
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryManifest.ManifestEntry

import scala.collection.mutable
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

/**
 * Created by ralpher on 28.12.13.
 */
@Service("FeatureCreator")
class FactorieLinearCrfFeatureCreator extends FeatureCreator {
  @Autowired
  private var pageDirectoryCollector: PageDirectoryCollector = _

  @Autowired
  //  @Qualifier("DivisionTreeTokenizer")
  @Qualifier("DomTokenizer")
  var featureTokenizer: FeatureTokenizer = _



  def createFeaturesAndSave(documentsDirectory: String, totalPages: Int, featuresDirectory: String, singleEntities: Boolean): Unit = {
    implicit val ec = ExecutionContext
      .fromExecutor(Executors.newFixedThreadPool(100))
    FileHelper.deleteDirectory(featuresDirectory)
    new File(featuresDirectory).mkdirs()
    val tmpBuffer = new ListBuffer[(String, ManifestEntry)]()
    val futures = new ListBuffer[Future[mutable.MutableList[HeavyFeatureSet]]]
    for (i <- 0 to totalPages - 1) {

        if (FileHelper.fileExists(documentsDirectory + "/" + i)) {
          var tokens = new mutable.MutableList[HeavyFeatureSet]()
          val conferenceDirectory = documentsDirectory + "/" + i
         // println(i + " ----------------------------------------------------" + conferenceDirectory)
          pageDirectoryCollector = new PageDirectoryCollector
          val manifest = pageDirectoryCollector.readPageManifestFromXml(conferenceDirectory)
            if(manifest!=null) {
              tmpBuffer.clear()
              manifest.result().foreach {
                manifestEntry =>
                  if (
                    FileHelper.fileExists(conferenceDirectory + "/" + manifestEntry._2._2)) {
                    println(manifestEntry._1 + " for document " + manifestEntry._2._2 + " " + manifestEntry._2._1.map(_.replaceAll("\\s+", "; ")))
                    tmpBuffer += ((conferenceDirectory + "/" + manifestEntry._2._2, manifestEntry))
                  }
              }
              featureTokenizer = new DomTokenizer()

              val future = Future[mutable.MutableList[HeavyFeatureSet]] {
                var tokenss = featureTokenizer.tokenizeFromFiles(tmpBuffer.result(), singleEntities)

                tokens ++= tokenss
              }
              futures += future
            }
        }
    }
    Await.result(Future.sequence(futures), Duration.Inf)
    var iter = 0
    futures.foreach(future => {
      saveTokens(future.value.get.get.toList, featuresDirectory + "/" + iter + "." + Conf.MANIFEST_INDEX_ENTRY.take(1) + ".txt")
      iter = iter + 1
    }
    )
  }

  override def findAndSaveStopWords(fromDirectory: String, destinyDirectory: String): Unit = {
    FileHelper.deleteDirectory(destinyDirectory)
    new File(destinyDirectory).mkdirs()
    new File(Conf.FEATURES_STORAGE_STOP_WORDS).mkdirs()
    if (Conf.STOP_WORDS_MODE) {
      //part one, counting words----------------------------------------------------------------------------------
      for (i <- 0 to 1100) {
        val filePath = fromDirectory + Conf.SLASH + i + ".0.txt"
        if (FileHelper.fileExists(filePath)) {
          val lines = FileHelper.readFileToLines(filePath)
          for (line <- lines.map(_.split("\\s+").toList)) {
            for (featureOrWord <- line) {
              if (featureOrWord.startsWith(WordFeatureDiscover.LABEL_FEAT)) {
                FactorieLinearCrfFeatureCreator.WordOccurances.get(featureOrWord) match {
                  case None => FactorieLinearCrfFeatureCreator.WordOccurances += ((featureOrWord, 1))
                  case Some(counted) => FactorieLinearCrfFeatureCreator.WordOccurances += ((featureOrWord, counted + 1))
                }
              }
            }
          }
        }
      }

      //part two, finding stopwords----------------------------------------------------------------------------------
      val histogramStop = scala.collection.mutable.Map[String, Integer]()
      val histogramNotStop = scala.collection.mutable.Map[String, Integer]()
      val notStopWordMaxDistance = 5
      for (i <- 0 to 1100) {
        val filePath = fromDirectory + Conf.SLASH + i + ".0.txt"
        if (FileHelper.fileExists(filePath)) {
          val lines = FileHelper.readFileToLines(filePath)

          var i2 = 0
          var nonStopWordRanges = ListBuffer[(Int, Int)]()
          for (line <- lines.map(_.split("\\s+").toList)) {
            val lineClass = line(Conf.FEAT_CLASS)
            if (lineClass != ConferenceHtmlAnnotation.OTHER_LABEL) {
              nonStopWordRanges += ((i2 - notStopWordMaxDistance, i2 + notStopWordMaxDistance))
            }
            i2 += 1
          }
          i2 = 0
          for (line <- lines.map(_.split("\\s+").toList)) {
            for (featureOrWord <- line) {
              if (featureOrWord.startsWith(WordFeatureDiscover.LABEL_FEAT)) {
                if (nonStopWordRanges.count(range => i2 >= range._1 && i2 <= range._2) > 0) {
                  histogramNotStop.get(featureOrWord) match {
                    case None => histogramNotStop += ((featureOrWord, 1))
                    case Some(counted) => histogramNotStop += ((featureOrWord, counted + 1))
                  }
                } else {
                  histogramStop.get(featureOrWord) match {
                    case None => histogramStop += ((featureOrWord, 1))
                    case Some(counted) => histogramStop += ((featureOrWord, counted + 1))
                  }
                }
              }
            }
            i2 += 1
          }
        }
      }
      println("STOP " + histogramStop.size)
      for (pair <- histogramStop.toList.sortWith(_._2 > _._2).take(5)) {
        println(pair._1.drop(2) + " & " + pair._2)
      }
      println("")
      println("NON STOP " + histogramNotStop.size)
      for (pair <- histogramNotStop.toList.sortWith(_._2 > _._2).take(5)) {
        println(pair._1.drop(2) + " & " + pair._2)
      }
      println("")
      FactorieLinearCrfFeatureCreator.StopWords ++= histogramStop.filterNot(kv => histogramNotStop.contains(kv._1))
      FactorieLinearCrfFeatureCreator.ClassWords ++= histogramNotStop.toList.sortBy(_._2).reverse.take(50000)
      println("STOP CLEANDED " + FactorieLinearCrfFeatureCreator.StopWords.size)
      for (pair <- histogramStop.filterNot(kv => histogramNotStop.contains(kv._1)).toList.sortWith(_._2 > _._2).take(5)) {
        println(pair._1.drop(2) + " & " + pair._2)
      }

      FactorieLinearCrfFeatureCreator.WordOccurances.filter(_._2 == 1).filterNot(kv => FactorieLinearCrfFeatureCreator.StopWords.contains(kv._1))
    }

    //part three, saving filtered files----------------------------------------------------------------------------------
    val tmpFeatureBuffer = new ListBuffer[String]
    val removeUnique = true
    val stopWordDiscover = new StopWordsRemoveDiscover(tmpFeatureBuffer, removeUnique)
    for (i <- 0 to 1100) {
      val filePath = fromDirectory + Conf.SLASH + i + ".0.txt"
      if (FileHelper.fileExists(filePath)) {
        val lines = FileHelper.readFileToLines(filePath)
        val writer = FileHelper.getFileWriter(destinyDirectory + "/" + i + "." + Conf.MANIFEST_INDEX_ENTRY.take(1) + ".txt")
        var lineIter = 0
        for (line <- lines) {
//          lineIter += 1
//          if (lineIter < 501) {
            val features = line.split("\\s+").toList
            tmpFeatureBuffer.clear()
            tmpFeatureBuffer ++= features
            stopWordDiscover.discoverFeature(Nil)
            if (tmpFeatureBuffer.nonEmpty) {
              writer.append(tmpFeatureBuffer.reduceLeft[String] { case (sum, x) => sum + " " + x})
              writer.newLine()
            }
//          }
        }
        writer.close()
      }
    }
  }

  def convertFeaturesToIOB(featuresDirectory: String, destinyDirectory: String, totalPages: Int): Unit = {
    FileHelper.deleteDirectory(destinyDirectory)
    new File(destinyDirectory).mkdirs()
    println("[convertFeaturesToIOB] starts")
    for (i <- 0 to totalPages - 1) {
      for (j <- 0 to 300) {
        val filePath = featuresDirectory + Conf.SLASH + i + "." + j + ".txt"
        val destinyFilePath = destinyDirectory + Conf.SLASH + i + "." + j + ".txt"
        if (FileHelper.fileExists(filePath)) {
          val lines = FileHelper.readFileToLines(filePath)
          var resultLines = List[String]()
          var prevLabel = ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.OTHER_LABEL).get

          for (line <- lines.map(_.split("\\s+").toList)) {
            val label = line(Conf.FEAT_CLASS)
            if (label == ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.OTHER_LABEL).get ||
              prevLabel == label) {
              resultLines = resultLines :+ line.foldLeft("") {
                case (aggr, feat) => aggr + " " + feat
              }.drop(1)
            } else {
              resultLines = resultLines :+ {
                line.take(Conf.FEAT_CLASS).foldLeft("") {
                  case (aggr, feat) => aggr + " " + feat
                }.drop(1) + " " +
                  ConferenceHtmlAnnotation.IOB_PREFIX + label + " " +
                  line.drop(Conf.FEAT_CLASS + 1).foldLeft("") {
                    case (aggr, feat) => aggr + " " + feat
                  }.drop(1)
              }
            }
            prevLabel = label
          }
          FileHelper.writeLinesToFile(resultLines, destinyFilePath)
        }
      }
    }
    println("[convertFeaturesToIOB] ended")
  }

  def saveTokens(tokens: List[HeavyFeatureSet], fileName: String) = {
    val writer = FileHelper.getFileWriter(fileName)
    val buffer = new StringBuilder
  println("Found "+tokens.size+" of tokens in file "+fileName)
    tokens.foreach {
      token =>
        buffer.clear()
        var firstIndex = true
        for (data <- Conf.FEAT_DATA_ORDER) {
          val text = data match {
            case Conf.FEAT_CLASS =>
              token.labelss.reduce(_ + " " + _)
            case Conf.FEAT_CONNS =>
              "[]"//TODO
            case Conf.FEAT_TOKEN_NO =>
              token.entity.head.tokenNo
            case Conf.FEAT_SENTENCE_NO =>
              token.entity.head.sequenceNo
            case Conf.FEAT_PARAGRAPH_NO =>
              token.entity.head.paragraphNo
            case Conf.FEAT_WORD =>
              token.entity.foldLeft("")(_ + " " + _.originalText).replaceAll("\\s+", "|")
            case Conf.FEATURES =>
              token.features.reduce(_ + " " + _)
          }
          if ( ! firstIndex) {
            buffer.append(" ")
          }
          firstIndex = false
          buffer.append(text)
        }
        writer.write(buffer.toString())
        writer.newLine()
    }
    writer.close
  }

}

object FactorieLinearCrfFeatureCreator {
  val WordOccurances = scala.collection.mutable.Map[String, Integer]()
  val StopWords = scala.collection.mutable.Map[String, Integer]()
  val ClassWords = scala.collection.mutable.Map[String, Integer]()
}