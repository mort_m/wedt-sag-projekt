package pl.edu.pw.ii.entityminer.database.datasource.annotation

import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.mining.token.feature.StopWords
import org.jsoup.nodes.TextNode
import scala.collection.mutable.ListBuffer
import pl.edu.pw.ii.entityminer.database.datasource.wikicfp.WikicfpConsts

/**
 * Created by Raphael Hazan on 1/13/14.
 */
trait AllSeparatelyTokenizer extends AnnotateStrategy {

  def tokenize(text: String, node: TextNode): List[WebToken] = {
    val result = ListBuffer[WebToken]()
    var currentPos = 0
    while (currentPos < text.size) {
      val currentToken = text.substring(currentPos).takeWhile(_.isLetterOrDigit)
      if (currentToken.trim.size > 0) {
        result += WebToken(currentToken.toLowerCase(), currentPos, currentPos + currentToken.size, node)
      } else if (this.isNotWhiteSpace(text.substring(currentPos, currentPos + 1))) {
        result += WebToken(text.substring(currentPos, currentPos + 1).toLowerCase(), currentPos, currentPos + 1, node)
      }
      currentPos += Math.max(currentToken.size, 1)
    }
    result.result
  }

  private def isNotWhiteSpace(str: String) = {
    ! str.matches("\\s+") &&
    str != "\u0013"
  }
}
