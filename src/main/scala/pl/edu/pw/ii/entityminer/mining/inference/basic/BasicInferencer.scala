package pl.edu.pw.ii.entityminer.mining.inference.basic

import cc.factorie.app.nlp

/**
 * Created by Raphael Hazan on 5/12/2014.
 */
trait BasicInferencer {
    def inference(document: nlp.Document): nlp.Document
}
