package pl.edu.pw.ii.entityminer.mining.statistics

import javax.annotation.Resource
import scala.collection.mutable
import scala.collection.JavaConversions._
import org.springframework.beans.factory.annotation.Autowired
import pl.edu.pw.ii.entityminer.mining.nlp.WebPageTokenizer
import org.springframework.stereotype.Service
import pl.edu.pw.ii.entityminer.Conf
import java.io.File
import pl.edu.pw.ii.entityminer.util.FileHelper
import pl.edu.pw.ii.entityminer.chart.TextWorkCharting
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation

/**
 * Created by Raphael Hazan on 16.01.14.
 */
@Service
class ConferenceStatistics {

  @Resource
  private var statisticsComputers: java.util.List[StatisticsComputer] = _

  @Autowired
  private var iterFactory: ScrappedWebPageIteratorFactory = _

  def computeAllStatistics(documentsDirectory: String, totalPages: Int): Map[String, DataStatistics] = {
    val statisticsContainer = mutable.Map[String, DataStatistics]()
    val fileIterator = iterFactory.buildIter(documentsDirectory, 0, totalPages)
    fileIterator.foreach { case (conferenceData, manifestEntry, document, documentPath) =>
      statisticsComputers.toList.foreach {
        computer =>
          val statistic = computer.computeStatistics(conferenceData, document, manifestEntry, documentPath)
          if (statisticsContainer.contains(statistic.CODE)) {
            statisticsContainer(statistic.CODE).accumulate(statistic)
          } else {
            statisticsContainer += statistic.CODE -> statistic
          }
      }
    }
    statisticsContainer.result.toMap
  }

  def makeStatistics(documentDirectory: String, totalPages: Int) = {
    // process independent statistics computation
//    FeaturesStatistics.processNames()
//    FeaturesStatistics.processHighlights()
//    FeaturesStatistics.processLayoutParents()
    FeaturesStatistics.processParagraphOccurs()
//    FeaturesStatistics.processNotInWordListOccurs()
//    FeaturesStatistics.processPredecessors()
    // and the rest
    val filePath = Conf.DATA_OUTPUT + Conf.SLASH + "beforeLabelStats.txt"
    new File(filePath).delete()

    val beforeLabelOccurances1 = scala.collection.mutable.Map[String, Int]()
    val beforeLabelOccurances2 = scala.collection.mutable.Map[String, Int]()
    val beforeLabelOccurances3 = scala.collection.mutable.Map[String, Int]()

    val statistics = this.computeAllStatistics(documentDirectory, totalPages)
    for ((statsCode, stats) <- statistics) {
      println("STATS " + stats.CODE)
      stats match {
        case paraStats: ParagraphNumerOccuranceStatistics =>
          println("DATA PARA NO")
          for (stat <- paraStats.dataParaNo.immutableMap.toList.sortWith{
            case (a, b) =>
              a._1._1.compareTo(b._1._1) < 0 ||
                a._1._1.compareTo(b._1._1) == 0 && a._1._2 < b._1._2
          }) {
            println(stat)
          }
          println("")
          println("DATA TOKEN NO")
          for (stat <- paraStats.dataTokenNo.immutableMap.toList.sortWith{
            case (a, b) =>
              a._1._1.compareTo(b._1._1) < 0 ||
                a._1._1.compareTo(b._1._1) == 0 && a._1._2 < b._1._2
          }) {
            println(stat)
          }
          for (label <- ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.values) {
            val xy = paraStats.dataParaNo.byFirstKey(label)
            TextWorkCharting.histogramDistribution(label, xy)
          }

        case abbrStats: AbbreviationShapeStatistics =>
          for ((len, num) <- abbrStats.lengthToNumber.toList.sortWith{ case (a, b) => a._2 > b._2 }) {
            println(len + " to " + num)
          }
        case abbrStats: AbbreviationLengthStatistics =>
          for ((len, num) <- abbrStats.lengthToNumber.toList.sortWith{ case (a, b) => a._1 > b._1 }) {
            println(len + " to " + num)
          }
          TextWorkCharting.makeBars(abbrStats.lengthToNumber.toMap.map(kv => (kv._1.toString, kv._2)))
        case mapToListStats: AnnotationStatistics =>
          val chartMap = scala.collection.mutable.Map[String, Int]()
          println("STARTED")
          for ((link, labelOccurences) <- mapToListStats.labelToStat.toList.sortWith { case (a, b) => a._2.values.sum > b._2.values.sum}) {
            println(link)
            for ((label, number) <- labelOccurences.toList.sortWith { case (a, b) => a._2 > b._2}) {
              println("    " + label + " " + number)
            }
            if (link.nonEmpty) {
              val chartLink = if (link == "0.html") "index" else link
              chartMap put(chartLink, labelOccurences.values.sum)
            }
          }
          TextWorkCharting.makeBars(chartMap.toMap)
        case mapToListStats: MapToListOccursDataStatistics =>
          for ((label, occurences) <- mapToListStats.labelToStat) {

            println("LABEL " + label)
            beforeLabelOccurances1.clear()
            beforeLabelOccurances2.clear()
            beforeLabelOccurances3.clear()

            for ((occur, number) <- occurences.toList.sortWith{ case (a, b) => a._2 > b._2 }) {
              var i = 0
              occur.reverse.foreach { word =>
                if (i == 0)
                  beforeLabelOccurances1 put(word, number + beforeLabelOccurances1.getOrElse(word, 0))
                else if (i == 1)
                  beforeLabelOccurances2 put(word, number + beforeLabelOccurances2.getOrElse(word, 0))
                else if (i == 2)
                  beforeLabelOccurances3 put(word, number + beforeLabelOccurances3.getOrElse(word, 0))
                i += 1
              }

              FileHelper.appendFile(filePath, "label |" + label + "|" + '\n')
              FileHelper.appendFile(filePath, "occur |" + occur + "| " + number + '\n')
              //              println("label |" + label + "|")
              //              println("occur |" + occur + "| " + number)
            }
            println("111:")
            for ((word, number) <- beforeLabelOccurances1.toList.sortWith{ case (a, b) => a._2 > b._2 }.take(50)) {
              println(word + " " + number)
            }
            println("222:")
            for ((word, number) <- beforeLabelOccurances2.toList.sortWith{ case (a, b) => a._2 > b._2 }.take(50)) {
              println(word + " " + number)
            }
            println("333:")
            for ((word, number) <- beforeLabelOccurances3.toList.sortWith{ case (a, b) => a._2 > b._2 }.take(50)) {
              println(word + " " + number)
            }
          }
        case boilerplateStats: BoilerplateStatistics =>
          for ((label, docLengths) <- boilerplateStats.data) {
            println("label |" + label + "|")
            println("boilerplate | dom: " + docLengths.head + "| " + docLengths.last)
          }
        case wordUndeLinkStats: WordsUnderLinkStatistics =>
          println("WordsUnderLinkStatistics:")
          println(wordUndeLinkStats.labelToYesAndNo)
      }
    }
  }
}
