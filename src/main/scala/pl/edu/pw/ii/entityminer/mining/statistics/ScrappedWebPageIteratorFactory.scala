package pl.edu.pw.ii.entityminer.mining.statistics

import org.springframework.stereotype.Service
import pl.edu.pw.ii.entityminer.crawler.dom.DomParser
import org.springframework.beans.factory.annotation.Autowired
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryCollector

/**
 * Created by Raphael Hazan on 17.01.14.
 */
@Service
class ScrappedWebPageIteratorFactory {
  @Autowired
  private var pageDirectoryCollector: PageDirectoryCollector = _

  @Autowired
  private var domParser: DomParser = _

  def buildIter(documentsDirectory: String, pagesStart: Int, pagesNo: Int) = {
    new ScrappedWebPageIterator(pageDirectoryCollector, domParser, documentsDirectory, pagesStart, pagesNo)

  }
}
