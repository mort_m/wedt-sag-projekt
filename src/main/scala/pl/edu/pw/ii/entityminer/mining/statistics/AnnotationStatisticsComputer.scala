package pl.edu.pw.ii.entityminer.mining.statistics

import org.jsoup.nodes.Document
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import scala.collection.mutable
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryManifest._
import scala.collection.JavaConversions._


/**
 * Created by Raphael Hazan on 17.01.14.
 */
class AnnotationStatisticsComputer extends StatisticsComputer {

  def computeStatistics(conferenceData: ConferenceData, document: Document, manifestEntry: ManifestEntry, documentPath: String): MapToListOccursDataStatistics = {
    val linkToAnnotationsOccured = mutable.Map[String, Set[String]]()
    manifestEntry._2._1.map(_.toLowerCase.trim.replaceAll("\\s+", " ")).foreach {
      linkLabel =>
        linkToAnnotationsOccured put(linkLabel, Set[String]())
    }
    ConferenceHtmlAnnotation.ALL_ANNOTATIONS.foreach {
      annotation =>
        document.select(annotation).toList match {
          case Nil =>
          case _ =>
            manifestEntry._2._1.map(_.toLowerCase.trim.replaceAll("\\s+", " ")).foreach {
              linkLabel =>
//                if (manifestEntry._2._2 == "0.html") {
//                  linkToAnnotationsOccured put("0.html", linkToAnnotationsOccured.get("0.html").get + annotation)
//                } else {
                  linkToAnnotationsOccured put(linkLabel, linkToAnnotationsOccured.get(linkLabel).get + annotation)
//                }
            }
        }
    }
    new AnnotationStatistics(scala.collection.mutable.Map(linkToAnnotationsOccured.mapValues {
      annotationSet =>
        val annotationMap = collection.mutable.Map[List[String], Int]()
        annotationSet.foreach (annot => annotationMap put (List(annot), 1))
        annotationMap
    }.toSeq: _*))
  }
}

case class AnnotationStatistics(labelToStat: collection.mutable.Map[String, collection.mutable.Map[List[String], Int]]) extends MapToListOccursDataStatistics {
  val CODE: String = "annotation occurances"
}