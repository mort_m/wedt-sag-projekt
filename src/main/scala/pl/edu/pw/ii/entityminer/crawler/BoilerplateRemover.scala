package pl.edu.pw.ii.entityminer.crawler

import java.io.{InputStreamReader, FileInputStream, File}
import de.l3s.boilerpipe.extractors.{CommonExtractors, KeepEverythingExtractor}
import org.springframework.stereotype.Service
import de.l3s.boilerpipe.sax.HTMLHighlighter
import org.jsoup.Jsoup

/**
 * Created by Raphael Hazan on 1/21/14.
 */
@Service
class BoilerplateRemover {
  def removeBoilerplate(filePath: String) = {
    val file = new File(filePath);
    val extractor = CommonExtractors.KEEP_EVERYTHING_EXTRACTOR
    val hh = HTMLHighlighter.newExtractingInstance()
    val cleanedHtml = hh.process(file.toURI.toURL, extractor)
    Jsoup.parse(cleanedHtml)
  }
}
