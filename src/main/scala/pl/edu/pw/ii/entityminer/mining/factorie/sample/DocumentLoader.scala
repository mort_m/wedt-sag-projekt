package pl.edu.pw.ii.entityminer.mining.factorie.sample

import cc.factorie.app.nlp.load.Load
import cc.factorie.app.nlp.ner.LabeledIobOntonotesNerTag
import cc.factorie.util.FastLogging
import cc.factorie.app.nlp._
import scala.collection.mutable.ArrayBuffer

/**
 * Created with IntelliJ IDEA.
 * User: ralpher
 * Date: 12/6/13
 * Time: 11:05 PM
 * To change this template use File | Settings | File Templates.
 */
object DocumentLoader extends Load with FastLogging {
  val conllToPennMap = Map("\"" -> "''", "(" -> "-LRB-", ")" -> "-RRB-", "NN|SYM" -> "NN")

  def fromSource(source:io.Source): Seq[Document] = {
    def newDocument(name:String): Document = {
      val document = new Document("").setName(name)
      document.annotators(classOf[Token]) = UnknownDocumentAnnotator.getClass // register that we have token boundaries
      document.annotators(classOf[Sentence]) = UnknownDocumentAnnotator.getClass // register that we have sentence boundaries
      document.annotators(classOf[pos.PennPosTag]) = UnknownDocumentAnnotator.getClass // register that we have POS tags
      document
    }

    val documents = new ArrayBuffer[Document]
    var document = newDocument("CoNLL2003-"+documents.length)
    documents += document
    var sentence = new Sentence(document)
    for (line <- source.getLines()) {
      if (line.length < 2) { // Sentence boundary
        //sentence.stringLength = document.stringLength - sentence.stringStart
        //document += sentence
        document.appendString("\n")
        sentence = new Sentence(document)
      } else if (line.startsWith("-DOCSTART-")) {
        // Skip document boundaries
        document.asSection.chainFreeze
        document = new Document().setName("CoNLL2003-"+documents.length)
        document.annotators(classOf[Token]) = UnknownDocumentAnnotator.getClass // register that we have token boundaries
        document.annotators(classOf[Sentence]) = UnknownDocumentAnnotator.getClass // register that we have sentence boundaries
        documents += document
      } else {
        val fields = line.split(' ')
        assert(fields.length == 4)
        val word = fields(0)
        val partOfSpeech = conllToPennMap.getOrElse(fields(1), fields(1))
        val ner = fields(3).stripLineEnd
        if (sentence.length > 0) document.appendString(" ")
        val token = new Token(sentence, word)
        token.attr += new LabeledIobOntonotesNerTag(token, ner)
        token.attr += new cc.factorie.app.nlp.pos.PennPosTag(token, partOfSpeech)
      }
    }
    //sentence.stringLength = document.stringLength - sentence.stringStart
    logger.info("Loaded "+documents.length+" documents with "+documents.map(_.sentences.size).sum+" sentences with "+documents.map(_.tokens.size).sum+" tokens total")
    documents
  }

}