package pl.edu.pw.ii.entityminer.database.datasource

import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation

/**
 * Conference main informations.
 * User: Rafael Hazan
 * Date: 7/3/13
 * Time: 11:35 PM
 */
case class ConferenceData (
  private var _uri: String,

  private var _names: List[String],
  private var _date: String,
  private var _place: String,
  private var _nameAbbreviation: String,
  private var _paperSubmissionDate: String,
  private var _paperNotificationDate: String,
  private var _paperFinalVersionDate: String,
  private var _paperSubmissionAbstract: String,
  private var _registrationDate: String,
  private var _acceptanceDate: String,
  private var _cameraReadyDate: String,
  private var _committee: String
  ) {
  def this() = this(null, Nil, null, null, null, null, null, null, null, null, null, null, null)

  def uri = _uri
  def uri_=(uri: String) = _uri = uri
  def date = _date
  def date_=(date: String) = _date = date
  def place = _place
  def place_=(place: String) = _place = place
  def names = _names
  def names_=(name: List[String]) = _names = name
  def nameAbbreviation = _nameAbbreviation
  def nameAbbreviation_=(nameAbbreviation: String) = _nameAbbreviation = nameAbbreviation
  def paperSubmissionDate = _paperSubmissionDate
  def paperSubmissionDate_=(paperSubmissionDate: String) = _paperSubmissionDate = paperSubmissionDate
  def paperNotificationDate = _paperNotificationDate
  def paperNotificationDate_=(paperNotificationDate: String) = _paperNotificationDate = paperNotificationDate
  def paperFinalVersionDate = _paperFinalVersionDate
  def paperFinalVersionDate_=(paperFinalVersionDate: String) = _paperFinalVersionDate = paperFinalVersionDate
  def paperSubmissionAbstract = _paperSubmissionAbstract
  def paperSubmissionAbstract_=(paperSubmissionAbstract: String) = _paperSubmissionAbstract = paperSubmissionAbstract
  def registrationDate = _registrationDate
  def registrationDate_=(registrationDate: String) = _registrationDate = registrationDate
  def acceptanceDate = _acceptanceDate
  def acceptanceDate_=(acceptanceDate: String) = _acceptanceDate = acceptanceDate
  def cameraReadyDate = _cameraReadyDate
  def cameraReadyDate_=(cameraReadyDate: String) = _cameraReadyDate = cameraReadyDate
  def committee = _committee
  def committee_=(committee: String) = _committee = committee

  def setInformationByLabel(label: String, information: Seq[String]) = {
    val informationStr = information.reduceLeft(_ + " " + _).replaceAll("\\|", " ").replaceAll("\\s+", " ")
    label match {
      case ConferenceHtmlAnnotation.NAME =>
        names = List(informationStr)
      case ConferenceHtmlAnnotation.ABBREVIATION =>
        nameAbbreviation = informationStr
      case ConferenceHtmlAnnotation.WHERE =>
        place = informationStr
      case ConferenceHtmlAnnotation.WHEN =>
        date = informationStr
      case ConferenceHtmlAnnotation.SUBMISSION =>
        paperSubmissionDate = informationStr
      case ConferenceHtmlAnnotation.NOTIFICATION =>
        paperNotificationDate = informationStr
      case ConferenceHtmlAnnotation.FINAL_VERSION =>
        paperFinalVersionDate = informationStr
    }
  }

  override def toString = {
    val result = new StringBuilder
    result append "ConferenceData ["
    //if (names.nonEmpty) result append ("\n name = " + names.head)
    //if (nameAbbreviation != null) result append ("\n abbreviation = " + nameAbbreviation)
    //if (place != null) result append ("\n place = " + place)
    if (date != null) result append ("\n date = " + date.replaceAll(",",""))
    if (paperSubmissionDate != null) result append ("\n paper submission date = " + paperSubmissionDate.replaceAll(",",""))
    if (paperNotificationDate != null) result append ("\n paper notification date = " + paperNotificationDate.replaceAll(",",""))
    if (paperFinalVersionDate != null) result append ("\n paper final version = " + paperFinalVersionDate.replaceAll(",",""))
    result append "\n]"
    result.toString()
  }
}
