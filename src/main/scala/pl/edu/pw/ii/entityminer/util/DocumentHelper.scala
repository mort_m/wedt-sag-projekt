package pl.edu.pw.ii.entityminer.util

import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import cc.factorie.app.nlp
import cc.factorie.app.nlp.Token

/**
 * Created by Raphael Hazan on 4/23/2014.
 */
object DocumentHelper {
  def intoText(tokens: List[WebToken], document: nlp.Document) = {
    for (webToken <- tokens) {
      val nlpToken = new Token(document, webToken.originalText.trim)
      nlpToken.attr += webToken
    }

//    val startEndPositions = collection.mutable.Map[(Int, Int), WebToken]()
//
//    val text = tokens.foldLeft("") {
//      case (aggregated, token) =>
//        startEndPositions put ((aggregated.size, aggregated.size + token.originalText.size), token)
//        aggregated + " " + token.originalText
//    }
//    (text.drop(1), startEndPositions)
  }
}
