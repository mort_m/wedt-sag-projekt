package pl.edu.pw.ii.entityminer.mining.accuracy

import cc.factorie.app.nlp
import cc.factorie.app.nlp.Token
import cc.factorie.model.TemplateModel
import org.springframework.stereotype.Service
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.database.datasource.annotation._
import pl.edu.pw.ii.entityminer.mining.extract._
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.LabeledTreeNerTag

import scala.collection.mutable

/**
 * Created by Raphael Hazan on 3/8/14.
 */

@Service
class FactorieAccuracyChecker {

  private val annotatorsMap = //if ( ! Conf.EXTRACT_DATES) {
    Map(
      ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP(ConferenceHtmlAnnotation.NAME) -> new ConferenceNameAnnotator(),
      ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP(ConferenceHtmlAnnotation.WHEN) -> new ConferenceDateAnnotator(),
      ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP(ConferenceHtmlAnnotation.WHERE) -> new ConferenceWhereAnnotator(),
      ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP(ConferenceHtmlAnnotation.ABBREVIATION) -> new ConferenceAbbreviationAnnotator(),
      //    )
      //  } else { Map() } ++ {
      //    if (Conf.EXTRACT_DATES) {
      //      Map(
      //        ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP(ConferenceHtmlAnnotation.WHEN) -> new ConferenceDateAnnotator(),
      ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP(ConferenceHtmlAnnotation.SUBMISSION) -> new ConferenceSubmissionAnnotator(),
      ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP(ConferenceHtmlAnnotation.NOTIFICATION) -> new ConferenceNotificationAnnotator(),
      ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP(ConferenceHtmlAnnotation.FINAL_VERSION) -> new ConferenceFinalVersionAnnotator()
    )
  //    } else {
  //      Nil
  //    }
  //  }

  private val entityChoosers = //if ( ! Conf.EXTRACT_DATES) {
    List(
      new AbbreviationEntityChooser,
      new NameEntityChooser,
      new WhenEntityChooser,
      new WhereEntityChooser,
      //    )
      //  } else { Nil } ++ {
      //    if (Conf.EXTRACT_DATES) {
      //      List(
      //        new WhenEntityChooser,
      new FinalVersionEntityChooser,
      new NotificationEntityChooser,
      new SubmissionEntityChooser
    )
  //    } else {
  //      Nil
  //    }
  //  }

  def checkAccuracy(model: TemplateModel, inferredDocs: Seq[nlp.Document]) = {
    val labelStats = mutable.Map[String, (Int, Int, Int, Int)]()
    val labelAccuracies = scala.collection.mutable.Map[String, (Int, Int)]()
    val labelPrecisions = scala.collection.mutable.Map[String, (Int, Int)]()
    val confusionMatrixInferredToReal = scala.collection.mutable.Map[String, mutable.Map[String, Int]]()
    for (label <- ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.values) {
      confusionMatrixInferredToReal += ((label, scala.collection.mutable.Map[String, Int]()))
      for (label2 <- ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.values) {
        confusionMatrixInferredToReal(label) += ((label2, 0))
      }
    }
    val labelOrder = List(ConferenceHtmlAnnotation.OTHER_LABEL, ConferenceHtmlAnnotation.NAME, ConferenceHtmlAnnotation.ABBREVIATION,
      ConferenceHtmlAnnotation.WHERE, ConferenceHtmlAnnotation.WHEN, ConferenceHtmlAnnotation.SUBMISSION,
      ConferenceHtmlAnnotation.NOTIFICATION, ConferenceHtmlAnnotation.FINAL_VERSION)

    var i = -1
    for (inferredDoc <- inferredDocs) {
      i += 1
      println("NEXT DOC--------------------------------------------------------------")
      println(inferredDoc.name)

      // map label -> (inferred not in real, real not inferred, inferred partly in real, inferred same like in real)
      val inferredOccur = findAllOccurs(inferredDoc, thisCategoryFunction)
      val realOccur = findAllOccurs(inferredDoc, targetCategoryFunction).
        mapValues(Set[Seq[Token]](_: _*)).mapValues(_.map(_.map(_.string)))

      for (token <- inferredDoc.tokens) {
        val realCategory = if (token.attr[LabeledTreeNerTag].target.categoryValue.startsWith("B_"))
          token.attr[LabeledTreeNerTag].target.categoryValue.substring(2)
        else
          token.attr[LabeledTreeNerTag].target.categoryValue
        val inferredCategory = if (token.attr[LabeledTreeNerTag].categoryValue.startsWith("B_"))
          token.attr[LabeledTreeNerTag].categoryValue.substring(2)
        else
          token.attr[LabeledTreeNerTag].categoryValue
        val finelyInferred = inferredCategory == realCategory
        labelAccuracies.get(realCategory) match {
          case None =>
            finelyInferred match {
              case true => labelAccuracies put(realCategory, (1, 0))
              case false => labelAccuracies put(realCategory, (0, 1))
            }
          case Some((good, bad)) =>
            finelyInferred match {
              case true => labelAccuracies put(realCategory, (good + 1, bad))
              case false => labelAccuracies put(realCategory, (good, bad + 1))
            }
        }
        labelPrecisions.get(inferredCategory) match {
          case None =>
            finelyInferred match {
              case true => labelPrecisions put(inferredCategory, (1, 0))
              case false => labelPrecisions put(inferredCategory, (0, 1))
            }
          case Some((good, bad)) =>
            finelyInferred match {
              case true => labelPrecisions put(inferredCategory, (good + 1, bad))
              case false => labelPrecisions put(inferredCategory, (good, bad + 1))
            }
        }
        confusionMatrixInferredToReal(inferredCategory) += ((realCategory, confusionMatrixInferredToReal(inferredCategory)(realCategory) + 1))
      }

      for (label <- ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.valuesNoIob) {
        realOccur.get(label) match {
          case None =>
            inferredOccur.get(label) match {
              case None =>
              case Some(inferredSeqs) =>
                println("nadmiarowo wykryte (" + label + "): " + inferredSeqs)
                addInfNotRealToLabelMap(labelStats, label)
            }
          case Some(realSeqsRaw) =>
            val realSeqs = realSeqsRaw.map(_.map(_.trim))
            inferredOccur.get(label) match {
              case None =>
                accountInferredNotFound(realSeqs, labelStats, label)
              case Some(inferredSeqs) =>
                entityChoosers.filter(_.operatingLabel == label).head.chooseBestEntity(inferredSeqs, model, label) match {
                  case None =>
                    accountInferredNotFound(realSeqs, labelStats, label)
                  case Some(bestInferred) =>
                    var printInfo = true
                    if (realSeqs.contains(bestInferred)) {
                      printInfo = false
                      println("xxxx " + bestInferred)
                      labelStats get (label) match {
                        case None =>
                          labelStats put(label, (0, 0, 0, 1))
                        case Some((infNotReal, realNotInf, partInf, same)) =>
                          labelStats put(label, (infNotReal, realNotInf, partInf, same + 1))
                      }
                    } else {
                      if (i >= 10) {
                        print(" ") // checkpointz
                      }
                      if (ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.NAME).isDefined &&
                        ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.NAME).get == label) {

                        if (realSeqs.flatten.filter(bestInferred.contains(_)).size > 3) {
                          addPartlyMatchedToLabelMap(labelStats, label, realSeqs)
                        } else {
                          addRealNotInfToLabelMap(labelStats, label)
                          addInfNotRealToLabelMap(labelStats, label)
                        }
                      } else if (ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.WHEN).isDefined &&
                        ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.WHEN).get == label) {

                        if (datesArePartlyEquals(realSeqs, bestInferred)) {
                          addPartlyMatchedToLabelMap(labelStats, label, realSeqs)
                        } else {
                          addRealNotInfToLabelMap(labelStats, label)
                          addInfNotRealToLabelMap(labelStats, label)
                        }
                      } else if (ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.WHERE).isDefined &&
                        ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.WHERE).get == label) {

                        if (realSeqs.flatten.filter(bestInferred.flatten.contains(_)).size > 0) {
                          addPartlyMatchedToLabelMap(labelStats, label, realSeqs)
                        } else {
                          addRealNotInfToLabelMap(labelStats, label)
                          addInfNotRealToLabelMap(labelStats, label)
                        }
                      } else if (ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.SUBMISSION).isDefined &&
                        ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.SUBMISSION).get == label) {
                        if (datesArePartlyEquals(realSeqs, bestInferred)) {
                          addPartlyMatchedToLabelMap(labelStats, label, realSeqs)
                        } else {
                          addRealNotInfToLabelMap(labelStats, label)
                          addInfNotRealToLabelMap(labelStats, label)
                        }
                      } else if (ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.NOTIFICATION).isDefined &&
                        ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.NOTIFICATION).get == label) {

                        if (datesArePartlyEquals(realSeqs, bestInferred)) {
                          addPartlyMatchedToLabelMap(labelStats, label, realSeqs)
                        } else {
                          addRealNotInfToLabelMap(labelStats, label)
                          addInfNotRealToLabelMap(labelStats, label)
                        }
                      } else if (ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.FINAL_VERSION).isDefined &&
                        ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.FINAL_VERSION).get == label) {

                        if (datesArePartlyEquals(realSeqs, bestInferred)) {
                          addPartlyMatchedToLabelMap(labelStats, label, realSeqs)
                        } else {
                          addRealNotInfToLabelMap(labelStats, label)
                          addInfNotRealToLabelMap(labelStats, label)
                        }
                      } else if (ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.ABBREVIATION).isDefined &&
                        ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.ABBREVIATION).get == label) {

                        if (realSeqs.flatten.filter(bestInferred.flatten.contains(_)).size > 0) {
                          addPartlyMatchedToLabelMap(labelStats, label, realSeqs)
                        } else {
                          addRealNotInfToLabelMap(labelStats, label)
                          addInfNotRealToLabelMap(labelStats, label)
                        }
                      }
                    }
                    if (printInfo) {
                      println("wykryte rozbieznie (" + label + "): " + bestInferred + ", REAL: " + realSeqs)
                    }
                }
            }
        }
      }

      println(labelStats)
      println(" ")
    }

    println("labelStats: (inferred not in real, real not inferred, inferred partly in real, inferred same like in real)")
    println(" ")
    println(printlnFinalResults(labelStats, labelOrder))

    println(" ")
    println(printlnTokenAccuracy(labelAccuracies, labelPrecisions, labelOrder))

    println("Confusion Matrix")
    println(" ")
    println(printConfusionMatrix(confusionMatrixInferredToReal, "Macierz konfuzji dla modelu z wszystkicmi cechami", "conf1"))
  }

  def printlnFinalResults(labelStats: mutable.Map[String, (Int, Int, Int, Int)], labelOrder: List[String]): String = {
    val f1 = new mutable.StringBuilder()
    val precision = new mutable.StringBuilder()
    val recall = new mutable.StringBuilder()
    val ok = new mutable.StringBuilder()
    val partly = new mutable.StringBuilder()
    val tooMuch = new mutable.StringBuilder()
    val notFound = new mutable.StringBuilder()
    for (labelInOrder <- labelOrder) {
      for (tuple <- labelStats) {
        if (tuple._1 == labelInOrder) {
          val prec = (1.0 * (tuple._2._3 + tuple._2._4 + 1) / (tuple._2._1 + tuple._2._3 + tuple._2._4 + 1))
          val rec = (1.0 * (tuple._2._3 + tuple._2._4 + 1) / (tuple._2._2 + tuple._2._3 + tuple._2._4 + 1))
          f1.append("& " + (2.0 * (prec * rec) / (prec + rec)).formatted("%.2f"))
          precision.append("& " + prec.formatted("%.2f"))
          recall.append("& " + rec.formatted("%.2f"))
          ok.append("& " + tuple._2._4)
          partly.append("& " + tuple._2._3)
          tooMuch.append("& " + tuple._2._1)
          notFound.append("& " + tuple._2._2)
        }
      }
    }
    """
      |\begin{table}[htb]
      |\centering
      |\caption{Dokładność i kompletność wybranych informacji.}
      |\label{tab:full1}
      |\begin{minipage}{\the\textwidth}
      |\setlength{\baselineskip}{2mm}
      |\centering
      |\scalebox{0.7}{
      |\begin{tabular}{c|c|c|c|c|c|c|c}
      |\textbf{miara} & \textbf{nazwa} & \textbf{skrót} & \textbf{miejsce} & \textbf{data} & \textbf{przyjęcie dok.} & \textbf{notyfikacja} & \textbf{wersja ost.} \\ \hline
      |\textbf{F1} %s \\ \hline
      |\textbf{dokładność} %s \\ \hline
      |\textbf{kompletność} %s \\ \hline
      |\textbf{\shortstack[c]{znalezione\\poprawne (TP)}} %s \\ \hline
      |\textbf{\shortstack[c]{znalezione\\częściowo}} %s \\ \hline
      |\textbf{\shortstack[c]{znalezione\\nieprawidłowe (FP)}} %s \\ \hline
      |\textbf{\shortstack[c]{nie znalezione\\poprawne (FN)}} %s \\ \hline
      |\end{tabular}
      |}
      |\end{minipage}
      |\end{table}
    """.stripMargin.format(f1, precision, recall, ok, partly, tooMuch, notFound)
  }

  def printConfusionMatrix(confusionInferredToReal: mutable.Map[String, mutable.Map[String, Int]], caption: String, tableId: String) = {
    """
      |\begin{table}[htb]
      |\centering
      |\caption{%s}
      |\label{tab:%s}
      |\begin{minipage}{\the\textwidth}
      |\setlength{\baselineskip}{2mm}
      |\centering
      |\scalebox{0.7}{
      |\begin{tabular}{c|c|c|c|c|c|c|c|c}
      | & \textbf{inne} & \textbf{nazwa} & \textbf{skrót} & \textbf{miejsce} & \textbf{data} & \textbf{przyjęcie dok.} & \textbf{notyfikacja} & \textbf{wersja ost.} \\ \hline
      |\textbf{inne} & %s & %s& %s& %s& %s& %s& %s& %s \\ \hline
      |\textbf{nazwa} & %s & %s& %s& %s& %s& %s& %s& %s \\ \hline
      |\textbf{skrót} & %s & %s& %s& %s& %s& %s& %s& %s \\ \hline
      |\textbf{miejsce} & %s & %s& %s& %s& %s& %s& %s& %s \\ \hline
      |\textbf{data} & %s & %s& %s& %s& %s& %s& %s& %s \\ \hline
      |\textbf{przyjęcie dok.} & %s & %s& %s& %s& %s& %s& %s& %s \\ \hline
      |\textbf{notyfikacja} & %s & %s& %s& %s& %s& %s& %s& %s \\ \hline
      |\textbf{wersja ost.} & %s & %s& %s& %s& %s& %s& %s& %s \\ \hline
      |\end{tabular}
      |}
      |\end{minipage}
      |\end{table}
    """.stripMargin.format(caption, tableId,
        confusionInferredToReal(ConferenceHtmlAnnotation.OTHER_LABEL)(ConferenceHtmlAnnotation.OTHER_LABEL),
        confusionInferredToReal(ConferenceHtmlAnnotation.OTHER_LABEL)(ConferenceHtmlAnnotation.NAME),
        confusionInferredToReal(ConferenceHtmlAnnotation.OTHER_LABEL)(ConferenceHtmlAnnotation.ABBREVIATION),
        confusionInferredToReal(ConferenceHtmlAnnotation.OTHER_LABEL)(ConferenceHtmlAnnotation.WHERE),
        confusionInferredToReal(ConferenceHtmlAnnotation.OTHER_LABEL)(ConferenceHtmlAnnotation.WHEN),
        confusionInferredToReal(ConferenceHtmlAnnotation.OTHER_LABEL)(ConferenceHtmlAnnotation.SUBMISSION),
        confusionInferredToReal(ConferenceHtmlAnnotation.OTHER_LABEL)(ConferenceHtmlAnnotation.NOTIFICATION),
        confusionInferredToReal(ConferenceHtmlAnnotation.OTHER_LABEL)(ConferenceHtmlAnnotation.FINAL_VERSION),

        confusionInferredToReal(ConferenceHtmlAnnotation.NAME)(ConferenceHtmlAnnotation.OTHER_LABEL),
        confusionInferredToReal(ConferenceHtmlAnnotation.NAME)(ConferenceHtmlAnnotation.NAME),
        confusionInferredToReal(ConferenceHtmlAnnotation.NAME)(ConferenceHtmlAnnotation.ABBREVIATION),
        confusionInferredToReal(ConferenceHtmlAnnotation.NAME)(ConferenceHtmlAnnotation.WHERE),
        confusionInferredToReal(ConferenceHtmlAnnotation.NAME)(ConferenceHtmlAnnotation.WHEN),
        confusionInferredToReal(ConferenceHtmlAnnotation.NAME)(ConferenceHtmlAnnotation.SUBMISSION),
        confusionInferredToReal(ConferenceHtmlAnnotation.NAME)(ConferenceHtmlAnnotation.NOTIFICATION),
        confusionInferredToReal(ConferenceHtmlAnnotation.NAME)(ConferenceHtmlAnnotation.FINAL_VERSION),

        confusionInferredToReal(ConferenceHtmlAnnotation.ABBREVIATION)(ConferenceHtmlAnnotation.OTHER_LABEL),
        confusionInferredToReal(ConferenceHtmlAnnotation.ABBREVIATION)(ConferenceHtmlAnnotation.NAME),
        confusionInferredToReal(ConferenceHtmlAnnotation.ABBREVIATION)(ConferenceHtmlAnnotation.ABBREVIATION),
        confusionInferredToReal(ConferenceHtmlAnnotation.ABBREVIATION)(ConferenceHtmlAnnotation.WHERE),
        confusionInferredToReal(ConferenceHtmlAnnotation.ABBREVIATION)(ConferenceHtmlAnnotation.WHEN),
        confusionInferredToReal(ConferenceHtmlAnnotation.ABBREVIATION)(ConferenceHtmlAnnotation.SUBMISSION),
        confusionInferredToReal(ConferenceHtmlAnnotation.ABBREVIATION)(ConferenceHtmlAnnotation.NOTIFICATION),
        confusionInferredToReal(ConferenceHtmlAnnotation.ABBREVIATION)(ConferenceHtmlAnnotation.FINAL_VERSION),

        confusionInferredToReal(ConferenceHtmlAnnotation.WHERE)(ConferenceHtmlAnnotation.OTHER_LABEL),
        confusionInferredToReal(ConferenceHtmlAnnotation.WHERE)(ConferenceHtmlAnnotation.NAME),
        confusionInferredToReal(ConferenceHtmlAnnotation.WHERE)(ConferenceHtmlAnnotation.ABBREVIATION),
        confusionInferredToReal(ConferenceHtmlAnnotation.WHERE)(ConferenceHtmlAnnotation.WHERE),
        confusionInferredToReal(ConferenceHtmlAnnotation.WHERE)(ConferenceHtmlAnnotation.WHEN),
        confusionInferredToReal(ConferenceHtmlAnnotation.WHERE)(ConferenceHtmlAnnotation.SUBMISSION),
        confusionInferredToReal(ConferenceHtmlAnnotation.WHERE)(ConferenceHtmlAnnotation.NOTIFICATION),
        confusionInferredToReal(ConferenceHtmlAnnotation.WHERE)(ConferenceHtmlAnnotation.FINAL_VERSION),

        confusionInferredToReal(ConferenceHtmlAnnotation.WHEN)(ConferenceHtmlAnnotation.OTHER_LABEL),
        confusionInferredToReal(ConferenceHtmlAnnotation.WHEN)(ConferenceHtmlAnnotation.NAME),
        confusionInferredToReal(ConferenceHtmlAnnotation.WHEN)(ConferenceHtmlAnnotation.ABBREVIATION),
        confusionInferredToReal(ConferenceHtmlAnnotation.WHEN)(ConferenceHtmlAnnotation.WHERE),
        confusionInferredToReal(ConferenceHtmlAnnotation.WHEN)(ConferenceHtmlAnnotation.WHEN),
        confusionInferredToReal(ConferenceHtmlAnnotation.WHEN)(ConferenceHtmlAnnotation.SUBMISSION),
        confusionInferredToReal(ConferenceHtmlAnnotation.WHEN)(ConferenceHtmlAnnotation.NOTIFICATION),
        confusionInferredToReal(ConferenceHtmlAnnotation.WHEN)(ConferenceHtmlAnnotation.FINAL_VERSION),

        confusionInferredToReal(ConferenceHtmlAnnotation.SUBMISSION)(ConferenceHtmlAnnotation.OTHER_LABEL),
        confusionInferredToReal(ConferenceHtmlAnnotation.SUBMISSION)(ConferenceHtmlAnnotation.NAME),
        confusionInferredToReal(ConferenceHtmlAnnotation.SUBMISSION)(ConferenceHtmlAnnotation.ABBREVIATION),
        confusionInferredToReal(ConferenceHtmlAnnotation.SUBMISSION)(ConferenceHtmlAnnotation.WHERE),
        confusionInferredToReal(ConferenceHtmlAnnotation.SUBMISSION)(ConferenceHtmlAnnotation.WHEN),
        confusionInferredToReal(ConferenceHtmlAnnotation.SUBMISSION)(ConferenceHtmlAnnotation.SUBMISSION),
        confusionInferredToReal(ConferenceHtmlAnnotation.SUBMISSION)(ConferenceHtmlAnnotation.NOTIFICATION),
        confusionInferredToReal(ConferenceHtmlAnnotation.SUBMISSION)(ConferenceHtmlAnnotation.FINAL_VERSION),

        confusionInferredToReal(ConferenceHtmlAnnotation.NOTIFICATION)(ConferenceHtmlAnnotation.OTHER_LABEL),
        confusionInferredToReal(ConferenceHtmlAnnotation.NOTIFICATION)(ConferenceHtmlAnnotation.NAME),
        confusionInferredToReal(ConferenceHtmlAnnotation.NOTIFICATION)(ConferenceHtmlAnnotation.ABBREVIATION),
        confusionInferredToReal(ConferenceHtmlAnnotation.NOTIFICATION)(ConferenceHtmlAnnotation.WHERE),
        confusionInferredToReal(ConferenceHtmlAnnotation.NOTIFICATION)(ConferenceHtmlAnnotation.WHEN),
        confusionInferredToReal(ConferenceHtmlAnnotation.NOTIFICATION)(ConferenceHtmlAnnotation.SUBMISSION),
        confusionInferredToReal(ConferenceHtmlAnnotation.NOTIFICATION)(ConferenceHtmlAnnotation.NOTIFICATION),
        confusionInferredToReal(ConferenceHtmlAnnotation.NOTIFICATION)(ConferenceHtmlAnnotation.FINAL_VERSION),

        confusionInferredToReal(ConferenceHtmlAnnotation.FINAL_VERSION)(ConferenceHtmlAnnotation.OTHER_LABEL),
        confusionInferredToReal(ConferenceHtmlAnnotation.FINAL_VERSION)(ConferenceHtmlAnnotation.NAME),
        confusionInferredToReal(ConferenceHtmlAnnotation.FINAL_VERSION)(ConferenceHtmlAnnotation.ABBREVIATION),
        confusionInferredToReal(ConferenceHtmlAnnotation.FINAL_VERSION)(ConferenceHtmlAnnotation.WHERE),
        confusionInferredToReal(ConferenceHtmlAnnotation.FINAL_VERSION)(ConferenceHtmlAnnotation.WHEN),
        confusionInferredToReal(ConferenceHtmlAnnotation.FINAL_VERSION)(ConferenceHtmlAnnotation.SUBMISSION),
        confusionInferredToReal(ConferenceHtmlAnnotation.FINAL_VERSION)(ConferenceHtmlAnnotation.NOTIFICATION),
        confusionInferredToReal(ConferenceHtmlAnnotation.FINAL_VERSION)(ConferenceHtmlAnnotation.FINAL_VERSION)
      )
  }

  def printlnTokenAccuracy(labelAccuracies: mutable.Map[String, (Int, Int)], labelPrecisions: mutable.Map[String, (Int, Int)], labelOrder: List[String]): String = {
    val builder = new mutable.StringBuilder()
    for (labelInOrder <- labelOrder) {
      for (tuple <- labelPrecisions) {
        if (tuple._1 == labelInOrder) {
          builder.append("& " + (1.0 * tuple._2._1 / (tuple._2._1 + tuple._2._2)).formatted("%.2f"))
        }
      }
    }
    val precision = builder.toString()
    builder.clear()
    for (labelInOrder <- labelOrder) {
      for (tuple <- labelAccuracies) {
        if (tuple._1 == labelInOrder) {
          builder.append("& " + (1.0 * tuple._2._1 / (tuple._2._1 + tuple._2._2)).formatted("%.2f"))
        }
      }
    }
    val recall = builder.toString()
    """
      |\begin{table}[htb]
      |\centering
      |\caption{}
      |\label{tab:token-accuracy1}
      |\begin{minipage}{\the\textwidth}
      |\setlength{\baselineskip}{2mm}
      |\centering
      |\scalebox{0.7}{
      |\begin{tabular}{c|c|c|c|c|c|c|c|c}
      |\textbf{miara} & \textbf{inne} & \textbf{nazwa} & \textbf{skrót} & \textbf{miejsce} & \textbf{data} & \textbf{przyjęcie dok.} & \textbf{notyfikacja} & \textbf{wersja ost.} \\ \hline
      |\textbf{dokładność} %s \\ \hline
      |\textbf{kompletność} %s \\ \hline
      |\end{tabular}
      |}
      |\end{minipage}
      |\end{table}
    """.stripMargin.format(precision, recall)
  }

  private def thisCategoryFunction: (Token) => String = {
    _.attr[LabeledTreeNerTag].categoryValue
  }

  private def targetCategoryFunction: (Token) => String = {
    _.attr[LabeledTreeNerTag].target.categoryValue
  }

  private def accountInferredNotFound(realSeqs: Set[Seq[String]], labelStats: mutable.Map[String, (Int, Int, Int, Int)], label: String) {
    println("w ogole nie wykryte (" + label + "): " + realSeqs)
    addRealNotInfToLabelMap(labelStats, label)
  }

  private def datesArePartlyEquals(realSeqs: Set[Seq[String]], bestInferred: Seq[String]): Boolean = {
    val punct = "[,-]"
    realSeqs.flatten.filterNot(_.matches(punct)).forall(bestInferred.contains(_)) ||
      bestInferred.filterNot(_.matches(punct)).forall(realSeqs.flatten.contains(_))
  }

  private def addInfNotRealToLabelMap(labelStats: mutable.Map[String, (Int, Int, Int, Int)], label: String) {
    labelStats get (label) match {
      case None =>
        labelStats put(label, (1, 0, 0, 0))
      case Some((infNotReal, realNotInf, partInf, same)) =>
        labelStats put(label, (infNotReal + 1, realNotInf, partInf, same))
    }
  }

  private def addRealNotInfToLabelMap(labelStats: mutable.Map[String, (Int, Int, Int, Int)], label: String) {
    labelStats get (label) match {
      case None =>
        labelStats put(label, (0, 1, 0, 0))
      case Some((infNotReal, realNotInf, partInf, same)) =>
        labelStats put(label, (infNotReal, realNotInf + 1, partInf, same))
    }
  }

  private def addPartlyMatchedToLabelMap(labelStats: mutable.Map[String, (Int, Int, Int, Int)], label: String, realSeqs: Set[Seq[String]]) {
    println("wykryte czesciowo (" + label + "): " + realSeqs)
    labelStats get (label) match {
      case None =>
        labelStats put(label, (0, 0, 1, 0))
      case Some((infNotReal, realNotInf, partInf, same)) =>
        labelStats put(label, (infNotReal, realNotInf, partInf + 1, same))
    }
  }

  private def findAllOccurs(document: nlp.Document, categoryValueFunction: Token => String) = {
    val result = scala.collection.mutable.Map[String, List[Seq[Token]]]()
    var isSpecialSeq = false
    var currentLabel = ""
    var currentSeq: Seq[nlp.Token] = Nil
    for (token <- document.tokens) {
      val labelNoIob = ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.convertNoIop(categoryValueFunction(token))
      if (labelNoIob != ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP(ConferenceHtmlAnnotation.OTHER_LABEL)) {
        isSpecialSeq = true

        if (currentLabel != labelNoIob && currentLabel.nonEmpty) {
          result put(currentLabel, result.getOrElse(currentLabel, Nil) :+ currentSeq)
          currentSeq = Nil
        }
        currentLabel = labelNoIob

        currentSeq = currentSeq :+ token
      } else if (isSpecialSeq == true) {
        result put(currentLabel, result.getOrElse(currentLabel, Nil) :+ currentSeq)
        isSpecialSeq = false
        currentLabel = ""
        currentSeq = Nil
      }
    }
    if (isSpecialSeq == true) {
      result put(currentLabel, result.getOrElse(currentLabel, Nil) :+ currentSeq)
    }
    result
  }

  private def getValueByLabel(conferenceData: ConferenceData, label: String) = {
    annotatorsMap.get(label) match {
      case None => None
      case Some(annotator) => Some(annotator.getValueTokens(conferenceData))
    }
  }

  private def findInconsistences(map1: scala.collection.mutable.Map[Int, Seq[String]], map2: scala.collection.mutable.Map[Int, Seq[String]],
                                 inferredDoc: nlp.Document) = {
    val mapNotFound = scala.collection.mutable.Set[(Int, Seq[String], Seq[String])]()
    for ((index, seq) <- map1.toSeq.sortBy(_._1)) {
      if (!map2.contains(index) ||
        seq.size > map2(index).size) {
        mapNotFound += ((index, seq, inferredDoc.tokens.slice(index, index + seq.size).map(_.attr[LabeledTreeNerTag].baseCategoryValue).toSeq))
      }
    }
    mapNotFound
  }

  private def fillInOccuranceMap(document: nlp.Document, categoryNameFunction: Token => String) = {
    val result = scala.collection.mutable.Map[Int, Seq[String]]()
    var iter = 0
    var isSpecialSeq = false
    var currIter = 0
    var currentSeq: Seq[String] = Nil
    for (token <- document.tokens) {
      val labelNoIob = ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.convertNoIop(categoryNameFunction(token))
      if (labelNoIob != ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP(ConferenceHtmlAnnotation.OTHER_LABEL)) {
        if (!isSpecialSeq) {
          currIter = iter
        }
        isSpecialSeq = true
        currentSeq = labelNoIob +: currentSeq
      } else if (isSpecialSeq == true) {
        result put(currIter, currentSeq)
        isSpecialSeq = false
        currentSeq = Nil
      }

      iter += 1
    }
    if (isSpecialSeq == true) {
      result put(currIter, currentSeq)
    }
    result
  }
}
