package pl.edu.pw.ii.entityminer.mining.factorie

import cc.factorie.model.{DotTemplateWithStatistics2, DotTemplateWithStatistics1, Parameters, TemplateModel}
import cc.factorie._
import cc.factorie.app.nlp._
import cc.factorie.app.nlp.ner._
import cc.factorie.variable.{HammingObjective, BinaryFeatureVectorVariable, CategoricalVectorDomain}
import cc.factorie.app.nlp.Token
import bsh.This
import pl.edu.pw.ii.entityminer.mining.factorie.sample.ExampleDocumentLoader
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.{TreeNerTag, LabeledTreeNerTag, ConferenceLabelDomain}
import cc.factorie.optimize.{Trainer, LikelihoodExample}
import cc.factorie.infer.InferByBPChain

/**
 * Created with IntelliJ IDEA.
 * User: ralpher
 * Date: 12/6/13
 * Time: 3:03 PM
 * To change this template use File | Settings | File Templates.
 */
object TreeLabelCrfModel {

  object TokenFeaturesDomain extends CategoricalVectorDomain[String]

  class TokenFeatures(val token: Token) extends BinaryFeatureVectorVariable[String] {
    override def skipNonCategories = true

    def domain = TokenFeaturesDomain
  }

  object LabelFeaturesDomain extends CategoricalVectorDomain[String]

  class LabelFeatures(val token: Token) extends BinaryFeatureVectorVariable[String] {
    def domain = LabelFeaturesDomain
  }

  val model = new TemplateModel with Parameters {
    addTemplates(
      // Bias term on each individual label
      new DotTemplateWithStatistics1[TreeNerTag] {
        //def statisticsDomains = Tuple1(Conll2003NerDomain)
        val weights = Weights(new la.DenseTensor1(ConferenceLabelDomain.size))
      },
      // Factor between label and observed token
      new DotTemplateWithStatistics2[TreeNerTag, TokenFeatures] {
        //def statisticsDomains = ((Conll2003NerDomain, TokenFeaturesDomain))
        val weights = Weights(new la.DenseTensor2(ConferenceLabelDomain.size, TokenFeaturesDomain.dimensionSize))

        def unroll1(label: TreeNerTag) = Factor(label, label.token.attr[TokenFeatures])

        def unroll2(tf: TokenFeatures) = Factor(tf.token.attr[TreeNerTag], tf)
      },
      new DotTemplateWithStatistics2[TreeNerTag, TreeNerTag] {
        //def statisticsDomains = ((Conll2003NerDomain, Conll2003NerDomain))
        val weights = Weights(new la.DenseTensor2(ConferenceLabelDomain.size, ConferenceLabelDomain.size))

        def unroll1(label: TreeNerTag) = {
          (if (label.token.hasPrev) List(Factor(label.token.prev.attr[TreeNerTag], label)) else Nil)
        }

        def unroll2(label: TreeNerTag) = {
          (if (label.token.hasNext) List(Factor(label, label.token.next.attr[TreeNerTag])) else Nil)
        }
      }
//    ,
////      Transition factors between two successive labels
//      new DotTemplateWithStatistics2[TreeNerTag, TreeNerTag] {
//        //def statisticsDomains = ((Conll2003NerDomain, Conll2003NerDomain))
//        val weights = Weights(new la.DenseTensor2(TreeNerDomain.size, TreeNerDomain.size))
//
//        def unroll1(label: TreeNerTag) = {
//          val res = {
//                      val parent = label.token.attr[TreeTag].parent
//                      if (parent != null) {
////                        val features = new LabelFeatures(token)
//                        List(Factor(parent.attr[TreeNerTag], label))
//                      } else {
//                        Nil
//                      }
//                    }
////            (if (label.token.hasPrev) List(Factor(label.token.prev.attr[TreeNerTag], label)) else Nil)
//          res
//        }
//
//        def unroll2(label: TreeNerTag) = {
//          var prev: Option[TreeNerTag] = None
//          val res = {
//            label.token.attr[TreeTag].children.foldLeft(List[Factor]()) {
//              case (res, child) =>
//                val sibling: Option[Factor] = prev match {
//                  case Some(node) => Some(Factor(node, child.attr[TreeNerTag]))
//                  case None => None
//                }
//                prev = Some(child.attr[TreeNerTag])
//                sibling match {
//                  case None =>
//                    Factor(label, child.attr[TreeNerTag]) :: res
//                  case Some(nodeFactor) =>
//                    nodeFactor :: Factor(label, child.attr[TreeNerTag]) :: res
//                }
//            }
//          }// ++
////            (if (label.token.hasNext) List(Factor(label, label.token.next.attr[TreeNerTag])) else Nil)
//          res
//        }
//      }
//      ,
      // Transition factors between two labels in tree
//      new DotTemplateWithStatistics2[TreeNerTag, TreeNerTag] {
//        //def statisticsDomains = ((Conll2003NerDomain, Conll2003NerDomain))
//        val weights = Weights(new la.DenseTensor2(ConferenceLabelDomain.size, ConferenceLabelDomain.size))
//
//        def unroll1(label: TreeNerTag) = {
//          val childrenFactors = label.token.attr[TreeTag].children.foldLeft(List[Factor]()) { case (res, child) =>
//            Factor(label, child.attr[TreeNerTag]) :: res
//          }
//          childrenFactors
//          val parent = label.token.attr[TreeTag].parent
//          if (parent != null) {
//            List(Factor(parent.attr[TreeNerTag], label))
//          } else {
//            Nil
//          } ++ childrenFactors
//        }
//
//        def unroll2(label: TreeNerTag) = {
//          Nil
////          val childrenFactors = label.token.attr[TreeTag].children.foldLeft(List[Factor]()) { case (res, child) =>
////            Factor(label, child.attr[TreeNerTag]) :: res
////          }
////          childrenFactors
////          val parent = label.token.attr[TreeTag].parent
////          if (parent != null) {
////            List(Factor(parent.attr[TreeNerTag], label))
////          } else {
////            Nil
////          } ++ childrenFactors
//        }
//      }
    )
  }

  def main(args: Array[String]) = {
    val tmp = ExampleDocumentLoader.fromSource
    val testSize = 1
    val trainDocuments = tmp.take(tmp.size - testSize)
    val testDocuments = tmp.takeRight(testSize)

    for (document <- trainDocuments ++ testDocuments; token <- document.tokens) {
      val features = new TokenFeatures(token)
      features += "W=" + token.string
      features += "SHAPE=" + cc.factorie.app.strings.stringShape(token.string, 2)
      features += "P=" + (token.attr[TreeTag].parent != null)
      token.attr += features
    }

    val trainLabelsSentences: Seq[Seq[LabeledTreeNerTag]] = trainDocuments.map(_.tokens.toSeq.map(_.attr[LabeledTreeNerTag]))
    val testLabelsSentences: Seq[Seq[LabeledTreeNerTag]] = testDocuments.map(_.tokens.toSeq.map(_.attr[LabeledTreeNerTag]))

    println("*** Starting training (#sentences=%d)".format(trainDocuments.map(_.sentences.size).sum))
    val start = System.currentTimeMillis

    implicit val random = new scala.util.Random(0)
    val examples = trainLabelsSentences.map(s => new LikelihoodExample(s, model, InferByBPChain))
    Trainer.batchTrain(model.parameters, examples)
    println("*** Starting inference (#sentences=%d)".format(testDocuments.map(_.sentences.size).sum))
    testLabelsSentences.foreach {
      variables => cc.factorie.infer.BP.inferTreeSum(variables, model).setToMaximize(null)
    }
    println("test token accuracy=" + HammingObjective.accuracy(testLabelsSentences.flatten))

    testDocuments.foreach(_.tokens.foreach(l => println("Token: " + l + " Label: " + l.attr[LabeledTreeNerTag].baseCategoryValue)))

    println("Total training took " + (System.currentTimeMillis - start) / 1000.0 + " seconds")

    model.templates.foreach {
      template =>
        println(template.toString)
    }
  }
}
