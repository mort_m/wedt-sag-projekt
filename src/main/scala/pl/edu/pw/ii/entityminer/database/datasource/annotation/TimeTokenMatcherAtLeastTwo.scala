package pl.edu.pw.ii.entityminer.database.datasource.annotation

import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.mining.token.feature.StopWords
import org.jsoup.nodes.TextNode
import scala.collection.mutable.ListBuffer
import pl.edu.pw.ii.entityminer.database.datasource.wikicfp.WikicfpConsts
import pl.edu.pw.ii.entityminer.mining.nlp.TimeName

/**
 * Created by Raphael Hazan on 1/13/14.
 */
trait TimeTokenMatcherAtLeastTwo extends AnnotateStrategy {
  val maxGapBetweenNextKeywords: Int = 2

  def findFirstOccurence(tokens: List[WebToken], seq: List[String]): (List[WebToken], List[WebToken]) = {
    val (notNumbers, numbers) = seq.foldLeft((Set[String](), Set[String]())) {
      case ((notNum, num), str) =>
        if (str.filter(_.isDigit).nonEmpty) {
          (notNum, num + str)
        } else {
          (notNum + str, num)
        }
    }
    var rest = tokens
    while (rest.nonEmpty) {
      val result = findFirstOccurence(rest, notNumbers, numbers, Set(),0, Nil, Nil, false)
      if (result.nonEmpty) {
        return (result, rest.drop(result.size))
      }
      rest = rest.drop(1)
    }
    (Nil, Nil)
  }

  private def isItYear(text: String) = {
    val numberStr = text.filter(_.isDigit)
    if (numberStr.isEmpty) {
      false
    } else {
      numberStr.toLong > 2000 && numberStr.toLong < 2050
    }
  }

  private def findFirstOccurence(tokens: List[WebToken], timeNotNumbers: Set[String], timeNumbers: Set[String], timeNumbersMatched: Set[String], currentGap: Int,
                                 matchedPart: List[WebToken], gapPart: List[WebToken], yearAlreadyMatched: Boolean): List[WebToken] = {
    tokens match {
      case Nil =>
        if (timeNotNumbers.isEmpty && timeNumbersMatched.nonEmpty) {
          matchedPart
        } else {
          Nil
        }

      case token :: rest if timeNotNumbers.contains(TimeName.getTimeName(token.text)) =>
        findFirstOccurence(rest, timeNotNumbers - TimeName.getTimeName(token.text), timeNumbers, timeNumbersMatched,
          maxGapBetweenNextKeywords, matchedPart ++ gapPart :+ token, Nil, yearAlreadyMatched)

      case token :: rest if timeNumbers.contains(TimeName.getTimeName(token.text)) && (!yearAlreadyMatched || !isItYear(token.text)) =>
        findFirstOccurence(rest, timeNotNumbers, timeNumbers, timeNumbersMatched + TimeName.getTimeName(token.text),
          maxGapBetweenNextKeywords, matchedPart ++ gapPart :+ token, Nil, yearAlreadyMatched || isItYear(token.text))

      //        if there are bad numbers, this is different date probably
      case token :: rest if currentGap > 0 && token.text.filter(_.isDigit).nonEmpty =>
        Nil

      case token :: rest if currentGap > 0 =>
        findFirstOccurence(rest, timeNotNumbers, timeNumbers, timeNumbersMatched, currentGap - 1, matchedPart, gapPart :+ token, yearAlreadyMatched)

      case token :: rest if currentGap <= 0 =>
        if (timeNotNumbers.isEmpty && timeNumbersMatched.nonEmpty) {
          matchedPart
        } else {
          Nil
        }
    }
  }
}
