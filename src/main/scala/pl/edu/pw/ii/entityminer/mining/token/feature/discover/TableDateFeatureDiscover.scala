package pl.edu.pw.ii.entityminer.mining.token.feature.discover

import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.crawler.dom.DomParser
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import org.jsoup.nodes.{TextNode, Node}
import scala.collection.mutable.ListBuffer
import pl.edu.pw.ii.entityminer.crawler.HtmlConsts
import pl.edu.pw.ii.entityminer.database.datasource.annotation.AnnotateStrategy

/**
 * Created by Raphael Hazan on 2/26/14.
 */
class TableDateFeatureDiscover(featureBuffer: ListBuffer[String], domParser: DomParser, tokenizer: AnnotateStrategy) extends FeatureDiscover {
  private val tokenTypeDiscover = new TokenTypeFeatureDiscover
  private val clueWords = Set(".")
  private val Colon = ":"

  var prev1Token: List[WebToken] = Nil
  var prev1Parent: List[Node] = Nil

  override def discoverFeature(entity: List[WebToken]): List[String] = {

    if (featureBuffer.contains(TokenTypeFeatureDiscover.FEAT_DATE)) {
      if (entity.last.nextToken != null && entity.last.nextToken.text.contains(Colon)) {
        getNextTokensAsFeatures(entity.last.nextToken, 6)
      } else if (entity.head.prevToken != null && entity.head.prevToken.text.contains(Colon)) {
        getPrevTokensAsFeatures(entity.head.prevToken, 6)
      } else {
        domParser.findParentParagraph(entity.head.node) match {
          case Some(parentParagraph) if isKindOfTableAndNotTooLong(parentParagraph) =>

            getPrevTokensAsFeatures(entity.head.prevToken, 6, Some(parentParagraph)) ::: getNextTokensAsFeatures(entity.last.nextToken, 6, Some(parentParagraph))
          case _ =>
            getPrevTokensAsFeatures(entity.head.prevToken, 6)
        }
      }
    } else {
      Nil
    }
  }


  // make sentence break for li elements
  // add erased feature if date is skreslona <- problably not needed

  //1 if after date is : (can be in different sentence) then take after it few words or untill not end of sentence
  //2 if before date is : (can be in different sentence) then take before it few words or untill not end of sentence
  //3 if parent is li then all li content
  //4 if parent is tr then all tr content
  //5 if nothing happened to date then check last few words from same sentence before until not one of key words occured (submission, final, deadline, notification)
  //5 if sentence was not long enough then take words from parent

  override def getPrefixes(): Set[String] = Set(Conf.PREFIX_FEAT_TABLE_TEXT)

  def isKindOfTableAndNotTooLong(parentParagraph: Node): Boolean = {
    (parentParagraph.nodeName() == HtmlConsts.Li ||
      parentParagraph.nodeName() == HtmlConsts.Tr) && {

      val parentIter = domParser.getDfsIterator(parentParagraph)
      var ok = true
      var wordsCounter = 0
      while (ok) {
        parentIter.next() match {
          case None =>
            ok = false
          case Some(node: TextNode) =>
            wordsCounter += tokenizer.tokenize(node.text(), node).size
          case _ =>
        }
      }
      wordsCounter <= 100
    }
  }

  private def getNextTokensAsFeatures(webToken: WebToken, howMany: Int, providedLeadingParent: Option[Node] = None): List[String] = {
    if (webToken != null) {
      var tmp: List[String] = Nil
      val sameParentForAll = providedLeadingParent match {
        case None => domParser.findParentParagraph(webToken.node)
        case node => node
      }
      val leadingSentence = webToken.sentence
      var nextToken: WebToken = webToken
      while (tmp.size < howMany) {
        if (nextToken == null || this.tokenNotForThisEntity(nextToken, sameParentForAll, leadingSentence)) {
          return tmp
        }
        if (this.tokenLastForThisEntity(nextToken)) {
          return this.createFeature(nextToken) ::: tmp
        }
        tmp = this.createFeature(nextToken) ::: tmp
        nextToken = nextToken.nextToken
      }
      tmp
    } else {
      Nil
    }
  }

  private def getPrevTokensAsFeatures(webToken: WebToken, howMany: Int, providedLeadingParent: Option[Node] = None): List[String] = {
    if (webToken != null) {
      var tmp: List[String] = Nil
      val sameParentForAll = providedLeadingParent match {
        case None => domParser.findParentParagraph(webToken.node)
        case node => node
      }
      val leadingSentence = webToken.sentence
      var prevToken: WebToken = webToken
      while (tmp.size < howMany) {
        if (prevToken == null || this.tokenNotForThisEntity(prevToken, sameParentForAll, leadingSentence)) {
          return tmp
        }
        if (this.tokenLastForThisEntity(prevToken)) {
          return this.createFeature(prevToken) ::: tmp
        }
        tmp = this.createFeature(prevToken) ::: tmp
        prevToken = prevToken.prevToken
      }
      tmp.filter(_.size > 2)
    } else {
      Nil
    }
  }

  private def createFeature(token: WebToken) = {
    if (token.text.toLowerCase.trim.size > 2 && token.text.toLowerCase.trim.forall(_.isLetter)) {
      List(TableDateFeatureDiscover.LABEL_FEAT + token.text.toLowerCase.trim)
    } else {
      Nil
    }
  }

  private def tokenNotForThisEntity(token: WebToken, leadingParent: Option[Node], leadingSentence: List[List[WebToken]]) = {
    (!tokenTypeDiscover.discoverFeature(token.tokenEntity).contains(TokenTypeFeatureDiscover.FEAT_STANDARD) &&
      !tokenTypeDiscover.discoverFeature(token.tokenEntity).contains(TokenTypeFeatureDiscover.FEAT_TERM) ) ||
      domParser.findParentParagraph(token.node) != leadingParent ||
      token.sentence.head.head != leadingSentence.head.head
  }

  private def tokenLastForThisEntity(token: WebToken) = {
    this.clueWords.contains(token.text.trim)
  }

  def discoverFeature(webToken: WebToken) = {
    Nil
  }
}

object TableDateFeatureDiscover {
  val LABEL_FEAT = Conf.PREFIX_FEAT_TABLE_TEXT
}