package pl.edu.pw.ii.entityminer

import gate._
import java.io.{OutputStreamWriter, BufferedOutputStream, FileOutputStream, File}
import pl.edu.pw.ii.entityminer.mining.token.feature.SequenceFeatureTokenizer

import collection.JavaConversions._
import org.springframework.context.support.{ClassPathXmlApplicationContext, AbstractApplicationContext}
import pl.edu.pw.ii.entityminer.database.service.MongoAccessService
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.{ConferenceLabelDomain, LabeledTreeNerTag}
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import cc.factorie.variable.HammingObjective
import weka.core.stemmers.SnowballStemmer
import cc.factorie.app.strings.PorterStemmer
import cc.factorie.util.BinarySerializer
import cc.factorie.app.chain.ChainModel
import cc.factorie.tutorial.ForwardBackwardPOS.PosModel
import cc.factorie.model.{Statistics, TemplateModel, Parameters}
import pl.edu.pw.ii.entityminer.mining.factorie.LinearCrf
import pl.edu.pw.ii.entityminer.mining.train.{SvmLiblinearTrainer, SvmRbfTrainer, SvmLinearClassifier}
import pl.edu.pw.ii.entityminer.mining.factorie.TreeLabelCrfModel.{TokenFeaturesDomain, TokenFeatures}
import java.util.Date
import cc.factorie.la

object Main {
  def main(args: Array[String]) = {
    val applicationContext: AbstractApplicationContext = new ClassPathXmlApplicationContext("META-INF/spring/module-context.xml")
    val rootFacade = applicationContext.getBean("RootFacade", classOf[RootFacade])

//    val inferredData = rootFacade.conferenceInferencer.inference("http://grammars.grlmc.com/lata2015/")
    val inferredData = rootFacade.conferenceInferencer.inference("http://www.icaisc.eu")///"http://www.icce-2016.org/")

    println(".")
    println(".")
    println(".")
    println("Znalezione informacje:")
    println(inferredData)
    println("done")
  }

  def main2(args: Array[String]): Unit = {
    val applicationContext: AbstractApplicationContext = new ClassPathXmlApplicationContext("META-INF/spring/module-context.xml")
    val rootFacade = applicationContext.getBean("RootFacade", classOf[RootFacade])
    //        val mongodb: MongoAccessService = applicationContext.getBean("mongoAccessService", classOf[MongoAccessService])
    //        mongodb.doSmth()

    //    val firstInferredDoc = 560
    val trainInTen = 7
    val maxValue = 999

    //    download conference web pages from web
    //        rootFacade.scrapConferences(110)

    //    save manifests in more readable format than binary
    //      rootFacade.saveManifestsInXml(Conf.DATA_STORAGE, maxValue)
    //    delete resources speed up copying fresh pages
    //      rootFacade.deleteWebPageResources(Conf.DATA_STORAGE, maxValue)

    //    some bug fix
    //    rootFacade.addIndexToManifestIfNotAdded(Conf.DATA_STORAGE, 1010)
    //    annotates conference pages with wikicfp info
    //    rootFacade.annotateConferencesWithLabels(Conf.DATA_STORAGE, Conf.DATA_STORAGE_ANNO, maxValue)
    //    rootFacade.removePagesWithoutAnnotations(Conf.DATA_STORAGE_ANNO, maxValue)

    rootFacade.createAndSaveFeatures(Conf.DATA_STORAGE_ANNO, Conf.getFeatureStorage, maxValue)
    if (Conf.IOB_MODE) rootFacade.convertFeaturesToIOB(Conf.FEATURES_STORAGE_SINGLE, Conf.FEATURES_STORAGE_IOB, maxValue)
    if (Conf.STOP_WORDS_MODE || Conf.STEM) rootFacade.extractStopWords(Conf.getFeatureStorage)

    val (trainData, testData) = rootFacade.readTrainTestData(Conf.getFeatureStoragePrepared, maxValue, trainInTen)
    val model = rootFacade.trainLinearCrfModel(trainData) // zmiana modelu
    TokenFeaturesDomain.freeze()
    rootFacade.saveModel(model, if (Conf.LINEAR_MODEL) if (Conf.IOB_MODE) Conf.LINEAR_MODEL_IOB_PATH else Conf.LINEAR_MODEL_PATH else null)
//        rootFacade.printData(trainData ++ testData) // zmiana modelu

    //     or if model trained, then just load it from file
//        val model = if (Conf.LINEAR_MODEL) new LinearCrf else null
//        rootFacade.loadModel(model, if (Conf.LINEAR_MODEL) if (Conf.IOB_MODE) Conf.LINEAR_MODEL_IOB_PATH else Conf.LINEAR_MODEL_PATH else null)

    //    val svmModel = rootFacade.trainSvm(trainData)
    //    rootFacade.saveSvmModel(Conf.SVM_MODEL_PATH, svmModel)
    //    rootFacade.inferWithSvm(testData, new SvmLinearClassifier(svmModel.model))
    //    val svmModel = rootFacade.loadSvmModel(Conf.SVM_MODEL_PATH, trainData.head.tokens.head.attr[TokenFeatures].domain.dimensionSize)
    //    rootFacade.inferWithSvm(testData, new SvmLinearClassifier(svmModel))
    //    val model: TemplateModel = null


    //    val svmRbf = new SvmRbfTrainer
    //    svmRbf.train(trainData)
    //    val inferredDos = rootFacade.inferDocsSvmNotFactorie(svmRbf, testData)


    //    val svmLiblinear = new SvmLiblinearTrainer
    //    svmLiblinear.train(trainData)
    //    val inferredDos = rootFacade.inferDocsSvmNotFactorie(svmLiblinear, testData)

    //
    //    for tests
    //    println(SequenceFeatureTokenizer.tableMap)
    //    for (v <- ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.values) {
    //      println(ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.values.map{vv => v + "|" + vv})
    //    }

    //    val inferencedDos = rootFacade.inferDocs(model, testData.subList(0, 1))
    //    val inferredDos = rootFacade.inferDocsWithSimpleClassifiers(model, testData)
    val inferredDos = rootFacade.inferDocs(model, testData)
    rootFacade.checkAccuracy(model, testData, trainInTen)



    //    rootFacade.makeStatistics(Conf.DATA_STORAGE_ANNO, maxValue)

    //    applicationContext.registerShutdownHook
    println("\n\ndone at " + (new Date()))

    //    showAnnotations("data/Cumulus Dublin 2013 - call for papers.anno.xml")
    //    automateAnnotate("data/Cumulus Dublin 2013 - main.htm")
  }

  private def printWeights(model: TemplateModel) = {
    val labelWeights = new la.DenseTensor2(ConferenceLabelDomain.size, ConferenceLabelDomain.size)
    //    val weights = model.templates.head.
    for (cat1 <- ConferenceLabelDomain.categories) {
      for (cat2 <- ConferenceLabelDomain.categories) {

      }
    }
  }

  /**
   * Czytanie annotacji z pliku z annotacjami
   * @param filePath
   */
  def showAnnotations(filePath: String) = {
    val file = new File(filePath)
    println(file.toURI.toURL)
    val document = Factory.newDocument(file.toURI.toURL, "UTF-8") // dokument otagggwany
    val annotationSet = document.getAnnotations // annotacje zdefiniowane przez uzytkownika

    val annotTypesRequired = new java.util.HashSet[String]()
    annotTypesRequired.add("Submission Deadline") // filtr annotacji, ktore chcemy wyswietlic
    val annos = new java.util.HashSet[Annotation](annotationSet.get(annotTypesRequired))
    //    val annos = new util.HashSet[Annotation](annotationSet.get()); // mozemy takze wyswietlic wszystkie annotacje
    annos.foreach {
      annot =>
        println("wartosc kolejnej annotacji " + Utils.contentFor(document, annot))
    }
  }

  /**
   * Annotowanie programowe
   * @param filePath
   */
  def automateAnnotate(filePath: String) = {
    val file = new File(filePath)
    println(file.toURI.toURL)
    val document = Factory.newDocument(file.toURI.toURL, "UTF-8")
    println("fast info " + " " + document.getCollectRepositioningInfo + " " + document.getMarkupAware + " " + document.getPreserveOriginalContent + " " + document.getSourceUrl)
    val annotationSet = document.getAnnotations

    // na pewno sa lepsze metody szukania i analizy dokumentu
    val whenInfo = "7-9 November 2013"
    var idx0 = document.getContent.toString.indexOf(whenInfo)
    annotationSet.add(idx0.toLong, (idx0 + whenInfo.size).toLong, "When", null)

    //zapis do pliku
    val outputFileName = document.getName + ".anno.xml"
    val outputFile = new File(file.getParentFile, outputFileName)
    val fos = new FileOutputStream(outputFile)
    val bos = new BufferedOutputStream(fos)
    val out = new OutputStreamWriter(bos, "UTF-8")
    out.write(document.toXml)
    out.close()
  }

}