package pl.edu.pw.ii.entityminer.mining.factorie

import cc.factorie.app.nlp.Document
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.{ConferenceLabelDomain, TreeNerTag, LabeledTreeNerTag}
import cc.factorie.optimize.{Trainer, LikelihoodExample}
import cc.factorie.infer.{InferByBPLoopyTreewise, MaximizeByBPLoopy, InferByBPLoopy, InferByBPChain}
import cc.factorie.variable.HammingObjective
import cc.factorie.model.{DotTemplateWithStatistics2, DotTemplateWithStatistics1, TemplateModel}
import cc.factorie._
import pl.edu.pw.ii.entityminer.mining.factorie.TreeLabelCrfModel.{TokenFeaturesDomain, TokenFeatures}
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired
import pl.edu.pw.ii.entityminer.mining.token.feature.FeatureCorrector
import java.util.Date
import pl.edu.pw.ii.entityminer.Conf

/**
 * Created by Raphael Hazan on 12/31/13.
 */
@Service("LinearCrfTrainer")
class LinearCrfTrainer extends ModelTrainer {

  @Autowired
  private var featureCorrector: FeatureCorrector = _

  val model = new LinearCrf

  private val dataDivideRatio = 0.7

  def train(trainTestData: Seq[Document]) = {
    val (trainData, testData) = trainTestData.splitAt(1000)//(dataDivideRatio * trainTestData.size).toInt)

    val trainLabelsSentences: Seq[Seq[LabeledTreeNerTag]] = trainData.map(_.tokens.toSeq.map(_.attr[LabeledTreeNerTag]))
//    val testLabelsSentences: Seq[Seq[LabeledTreeNerTag]] = testData.map(_.tokens.toSeq.map(_.attr[LabeledTreeNerTag]))

//    println("*** Starting training (#sentences=%d)".format(trainData.map(_.sentences.size).sum))
    val start = System.currentTimeMillis

    implicit val random = new scala.util.Random((new Date()).getTime)
    val examples = if (Conf.SKIP_CHAIN) {
      trainLabelsSentences.map(s => new LikelihoodExample(s, model, InferByBPLoopy))
    } else {
      trainLabelsSentences.map(s => new LikelihoodExample(s, model, InferByBPChain))
    }
    Trainer.onlineTrain(model.parameters, examples)//, () => (), true, 8)
//    println("*** Starting inference (#sentences=%d)".format(testData.map(_.sentences.size).sum))
//    testLabelsSentences.foreach {
//      variables => cc.factorie.infer.BP.inferChainMax(variables, model).setToMaximize(null)
//    }

    println("Total training took " + (System.currentTimeMillis - start) / 1000.0 + " seconds")

    model
  }
}
