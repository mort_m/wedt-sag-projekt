package pl.edu.pw.ii.entityminer.mining.token.feature.discover

import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import pl.edu.pw.ii.entityminer.database.model.token.WebToken

/**
 * Created by Raphael Hazan on 6/28/2014.
 */
class RealClassDiscover extends FeatureDiscover {
  override def discoverFeature(webToken: WebToken): List[String] = discoverFeature(List(webToken))

  override def discoverFeature(entity: List[WebToken]): List[String] = {
    List(Conf.PREFIX_FEAT_REAL_CLASS + getEntityLabel(entity))

//    if (getEntityLabel(entity) == "ABBRE") {
//      List("L=" + getEntityLabel(entity))
//    } else {
//      List("N")
//    }
  }

  private def getEntityLabel(entity: List[WebToken]): String = {
    val possibleLabels = entity.map(_.getLabel)

    val longDiffs = entity.filter(a => a.text.size > 4 && a.getLabel == ConferenceHtmlAnnotation.OTHER_LABEL)
    //    if (longDiffs.nonEmpty && possibleLabels.toSet.size > 1) {
    //      println("sygnatura " + longDiffs.map(_.text) + " | " + entity.map(_.text))
    //    }

    if (possibleLabels.toSet.size == 1) {
      possibleLabels.head
    } else {
      possibleLabels.filterNot(_.toUpperCase == ConferenceHtmlAnnotation.OTHER_LABEL).groupBy(identity).maxBy(_._2.size)._1
    }
  }

  override def getPrefixes(): Set[String] = Set(Conf.PREFIX_FEAT_REAL_CLASS)
}
