package pl.edu.pw.ii.entityminer.mining.inference.basic

import cc.factorie.app.nlp
import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP
import pl.edu.pw.ii.entityminer.mining.factorie.TreeLabelCrfModel.TokenFeatures
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.{ConferenceLabelDomain, LabeledTreeNerTag}
import pl.edu.pw.ii.entityminer.mining.token.feature.discover.TokenTypeFeatureDiscover

/**
 * Created by Raphael Hazan on 5/12/2014.
 */

class BasicNotificationInferencer extends BasicInferencer {

  def inference(document: nlp.Document): nlp.Document = {
    for (token <- document.tokens) {
      val features = token.attr[TokenFeatures]
      if (features.activeCategories.contains(TokenTypeFeatureDiscover.FEAT_DATE) &&
        features.activeCategories.contains(Conf.PREFIX_FEAT_TABLE_TEXT + "notification")) {

        token.attr[LabeledTreeNerTag] := ConferenceLabelDomain.value(ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.NOTIFICATION).get)
        return document
      }
    }
    
    document
  }
}
