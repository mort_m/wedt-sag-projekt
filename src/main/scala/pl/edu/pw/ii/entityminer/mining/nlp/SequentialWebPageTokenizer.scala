package pl.edu.pw.ii.entityminer.mining.nlp

import org.jsoup.nodes.{TextNode, Document}
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import org.springframework.stereotype.Service
import scala.collection.mutable.ListBuffer
import pl.edu.pw.ii.entityminer.mining.token.feature.StopWords
import org.springframework.beans.factory.annotation.Autowired
import pl.edu.pw.ii.entityminer.crawler.dom.DomParser
import scala.text

/**
 * Created by Raphael Hazan on 17.01.14.
 */
@Service("WebPageTokenizer")
class SequentialWebPageTokenizer extends WebPageTokenizer {

  @Autowired
  private var domParser: DomParser = _

  def tokenize(document: Document): List[WebToken] = {
    val result = ListBuffer[WebToken]()
    var loop = true
    val iter = domParser.getDfsIterator(document)
    var section = 0
    while (loop) {
      iter.next() match {
        case None => loop = false
        case Some(node) =>
          if (node.isInstanceOf[TextNode]) {
            tokenizeText(node.asInstanceOf[TextNode], section, result)
            section += 1 //TODO sekcje wg. divuw a nie wszystkich tagow nie tekstowych
          }
      }
    }
    result.result
  }

  private def tokenizeText(textNode: TextNode, section: Int, buffer: ListBuffer[WebToken]) = {
    var currentPos = 0
    while (currentPos < textNode.text.size) {
      val currentToken = textNode.text.substring(currentPos).takeWhile(_.isLetterOrDigit)
      if (currentToken.size > 0 &&
        !StopWords.StopWords.contains(currentToken.toLowerCase())) {
        buffer += WebToken(currentToken.toLowerCase(), currentPos, currentPos + currentToken.size, textNode, section)
      }
      currentPos += currentToken.size + 1
    }
  }
}
