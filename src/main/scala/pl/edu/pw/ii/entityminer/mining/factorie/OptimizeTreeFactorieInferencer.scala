package pl.edu.pw.ii.entityminer.mining.factorie

import cc.factorie.app.nlp.Document
import cc.factorie.model.TemplateModel
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.LabeledTreeNerTag
import org.springframework.stereotype.Service
import cc.factorie.infer.MaximizeByBPLoopy
import pl.edu.pw.ii.entityminer.Conf

/**
 * Created by Raphael Hazan on 1/2/14.
 */

class OptimizeTreeFactorieInferencer extends FactorieInferencer {
  def inference(document: Document, model: TemplateModel, year: String): Document = {
    val docSentences: Seq[LabeledTreeNerTag] = document.tokens.toSeq.map(_.attr[LabeledTreeNerTag])

    if (Conf.SKIP_CHAIN) {
      // skip chain
      try {
        MaximizeByBPLoopy.infer(docSentences, model).setToMaximize(null)
      }
      catch{
        case oome: OutOfMemoryError => /*sweet ignore*/
      }
//      cc.factorie.infer.BP.inferLoopyTreewiseMax(docSentences, model).setToMaximize(null)
//      cc.factorie.infer.BP.inferLoopy(summary)
      //      cc.factorie.infer.BP.inferLoopyTreewise(docSentences, model).setToMaximize(null)
    } else {
      // linear chain
      cc.factorie.infer.BP.inferChainMax(docSentences, model).setToMaximize(null)
    }
    document
  }

  override def train(trainSet: Seq[Document]): Unit = ???
}
