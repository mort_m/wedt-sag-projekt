package pl.edu.pw.ii.entityminer.mining.factorie

import cc.factorie.model.TemplateModel
import cc.factorie.app.nlp

/**
 * Created by Raphael Hazan on 1/2/14.
 */
trait FactorieInferencer {
  def inference(document: nlp.Document, model: TemplateModel, year: String): nlp.Document
  def train(trainSet: Seq[nlp.Document])
}
