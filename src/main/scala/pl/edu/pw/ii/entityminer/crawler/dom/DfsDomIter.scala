package pl.edu.pw.ii.entityminer.crawler.dom

import org.w3c.dom.{Text, Document, Node}

/**
 * Created by Raphael Hazan on 28.12.13.
 */
class DfsDomIter(val document: Document) extends DomIter {
  val stack = new collection.mutable.Stack[Node]()
  stack.push(document.getDocumentElement)

  def next() = {
    if (stack.nonEmpty) {
      val iter = stack.pop()
      val childNo = iter.getChildNodes.getLength
      for (childIndex <- childNo - 1 to 0 by -1) {
        stack.push(iter.getChildNodes.item(childIndex))
      }
      Some(iter)
    } else {
      None
    }
  }
}
