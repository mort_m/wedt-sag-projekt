package pl.edu.pw.ii.entityminer.mining.token.feature

import pl.edu.pw.ii.entityminer.crawler.dom.DomParser
import org.jsoup.nodes.{Node, Element, TextNode}
import org.springframework.beans.factory.annotation.Autowired
import pl.edu.pw.ii.entityminer.crawler.BoilerplateRemover
import cc.factorie.app.nlp
import cc.factorie.app.nlp._
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import scala.Some
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import cc.factorie.app.nlp.pos.PennPosTag
import scala.collection.mutable.ListBuffer
import org.springframework.stereotype.Service
import pl.edu.pw.ii.entityminer.mining.token.feature.discover.GateFeatureDiscover
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryManifest.ManifestEntry

/**
 * Created by Raphael Hazan on 2/4/14.
 */
@Service("DivisionTreeTokenizer")
class DivisionTreeTokenizer extends DomTokenizer {

  override def tokenizeDocuments(documents: List[(ManifestEntry, org.jsoup.nodes.Document)], singleEntities: Boolean): List[HeavyFeatureSet] = {
//    val dfsIter = domParser.getDfsIterator(document)
//
//    var paragraphNode: Option[Node] = None
//    val textNodesInParagraph = ListBuffer[TextNode]()
//    val resultFeatures = ListBuffer[HeavyFeatureSet]()
//
//    while (dfsIter.next() match {
//      case Some(node) =>
//        if (node.isInstanceOf[TextNode] &&
//          node.asInstanceOf[TextNode].text().trim.nonEmpty) {
//
//          paragraphNode match {
//            case None =>
//              paragraphNode = domParser.findParentParagraph(node)
//              if (paragraphNode.isDefined) {
//                addFeaturesFromParagraph(textNodesInParagraph.result(), None, resultFeatures)
//                textNodesInParagraph.clear()
//              }
//            case Some(leadingParentNode) =>
//              domParser.findParentParagraph(node) match {
//                case None =>
//                  addFeaturesFromParagraph(textNodesInParagraph.result(), paragraphNode, resultFeatures)
//                  paragraphNode = None
//                  textNodesInParagraph.clear
//                case Some(currentParent) =>
//                  if (currentParent != leadingParentNode) {
//                    addFeaturesFromParagraph(textNodesInParagraph.result(), paragraphNode, resultFeatures)
//                    paragraphNode = Some(currentParent)
//                    textNodesInParagraph.clear()
//                  }
//              }
//          }
//          textNodesInParagraph += node.asInstanceOf[TextNode]
//        }
//        true
//      case None =>
//        false
//    }) {}
//    if (textNodesInParagraph.nonEmpty) {
//      addFeaturesFromParagraph(textNodesInParagraph.result(), paragraphNode, resultFeatures)
//    }
//
//    val rootNode = HeavyFeatureSet (
//      Nil,
//      List(ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP(ConferenceHtmlAnnotation.PARAGRAPH)),
//      List(
//        StopWords.FeaturePosNA,
//        StopWords.FeatureWord + StopWords.FeatureNA,
//        StopWords.FeatureParagraph + StopWords.FeatureNA,
//        StopWords.FeatureShape + StopWords.FeatureNA
//      ),
//      null,
//      resultFeatures.result
//    )
//    resultFeatures.prepend(rootNode)
//    resultFeatures.result()
    null //TODO if use tree
  }

  private def addFeaturesFromParagraph( nodes: List[TextNode], parent: Option[Node], resultFeatures: ListBuffer[HeavyFeatureSet]) = {
    if (nodes.nonEmpty) {
      val children = ListBuffer[HeavyFeatureSet]()

      val nlpDocument = new nlp.Document("").setName("tempName")
      nlpDocument.annotators(classOf[Token]) = UnknownDocumentAnnotator.getClass // register that we have token boundaries
      nlpDocument.annotators(classOf[Sentence]) = UnknownDocumentAnnotator.getClass // register that we have sentence boundaries
      val sentence = new Sentence(nlpDocument)

      val webTokens = ListBuffer[WebToken]()
      nodes.foreach { textNode =>
        val text = textNode.text()
        var currentPos = 0
        while (currentPos < text.size) {
          val currentToken = text.substring(currentPos).takeWhile(_.isLetterOrDigit)
          if (currentToken.size > 1 &&
            !stopWords.contains(currentToken.toLowerCase()) &&
            !currentToken.matches(".*\\d.*")) {
            val webToken = WebToken(currentToken, currentPos, currentPos + currentToken.size, textNode)
            webToken.nlpToken = new Token(sentence, currentToken.toLowerCase())

            webTokens += webToken
          }
          currentPos += currentToken.size + 1
        }
      }
      DocumentAnnotatorPipeline(DocumentAnnotatorPipeline.defaultDocumentAnnotationMap, Nil, List(classOf[pos.PennPosTag])).process(nlpDocument)
      webTokens.foreach { webToken =>
        children += createHeavyFeature(webToken)
      }

      parent match {
        case None =>
        case Some(parentNode) =>
          val parentLabel = ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.getOrElse(parentNode.parent().nodeName(),
            ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP(ConferenceHtmlAnnotation.PARAGRAPH))
          resultFeatures += HeavyFeatureSet (
            Nil,
            List(parentLabel),
            List(
              StopWords.FeaturePosNA,
              StopWords.FeatureWord + parentNode.nodeName(),
              StopWords.FeatureParagraph + "true",
              StopWords.FeatureShape + StopWords.FeatureNA
            ),
            null,
            children.result
          )
      }
      resultFeatures ++= children.result
    }
  }


  private def createHeavyFeature(webToken: WebToken) = {
    val label =
      webToken.node.parent match {
        case null => ConferenceHtmlAnnotation.OTHER_LABEL
        case parent =>
          ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(parent.nodeName().toLowerCase) match {
            case None => ConferenceHtmlAnnotation.OTHER_LABEL
            case Some(label) => label
          }
      }
    HeavyFeatureSet(List(webToken),
      List(label),
      List(
        webToken.nlpToken.attr[PennPosTag].categoryValue,
        StopWords.FeatureWord + webToken.text.toLowerCase(),
        StopWords.FeatureParagraph + "false",
        StopWords.FeatureShape + cc.factorie.app.strings.stringShape(webToken.text, 2)
      ))
  }
}
