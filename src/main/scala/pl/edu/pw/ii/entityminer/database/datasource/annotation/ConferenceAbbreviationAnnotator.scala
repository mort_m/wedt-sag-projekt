package pl.edu.pw.ii.entityminer.database.datasource.annotation

import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.mining.token.feature.StopWords
import org.jsoup.nodes.TextNode
import scala.collection.mutable.ListBuffer
import org.springframework.stereotype.Service

/**
 * Created by Raphael Hazan on 1/13/14.
 */
class ConferenceAbbreviationAnnotator extends AnnotateStrategy {
  val annotationTagName: String = ConferenceHtmlAnnotation.ABBREVIATION

  def getValueTokens(conferenceData: ConferenceData): List[List[String]] = {
    List(
      conferenceData.nameAbbreviation.split("\\s+").map(_.trim.replaceAll("[(),]", "")).
        filterNot(StopWords.StopWords.contains(_)).filterNot(_.matches("\\d+")).toList.map(_.trim).filter(_.filter(_.isUpper).size > 1).map(_.toLowerCase)
    )
  }

  def tokenize(text: String, node: TextNode): List[WebToken] = {
    val result = ListBuffer[WebToken]()
    var currentPos = 0
    while (currentPos < text.size) {
      val currentToken = text.substring(currentPos).takeWhile(_.isLetterOrDigit)
      if (currentToken.size > 0 &&
        !StopWords.StopWords.contains(currentToken.toLowerCase())) {
        result += WebToken(currentToken.toLowerCase(), currentPos, currentPos + currentToken.size, node)
      }
      currentPos += currentToken.size + 1
    }
    result.result
  }

  /**
   *
   * @param tokens
   * @param seq
   * @return (occurence tokens, rest tokens not analyzed yet)
   */
  override def findFirstOccurence(tokens: List[WebToken], seq: List[String]): (List[WebToken], List[WebToken]) = {
    for (i <- 0 to tokens.size - 1) {
      if (seq.contains(tokens(i).text.toLowerCase().trim)) {
        return (List(tokens(i)), tokens.takeRight(tokens.size - 1 - i))
      }
    }
    (Nil, Nil)
  }
}
