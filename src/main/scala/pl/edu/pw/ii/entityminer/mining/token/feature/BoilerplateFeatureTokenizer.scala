package pl.edu.pw.ii.entityminer.mining.token.feature

import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired
import pl.edu.pw.ii.entityminer.crawler.BoilerplateRemover
import pl.edu.pw.ii.entityminer.crawler.dom.DomParser
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryManifest._

/**
 * Created by Raphael Hazan on 1/21/14.
 */
trait BoilerplateFeatureTokenizer extends FeatureTokenizer {

  protected var boilerplateRemover: BoilerplateRemover

  def tokenizeFromFiles(manifestEntries: List[(String, ManifestEntry)], singleEntities: Boolean): List[HeavyFeatureSet] = {
    val documents = manifestEntries.map {
      case (documentPath, entry) =>
        println("[DomTokenizer] tokenize: " + documentPath)
        val cleanedPage = boilerplateRemover.removeBoilerplate(documentPath)
        (entry, cleanedPage)
    }
    this.tokenizeDocuments(documents, singleEntities)
  }
}
