package pl.edu.pw.ii.entityminer.crawler.dynamicpage

import com.gargoylesoftware.htmlunit._
import com.gargoylesoftware.htmlunit.html.{HtmlElement, HtmlPage}
import scala.util.control.Breaks._
import scala.collection.JavaConversions._
import org.apache.http.impl.client.AbstractHttpClient
import org.springframework.stereotype.Service

/**
 * Created with IntelliJ IDEA.
 * User: ralpher
 * Date: 2/16/13
 * Time: 5:39 PM
 * To change this template use File | Settings | File Templates.
 */
@Service("HtmlUnitSpider")
class HtmlUnitSpider {
  private val webClient = new WebClient(BrowserVersion.FIREFOX_24)

  def getPageOmmitScriptsWaitForJs(url: String, waitMs: Int = 10000): HtmlPage = {
    prepareWebClient
   // new ScriptOmmitingConnectionWrapper(webClient)
    val page: HtmlPage = webClient.getPage(url)
    webClient.waitForBackgroundJavaScript(waitMs);

    if(page.getTitleText.contains("404 Not Found"))
      null
    else
      page
  }

  def getPageUntilXpathNotEmpty(url: String, xpath: String): HtmlPage = {
    prepareWebClient

    val page: HtmlPage = webClient.getPage(url)
    waitUptillPageNotLoaded(page, xpath)
  }


  private def prepareWebClient {
    webClient.getOptions().setThrowExceptionOnScriptError(false)
    webClient.getOptions.setJavaScriptEnabled(true)
    webClient.getOptions.setCssEnabled(true)
    webClient.getOptions.setRedirectEnabled(true)
    webClient.getOptions.setUseInsecureSSL(true)
    webClient.getOptions.setThrowExceptionOnFailingStatusCode(false)
    webClient.getOptions.setHomePage("www.google.com")
    webClient.getOptions.setTimeout(20000)
//    webClient.setWebConnection(new HttpWebConnection(webClient) {
//      override def getHttpClientBuilder() = {
//        val  client = super.getHttpClientBuilder()
        //Sets the socket timeout (SO_TIMEOUT) in milliseconds to
        //be used when executing the method.
        //A timeout value of zero is interpreted as an infinite timeout.
        //Time that a read operation will block for, before generating
        //an java.io.InterruptedIOException
//        client.getParams().setParameter("http.socket.timeout", 20000);
        //The timeout in milliseconds used when retrieving an
        // HTTP connection from the HTTP connection manager.
        // Zero means to wait indefinitely.
//        client.getParams().setParameter("http.connection-manager.timeout", 20000);
//        client.getParams().setParameter("http.tcp.nodelay", true);
//        client
//      }
//    })
  }

  def clickAndWaitUptill(page: HtmlPage, elementToClick: HtmlElement, condXpath: String): HtmlPage = {
    waitUptillPageNotLoaded(elementToClick.click().asInstanceOf[HtmlPage], condXpath)
  }

  private def waitUptillPageNotLoaded(page: HtmlPage, condXpath: String): HtmlPage = {
    breakable {
      for (i <- 0 to 20) {
        webClient.waitForBackgroundJavaScript(500 + scala.util.Random.nextInt(1000));
        try {
          if (page.getFirstByXPath(condXpath) != null) {
            break
          }
        } catch {
          case e: RuntimeException => println("[HtmlUnitSpider.waitUptillPageNotLoaded] RuntimeException")
        }
      }
    }
    page
  }

  def waitTime(time: Int) = webClient.waitForBackgroundJavaScript(time)

  def goBack() = {
    webClient.getWebWindows.toList.foreach(_.getHistory.back())
    webClient.waitForBackgroundJavaScript(5000);
  }

  def closeAllWindows() = webClient.closeAllWindows();
}
