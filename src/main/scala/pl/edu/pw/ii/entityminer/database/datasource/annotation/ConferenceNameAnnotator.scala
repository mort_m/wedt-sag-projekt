package pl.edu.pw.ii.entityminer.database.datasource.annotation

import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.mining.token.feature.StopWords
import pl.edu.pw.ii.entityminer.mining.token.feature.discover.TokenTypeFeatureDiscover

/**
 * Created by Raphael Hazan on 1/13/14.
 */
class ConferenceNameAnnotator extends AnnotateStrategy with AllSeparatelyTokenizer {
  val annotationTagName: String = ConferenceHtmlAnnotation.NAME
  private val maxGapBetweenNextKeywords = 2
  private val namesToOmmit = Set("international", "conference", "workshop", "the")
  private val maxGapTokenSize = 4

  private val featureDiscover = new TokenTypeFeatureDiscover

  /**
   *
   * @param tokens
   * @param seq
   * @return (occurence tokens, rest tokens not analyzed yet)
   */
  def findFirstOccurence(tokens: List[WebToken], seq: List[String]): (List[WebToken], List[WebToken]) = {
    val maxWordsOmmitAtBeggining = 0//Math.max(seq.size - 5, 0)
    var currOmmitStep = 1
    var result = findFirstOccurence(tokens, seq, 0, Nil)
    while (result._1.isEmpty && currOmmitStep < maxWordsOmmitAtBeggining && (seq.size - currOmmitStep) >= 3) {
      result = findFirstOccurence(tokens, seq.drop(currOmmitStep), 0, Nil)
      currOmmitStep += 1
    }
    result
  }

  //TODO fifth w nazwie powinno byc usuniete (974)973?
  private def findFirstOccurence(tokens: List[WebToken], seq: List[String], currentGap: Int,
                                 matchedPart: List[WebToken]): (List[WebToken], List[WebToken]) = {
    //    val testTmp = tokens.foldLeft(""){ case (str, t) => str + " " + t.originalText }.drop(1)
    //    testTmp.isEmpty
    if (seq.isEmpty) {
      (matchedPart, tokens)//TODO 64 reusable wher??
    } else {
      tokens match {
        //TODO consider usuwanie s na koncu slowa
        case Nil =>
          (Nil, Nil)
        case token :: rest if namesToOmmit.contains(token.text) =>
          findFirstOccurence(rest, seq, maxGapBetweenNextKeywords, matchedPart :+ token)
//        case _ if namesToOmmit.contains(seq.head) =>
//          findFirstOccurence(tokens, seq.tail, currentGap, matchedPart)
        case token :: rest if token.text == seq.head =>
          if (seq.size > 1) {
            val result = findFirstOccurence(rest, seq.tail, maxGapBetweenNextKeywords, matchedPart :+ token)
            if (result._1.isEmpty) {
              findFirstOccurence(rest, seq, 0, Nil)
            } else {
              result
            }
          } else {
            (matchedPart :+ token, rest)
          }
        case token :: rest if currentGap > 0 && token.text.size <= maxGapTokenSize =>
          findFirstOccurence(rest, seq, currentGap - 1, matchedPart :+ token)
        case token :: rest =>
          if (matchedPart.isEmpty) {
            findFirstOccurence(rest, seq, 0, Nil)
          } else {
            (Nil, Nil)
          }
      }
    }
  }

  def getValueTokens(conferenceData: ConferenceData): List[List[String]] = {
    if (conferenceData.names == null) {
      Nil
    } else {
      val abbreviation = conferenceData.nameAbbreviation.toLowerCase.filter(_.isLetter)

      conferenceData.names.map {
        conferenceAlias =>
          val conferenceTokens = conferenceAlias.replaceAll("-", " - ").split("\\s+").map(_.trim.replaceAll("[().,]", " ")).
            filterNot(StopWords.StopWords.contains(_)).
            filter(_.length > 1).
            filterNot(_.matches(".*\\d.*")).
            filterNot(_.toLowerCase.trim == abbreviation).toList

          if (conferenceTokens.filter(featureDiscover.isUpperKind(_)).size == 1) {
            conferenceTokens.filterNot(featureDiscover.isUpperKind(_))
          } else {
            conferenceTokens
          }
      }.filter(_.size > 1).
        map(_.filterNot(word => word.filter(_.isUpper).size > 1 && word.size < 5).map(_.toLowerCase.trim).filterNot(namesToOmmit.contains(_)))
    }
  }
}
