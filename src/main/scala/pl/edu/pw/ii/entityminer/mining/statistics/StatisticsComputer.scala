package pl.edu.pw.ii.entityminer.mining.statistics

import org.jsoup.nodes.Document
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryManifest.ManifestEntry

/**
 * Created by Raphael Hazan on 16.01.14.
 */
trait StatisticsComputer {
  def computeStatistics(conferenceData: ConferenceData, document: Document, manifestEntry: ManifestEntry, documentPath: String): DataStatistics
}