package pl.edu.pw.ii.entityminer.mining.token.feature.discover

import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import pl.edu.pw.ii.entityminer.mining.token.feature.FactorieLinearCrfFeatureCreator
import weka.core.stemmers.SnowballStemmer

import scala.collection.mutable.ListBuffer

/**
 * Created by Raphael Hazan on 7/12/2014.
 */
class StopWordsRemoveDiscover(featureBuffer: ListBuffer[String], uniqueEnabled: Boolean) extends FeatureDiscover {

  private val stemmer = new SnowballStemmer()

  override def discoverFeature(webToken: WebToken): List[String] = Nil

  override def getPrefixes(): Set[String] = Set()

  override def discoverFeature(entity: List[WebToken]): List[String] = {
    if (Conf.STOP_WORDS_MODE) {
      val isItStopWord = !featureBuffer.forall(feature => !(feature.startsWith(WordFeatureDiscover.LABEL_FEAT) && FactorieLinearCrfFeatureCreator.StopWords.contains(feature)))
      if (isItStopWord) {
        featureBuffer.clear()
      } else if (uniqueEnabled) {
        val isItOnlyWord = !featureBuffer.forall(feature => !(FactorieLinearCrfFeatureCreator.WordOccurances.contains(feature) &&
          FactorieLinearCrfFeatureCreator.WordOccurances(feature) == 1))
        if (isItOnlyWord) {
          featureBuffer.clear()
        }
      }

      val tmpBuffer = new ListBuffer[String]()
      for (feat <- featureBuffer) {
        if (feat.startsWith(TableDateFeatureDiscover.LABEL_FEAT)){
          val featTmpVal = WordFeatureDiscover.LABEL_FEAT + feat.substring(TableDateFeatureDiscover.LABEL_FEAT.size)
          val notStopWord = ! FactorieLinearCrfFeatureCreator.StopWords.contains(featTmpVal)
          if (notStopWord) {
            tmpBuffer += feat
          }
        } else {
          tmpBuffer += feat
        }
      }
      if (tmpBuffer.nonEmpty) {
        featureBuffer.clear()
        featureBuffer ++= tmpBuffer
      }
    }
    if(Conf.STEM) {
      val tmpBuffer = new ListBuffer[String]()
      for (feat <- featureBuffer) {
        if (feat.startsWith(WordFeatureDiscover.LABEL_FEAT)) {
//          if (FactorieLinearCrfFeatureCreator.ClassWords.contains(feat)) {
            tmpBuffer += WordFeatureDiscover.LABEL_FEAT + stemmer.stem(feat.substring(WordFeatureDiscover.LABEL_FEAT.size))
//          }
        } else {
          tmpBuffer += feat
        }
      }
      if (tmpBuffer.nonEmpty) {
        featureBuffer.clear()
        featureBuffer ++= tmpBuffer
      }
    }
    Nil
  }
}
