package pl.edu.pw.ii.entityminer.mining.factorie

import java.text.{DateFormat, ParsePosition, SimpleDateFormat}
import java.util.{Date, Locale}

import cc.factorie.app.nlp.Document
import cc.factorie.model.TemplateModel
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.LabeledTreeNerTag
import org.springframework.stereotype.Service
import cc.factorie.infer.{MaximizeByBPChain, MaximizeByBPLoopy}
import cc.factorie.variable.DiffList
import org.json4s.ParserUtil.ParseException
import pl.edu.pw.ii.entityminer.Conf

import scala.collection.mutable


/**
 * Created by Marta on 2016-06-07.
 */

@Service("FactorieInferencer")
class OptimizeChainFactorieInferencer extends FactorieInferencer {
    def inference(document: Document, model: TemplateModel, year: String): Document = {
        var docSentences: Seq[LabeledTreeNerTag] = document.tokens.toSeq.map(_.attr[LabeledTreeNerTag])
            try {
                //Viterbi here
                println("Viterbi")
                //Viterbi wybiera lancuch danych by CRF o najwiekszym prawdopodobienstwie
                MaximizeByBPChain.infer(docSentences, model).setToMaximize(null)

                /**
                  * Nałożone ograniczenia na lancuchy dat
                  */
                checkConstrains(year, docSentences)
            }
            catch{
                case oome: OutOfMemoryError => /*sweet ignore*/
            }

        document
    }

    def checkConstrains(year: String, docSentences: Seq[LabeledTreeNerTag]): Unit = {
        var whenDates = new mutable.MutableList[Date]()
        var submDates = new mutable.MutableList[Date]()
        var notfDates = new mutable.MutableList[Date]()
        var finvDates = new mutable.MutableList[Date]()
        docSentences.foreach(f => {
            if (f.token.containsDigit && f.token.attr[LabeledTreeNerTag].categoryValue.equals("WHEN")) {
                //println("TESTWHEN: " + f.token.string)
                constrain(f, year, finvDates, whenDates)
            }
            if (f.token.containsDigit && f.token.attr[LabeledTreeNerTag].categoryValue.equals("SUBM")) {
                //println("TESTSUBM: " + f.token.string)
                constrain(f, year, null, submDates)
            }
            if (f.token.containsDigit && f.token.attr[LabeledTreeNerTag].categoryValue.equals("NOTF")) {
                // println("TESTNOTF: " + f.token.string)
                constrain(f, year, submDates, notfDates)
            }
            if (f.token.containsDigit && f.token.attr[LabeledTreeNerTag].categoryValue.equals("FINV")) {
                // println("TESTFINV: " + f.token.string)
                finvDates += parseDate(f.token.string)
                constrain(f, year, notfDates, finvDates)
            }
        }
        )
    }

    def constrain(f: LabeledTreeNerTag, year: String, beforeDates: mutable.MutableList[Date], afterDays: mutable.MutableList[Date]): Unit ={
        var newAfterDate = parseDate(f.token.string)
        var ok = true
        if(Integer.parseInt(year)!=0){
            if(newAfterDate!=null&& !(newAfterDate.getYear()+1900).equals(Integer.parseInt(year))){
                //println("Wrong year "+(newAfterDate.getYear()+1900)+" "+Integer.parseInt(year))
                ok = false
            }
        }
        if(beforeDates!=null) {
            beforeDates.foreach(beforeDate => {
                if (beforeDate != null && newAfterDate != null) {
                    if (beforeDate.after(newAfterDate)) {
                        //println("Wrong sequence " + beforeDate + " " + newAfterDate)
                        ok = false
                    }
                }
            })
        }
        if(ok) {
            if(newAfterDate!=null) {
                afterDays += newAfterDate
            }
        }else{
            f.token.attr[LabeledTreeNerTag].setCategory("DIFF")(null)
        }
    }

    //analyze dates with regexes
    @throws(classOf[ParseException])
    def parseDate(date: String): Date = {
        var cleanedDate = date.replaceAll("\\|"," ").replaceAll(","," ").replaceAll(" {1,}",":").substring(1)
        val formats = new mutable.MutableList[DateFormat]
        formats+= new SimpleDateFormat("MMM:dd:yyyy", Locale.UK)
        var parsedDate:Date = null

        if(cleanedDate.contains("-")){//zawiera przedział dat
            cleanedDate = cleanedDate.replaceAll("^(\\w+:\\d{1,2})(:-:\\d{1,2})?(:\\d{4})","$1$3")
        }

            formats.foreach(format =>{
                try{
                    parsedDate = format.parse(cleanedDate,new ParsePosition(0))
                }catch{
                    case e: java.text.ParseException => {
                        parsedDate=null
                        println("ParseException " + cleanedDate)
                    }
                }
            })

        parsedDate
    }
    override def train(trainSet: Seq[Document]): Unit = ???
}
