package pl.edu.pw.ii.entityminer.mining.token.feature.discover

import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import scala.collection.mutable.ListBuffer
import pl.edu.pw.ii.entityminer.crawler.HtmlConsts
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation

/**
 * Created by Raphael Hazan on 2/26/14.
 */
class ShortWordDiscover(featureBuffer: ListBuffer[String]) extends FeatureDiscover {
  override def getPrefixes(): Set[String] = Set(Conf.PREFIX_FEAT_SHORT)

  override def discoverFeature(webToken: WebToken): List[String] = {
     if ( webToken.text.trim.toLowerCase.filterNot(x => x.isLetter) == 0 &&
       webToken.text.trim.size >= 2 && webToken.text.trim.size <= 6) {
      List(Conf.PREFIX_FEAT_SHORT + "true")
    } else {
      Nil
    }
  }

  override def discoverFeature(entity: List[WebToken]): List[String] = if (entity.size == 1) discoverFeature(entity.head) else Nil
}
