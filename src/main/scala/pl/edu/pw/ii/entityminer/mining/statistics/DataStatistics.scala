package pl.edu.pw.ii.entityminer.mining.statistics

/**
 * Created by Raphael Hazan on 1/30/14.
 */
trait DataStatistics {
  val CODE: String
  def accumulate(other: DataStatistics): Unit
}
