package pl.edu.pw.ii.entityminer.database.datasource

/**
 * Created by ralpher on 12/19/13.
 */
trait WebPageToFeaturesConverter {
  /**
   * Prepare data from collected web pages and save it to text files.
   * @param pagesDir directore, where are stored web pages in Manifest format.
   * @return directory where are stored output, files ready to train and test classifier.
   */
  def prepareData(pagesDir: String) : String
}
