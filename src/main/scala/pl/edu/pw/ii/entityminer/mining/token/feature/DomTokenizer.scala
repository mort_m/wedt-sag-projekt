package pl.edu.pw.ii.entityminer.mining.token.feature

import org.springframework.beans.factory.annotation.{Autowired, Qualifier}
import pl.edu.pw.ii.entityminer.crawler.dom.DomParser
import org.springframework.stereotype.Service
import pl.edu.pw.ii.entityminer.database.datasource.annotation.{AnnotateStrategy, ConferenceDateAnnotator}
import pl.edu.pw.ii.entityminer.mining.token.feature.discover.{FeatureDiscover, GateFeatureDiscover}
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryManifest._
import pl.edu.pw.ii.entityminer.Conf

/**
 * Created by Raphael Hazan on 1/21/14.
 */
@Service("DomTokenizer")
class DomTokenizer extends SequenceFeatureTokenizer {

  @Autowired
  @Qualifier("GateFeatureDiscover")
  var gateFeatureDiscover: FeatureDiscover = _

  @Autowired
  var domParser: DomParser = new DomParser()

  @Autowired
  @Qualifier("ConferenceDateAnnotator")//TODO separate annotation strategy with tokenizer
  var annotateStrategy: AnnotateStrategy = _

  @Autowired
  @Qualifier("ParagraphDivisor")
  var paragraphDivisor: ParagraphDivisor = _

  def tokenizeFromFiles(manifestEntries: List[(String, ManifestEntry)], singleEntities: Boolean): List[HeavyFeatureSet] = {
   //println(manifestEntries)
    val documents = manifestEntries.
      //filter(pathAndEntry => Conf.FEATURED_LINKS.filter(pathAndEntry._2._2._1.contains(_)).nonEmpty).
      map{
      case (documentPath, entry) =>
        
        val document = domParser.cleanAndConvertToDocument(documentPath)
        (entry, document)
    }

    this.tokenizeDocuments(documents, singleEntities)
  }
}
