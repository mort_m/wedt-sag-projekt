package pl.edu.pw.ii.entityminer.mining.token.feature.discover

import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.crawler.dom.DomParser
import pl.edu.pw.ii.entityminer.database.model.token.WebToken

import scala.collection.mutable.ListBuffer

/**
 * Created by Raphael Hazan on 2/26/14.
 */
class ParagraphNoFeatureDiscover(featureBuffer: ListBuffer[String]) extends FeatureDiscover {

  private var prevParagraphNo = 0
  private var prevParagraphOrderNo = -1

  override def getPrefixes(): Set[String] = Set(Conf.PREFIX_FEAT_PARAGRAPH)

  override def discoverFeature(webToken: WebToken): List[String] = {
    if (featureBuffer.contains(ContainerFeatureDiscover.LABEL_FEAT + "p")) {
      val currParagraphOrderNo = webToken.paragraphNo
      if (currParagraphOrderNo > prevParagraphNo) {
        prevParagraphOrderNo = currParagraphOrderNo
        prevParagraphNo += 1
      }
      if (prevParagraphNo < 9) {
        List(Conf.PREFIX_FEAT_PARAGRAPH + prevParagraphNo)
      } else {
        Nil
      }
    } else {
      Nil
    }
  }

  override def discoverFeature(entity: List[WebToken]): List[String] = discoverFeature(entity.head)
}

