package pl.edu.pw.ii.entityminer.mining.factorie

import cc.factorie.app.nlp.Document
import cc.factorie.model.{Parameters, TemplateModel}

/**
 * Created by Raphael Hazan on 12/31/13.
 */
trait ModelTrainer {
  def train(trainTestData: Seq[Document]): TemplateModel with Parameters
}
