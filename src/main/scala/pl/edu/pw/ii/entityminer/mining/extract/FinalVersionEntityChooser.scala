package pl.edu.pw.ii.entityminer.mining.extract

import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import cc.factorie.app.nlp
import cc.factorie.model.TemplateModel
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.LabeledTreeNerTag


/**
  * Created by Raphael Hazan on 3/29/2014.
  */
class FinalVersionEntityChooser extends EntityChooser {
   override val operatingLabel: String = ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.FINAL_VERSION).get

   /**
    * @param inferredSeqs theese sequences are ordered in mining order.
    * @return
    */
   override def chooseBestEntity(inferredSeqs: List[Seq[nlp.Token]], model: TemplateModel): Option[Seq[String]] = {
     if (inferredSeqs.size == 1) {
       Some(inferredSeqs.head.map(_.string.trim))
     } else {
       Some(
         inferredSeqs.map {
           seq =>
             seq.foldLeft(0.0) {
               case (sum, token) =>
                 sum + token.attr[LabeledTreeNerTag].proportions(model).toArray.toList.max
             } / seq.size
         }.zip(inferredSeqs).
           maxBy(_._1).
           _2.
           map(_.string.trim)
       )
     }
   }
 }
