package pl.edu.pw.ii.entityminer.crawler

import com.ning.http.client.Response
import org.htmlcleaner.{CleanerProperties, DomSerializer, HtmlCleaner, TagNode}
import pl.edu.pw.ii.entityminer.util.FileHelper
import java.io.{FileInputStream, InputStream, StringWriter}
import java.util.regex.Matcher

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.parser.Parser
import org.apache.commons.io.IOUtils
import pl.edu.pw.ii.entityminer.web.WebPage

/**
 * Created with IntelliJ IDEA.
 * User: Raphael Hazan
 * Date: 21.12.13
 * Time: 01:01
 * To change this template use File | Settings | File Templates.
 */
trait HtmlContentCleaner {
  private val cleaner: HtmlCleaner = new HtmlCleaner
  private val props: CleanerProperties = {
    val tmp = cleaner.getProperties
    tmp.setNamespacesAware(false)
    tmp.setOmitDeprecatedTags(false)
    tmp.setOmitUnknownTags(false)
    tmp.setCharset("UTF-8")
    tmp.setAdvancedXmlEscape(true)
    tmp
  }

  def cleanAndConvertToDocument(response: Response) = {
    cleanAndConverToDocument(response.getResponseBodyAsStream)
  }

  def cleanAndConvertToDocument(filePath: String) = {
    var file:String = ""
//    try {
      file = FileHelper.readFileToString(filePath)
//    }
//    catch{
//    //  case e: java.nio.charset.MalformedInputException =>
//    }
    println("file: "+file)
    file = cleanBody(file)
    Jsoup.parse(file, "", Parser.xmlParser())
  }
  def cleanBody(contentt: String) = {
    val content = contentt
        .replaceAll("(?s)<script[^>]*>.*?</script>|(?s)<style[^>]*>.*?</style>|(?s)<strike[^>]*>.*?</strike>", "").replaceAll("\n{2,}", "n")
    val c2 = "&[a-z]+;".r.replaceAllIn(content,
      x => WebPage.entities.getOrElse(x.toString, Matcher.quoteReplacement(x.toString)))
    val c3 = "&#([0-9]+);".r.replaceAllIn(c2,
      x => Matcher.quoteReplacement(Integer.parseInt(x.group(1)).asInstanceOf[Char].toString))

    c3.replaceAll("\\s+", " ")
      .replaceAll("<br/>|<br>|</tr>", "\n")
      //            .replaceAll("<[^>]*>", " ")
      .replaceAll(" +", " ")
      .replaceAll("( *\n+ *)+", "\n")
      .trim
  }
  def cleanAndConvertToDomDocument(response: Response) = {
        val tagNode: TagNode = cleaner.clean(response.getResponseBodyAsStream, "UTF-8")
        new DomSerializer(props).createDOM(tagNode)
  }

  private def cleanAndConverToDocument(inputStream: InputStream): Document = {
//    Jsoup.parse(inputStream, "UTF-8", "")
    val writer = new StringWriter()
    IOUtils.copy(inputStream, writer, "UTF-8")
    val str = writer.toString
    val result = Jsoup.parse(str, "", Parser.xmlParser())
    inputStream.close()
    result
  }
}
