package pl.edu.pw.ii.entityminer.mining.inference.basic

import cc.factorie.app.nlp
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.{Qualifier, Autowired}
import pl.edu.pw.ii.entityminer.mining.token.feature.discover.{GateFeatureDiscover, FeatureDiscover}
import pl.edu.pw.ii.entityminer.mining.factorie.TreeLabelCrfModel.TokenFeatures
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.{ConferenceLabelDomain, LabeledTreeNerTag}
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP
import scala.collection.mutable.ListBuffer

/**
 * Created by Raphael Hazan on 5/12/2014.
 */

class BasicWhereInferencer extends BasicInferencer {

  def inference(document: nlp.Document): nlp.Document = {
    var currWhereToken = 0
    val tokensBetweenWhereTokens = ListBuffer[nlp.Token]()
    for (token <- document.tokens) {
      val features = token.attr[TokenFeatures]
      if (features.activeCategories.contains("LOC=location")) {
        currWhereToken += 1
        token.attr[LabeledTreeNerTag] := ConferenceLabelDomain.value(ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.WHERE).get)
        return document
//        if (currWhereToken == 2) {
//          if (tokensBetweenWhereTokens.size < 4) {
//            tokensBetweenWhereTokens.foreach(_.attr += new LabeledTreeNerTag(token, ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.WHERE).get))
//          }
//          return document
//        }
      } else if (currWhereToken > 0) {
        tokensBetweenWhereTokens += token
      }
    }
    
    document
  }
}
