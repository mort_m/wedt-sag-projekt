package pl.edu.pw.ii.entityminer.mining.factorie

import org.springframework.stereotype.Service
import pl.edu.pw.ii.entityminer.util.FileHelper
import util.control.Breaks._
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.{SkipTagAA, SkipTagNA, SkipTagWW, LabeledTreeNerTag}
import scala.collection.mutable.{ListBuffer, ArrayBuffer}
import cc.factorie.app.nlp._
import pl.edu.pw.ii.entityminer.mining.factorie.TreeLabelCrfModel.TokenFeatures
import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.mining.token.feature.discover.TokenTypeFeatureDiscover
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation

/**
 * Created by Raphael Hazan on 12/29/13.
 */
@Service
class DataSetReader {
  def readDataSestForLinearCrf(dataDirectory: String, pagesFrom: Int, pagesNo: Int, trainInTen: Int, ignoreLabels: Boolean = false) = {
    val docs = ArrayBuffer[Document]()
    for (i <- pagesFrom to pagesFrom + pagesNo - 1) {
      if (FileHelper.fileExists(dataDirectory + "/" + i + ".0.txt")) {
        breakable {
          for (j <- 0 to 1000) {
            val fileName = dataDirectory + "/" + i + "." + j + ".txt"
            if (FileHelper.fileExists(fileName)) {
              fromSource(fileName, io.Source.fromFile(fileName), docs)
            }
          }
        }
      }
    }

//    println("Skip conns:")
//    println(Stats.skipPairsWW.groupBy(identity).mapValues(_.size).toSeq)
//    println(Stats.skipPairsNA.groupBy(identity).mapValues(_.size).toSeq)
//    println(Stats.skipPairToDistanceWW)
//    println(Stats.skipPairToDistanceNA)

    val trainTestData = docs.result
//    println(trainTestData.size)
//    println(dataDirectory)
    val trainTestDataDivided = trainTestData.zipWithIndex.map(valueWithIndex => (valueWithIndex._1, valueWithIndex._2 % 10 >= trainInTen)).groupBy(_._2).values.map(_.map(_._1))
    if (trainTestDataDivided.head.size > trainTestDataDivided.last.size)
      (trainTestDataDivided.head, trainTestDataDivided.last)
    else
      (trainTestDataDivided.last, trainTestDataDivided.head)
  }

  def readDataSestForTreeCrf(dataDirectory: String, pagesFrom: Int, pagesNo: Int, ignoreLabels: Boolean = false): Seq[Document] = {
    val docs = ArrayBuffer[Document]()
    breakable {
      for (repetitions <- 1 to 30) {
        for (i <- pagesFrom to pagesFrom + pagesNo - 1) {
          //        if (i == 99) {
          if (FileHelper.fileExists(dataDirectory + "/" + i + ".0.txt")) {
            for (j <- 0 to 1000) {
              val fileName = dataDirectory + "/" + i + "." + j + ".txt"
              if (FileHelper.fileExists(fileName)) {
                fromSourceForTreeCrf(fileName, io.Source.fromFile(fileName), docs, ignoreLabels)
              }
            }
          }
          //        }
        }
      }
    }
    docs.result
  }

  private def fromSource(fileName: String, source: io.Source, documents: ArrayBuffer[Document]): Unit = {
    def newDocument(name: String): Document = {
      val document = new Document("").setName(name)
      document.annotators(classOf[Token]) = UnknownDocumentAnnotator.getClass // register that we have token boundaries
      document.annotators(classOf[Sentence]) = UnknownDocumentAnnotator.getClass // register that we have sentence boundaries
      document
    }

    val tokens = new ListBuffer[Token]()
    var document = newDocument(fileName)
    documents += document
    val sentence = new Sentence(document)
    val abbrTokensMap = scala.collection.mutable.Map[String, scala.collection.mutable.ListBuffer[Token]]()

    var currentSpecialTokenIndex = 0
    val featureSet = scala.collection.mutable.Set[String]()
    val queueMaxGap = 3
    var prevSpecialTokenIndex = -queueMaxGap - 2
    var prevSkipToken: Option[Token] = None
    var prevSentenceNo = -1
    var skipTokenNo = -1
    var wasDate = false
    var wasLocation = false

    var currentSpecialTokenIndex2 = 0
    val queueMaxGap2 = 2
    var prevSpecialTokenIndex2 = -queueMaxGap2 - 2
    var prevSkipToken2: Option[Token] = None
    var skipTokenNo2 = 0
    var prevSentenceNo2 = -1
    var nameTurn = true

    source.getLines().foreach {
      sample =>
        tokens.clear()
        val fields = sample.split("\\s+")
        val word = fields(Conf.FEAT_WORD)
        val ner = fields(Conf.FEAT_CLASS)
        val sentenceNo = fields(Conf.FEAT_SENTENCE_NO).toInt
        if (sentence.length > 1) document.appendString(" ")
        val token = new Token(sentence, word)
        token.attr += new LabeledTreeNerTag(token, ner)

        if (fields.size > Conf.FEATURES) {
          val features = new TokenFeatures(token)
          for (i <- Conf.FEATURES to fields.size - 1) {
            val feat = fields(i)
            if (Conf.UsedFeatures.count(feat.startsWith(_)) > 0) {
              features += feat
            }
          }

          if (Conf.SKIP_CHAIN) {
            featureSet.clear
            for (i <- Conf.FEATURES to fields.size - 1) {
              featureSet += fields(i)
            }

            if (featureSet.contains(TokenTypeFeatureDiscover.FEAT_DATE) ||
              (featureSet.contains(TokenTypeFeatureDiscover.FEAT_TERM) && featureSet.contains("LOC=location"))) {

              val currentGap = currentSpecialTokenIndex - prevSpecialTokenIndex - 1
              if (currentGap <= queueMaxGap && sentenceNo == prevSentenceNo) {
                skipTokenNo += 1

                if (featureSet.contains(TokenTypeFeatureDiscover.FEAT_DATE) && wasDate ||
                  featureSet.contains("LOC=location") && wasLocation) {

                  skipTokenNo = 1
                } else {
                  if (skipTokenNo == 2) {
                    prevSkipToken.get.attr += SkipTagWW()
                  }
                  val tmp = SkipTagWW()
                  // uncomment if you want to make connection all-to-all
                  //                  tmp.connections ++= prevSkipToken.get.attr[SkipTag].connections
                  tmp.connections += prevSkipToken.get
                  token.attr += tmp
                  prevSkipToken.get.attr[SkipTagWW].connections += token

                  val pair = (prevSkipToken.get.attr[LabeledTreeNerTag].categoryValue, token.attr[LabeledTreeNerTag].categoryValue)
                  val distance = currentGap
                  Stats.skipPairsWW += (pair)
                  Stats.skipPairToDistanceWW.get(pair) match {
                    case None => Stats.skipPairToDistanceWW += ((pair, scala.collection.mutable.Map(distance -> 1)))
                    case Some(distanceToNo) =>
                      distanceToNo.get(distance) match {
                        case None =>
                          distanceToNo += ((distance, 1))
                        case Some(number) =>
                          distanceToNo += ((distance, number + 1))
                      }
                  }

                  prevSkipToken.get.attr[TokenFeatures] += "SKIP=WW"
                  features += "SKIP=WW"
                }
              } else {
                skipTokenNo = 1
                prevSentenceNo = sentenceNo
                wasDate = false
                wasLocation = false
              }
              if (featureSet.contains(TokenTypeFeatureDiscover.FEAT_DATE)) {
                wasDate = true
              }
              if (featureSet.contains("LOC=location")) {
                wasLocation = true
              }
              prevSkipToken = Some(token)
              prevSpecialTokenIndex = currentSpecialTokenIndex
            }

            if (featureSet.contains(TokenTypeFeatureDiscover.FEAT_LONG_TERM) ||
              featureSet.contains(TokenTypeFeatureDiscover.FEAT_UPTERM)) {

              if (featureSet.contains(TokenTypeFeatureDiscover.FEAT_UPTERM)) {
                abbrTokensMap.get(word) match {
                  case None => abbrTokensMap += (word -> scala.collection.mutable.ListBuffer(token))
                  case Some(tokenSet) => tokenSet += token
                }
              }

              val currentGap2 = currentSpecialTokenIndex2 - prevSpecialTokenIndex2 - 1
              if (featureSet.contains(TokenTypeFeatureDiscover.FEAT_LONG_TERM)) {
                prevSkipToken2 = Some(token)
                nameTurn = false
                prevSentenceNo2 = sentenceNo
                prevSpecialTokenIndex2 = currentSpecialTokenIndex2

              } else if (featureSet.contains(TokenTypeFeatureDiscover.FEAT_UPTERM) && !nameTurn
                && currentGap2 <= queueMaxGap2 && sentenceNo == prevSentenceNo2) {

                val tmp = SkipTagNA()
                tmp.connections += prevSkipToken2.get
                token.attr += tmp
                val tmp2 = SkipTagNA()
                tmp2.connections += token
                prevSkipToken2.get.attr += tmp2

                val pair = (prevSkipToken2.get.attr[LabeledTreeNerTag].categoryValue, ner)
                val distance = currentGap2
                Stats.skipPairsNA += (pair)
                Stats.skipPairToDistanceNA.get(pair) match {
                  case None => Stats.skipPairToDistanceNA += ((pair, scala.collection.mutable.Map(distance -> 1)))
                  case Some(distanceToNo) =>
                    distanceToNo.get(distance) match {
                      case None =>
                        distanceToNo += ((distance, 1))
                      case Some(number) =>
                        distanceToNo += ((distance, number + 1))
                    }
                }

                prevSkipToken2.get.attr[TokenFeatures] += "SKIP=BA"
                features += "SKIP=NA"
                if (ner == ConferenceHtmlAnnotation.ABBREVIATION &&
                  prevSkipToken2.get.attr[LabeledTreeNerTag].categoryValue == ConferenceHtmlAnnotation.NAME) {

//                                    println("name ok abbr ok: " + prevSkipToken2.get.string + " " + word)
                } else if (ner == ConferenceHtmlAnnotation.ABBREVIATION &&
                  prevSkipToken2.get.attr[LabeledTreeNerTag].categoryValue == ConferenceHtmlAnnotation.OTHER_LABEL) {

//                  println("for doc " + fileName)
//                  println(sentenceNo + " name bad abbr ok: " + prevSkipToken2.get.string + " " + word)
                } else if (ner == ConferenceHtmlAnnotation.OTHER_LABEL &&
                  prevSkipToken2.get.attr[LabeledTreeNerTag].categoryValue == ConferenceHtmlAnnotation.NAME) {

                  //                  println("name ok abbr bad: " + prevSkipToken2.get.string + " " + word)
                } else if (ner == ConferenceHtmlAnnotation.OTHER_LABEL &&
                  prevSkipToken2.get.attr[LabeledTreeNerTag].categoryValue == ConferenceHtmlAnnotation.OTHER_LABEL) {

                  //                  println("name bad abbr bad: " + prevSkipToken2.get.string + " " + word)
                } else {
                  //                  println("???: " + prevSkipToken2.get.string + " " + word)
                }

                nameTurn = true
              }
            }

            currentSpecialTokenIndex += 1
            currentSpecialTokenIndex2 += 1
          }
          token.attr += features
        }

        tokens += token
    }

    createSkipConnectionsForSameTokensWithFirst(abbrTokensMap)
  }

  object Stats {
    var skipPairToDistanceWW = scala.collection.mutable.Map[(String, String), scala.collection.mutable.Map[Int, Int]]()
    var skipPairToDistanceNA = scala.collection.mutable.Map[(String, String), scala.collection.mutable.Map[Int, Int]]()
    var skipPairsWW: ListBuffer[(String, String)] = new ListBuffer[(String, String)]()
    var skipPairsNA: ListBuffer[(String, String)] = new ListBuffer[(String, String)]()
  }

  private def createSkipConnectionsForSameTokens(abbrTokensMap: scala.collection.mutable.Map[String, scala.collection.mutable.ListBuffer[Token]]) {
    def connectTokens(abbreviation: Token, prevToken: Token) {
      abbreviation.attr[SkipTagAA] match {
        case null =>
          abbreviation.attr += SkipTagAA(scala.collection.mutable.Set(prevToken))
        case skip =>
          skip.connections += prevToken
      }
    }

    for (sameAbbreviations <- abbrTokensMap.values.filter(_.size > 1)) {
      var prev: Option[Token] = None
      for (abbreviation <- sameAbbreviations) {
        prev match {
          case None =>
          case Some(prevToken) =>
            connectTokens(abbreviation, prevToken)
            connectTokens(prevToken, abbreviation)
        }
        prev = Some(abbreviation)
      }
    }
  }

  private def createSkipConnectionsForSameTokensWithFirst(abbrTokensMap: scala.collection.mutable.Map[String, scala.collection.mutable.ListBuffer[Token]]) {
    def connectTokens(abbreviation: Token, prevToken: Token) {
      abbreviation.attr[SkipTagAA] match {
        case null =>
          abbreviation.attr += SkipTagAA(scala.collection.mutable.Set(prevToken))
        case skip =>
          skip.connections += prevToken
      }
    }

    for (sameAbbreviations <- abbrTokensMap.values.filter(_.size > 1)) {
      val first: Token = sameAbbreviations.head
      for (abbreviation <- sameAbbreviations.tail) {
        connectTokens(abbreviation, first)
        connectTokens(first, abbreviation)
      }
    }
  }

  private def fromSourceForTreeCrf(fileName: String, source: io.Source, documents: ArrayBuffer[Document], ignoreLabels: Boolean): Unit = {
    def newDocument(name: String): Document = {
      val document = new Document("").setName(name)
      document.annotators(classOf[Token]) = UnknownDocumentAnnotator.getClass // register that we have token boundaries
      document.annotators(classOf[Sentence]) = UnknownDocumentAnnotator.getClass // register that we have sentence boundaries
      //      document.annotators(classOf[pos.PennPosTag]) = UnknownDocumentAnnotator.getClass // register that we have POS tags
      document
    }

    val tokens = new ListBuffer[Token]()
    var document = newDocument(fileName)
    documents += document
    val sentence = new Sentence(document)

    var numberOfChildren = 0
    var rootParent: Option[LabeledTreeNerTag] = None
    var parent: Option[LabeledTreeNerTag] = None

    source.getLines().foreach {
      sample =>
        tokens.clear()
        val fields = sample.split("\\s+")
        val word = fields(Conf.FEAT_WORD)
        val ner = fields(Conf.FEAT_CLASS)
        if (sentence.length > 0) document.appendString(" ")
        val token = new Token(sentence, word)
        if (ignoreLabels) {
          token.attr += new LabeledTreeNerTag(token, ConferenceHtmlAnnotation.OTHER_LABEL)
        } else {
          token.attr += new LabeledTreeNerTag(token, ner)
        }

        if (fields.size > Conf.FEATURES) {
          val features = new TokenFeatures(token)
          for (i <- Conf.FEATURES to fields.size - 1) {
            features += fields(i)
          }
          token.attr += features
        }

        if (rootParent.isEmpty) {
          rootParent = Some(token.attr[LabeledTreeNerTag])
          token.attr += TreeTag(null, new ListBuffer[Token]())
        }
        else if (numberOfChildren > 0) {
          token.attr += TreeTag(parent.get.token, new ListBuffer[Token]())
          parent.get.token.attr[TreeTag].children += token
          numberOfChildren -= 1
        }
        else if (numberOfChildren == 0) {
          numberOfChildren = fields(0).toInt
          if (numberOfChildren > 0) {
            parent = Some(token.attr[LabeledTreeNerTag])
          }
          token.attr += TreeTag(rootParent.get.token, new ListBuffer[Token]())
          rootParent.get.token.attr[TreeTag].children += token
        }

        tokens += token
    }
  }
}
