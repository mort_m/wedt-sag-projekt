package pl.edu.pw.ii.entityminer.mining.statistics

import org.jsoup.nodes.{TextNode, Document}
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryManifest._
import pl.edu.pw.ii.entityminer.util.ThreeMap
import org.springframework.beans.factory.annotation.{Qualifier, Autowired}
import pl.edu.pw.ii.entityminer.mining.token.feature.ParagraphDivisor
import pl.edu.pw.ii.entityminer.database.datasource.annotation.{ConferenceHtmlAnnotation, AnnotateStrategy}

/**
 * Created by Raphael Hazan on 5/11/2014.
 */
class ParagraphNumerOccuranceStatisticsComputer extends MapToListOccursStatisticsComputer {

  @Autowired
  @Qualifier("ParagraphDivisor")
  var paragraphDivisor: ParagraphDivisor = _

  @Autowired
  @Qualifier("ConferenceDateAnnotator") //TODO separate annotation strategy with tokenizer
  var annotateStrategy: AnnotateStrategy = _

  def computeStatistics(conferenceData: ConferenceData, document: Document, manifestEntry: ManifestEntry, documentPath: String): ParagraphNumerOccuranceStatistics = {
    val dataParaNo = new ThreeMap[String, Int, Int]()
    val dataTokenNo = new ThreeMap[String, Int, Int]()

    val paragraphs = paragraphDivisor.divideToParagraphs(document)
    var paragraphNo = 0
    var tokenNo = 0
    for (paragraph <- paragraphs) {
      for (node <- paragraph) {
        val text = node.text()
        val tmpWebTokens = annotateStrategy.tokenize(text, node.asInstanceOf[TextNode])
        for (token <- tmpWebTokens) {
          val label = token.getLabel
          if (label != ConferenceHtmlAnnotation.OTHER_LABEL &&
            !dataParaNo.containsFirstKey(label)) {

            dataParaNo.put(label)(paragraphNo)(1)
            dataTokenNo.put(label)(tokenNo)(1)
          }

          tokenNo += 1
        }
      }

      paragraphNo += 1
    }
    new ParagraphNumerOccuranceStatistics(dataParaNo, dataTokenNo)
  }
}

case class ParagraphNumerOccuranceStatistics(
                                              // label -> first paraNo -> occurNo
                                              dataParaNo: ThreeMap[String, Int, Int],
                                              // label -> first tokenNo -> occurNo
                                              dataTokenNo: ThreeMap[String, Int, Int]
                                              ) extends DataStatistics {
  val CODE: String = "paragraph number occurance label"

  override def accumulate(other: DataStatistics): Unit = {
    val stats2 = other.asInstanceOf[ParagraphNumerOccuranceStatistics]
    for (stat <- stats2.dataParaNo.immutableMap) {
      dataParaNo.update(stat._1._1, stat._1._2, stat._2, _ + _)
    }
    for (stat <- stats2.dataTokenNo.immutableMap) {
      dataTokenNo.update(stat._1._1, stat._1._2, stat._2, _ + _)
    }
  }
}