package pl.edu.pw.ii.entityminer.chart

import org.jfree.chart.plot.{XYPlot, PlotOrientation}
import org.jfree.chart.{ChartUtilities, ChartFactory}
import java.awt.{Font, BasicStroke, Shape, Color}
import org.jfree.data.category.DefaultCategoryDataset
import org.jfree.data.xy.{XYSeries, XYSeriesCollection}
import java.io.File
import org.jfree.chart.renderer.xy.{XYLineAndShapeRenderer, StandardXYItemRenderer}
import org.jfree.chart.axis.{NumberTickUnit, CategoryLabelPositions, NumberAxis}
import org.jfree.util.ShapeUtilities

/**
 * Created by Raphael Hazan on 4/12/2014.
 */
object TextWorkCharting {
  private val chartDir = "data/charts/"

  def main (args: Array[String]) {
//    svmOneDimensionalChart
    svmExamlpleChartBoundariesWithMargins()
  }

  def makeBars(xy: Map[String, Int]) = {
//    val name = "Długość skrótów nazw konferencji."
    val name = ""
//    val xySorted = xy.toList.filter(_._2 > 10).sortWith{
//      case ((x1, y1), (x2, y2)) =>
//        x1 < x2
//    }
    val xySorted = xy.toList.sortWith{
      case ((x1, y1), (x2, y2)) =>
        y1 > y2
    }.take(10)

    val dataSet = new DefaultCategoryDataset()
    for ((x, y) <- xySorted) {
      dataSet.setValue(y, "liczba przypadków", x)
    }

//    val xName = "długość skrótu wyrażona w liczbie znaków"
//    val xName = "numer porządkowy paragrafu"
    val xName = "tekst pod odnośnikiem do podstrony"
//    val yName = "liczba przypadków"
    val yName = "liczba przypadków znaleznienia informacji"

    val chart = ChartFactory.createBarChart(name, xName, yName, dataSet, PlotOrientation.VERTICAL,
      false, true, false)

    val plot = chart.getCategoryPlot()
    val domainAxis = plot.getDomainAxis()
    domainAxis.setCategoryLabelPositions(
      CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 6.0)
    )
    domainAxis.setTickLabelFont(new Font("Dialog", Font.PLAIN, 20))

    ChartUtilities.saveChartAsPNG(new File(chartDir + name + ".png"), chart, 1000, 500)
  }

  def histogramDistribution(name: String, xy: Map[Int, Int]) = {
    val xySorted = xy.toList.sortWith{
      case ((x1, y1), (x2, y2)) =>
        y1 < y2
    }
    val distribuant = xySorted.foldLeft(List[(Int, Int)]((0, 0))) {
      case (distribuant, (x, y)) =>
        (x, y + distribuant.head._2) :: distribuant
    }.reverse

    val series1 = new XYSeries("")
    for ((x, y) <- xySorted) {
      series1.add(x, y)
    }
    val dataSet = new XYSeriesCollection()
    dataSet.addSeries(series1)

    val chart = ChartFactory.createXYBarChart("Dystrybuanta pierwszego pojawienia się klasy " + name + " w dokumencie.",
      "x", false, "y", dataSet)

    ChartUtilities.saveChartAsPNG(new File(chartDir + name + ".png"), chart, 500, 500)
  }

  def svmExamlpleChart() = {
    val x1 = "x1"
    val x2 = "x2"

    val series1 = new XYSeries("granica decyzyjna g(x)")
    series1.add(-2, -2)
    series1.add(10, 10)
    val series2 = new XYSeries("margines górny H1 i dolny H2")
    series2.add(-1, 4)
    series2.add(5, 10)
    val series3 = new XYSeries("margines dolny")
    series3.add(4, -1)
    series3.add(10, 5)
    val series4 = new XYSeries("klasa -1")
    series4.add(1, 6)
    series4.add(3, 8)
    series4.add(2, 9)
    series4.add(1, 8)
    val series5 = new XYSeries("klasa 1")
    series5.add(8, 2)
    series5.add(6, 1)
    series5.add(9, 1)

    val series6 = new XYSeries("1")
    series6.add(1, 6)
    series6.add(2, 9)
    series6.add(1, 8)
    val series7 = new XYSeries("2")
    series7.add(8, 2)
    series7.add(6, 1)
    series7.add(9, 1)
    val series8 = new XYSeries("3")
    series8.add(1, 6)
    series8.add(3, 8)
    val series9 = new XYSeries("4")
    series9.add(3, 8)
    series9.add(2, 9)
    val series10 = new XYSeries("5")
    series10.add(6, 1)
    series10.add(9, 1)


    val dataSet = new XYSeriesCollection()
    dataSet.addSeries(series1)
    dataSet.addSeries(series2)
    dataSet.addSeries(series3)
    dataSet.addSeries(series4)
    dataSet.addSeries(series5)
    dataSet.addSeries(series6)
    dataSet.addSeries(series7)
    dataSet.addSeries(series8)
    dataSet.addSeries(series9)
    dataSet.addSeries(series10)

    val chart = ChartFactory.createXYLineChart("",
      x1, x2, dataSet)
    chart.setBackgroundPaint(Color.white)

    val plot = chart.getXYPlot
    plot.setBackgroundPaint(Color.white)
    plot.setDomainGridlinePaint(Color.lightGray)
    plot.setRangeGridlinePaint(Color.lightGray)

    val renderer = new XYLineAndShapeRenderer()
    renderer.setSeriesPaint(0, Color.black)
    renderer.setSeriesPaint(1, Color.black)
    renderer.setSeriesPaint(2, Color.black)
    renderer.setSeriesPaint(3, Color.black)
    renderer.setSeriesPaint(4, Color.black)
    renderer.setSeriesPaint(5, Color.black)
    renderer.setSeriesPaint(6, Color.black)
    renderer.setSeriesPaint(7, Color.black)
    renderer.setSeriesPaint(8, Color.black)
    renderer.setSeriesPaint(9, Color.black)
    renderer.setSeriesVisibleInLegend(2, false)
    renderer.setSeriesVisibleInLegend(5, false)
    renderer.setSeriesVisibleInLegend(6, false)
    renderer.setSeriesVisibleInLegend(7, false)
    renderer.setSeriesVisibleInLegend(8, false)
    renderer.setSeriesVisibleInLegend(9, false)
    renderer.setSeriesShapesVisible(0, false)
    renderer.setSeriesShapesVisible(1, false)
    renderer.setSeriesShapesVisible(2, false)
    renderer.setSeriesShapesVisible(5, false)
    renderer.setSeriesShapesVisible(6, false)
    renderer.setSeriesShapesVisible(7, false)
    renderer.setSeriesShapesVisible(8, false)
    renderer.setSeriesShapesVisible(9, false)
    renderer.setSeriesLinesVisible(3, false)
    renderer.setSeriesLinesVisible(4, false)
    renderer.setSeriesStroke(1, new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1, Array[Float](2, 6), 0))
    renderer.setSeriesStroke(2, new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1, Array[Float](2, 6), 0))
    renderer.setSeriesShape(3, ShapeUtilities.createRegularCross(5, 0.5f))
    renderer.setSeriesShapesFilled(3, false)
    renderer.setSeriesShape(4, ShapeUtilities.createRegularCross(4, 4))
    renderer.setSeriesShapesFilled(4, false)
    plot.setRenderer(renderer)

    val rangeAxis = plot.getRangeAxis.asInstanceOf[NumberAxis]
    rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits())
    rangeAxis.setRange(-1, 10)
    val domainAxis = plot.getDomainAxis.asInstanceOf[NumberAxis]
    domainAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits())
    domainAxis.setRange(-1, 10)

    ChartUtilities.saveChartAsPNG(new File(chartDir + "svmExample.png"), chart, 500, 500)
  }

  def svmExamlpleChartBoundaries() = {
    val x1 = "x1"
    val x2 = "x2"

    val series1 = new XYSeries("g1(x)")
    series1.add(-2, -2)
    series1.add(10, 10)
    val series2 = new XYSeries("g2(x)")
    series2.add(-1, 2)
    series2.add(5, 10)
    val series3 = new XYSeries("g4(x)")
    series3.add(-1, 6)
    series3.add(10, 2)
    val series6 = new XYSeries("g3(x)")
    series6.add(0, -1)
    series6.add(7, 10)


    val series4 = new XYSeries("klasa -1")
    series4.add(1, 6)
    series4.add(3, 8)
    series4.add(2, 9)
    series4.add(1, 8)
    val series5 = new XYSeries("klasa 1")
    series5.add(8, 2)
    series5.add(6, 1)
    series5.add(9, 1)

    val dataSet = new XYSeriesCollection()
    dataSet.addSeries(series1)
    dataSet.addSeries(series2)
    dataSet.addSeries(series3)
    dataSet.addSeries(series4)
    dataSet.addSeries(series5)
    dataSet.addSeries(series6)

    val chart = ChartFactory.createXYLineChart("",
      x1, x2, dataSet)
    chart.setBackgroundPaint(Color.white)

    val plot = chart.getXYPlot
    plot.setBackgroundPaint(Color.white)
    plot.setDomainGridlinePaint(Color.lightGray)
    plot.setRangeGridlinePaint(Color.lightGray)

    val renderer = new XYLineAndShapeRenderer()
    renderer.setSeriesPaint(0, Color.black)
    renderer.setSeriesPaint(1, Color.black)
    renderer.setSeriesPaint(2, Color.black)
    renderer.setSeriesPaint(3, Color.black)
    renderer.setSeriesPaint(4, Color.black)
    renderer.setSeriesPaint(5, Color.black)
    renderer.setSeriesPaint(6, Color.black)
    renderer.setSeriesPaint(7, Color.black)
    renderer.setSeriesPaint(8, Color.black)
    renderer.setSeriesPaint(9, Color.black)
    renderer.setSeriesVisibleInLegend(5, false)
    renderer.setSeriesVisibleInLegend(2, false)
    renderer.setSeriesVisibleInLegend(1, false)
    renderer.setSeriesVisibleInLegend(0, false)
    renderer.setSeriesShapesVisible(0, false)
    renderer.setSeriesShapesVisible(1, false)
    renderer.setSeriesShapesVisible(2, false)
    renderer.setSeriesShapesVisible(5, false)
    renderer.setSeriesShapesVisible(6, false)
    renderer.setSeriesShapesVisible(7, false)
    renderer.setSeriesShapesVisible(8, false)
    renderer.setSeriesShapesVisible(9, false)
    renderer.setSeriesLinesVisible(3, false)
    renderer.setSeriesLinesVisible(4, false)
    renderer.setSeriesShape(3, ShapeUtilities.createRegularCross(5, 0.5f))
    renderer.setSeriesShapesFilled(3, false)
    renderer.setSeriesShape(4, ShapeUtilities.createRegularCross(4, 4))
    renderer.setSeriesShapesFilled(4, false)
    plot.setRenderer(renderer)

    val rangeAxis = plot.getRangeAxis.asInstanceOf[NumberAxis]
    rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits())
    rangeAxis.setRange(-1, 10)
    val domainAxis = plot.getDomainAxis.asInstanceOf[NumberAxis]
    domainAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits())
    domainAxis.setRange(-1, 10)

    ChartUtilities.saveChartAsPNG(new File(chartDir + "svmExample.png"), chart, 500, 500)
  }

  def svmExamlpleChartBoundariesWithMargins() = {
    val x1 = "x1"
    val x2 = "x2"

    val series1 = new XYSeries("g1(x)")
    series1.add(-1 , 6 + 8.0 / 11)
    series1.add(10, 2 + 8.0 / 11)
    val series2 = new XYSeries("g2(x)")
    series2.add(-1, 6 - 8.0 / 11)
    series2.add(10, 2 - 8.0 / 11)
    val series3 = new XYSeries("g4(x)")
    series3.add(-1, 6)
    series3.add(10, 2)
    val series6 = new XYSeries("g3(x)")
    series6.add(0, -1)
    series6.add(7, 10)
    val margin2 = 30.0
    val series7 = new XYSeries("g5(x)")
    series7.add(0 + margin2 / 11, -1)
    series7.add(7 + margin2 / 11, 10)
    val series8 = new XYSeries("g6(x)")
    series8.add(0 - margin2 / 11, -1)
    series8.add(7 - margin2 / 11, 10)

    val series4 = new XYSeries("klasa -1")
    series4.add(1, 6)
    series4.add(3, 8)
    series4.add(2, 9)
    series4.add(1, 8)
    val series5 = new XYSeries("klasa 1")
    series5.add(8, 2)
    series5.add(6, 1)
    series5.add(9, 1)

    val dataSet = new XYSeriesCollection()
    dataSet.addSeries(series1)
    dataSet.addSeries(series2)
    dataSet.addSeries(series3)
    dataSet.addSeries(series4)
    dataSet.addSeries(series5)
    dataSet.addSeries(series6)
    dataSet.addSeries(series8)
    dataSet.addSeries(series7)

    val chart = ChartFactory.createXYLineChart("",
      x1, x2, dataSet)
    chart.setBackgroundPaint(Color.white)

    val plot = chart.getXYPlot
    plot.setBackgroundPaint(Color.white)
    plot.setDomainGridlinePaint(Color.lightGray)
    plot.setRangeGridlinePaint(Color.lightGray)

    val renderer = new XYLineAndShapeRenderer()
    renderer.setSeriesPaint(0, Color.black)
    renderer.setSeriesPaint(1, Color.black)
    renderer.setSeriesPaint(2, Color.black)
    renderer.setSeriesPaint(3, Color.black)
    renderer.setSeriesPaint(4, Color.black)
    renderer.setSeriesPaint(5, Color.black)
    renderer.setSeriesPaint(6, Color.black)
    renderer.setSeriesPaint(7, Color.black)
    renderer.setSeriesPaint(8, Color.black)
    renderer.setSeriesPaint(9, Color.black)
    renderer.setSeriesVisibleInLegend(5, false)
    renderer.setSeriesVisibleInLegend(6, false)
    renderer.setSeriesVisibleInLegend(2, false)
    renderer.setSeriesVisibleInLegend(1, false)
    renderer.setSeriesVisibleInLegend(0, false)
    renderer.setSeriesShapesVisible(0, false)
    renderer.setSeriesShapesVisible(1, false)
    renderer.setSeriesShapesVisible(2, false)
    renderer.setSeriesShapesVisible(5, false)
    renderer.setSeriesShapesVisible(6, false)
    renderer.setSeriesShapesVisible(7, false)
    renderer.setSeriesShapesVisible(8, false)
    renderer.setSeriesShapesVisible(9, false)
    renderer.setSeriesLinesVisible(3, false)
    renderer.setSeriesLinesVisible(4, false)
    renderer.setSeriesStroke(0, new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1, Array[Float](2, 6), 0))
    renderer.setSeriesStroke(1, new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1, Array[Float](2, 6), 0))
    renderer.setSeriesStroke(6, new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1, Array[Float](2, 6), 0))
    renderer.setSeriesStroke(7, new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1, Array[Float](2, 6), 0))
    renderer.setSeriesShape(3, ShapeUtilities.createRegularCross(5, 0.5f))
    renderer.setSeriesShapesFilled(3, false)
    renderer.setSeriesShape(4, ShapeUtilities.createRegularCross(4, 4))
    renderer.setSeriesShapesFilled(4, false)
    plot.setRenderer(renderer)

    val rangeAxis = plot.getRangeAxis.asInstanceOf[NumberAxis]
    rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits())
    rangeAxis.setRange(-1, 10)
    val domainAxis = plot.getDomainAxis.asInstanceOf[NumberAxis]
    domainAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits())
    domainAxis.setRange(-1, 10)

    ChartUtilities.saveChartAsPNG(new File(chartDir + "svmExample.png"), chart, 500, 500)
  }

  def svmOneDimensionalChart() = {

    // axis line
    val series1 = new XYSeries("axisX")
//        series1.add(-3, 0)
//        series1.add(3, 0)

    val series2 = new XYSeries("1")
    series2.add(-1, 0)
    series2.add(2, 0)
    val series3 = new XYSeries("2")
    series3.add(0, 0)
    val dataSet = new XYSeriesCollection()
    dataSet.addSeries(series1)
    dataSet.addSeries(series2)
    dataSet.addSeries(series3)

    val chart = ChartFactory.createXYLineChart(
      "","","",dataSet,PlotOrientation.VERTICAL, false, true,false);

    val plot = chart.getPlot().asInstanceOf[XYPlot]

    plot.setBackgroundPaint(Color.white)
    plot.setDomainGridlinePaint(Color.lightGray)
    plot.setRangeGridlinePaint(Color.lightGray)
    plot.setRangeGridlinesVisible(false);
    plot.setDomainCrosshairVisible( false );
    plot.setRangeCrosshairVisible( true );

    val range = plot.getRangeAxis().asInstanceOf[NumberAxis]
    range.setVisible(false)
    range.setRange(0.0, 1.0)
    range.setTickUnit(new NumberTickUnit(0.5))

    val renderer = new XYLineAndShapeRenderer()
    renderer.setSeriesPaint(1, Color.black)
    renderer.setSeriesPaint(2, Color.black)
    renderer.setSeriesShape(1, ShapeUtilities.createRegularCross(5, 0.5f))
    renderer.setSeriesShapesFilled(1, false)
    renderer.setSeriesShape(2, ShapeUtilities.createRegularCross(4, 4))
    renderer.setSeriesShapesFilled(2, false)
    renderer.setSeriesLinesVisible(1, false)
    renderer.setSeriesLinesVisible(2, false)
//    renderer.setSeriesShapesVisible(0, false)
//    renderer.setSeriesStroke(0, new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1, Array[Float](2, 6), 0))
    plot.setRenderer(renderer)

    val rangeAxis = plot.getRangeAxis.asInstanceOf[NumberAxis]
    rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits())
    rangeAxis.setRange(-1, 1)
    val domainAxis = plot.getDomainAxis.asInstanceOf[NumberAxis]
    domainAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits())
    domainAxis.setRange(-3, 3)

    ChartUtilities.saveChartAsPNG(new File(chartDir + "svmOneDimension.png"), chart, 500, 100)
  }

  def svmOneDimensionalConvertedToTwoChart() = {
    val x1 = "x1"
    val x2 = "x2"

    val series1 = new XYSeries("granica decyzyjna g(x)")
    series1.add(0.5, -2)
    series1.add(0.5, 5)
    val series4 = new XYSeries("klasa -1")
    series4.add(1, 1)
    series4.add(4, 1)
    val series5 = new XYSeries("klasa 1")
    series5.add(0, 1)

    val dataSet = new XYSeriesCollection()
    dataSet.addSeries(series1)
    dataSet.addSeries(series4)
    dataSet.addSeries(series5)

    val chart = ChartFactory.createXYLineChart("",
      x1, x2, dataSet, PlotOrientation.VERTICAL, false, true, false)
    chart.setBackgroundPaint(Color.white)

    val plot = chart.getXYPlot
    plot.setBackgroundPaint(Color.white)
    plot.setDomainGridlinePaint(Color.lightGray)
    plot.setRangeGridlinePaint(Color.lightGray)

    val renderer = new XYLineAndShapeRenderer()
    renderer.setSeriesPaint(0, Color.black)
    renderer.setSeriesPaint(1, Color.black)
    renderer.setSeriesPaint(2, Color.black)
    renderer.setSeriesShapesVisible(0, false)
    renderer.setSeriesLinesVisible(1, false)
    renderer.setSeriesLinesVisible(2, false)
    renderer.setSeriesShape(1, ShapeUtilities.createRegularCross(5, 0.5f))
    renderer.setSeriesShapesFilled(1, false)
    renderer.setSeriesShape(2, ShapeUtilities.createRegularCross(4, 4))
    renderer.setSeriesShapesFilled(2, false)
    plot.setRenderer(renderer)

    val rangeAxis = plot.getRangeAxis.asInstanceOf[NumberAxis]
    rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits())
    rangeAxis.setRange(-1, 5)
    val domainAxis = plot.getDomainAxis.asInstanceOf[NumberAxis]
    domainAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits())
    domainAxis.setRange(-1, 5)

    ChartUtilities.saveChartAsPNG(new File(chartDir + "svmOneDim2Example.png"), chart, 500, 500)
  }
}
