package pl.edu.pw.ii.entityminer.mining.statistics

import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.chart.TextWorkCharting
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import pl.edu.pw.ii.entityminer.mining.token.feature.discover._
import pl.edu.pw.ii.entityminer.util.FileHelper

import scala.collection.mutable.ListBuffer

/**
 * Created by Raphael Hazan on 7/13/2014.
 */
object FeaturesStatistics {
  def processNames() = {
    val wordToOccurNo = scala.collection.mutable.Map[String, Integer]()

    for (i <- 0 to 1100) {
      val filePath = Conf.FEATURES_STORAGE_ENTITY + Conf.SLASH + i + ".0.txt"
      if (FileHelper.fileExists(filePath)) {
        val lines = FileHelper.readFileToLines(filePath)
        for (line <- lines.map(_.split("\\s+").toList)) {
          val lineClass = line(Conf.FEAT_CLASS)
          if (lineClass == ConferenceHtmlAnnotation.NAME) {
            for (featureOrWord <- line) {
              if (featureOrWord.startsWith(WordFeatureDiscover.LABEL_FEAT)) {
                wordToOccurNo.get(featureOrWord) match {
                  case None => wordToOccurNo += ((featureOrWord, 1))
                  case Some(counted) => wordToOccurNo += ((featureOrWord, counted + 1))
                }
              }
            }
          }
        }
      }
    }

    println("ConferenceNameWordsStatistics result: ")
    for (pair <- wordToOccurNo.toList.sortWith(_._2 > _._2).take(999999)) {
      println(pair._1.drop(2) + " & " + pair._2 + " \\\\")
    }
  }

  def processHighlights() = {
    val htypeToClassToYesNo = scala.collection.mutable.Map[String, scala.collection.mutable.Map[String, (Int, Int)]]()
    var ohterYes = 0
    var ohterNo = 0

    for (i <- 0 to 1100) {
      val filePath = Conf.FEATURES_STORAGE_ENTITY + Conf.SLASH + i + ".0.txt"
      if (FileHelper.fileExists(filePath)) {
        val lines = FileHelper.readFileToLines(filePath)
        for (line <- lines.map(_.split("\\s+").toList)) {
          val lineClass = line(Conf.FEAT_CLASS)
          var isHighlighted = false
          var tokenType = ""
          var htype = ""
          for (featureOrWord <- line) {
            if (featureOrWord.startsWith(LinkFeatureDiscover.LABEL_FEAT)) {
              isHighlighted = true
              htype = featureOrWord

            } else if (featureOrWord.startsWith(TokenTypeFeatureDiscover.LABEL_FEAT)) {
              tokenType = featureOrWord

            }
          }

          if (lineClass != ConferenceHtmlAnnotation.OTHER_LABEL ||
            Set(TokenTypeFeatureDiscover.FEAT_DATE, TokenTypeFeatureDiscover.FEAT_LONG_TERM, TokenTypeFeatureDiscover.FEAT_UPTERM).contains(tokenType)) {

            htypeToClassToYesNo.get(htype) match {
              case None =>
                val tmp = scala.collection.mutable.Map[String, (Int, Int)]()
                isHighlighted match {
                  case true => tmp put (lineClass, (1, 0))
                  case false => tmp put (lineClass, (0, 1))
                }
                htypeToClassToYesNo put (htype, tmp)
              case Some(classToYesNo) =>
                classToYesNo.get(lineClass) match {
                  case None =>
                    isHighlighted match {
                      case true => classToYesNo put(lineClass, (1, 0))
                      case false => classToYesNo put(lineClass, (0, 1))
                    }
                  case Some((yes, no)) =>
                    isHighlighted match {
                      case true => classToYesNo put(lineClass, (yes + 1, no))
                      case false => classToYesNo put(lineClass, (yes, no + 1))
                    }
                }
            }
          }
        }
      }
    }

    println("Highlight Statistics result: ")
    val otherStats = htypeToClassToYesNo.filter(_._1 == "").head._2
    for (pair <- htypeToClassToYesNo) {
      println("|")
      println(pair._1)
      for (pair2 <- pair._2) {
        println(pair2._1 + " | " + (1.0 * pair2._2._1 / otherStats(pair2._1)._2))
//        println(pair2._1 + " | " + (1.0 * pair2._2._2 / otherStats(pair2._1)._2))
      }
    }
  }

  def processLayoutParents() = {
    val parentToClassToNumber = scala.collection.mutable.Map[String, scala.collection.mutable.Map[String, Int]]()

    for (i <- 0 to 1100) {
      val filePath = Conf.FEATURES_STORAGE_ENTITY + Conf.SLASH + i + ".0.txt"
      if (FileHelper.fileExists(filePath)) {
        val lines = FileHelper.readFileToLines(filePath)
        for (line <- lines.map(_.split("\\s+").toList)) {
          val lineClass = line(Conf.FEAT_CLASS)
          var tokenType = ""
          var parent = ""
          for (featureOrWord <- line) {
            if (featureOrWord.startsWith(ContainerFeatureDiscover.LABEL_FEAT)) {
              parent = featureOrWord

            } else if (featureOrWord.startsWith(TokenTypeFeatureDiscover.LABEL_FEAT)) {
              tokenType = featureOrWord

            }
          }
          if (parent.startsWith("P=h")) {
            parent = "P=h"
          }

          parentToClassToNumber.get(parent) match {
            case None =>
              val tmp = scala.collection.mutable.Map[String, Int]()
              tmp put (lineClass, 1)
              parentToClassToNumber put (parent, tmp)
            case Some(classToNumber) =>
              classToNumber.get(lineClass) match {
                case None =>
                  classToNumber put(lineClass, 1)
                case Some(number) =>
                  classToNumber put(lineClass, number + 1)
              }
          }
        }
      }
    }

    val classToTotalNumber = ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.allValues.map{ label =>
      (label, parentToClassToNumber.foldLeft(0) {
        case (total, (_, classToNumber)) =>
          total + classToNumber.filter(_._1 == label).map(_._2).sum
      })
    }.toMap
    println("Layout Statistics result: ")
    for (pair <- parentToClassToNumber) {
      println("|")
      println(pair._1)
      for (pair2 <- pair._2) {
        println(pair2._1 + " | " + (1.0 * pair2._2 / classToTotalNumber(pair2._1)))
      }
    }
  }

  def processParagraphOccurs() = {
    val paragraphNoToNumber = scala.collection.mutable.Map[Int, Int]()

    for (i <- 0 to 1100) {
      val filePath = Conf.FEATURES_STORAGE_ENTITY + Conf.SLASH + i + ".0.txt"
      if (FileHelper.fileExists(filePath)) {
        val lines = FileHelper.readFileToLines(filePath)
        var lastElementNo = -1
        var lastParagraphNo = 0
        var pointsAddedForLastParagraphNo = -1
        for (line <- lines.map(_.split("\\s+").toList)) {
          val lineClass = line(Conf.FEAT_CLASS)
          val currElementNo = line(Conf.FEAT_PARAGRAPH_NO).toInt
          var parent = ""
          for (featureOrWord <- line) {
            if (featureOrWord.startsWith(ContainerFeatureDiscover.LABEL_FEAT)) {
              parent = featureOrWord
            }
          }
          if (parent.startsWith("P=p")) {// || parent.startsWith("P=div")) {
            if (currElementNo > lastElementNo) {
              lastElementNo = currElementNo
              lastParagraphNo += 1
            }
            if (pointsAddedForLastParagraphNo < lastParagraphNo && lineClass != ConferenceHtmlAnnotation.OTHER_LABEL) {
              pointsAddedForLastParagraphNo = lastParagraphNo
              paragraphNoToNumber.get(lastParagraphNo) match {
                case None => paragraphNoToNumber put (lastParagraphNo, 1)
                case Some(number) => paragraphNoToNumber put (lastParagraphNo, number + 1)
              }
            }
          }
        }
      }
    }

    println("Pragraph number Statistics result: ")
    for (pair <- paragraphNoToNumber) {
      println(pair._1 + " | " + pair._2)
      TextWorkCharting.makeBars(paragraphNoToNumber.toList.sortBy(_._1).take(35).toMap.map(kv => (kv._1.toString, kv._2)))
    }
  }

  def processNotInWordListOccurs() = {
    val classToYesNo = scala.collection.mutable.Map[String, (Int, Int)]()

    for (i <- 0 to 1100) {
      val filePath = Conf.FEATURES_STORAGE_ENTITY + Conf.SLASH + i + ".0.txt"
      if (FileHelper.fileExists(filePath)) {
        val lines = FileHelper.readFileToLines(filePath)
        for (line <- lines.map(_.split("\\s+").toList)) {
          val lineClass = line(Conf.FEAT_CLASS)
          var notInDict = false
          for (featureOrWord <- line) {
            if (featureOrWord.startsWith(NotInWordListDiscover.LABEL_FEAT)) {
              notInDict = true
            }
          }
          classToYesNo.get(lineClass) match {
            case None =>
              notInDict match {
                case true => classToYesNo put (lineClass, (1, 0))
                case false => classToYesNo put (lineClass, (0, 1))
              }
            case Some((notInDictNo, inDictNo)) =>
              notInDict match {
                case true => classToYesNo put (lineClass, (notInDictNo + 1, inDictNo))
                case false => classToYesNo put (lineClass, (notInDictNo, inDictNo + 1))
              }
          }
        }
      }
    }

    println("Not in dict Statistics result: ")
    for (pair <- classToYesNo) {
      println(pair._1 + " | " + pair._2 + " | " + (1.0 * pair._2._1 / (pair._2._1 + pair._2._2)))
    }
  }

  def processPredecessors() = {
    val classToWordToNumber = scala.collection.mutable.Map[String, scala.collection.mutable.Map[String, Int]]()

    for (i <- 0 to 1100) {
      val filePath = Conf.FEATURES_STORAGE_ENTITY + Conf.SLASH + i + ".0.txt"
      if (FileHelper.fileExists(filePath)) {
        val lines = FileHelper.readFileToLines(filePath)
        for (line <- lines.map(_.split("\\s+").toList)) {
          val lineClass = line(Conf.FEAT_CLASS)
          var prevWords = ListBuffer[String]()
          for (featureOrWord <- line) {
            if (featureOrWord.startsWith(TableDateFeatureDiscover.LABEL_FEAT)) {
              prevWords += featureOrWord

            }
          }

          for (prevWord <- prevWords) {
            classToWordToNumber.get(lineClass) match {
              case None =>
                val tmp = scala.collection.mutable.Map[String, Int]()
                tmp put(prevWord, 1)
                classToWordToNumber put(lineClass, tmp)
              case Some(wordToNumber) =>
                wordToNumber.get(prevWord) match {
                  case None =>
                    wordToNumber put(prevWord, 1)
                  case Some(number) =>
                    wordToNumber put(prevWord, number + 1)
                }
            }
          }
        }
      }
    }

    println("frequent predecessors Statistics result: ")
    for (pair <- classToWordToNumber) {
      println("|")
      println(pair._1)
      for (pair2 <- pair._2.toList.sortWith(_._2 > _._2).take(20)) {
        println(pair2._1 + " | " + pair2._2)
      }
    }
  }
}
