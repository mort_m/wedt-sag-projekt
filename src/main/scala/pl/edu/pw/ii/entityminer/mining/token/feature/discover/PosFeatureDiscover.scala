package pl.edu.pw.ii.entityminer.mining.token.feature.discover

import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import cc.factorie.app.nlp.pos.PennPosTag
import scala.collection.mutable.ListBuffer

/**
 * Created by Raphael Hazan on 2/26/14.
 */
class PosFeatureDiscover(featureBuffer: ListBuffer[String]) extends FeatureDiscover {
  override def getPrefixes(): Set[String] = Set(Conf.PREFIX_FEAT_POS)

  override def discoverFeature(entity: List[WebToken]): List[String] = {
    if (canBeProcessed()) {

      val posCompound = entity.foldLeft(Set[String]()) {
        case (set, token) =>
          set ++ Set(token.nlpToken.attr[PennPosTag].categoryValue.toString)
      }.toList.sorted.reduceLeft[String] {
        case (folded, pos) =>
          folded + "#" + pos
      }
      List(Conf.PREFIX_FEAT_POS + posCompound)
    } else {
      Nil
    }
  }

  override def discoverFeature(webToken: WebToken): List[String] = {
    if (canBeProcessed()) {

      List(Conf.PREFIX_FEAT_POS + webToken.nlpToken.attr[PennPosTag].categoryValue)
    } else {
      Nil
    }
  }

  private def canBeProcessed() = {
    featureBuffer.contains(TokenTypeFeatureDiscover.FEAT_STANDARD) ||
      featureBuffer.contains(TokenTypeFeatureDiscover.FEAT_TERM) ||
      featureBuffer.contains(TokenTypeFeatureDiscover.FEAT_LONG_TERM) ||
      featureBuffer.contains(TokenTypeFeatureDiscover.FEAT_UPTERM)
  }
}
