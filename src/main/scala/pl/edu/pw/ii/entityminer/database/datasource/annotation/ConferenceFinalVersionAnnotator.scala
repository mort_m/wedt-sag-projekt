package pl.edu.pw.ii.entityminer.database.datasource.annotation

import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.mining.token.feature.StopWords
import org.jsoup.nodes.TextNode
import scala.collection.mutable.ListBuffer
import org.springframework.stereotype.Service
import pl.edu.pw.ii.entityminer.database.datasource.wikicfp.WikicfpConsts
import pl.edu.pw.ii.entityminer.mining.nlp.TimeName

/**
 * Created by Raphael Hazan on 1/13/14.
 */
class ConferenceFinalVersionAnnotator extends AllSeparatelyTokenizer with TimeTokenMatcherAtLeastTwo {
  val annotationTagName: String = ConferenceHtmlAnnotation.FINAL_VERSION

  def getValueTokens(conferenceData: ConferenceData): List[List[String]] = {
    if (conferenceData.paperFinalVersionDate == null ||
        conferenceData.paperFinalVersionDate.contains(WikicfpConsts.NA)) {
      Nil
    } else {
      val dateTokens = conferenceData.paperFinalVersionDate.toLowerCase.split("\\s+").map(_.trim.replaceAll("[().,-]", "")).
        filterNot(StopWords.StopWords.contains(_)).filter(_.nonEmpty).
        toList.map( str => TimeName.getTimeName(str))
      List(dateTokens)
    }
  }
}
