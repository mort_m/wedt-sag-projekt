package pl.edu.pw.ii.entityminer.mining.statistics

/**
 * Created by Raphael Hazan on 17.01.14.
 */
trait MapToListOccursDataStatistics extends DataStatistics {
  val labelToStat: collection.mutable.Map[String, collection.mutable.Map[List[String], Int]]

  def accumulate(other: DataStatistics): Unit = {
    for ((label, stats) <- other.asInstanceOf[MapToListOccursDataStatistics].labelToStat) {
      if (labelToStat.contains(label)) {
        for ((sequence, occurNo) <- stats) {
          if (labelToStat(label).contains(sequence)) {
            labelToStat(label).put(sequence, labelToStat(label)(sequence) + occurNo)
          } else {
            labelToStat(label).put(sequence, occurNo)
          }
        }
      } else {
        labelToStat.put(label, stats)
      }
    }
  }
}
