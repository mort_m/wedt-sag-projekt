package pl.edu.pw.ii.entityminer.mining.token.feature

import cc.factorie.app.nlp.Document

/**
 * Created by Raphael Hazan on 2/21/14.
 */
trait FeatureCorrector {
  def correctFeatures(directory: String, firstTestPageNo: Int): Unit

  def correctFeatures(trainDocs: Seq[Document], testDocs: Seq[Document]): Unit
}
