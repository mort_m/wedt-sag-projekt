package pl.edu.pw.ii.entityminer

import cc.factorie.app.nlp.Document
import org.springframework.beans.factory.annotation.{Qualifier, Autowired}
import pl.edu.pw.ii.entityminer.database.datasource.wikicfp.WikicfpDatasource
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryCollector
import pl.edu.pw.ii.entityminer.database.datasource.annotation.{ConferenceHtmlAnnotation, WebPageAnnotator}
import pl.edu.pw.ii.entityminer.mining.factorie.{FactorieDocumentPrinter, FactorieInferencer, ModelTrainer, DataSetReader}
import cc.factorie.app.nlp
import cc.factorie.model.{Parameters, TemplateModel}
import pl.edu.pw.ii.entityminer.mining.inference.ConferenceInferencer
import pl.edu.pw.ii.entityminer.mining.statistics._
import pl.edu.pw.ii.entityminer.mining.token.feature.FeatureCreator
import pl.edu.pw.ii.entityminer.mining.accuracy.FactorieAccuracyChecker
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.{ConferenceLabelDomain, LabeledTreeNerTag}
import cc.factorie.variable.HammingObjective
import pl.edu.pw.ii.entityminer.mining.factorie.tools.FactorieModelSaver
import pl.edu.pw.ii.entityminer.mining.inference.basic.BasicInferencer
import javax.annotation.Resource
import pl.edu.pw.ii.entityminer.mining.factorie.TreeLabelCrfModel.TokenFeatures
import pl.edu.pw.ii.entityminer.mining.train.{SvmRbfTrainer, SvmLinearClassifier, SvmLinearTrainer}
import cc.factorie.app.classify.backend.LinearMulticlassClassifier

/**
 * Created with IntelliJ IDEA.
 * User: Raphael Hazan
 * Date: 22.12.13
 * Time: 16:36
 * To change this template use File | Settings | File Templates.
 */
class RootFacade {
  @Resource
  private var basicInferencers: java.util.List[BasicInferencer] = _

  @Autowired
  var conferenceInferencer: ConferenceInferencer = _

  @Autowired
  private var pageDirectoryCollector: PageDirectoryCollector = _

  @Autowired
  var wikicfpDatasource: WikicfpDatasource = _

  @Autowired
  var annotator: WebPageAnnotator = _

  @Autowired
  var featureCreator: FeatureCreator = _

  @Autowired
  private var dataSetReader: DataSetReader = _

  @Autowired
  private var documentPrinter: FactorieDocumentPrinter = _

  @Autowired
  //  @Qualifier("TreeCrfTrainer")
  @Qualifier("LinearCrfTrainer")
  private var modelTrainer: ModelTrainer = _

  @Autowired
  private var inferencer: FactorieInferencer = _

  @Autowired
  private var statisticsBuilder: ConferenceStatistics = _

  @Autowired
  private var accuracyChecker: FactorieAccuracyChecker = _

  @Autowired
  private var scrappedWebPageIteratorFactory: ScrappedWebPageIteratorFactory = _

  @Autowired
  private var modelSaver: FactorieModelSaver = _

  def scrapConferences(totalCount: Int) = {
    var counter = 0
    wikicfpDatasource.setUp()
    while (counter < totalCount) {
      var conferenceSubpages = wikicfpDatasource.nextPage()
      conferenceSubpages = conferenceSubpages.take(Math.min(conferenceSubpages.size, totalCount - counter))
      counter += conferenceSubpages.size
      val scrappedData = wikicfpDatasource.nextConferencePages(conferenceSubpages)
      pageDirectoryCollector.collectPages(scrappedData, Conf.DATA_STORAGE)
      println("[scrapConferences] scrapped pages no: " + counter)
    }
  }

  def deleteWebPageResources(documentsDirectory: String, totalPages: Int) = {
    pageDirectoryCollector.deleteWebPageResources(documentsDirectory, totalPages)
  }

  def saveManifestsInXml(documentsDirectory: String, totalPages: Int) = {
    pageDirectoryCollector.saveManifestsInXml(documentsDirectory, totalPages)
  }

  def annotateConferencesWithLabels(sourceDocDirectory: String, destinyDocDirectory: String, totalPages: Int) = {
    annotator.annotateConferencesWithLabels(sourceDocDirectory, destinyDocDirectory: String, totalPages)
  }

  def createAndSaveFeatures(documentsDirectory: String, destinyDirectory: String, totalPages: Int) = {
    featureCreator.createFeaturesAndSave(documentsDirectory, totalPages, destinyDirectory, Conf.SINGLE_ENTITIES)
  }

  def extractStopWords(fromDirectory: String) = {
    featureCreator.findAndSaveStopWords(fromDirectory: String, Conf.FEATURES_STORAGE_STOP_WORDS)
  }

  def convertFeaturesToIOB(featuresDirectory: String, destinyDirectory: String, totalPages: Int) = {
    featureCreator.convertFeaturesToIOB(featuresDirectory, destinyDirectory, totalPages)
  }

  //TODO dodac kroki wygladzajace pliki features - dzielace zbior na treningowy i testowy - testowy ma miec unknown slowa, jesli nie wystapily w treningowym
  // treningowy ma miec unknown w word jesli tylko raz wystapily
  // dodac dokument z wszystkimi mozliwymi wyliczeniowymi tokenami - opcjonalnie
  def readTrainTestData(documentDirectory: String, totalPages: Int, trainNoInTen: Int) = {
    if (Conf.LINEAR_MODEL) {
      dataSetReader.readDataSestForLinearCrf(documentDirectory, 0, totalPages, trainNoInTen)
    } else {
      (dataSetReader.readDataSestForTreeCrf(documentDirectory, 0, totalPages), Nil)
    }
  }

  def printData(trainTestData: Seq[nlp.Document]) = {
    if (! Conf.LINEAR_MODEL) {
      documentPrinter.printData(trainTestData)
    } else {
      documentPrinter.printChainData(trainTestData)
    }
  }

  def trainLinearCrfModel(trainTestData: Seq[nlp.Document]) = {
    modelTrainer.train(trainTestData)
  }

  def loadModel(model: Parameters, filePath: String) = {
    modelSaver.deserialize(filePath, model)
  }

  def saveModel(model: Parameters, filePath: String) = {
    modelSaver.serialize(model, filePath)
  }

  def inferDocs(model: TemplateModel, testData: Seq[nlp.Document]) = {
    testData.foreach {
      doc =>
        for (token <- doc.tokens) {
          token.attr[LabeledTreeNerTag] := ConferenceLabelDomain.value(ConferenceHtmlAnnotation.OTHER_LABEL)
        }
//        val basicInfIter = basicInferencers.iterator()
//        while (basicInfIter.hasNext) {
//          basicInfIter.next().inference(doc)
//        }
          inferencer.inference(doc, model,"")
    }
    testData
  }

  def inferDocsWithSimpleClassifiers(model: TemplateModel, testData: Seq[nlp.Document]) = {
    testData.foreach {
      doc =>
        for (token <- doc.tokens) {
          token.attr[LabeledTreeNerTag] := ConferenceLabelDomain.value(ConferenceHtmlAnnotation.OTHER_LABEL)
        }
                val basicInfIter = basicInferencers.iterator()
                while (basicInfIter.hasNext) {
                  basicInfIter.next().inference(doc)
                }
//        inferencer.inference(doc, model)
    }
    testData
  }

  def inferDocsSvmNotFactorie(model: FactorieInferencer, testData: Seq[nlp.Document]) = {
    testData.foreach {
      doc =>
        for (token <- doc.tokens) {
          token.attr[LabeledTreeNerTag] := ConferenceLabelDomain.value(ConferenceHtmlAnnotation.OTHER_LABEL)
        }
        model.inference(doc, null,"")
    }
    testData
  }

  def trainSvm(trainData: Seq[nlp.Document]) = {
    val dimensionSize = trainData.head.tokens.head.attr[TokenFeatures].domain.dimensionSize
    val trainer = new SvmLinearTrainer(dimensionSize)
    trainer.train(trainData)
  }

  def inferWithSvm(testData: Seq[nlp.Document], model: SvmLinearClassifier) = {
    for (doc <- testData) {
      for (token <- doc.tokens) {
        token.attr[LabeledTreeNerTag] := ConferenceLabelDomain.value(ConferenceHtmlAnnotation.OTHER_LABEL)
      }

      model.inference(doc)
    }
  }

  def saveSvmModel(filePath: String, svmTrainer: SvmLinearTrainer) = {
    svmTrainer.saveModel(filePath)
  }

  def loadSvmModel(filePath: String, featuresDimension: Int) = {
    val trainer = new SvmLinearTrainer(featuresDimension)
    trainer.loadModel(filePath)
  }

  def checkAccuracy(model: TemplateModel, inferencedData: Seq[nlp.Document], trainInTen: Int) = {
    println("test token accuracy=" + HammingObjective.accuracy(inferencedData.map(_.tokens.toSeq.map(_.attr[LabeledTreeNerTag])).flatten))

    accuracyChecker.checkAccuracy(model, inferencedData)
    //    val fileIterator = scrappedWebPageIteratorFactory.buildIter(documentDirectory, pagesFrom, pagesNo)
    //    val conferenceInfos = scala.collection.mutable.LinkedList[ConferenceData]()
    //    fileIterator.foreach {
    //      case (conferenceData, manifestEntry, document, documentPath) =>
    //        conferenceInfos append mutable.LinkedList(conferenceData)
    //    }
    //    accuracyChecker.checkAccuracy(conferenceInfos, inferencedData)
  }

  def removePagesWithoutAnnotations(documentDirectory: String, totalPages: Int) = {
    pageDirectoryCollector.removeNotAnnotatedPages(documentDirectory, totalPages)
  }

  def addIndexToManifestIfNotAdded(documentDirectory: String, totalPages: Int) = {
    pageDirectoryCollector.addIndexToManifestIfNotAdded(documentDirectory, totalPages)
  }

  def makeStatistics(documentDirectory: String, totalPages: Int) = {
    statisticsBuilder.makeStatistics(documentDirectory, totalPages)
  }
}
