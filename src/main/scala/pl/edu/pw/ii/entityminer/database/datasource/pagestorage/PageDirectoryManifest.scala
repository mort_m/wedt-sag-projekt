package pl.edu.pw.ii.entityminer.database.datasource.pagestorage

import scala.collection.mutable

/**
 * Created with IntelliJ IDEA.
 * User: ralpher
 * Date: 7/8/13
 * Time: 12:01 AM
 * To change this template use File | Settings | File Templates.
 */
class PageDirectoryManifest(manifest: PageDirectoryManifest.ManifestType) extends Serializable {
  def result() = manifest
}

object PageDirectoryManifest {
  type Builder = mutable.MapBuilder[String, (Set[String], String), PageDirectoryManifest.ManifestType]
  /** Map web address -> set of A tag labels (many labels can lead to same location) and filePath on local disk*/
  type ManifestType = Map[String, (Set[String], String)]
  type ManifestEntry = (String, (Set[String], String))

  def getManifestBuilder(): Builder = new mutable.MapBuilder[String, (Set[String], String), ManifestType](Map[String, (Set[String], String)]())

  def addToBuilder(manifestBuilder: Builder, tuple: (String, (Set[String], String))) = {
    manifestBuilder += tuple
    manifestBuilder
  }

  def getFileNameOnDrive(entry: ManifestEntry) = entry._2._2

  def build(manifestBuilder: Builder) = new PageDirectoryManifest(manifestBuilder.result())
}
