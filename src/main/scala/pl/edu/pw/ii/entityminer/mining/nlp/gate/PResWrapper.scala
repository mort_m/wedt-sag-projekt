package pl.edu.pw.ii.entityminer.mining.nlp.gate

import gate.creole.SerialAnalyserController
import gate._
import java.io.File
import java.net.URL
import java.util

/**
 * Created by ralpher on 12/15/13.
 */
object PResWrapper {
  def loadChunker(annieController: SerialAnalyserController): SerialAnalyserController = {
    val params: FeatureMap = Factory.newFeatureMap
    val pluginsHome: File = new File(Gate.getGateHome, "plugins/Tagger_NP_Chunking")
    System.out.println(pluginsHome)
    Gate.getCreoleRegister.registerDirectories(pluginsHome.toURI.toURL)
    params.put("annotationName", "BaseNP")
    val pr: ProcessingResource = Factory.createResource("mark.chunking.GATEWrapper", params).asInstanceOf[ProcessingResource]
    annieController.add(pr)
    return annieController
  }

  def getNPTagged(corpusIter: Iterator[Document]): util.ArrayList[String] = {
    val annotTypesRequired: util.Set[String] = new util.HashSet[String]
    annotTypesRequired.add("BaseNP")
    val outputXMLs: util.ArrayList[String] = new util.ArrayList[String]
    while (corpusIter.hasNext) {
      val doc: Document = corpusIter.next
      val defaultAnnotSet: AnnotationSet = doc.getAnnotations
      val chosenAnnots: util.Set[Annotation] = new util.HashSet[Annotation](defaultAnnotSet.get(annotTypesRequired))
      outputXMLs.add(doc.toXml(chosenAnnots, false))
    }
    return outputXMLs
  }

  def loadTransducter(grammarUrl: String): ProcessingResource = {
    val u: URL = new URL(grammarUrl)
    val params: FeatureMap = Factory.newFeatureMap
    params.put(gate.creole.Transducer.TRANSD_GRAMMAR_URL_PARAMETER_NAME, u)
    System.out.println("Creating cleaning transducer: " + gate.creole.Transducer.TRANSD_GRAMMAR_URL_PARAMETER_NAME + "=" + u)
    val pr: ProcessingResource = Factory.createResource("gate.creole.Transducer", params).asInstanceOf[ProcessingResource]
    return pr
  }
}
