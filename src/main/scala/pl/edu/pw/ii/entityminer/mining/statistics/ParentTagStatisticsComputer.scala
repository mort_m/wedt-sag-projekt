package pl.edu.pw.ii.entityminer.mining.statistics

import org.jsoup.nodes.Document
import org.springframework.beans.factory.annotation.Autowired
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.mining.nlp.WebPageTokenizer
import scala.collection.mutable
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryManifest._

/**
 * Created by Raphael Hazan on 17.01.14.
 */
class ParentTagStatisticsComputer extends StatisticsComputer {

  @Autowired
  private var webPageTokenizer: WebPageTokenizer = _

  def computeStatistics(conferenceData: ConferenceData, document: Document, manifestEntry: ManifestEntry, documentPath: String): MapToListOccursDataStatistics = {
    val labelToParentTag = mutable.Map[String, mutable.Map[List[String], Int]]()
    val tokens = webPageTokenizer.tokenize(document)
    for (token <- tokens) {
      if (ConferenceHtmlAnnotation.ALL_ANNOTATIONS.contains(token.parentName)) {
        var nodeIter = token.node.parent().parent()
        var currentTagPath = List[String]()
        while (nodeIter != null) {
          currentTagPath = nodeIter.nodeName() :: currentTagPath
          if (labelToParentTag.contains(token.parentName)) {
            if (labelToParentTag(token.parentName).contains(currentTagPath)) {
              labelToParentTag(token.parentName).put(currentTagPath, labelToParentTag(token.parentName)(currentTagPath) + 1)
            } else {
              labelToParentTag(token.parentName).put(currentTagPath, 1)
            }
          } else {
            labelToParentTag.put(token.parentName, mutable.Map((currentTagPath, 1)))
          }
          nodeIter = nodeIter.parent()
        }
      }
    }
    new ParentTagStatistics(labelToParentTag)
  }
}

case class ParentTagStatistics(
                                labelToStat: collection.mutable.Map[String, collection.mutable.Map[List[String], Int]]) extends MapToListOccursDataStatistics {
  val CODE: String = "parent tag"
}
