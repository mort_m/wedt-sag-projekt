package pl.edu.pw.ii.entityminer.mining.factorie

import cc.factorie.app.nlp.Token
import scala.collection.mutable.ListBuffer

/**
 * Created with IntelliJ IDEA.
 * User: ralpher
 * Date: 12/6/13
 * Time: 11:55 PM
 * To change this template use File | Settings | File Templates.
 */
case class TreeTag(parent: Token, children: ListBuffer[Token])