package pl.edu.pw.ii.entityminer.crawler.dom

import org.jsoup.nodes.Node

/**
 * Created by Raphael Hazan on 28.12.13.
 */
trait JsoupIter {
  val document: Node

  def next(): Option[Node]
}
