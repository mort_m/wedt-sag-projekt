package pl.edu.pw.ii.entityminer.mining.factorie.treecrf

import cc.factorie.app.nlp.Document
import cc.factorie.optimize.{Trainer, LikelihoodExample}
import cc.factorie.infer.InferByBPChain
import cc.factorie.variable.HammingObjective
import cc.factorie.model.{DotTemplateWithStatistics2, DotTemplateWithStatistics1}
import cc.factorie._
import pl.edu.pw.ii.entityminer.mining.factorie.TreeLabelCrfModel.{TokenFeaturesDomain, TokenFeatures}
import org.springframework.stereotype.Service
import pl.edu.pw.ii.entityminer.mining.factorie.{TreeTag, ModelTrainer}

/**
 * Created by Raphael Hazan on 12/31/13.
 */
@Service("TreeCrfTrainer")
class TreeCrfTrainer extends ModelTrainer {
  val model = new TemplateModel with Parameters {
    addTemplates(
      // Bias term on each individual label
      new DotTemplateWithStatistics1[TreeNerTag] {
        //def statisticsDomains = Tuple1(Conll2003NerDomain)
        val weights = Weights(new la.DenseTensor1(ConferenceLabelDomain.size))
      },
      // Factor between label and observed token
      new DotTemplateWithStatistics2[TreeNerTag, TokenFeatures] {
        //def statisticsDomains = ((Conll2003NerDomain, TokenFeaturesDomain))
        val weights = Weights(new la.DenseTensor2(ConferenceLabelDomain.size, TokenFeaturesDomain.dimensionSize))

        def unroll1(label: TreeNerTag) = Factor(label, label.token.attr[TokenFeatures])

        def unroll2(tf: TokenFeatures) = Factor(tf.token.attr[TreeNerTag], tf)
      }
      ,
      new DotTemplateWithStatistics2[TreeNerTag, TreeNerTag] {
        //def statisticsDomains = ((Conll2003NerDomain, Conll2003NerDomain))
        val weights = Weights(new la.DenseTensor2(ConferenceLabelDomain.size, ConferenceLabelDomain.size))

        def unroll1(label: TreeNerTag) = {
          if (label.token.hasPrev) List(Factor(label.token.prev.attr[TreeNerTag], label)) else Nil
        }

        def unroll2(label: TreeNerTag) = {
          if (label.token.hasNext) List(Factor(label, label.token.next.attr[TreeNerTag])) else Nil
        }
      }
      ,
      // Transition factors between two labels in tree
      new DotTemplateWithStatistics2[TreeNerTag, TreeNerTag] {
        //def statisticsDomains = ((Conll2003NerDomain, Conll2003NerDomain))
        val weights = Weights(new la.DenseTensor2(ConferenceLabelDomain.size, ConferenceLabelDomain.size))

        def unroll1(label: TreeNerTag) = {
          val childrenFactors = label.token.attr[TreeTag].children.foldLeft(List[Factor]()) {
            case (res, child) =>
              Factor(label, child.attr[TreeNerTag]) :: res
          }
          val parent = label.token.attr[TreeTag].parent
          val result = (if (parent != null) {
            List(Factor(parent.attr[TreeNerTag], label))
          } else {
            Nil
          }) ++ childrenFactors
          result
        }

        def unroll2(label: TreeNerTag) = {
//          Nil
          val childrenFactors = label.token.attr[TreeTag].children.foldLeft(List[Factor]()) {
            case (res, child) =>
              Factor(label, child.attr[TreeNerTag]) :: res
          }
          val parent = label.token.attr[TreeTag].parent
          val result = (if (parent != null) {
            List(Factor(parent.attr[TreeNerTag], label))
          } else {
            Nil
          }) ++ childrenFactors
          result
        }
      }
    )
  }

  private val dataDivideRatio = 0.7

  def train(trainTestData: Seq[Document]) = {
    val (trainData, testData) = trainTestData.splitAt((dataDivideRatio * trainTestData.size).toInt)

    val trainLabelsSentences: Seq[Seq[LabeledTreeNerTag]] = trainData.map(_.tokens.toSeq.map(_.attr[LabeledTreeNerTag]))
    val testLabelsSentences: Seq[Seq[LabeledTreeNerTag]] = testData.map(_.tokens.toSeq.map(_.attr[LabeledTreeNerTag]))

    println("*** Starting training (#sentences=%d)".format(trainData.map(_.sentences.size).sum))
    val start = System.currentTimeMillis

    implicit val random = new scala.util.Random(0)
    val examples = trainLabelsSentences.map(s => new LikelihoodExample(s, model, InferByBPChain))
    Trainer.batchTrain(model.parameters, examples)
    println("*** Starting inference (#sentences=%d)".format(testData.map(_.sentences.size).sum))
    testLabelsSentences.foreach {
      variables => cc.factorie.infer.BP.inferChainSum(variables, model).setToMaximize(null)
    }
    println("test token accuracy=" + HammingObjective.accuracy(testLabelsSentences.flatten))

    testData.foreach{d => println(""); d.tokens.foreach(l => println("Token: " + l + " Label: " + l.attr[LabeledTreeNerTag].baseCategoryValue))}

    println("Total training took " + (System.currentTimeMillis - start) / 1000.0 + " seconds")

    model
  }
}
