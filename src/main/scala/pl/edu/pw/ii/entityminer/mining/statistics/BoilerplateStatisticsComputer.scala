package pl.edu.pw.ii.entityminer.mining.statistics

import org.jsoup.nodes.Document
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryManifest._
import pl.edu.pw.ii.entityminer.util.CollectionHelper
import org.springframework.beans.factory.annotation.{Qualifier, Autowired}
import pl.edu.pw.ii.entityminer.mining.token.feature.FeatureTokenizer
import scala.collection
import scala.collection


/**
 * Created by Raphael Hazan on 17.01.14.
 */
class BoilerplateStatisticsComputer extends StatisticsComputer {
  @Autowired
  @Qualifier("BoilerplateFeatureTokenizer")
  private var boilerplateFeatureTokenizer: FeatureTokenizer = _

  @Autowired
  @Qualifier("DomTokenizer")
  private var domFeatureTokenizer: FeatureTokenizer = _

  def computeStatistics(conferenceData: ConferenceData, document: Document, manifestEntry: ManifestEntry, documentPath: String): DataStatistics = {
    val resultMap = collection.mutable.Map[String, List[Int]]()
    manifestEntry._2._1.map(_.toLowerCase.trim.replaceAll("\\s+", " ")).foreach {
      linkLabel =>
        val boilerplateTokens = boilerplateFeatureTokenizer.tokenizeFromFiles(List((documentPath + "/" + manifestEntry._2._2, manifestEntry)), false)
        val domTokens = domFeatureTokenizer.tokenizeFromFiles(List((documentPath + "/" + manifestEntry._2._2, manifestEntry)), false)
        resultMap put (linkLabel, List(boilerplateTokens.size, domTokens.size))
    }
    BoilerplateStatistics(resultMap)
  }
}

case class BoilerplateStatistics(data: collection.mutable.Map[String, List[Int]]) extends DataStatistics {
  val CODE: String = "boilerplate stats"

  def accumulate(otherX: DataStatistics): Unit = {
    for ((otherKV) <- otherX.asInstanceOf[BoilerplateStatistics].data) {
      CollectionHelper.updateMap(data, otherKV, (v1: List[Int], v2: List[Int]) => List(v1.head + v2.head, v1.last + v2.last))
    }
  }
}