package pl.edu.pw.ii.entityminer.mining.token.feature.discover

import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import scala.collection.mutable.ListBuffer

/**
 * Created by Raphael Hazan on 2/26/14.
 */
class WordShapeFeatureDiscover(featureBuffer: ListBuffer[String]) extends FeatureDiscover {
  override def discoverFeature(entity: List[WebToken]): List[String] = if (entity.size == 1) discoverFeature(entity.head) else Nil

  override def discoverFeature(webToken: WebToken): List[String] = {
    if (featureBuffer.contains(TokenTypeFeatureDiscover.FEAT_STANDARD) ||
      featureBuffer.contains(TokenTypeFeatureDiscover.FEAT_TERM) ||
      featureBuffer.contains(TokenTypeFeatureDiscover.FEAT_LONG_TERM) ||
      featureBuffer.contains(TokenTypeFeatureDiscover.FEAT_UPTERM)) {
      List(WordShapeFeatureDiscover.Prefix + cc.factorie.app.strings.stringShape(webToken.originalText, 2))
    } else {
      Nil
    }
  }

  override def getPrefixes(): Set[String] = Set(Conf.PREFIX_FEAT_SHAPE)
}

object  WordShapeFeatureDiscover {
  val Prefix = Conf.PREFIX_FEAT_SHAPE
  val ShapeAA = Prefix + "AA"
  val ShapeAaaAaa = Prefix + "AaaAaa"
  val ShapeAaaAA = Prefix + "AaaAA"
  val ShapeAA1AA = Prefix + "AA1AA"
  val ShapeAAaa = Prefix + "AAaa"
  val ShapeAaAA = Prefix + "AaAA"
  val ShapeAAa = Prefix + "AAa"
  val ShapeAAaAA = Prefix + "AAaAA"
}