package pl.edu.pw.ii.entityminer.crawler.dom

import org.jsoup.nodes.{Node, Document}

/**
 * Created by Raphael Hazan on 28.12.13.
 */
class DfsJsoupIter(val document: Node) extends JsoupIter {
  val stack = new collection.mutable.Stack[Node]()
  stack.push(document)

  def next() = {
    if (stack.nonEmpty) {
      val iter = stack.pop()
      val childNo = iter.childNodes().size()
      for (childIndex <- childNo - 1 to 0 by -1) {
        stack.push(iter.childNode(childIndex))
      }
      Some(iter)
    } else {
      None
    }
  }
}
