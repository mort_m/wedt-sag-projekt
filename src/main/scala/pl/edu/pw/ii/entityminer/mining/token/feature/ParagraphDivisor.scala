package pl.edu.pw.ii.entityminer.mining.token.feature

import org.jsoup.nodes.{TextNode, Node}
import scala.collection.mutable.ListBuffer
import org.springframework.beans.factory.annotation.Autowired
import pl.edu.pw.ii.entityminer.crawler.dom.DomParser
import org.springframework.stereotype.Service
import pl.edu.pw.ii.entityminer.crawler.HtmlConsts

/**
 * Created by Raphael Hazan on 4/23/2014.
 */


@Service("ParagraphDivisor")
class ParagraphDivisor {

  @Autowired
  var domParser: DomParser = new DomParser()

  def divideToParagraphs(document: org.jsoup.nodes.Document): List[List[TextNode]] = {
    val result = ListBuffer[List[TextNode]]()
    val dfsIter = domParser.getDfsIterator(document)

    var paragraphNode: Option[Node] = None
    val textNodesInParagraph = ListBuffer[TextNode]()

    while (dfsIter.next() match {
      case Some(node) =>
        if (node.nodeName() == HtmlConsts.BreakLine) {
          if (textNodesInParagraph.nonEmpty)
            result += textNodesInParagraph.result
          textNodesInParagraph.clear
          paragraphNode = None

        } else if (node.isInstanceOf[TextNode] &&
          node.asInstanceOf[TextNode].text().trim.nonEmpty) {
          paragraphNode match {
            case None =>
              paragraphNode = domParser.findParentParagraph(node)
              if (paragraphNode.isDefined) {
                if (textNodesInParagraph.nonEmpty)
                  result += textNodesInParagraph.result
                textNodesInParagraph.clear()
              }
            case Some(leadingParentNode) =>
              domParser.findParentParagraph(node) match {
                case None =>
                  if (textNodesInParagraph.nonEmpty)
                    result += textNodesInParagraph.result
                  paragraphNode = None
                  textNodesInParagraph.clear
                case Some(currentParent) =>
                  if (currentParent != leadingParentNode) {
                    if (textNodesInParagraph.nonEmpty)
                      result += textNodesInParagraph.result
                    paragraphNode = Some(currentParent)
                    textNodesInParagraph.clear()
                  }
              }
          }
          textNodesInParagraph += node.asInstanceOf[TextNode]
        }
        true
      case None =>
        false
    }) {}
    if (textNodesInParagraph.nonEmpty) {
      result += textNodesInParagraph.result
    }
    result.result
  }
}
