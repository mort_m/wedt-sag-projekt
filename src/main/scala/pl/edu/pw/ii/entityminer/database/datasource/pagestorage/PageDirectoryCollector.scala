package pl.edu.pw.ii.entityminer.database.datasource.pagestorage

import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import java.io.{File, FileInputStream, FileNotFoundException, ObjectInputStream}

import pl.edu.pw.ii.entityminer.util.{FileHelper, NetHelper}

import scala.collection.JavaConversions.iterableAsScalaIterable
import pl.edu.pw.ii.entityminer.Conf
import org.w3c.dom.DOMException
import java.net.{MalformedURLException, URISyntaxException, UnknownHostException}

import pl.edu.pw.ii.entityminer.database.datasource.wikicfp.WikicfpDatasource
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired
import pl.edu.pw.ii.entityminer.crawler.dom.DomParser
import pl.edu.pw.ii.entityminer.crawler.dynamicpage.HtmlUnitSpider
import com.gargoylesoftware.htmlunit.html.{HtmlAnchor, HtmlPage}

import scala.collection.mutable
import org.apache.commons.io.FileUtils
import org.apache.http.conn.ConnectTimeoutException
import pl.edu.pw.ii.entityminer.mining.statistics.ScrappedWebPageIteratorFactory
import javax.net.ssl.{SSLHandshakeException, SSLProtocolException}

import scala.concurrent.ExecutionContext.Implicits.global
import akka.actor.Props
import org.apache.http.client.ClientProtocolException
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import wedt.sag.extension.Actors.{WebCrawler}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.xml.{Elem, Node, NodeSeq}

/**
  * Created with IntelliJ IDEA.
  * User: Rafael Hazan
  * Date: 7/4/13
  * Time: 3:50 PM
  */
@Service
class PageDirectoryCollector {
  val INDEX = "0.html"
  val MANIFEST = "pageManifest"
  val SCRAPPED_FILE_NAME = "/scrapped.txt"

  @Autowired
  //  var httpClient: HttpClient = _
  var httpClient: HtmlUnitSpider = _

  @Autowired
  private var domParser: DomParser = _

  @Autowired
  private var iterFactory: ScrappedWebPageIteratorFactory = _

  def collectPages(conferenceData: ConferenceData, iter: Int, outDir: String) {
    val alreadyScrapped = readAlreadyScrappedPages(outDir)

    if (
      !alreadyScrapped.contains(conferenceData.uri) &&
        premilinaryValidUri(conferenceData.uri) &&
        iter >= 0
    ) {
      val nextDirNo = getNextDirNumber(outDir)
      val outDirForWebPage = outDir + Conf.SLASH + iter
      (new File(outDirForWebPage)).mkdirs()
      WikicfpDatasource.saveConference(conferenceData, outDirForWebPage)
      var subpageIter = 1

      //        val conferenceDomainName = NetHelper.getRootUri(conferenceData.uri)
      //        val response = httpClient.syncDownload(NetHelper.composeUri(conferenceDomainName, conferenceData.uri))
      //        val mainPage = domParser.cleanAndConvertToDocument(response)
      println(iter + " loading main: " + conferenceData.uri)
      val mainPage = try {
        if ("http://www.springer.com/computer/swe/journal/11334" == conferenceData.uri) {
          throw new RuntimeException("Scrapper couldn't effort this page. Ommiting.")
        }
        httpClient.getPageOmmitScriptsWaitForJs(conferenceData.uri)
      } catch {
        case e: ClientProtocolException => println("[collectPages] ClientProtocolException " + conferenceData.uri); null
        case e: SSLProtocolException =>  println("[collectPages] SSLProtocolException " + conferenceData.uri); null
        case e: SSLHandshakeException => println("[collectPages] SSLHandshakeException " + conferenceData.uri); null
        case e: UnknownHostException => println("[collectPages] UnknownHostException " + conferenceData.uri); null
        case e: ConnectTimeoutException => println("[collectPages] ConnectTimeoutException " + conferenceData.uri); null
        case e: ClassCastException => println("[collectPages] ClassCastException " + conferenceData.uri); null
        case e: RuntimeException => println("[RuntimeException] ClassCastException " + e.getMessage + " " + conferenceData.uri); null
      }
      if (mainPage != null) {
        val future = Future[PageDirectoryManifest] {
          val conferenceDomainName = mainPage.getUrl.getHost
          //        FileHelper.saveDocumentOnDisk(mainPage, outDirForWebPage + Conf.SLASH + INDEX)
          FileHelper.saveHtmlPage(mainPage, outDirForWebPage + Conf.SLASH + INDEX)
          //        val subpageNodes = (new DOMXPath("//a")).selectNodes(mainPage).toList.asInstanceOf[List[Node]]
          val subpageNodes = mainPage.getByXPath("//a").toList.asInstanceOf[List[HtmlAnchor]]
          val manifestBuilder = PageDirectoryManifest.getManifestBuilder()
          PageDirectoryManifest.addToBuilder(manifestBuilder, (conferenceData.uri ->(Set(INDEX), INDEX)))

          subpageNodes.foreach {
            case subpageNode =>
              if (subpageNode.getAttributes.getNamedItem(NetHelper.HREF_ATTRIBUTE) != null) {
                try {
                  //              val relativeSubPageUri = subpageNode.getAttributes.getNamedItem(NetHelper.HREF_ATTRIBUTE).getNodeValue
                  //              if (premilinaryValidUri(relativeSubPageUri)) {
                  val subpageLabel = subpageNode.getTextContent
                  //                val subpageDomainName = NetHelper.getRootUri(relativeSubPageUri) match {
                  //                  case null | "" => conferenceDomainName
                  //                  case smth => smth
                  //                }
                  //                val fullSubpageUri = NetHelper.composeUri(conferenceDomainName, relativeSubPageUri)
                  //                println("subpage: " + fullSubpageUri + " ||| from " + conferenceDomainName + " |and| " + relativeSubPageUri)

                  //                if (! fillManifestIfItContainsUri(manifestBuilder, fullSubpageUri, subpageLabel)) {
                  //                  if (conferenceDomainName == subpageDomainName ||
                  //                      relativeSubPageUri.startsWith(Conf.SLASH)) {
                  //                    val response = httpClient.syncDownload(fullSubpageUri)
                  val subPageUrl = mainPage.getFullyQualifiedUrl(subpageNode.getHrefAttribute())
                  try {
                    if (
                      conferenceDomainName == subPageUrl.getHost &&
                        premilinaryValidUri(subPageUrl.toString) &&
                        !fillManifestIfItContainsUri(manifestBuilder, subPageUrl.toString.replaceAll("#.*", ""), subpageLabel)
                    ) {
                      val response: HtmlPage = subpageNode.click()
                      if (response != null) {
                        if (subPageUrl.toString != response.getUrl.toString) {
                          println("not exact urls before and after loading anchor " + subPageUrl + " | " + response.getUrl)
                        }

                         //                       FileHelper.saveDocumentOnDisk(domParser.cleanAndConvertToDocument(response),
                          //                        outDirForWebPage + Conf.SLASH + subpageIter + NetHelper.HTML_SUFFIX)
                        //response.cleanUp()
                        FileHelper.saveHtmlPage(response, outDirForWebPage + Conf.SLASH + subpageIter + NetHelper.HTML_SUFFIX)
                        PageDirectoryManifest.addToBuilder(manifestBuilder, (response.getUrl.toString ->(Set(subpageLabel), subpageIter + NetHelper.HTML_SUFFIX)))
                        subpageIter += 1
                      }
                    }
                    //                  }
                  } catch {
                    case e: RuntimeException => println("[collectPages] RuntimeException for page " + subPageUrl) //; e.printStackTrace()
                    case e: ClassCastException => println("[collectPages] ClassCastException for page " + subPageUrl) //; e.printStackTrace()
                    case e: DOMException => println("[PageDirectoryCollector.collectPages] DomException")
                  }
                  //                }
                  //              }
                } catch {
                  case e: MalformedURLException =>
                    //e.printStackTrace()
                  case e: URISyntaxException =>
                    //e.printStackTrace()
                  case e: IllegalArgumentException =>
                   // e.printStackTrace()
                }
              }
          }
          val resManifest = PageDirectoryManifest.build(manifestBuilder)
          //            FileHelper.saveBinary(resManifest)(outDirForWebPage + Conf.SLASH + MANIFEST)
          saveManifestInXml(outDirForWebPage + Conf.SLASH + MANIFEST + ".xml", resManifest)
          addToAlreadyScrapped(outDir, conferenceData.uri, iter)

          resManifest
        }
        Await.result(future, Duration.Inf)
        Thread.sleep(1000)
        println("Created Manifest")
        //addIndexToManifestIfNotAdded(outDir,1000)
      }
    }
  }
  def collectPages(uri: String, iter: Int, outDir: String) {
    val alreadyScrapped = readAlreadyScrappedPages(outDir)

    if (
      !alreadyScrapped.contains(uri) &&
        premilinaryValidUri(uri) &&
        iter >= 0
    ) {
      val outDirForWebPage = outDir + Conf.SLASH + iter
      (new File(outDirForWebPage)).mkdirs()

      var subpageIter = 1
      println(iter + " loading main: " + uri)
      val mainPage = try {
        if ("http://www.springer.com/computer/swe/journal/11334" == uri) {
          throw new RuntimeException("Scrapper couldn't effort this page. Ommiting.")
        }
        httpClient.getPageOmmitScriptsWaitForJs(uri)
      } catch {
        case e: ClientProtocolException => /*println("[collectPages] ClientProtocolException " + uri);*/ null
        case e: SSLProtocolException => /*println("[collectPages] SSLProtocolException " + uri);*/ null
        case e: SSLHandshakeException => /*println("[collectPages] SSLHandshakeException " + uri);*/ null
        case e: UnknownHostException => /*println("[collectPages] UnknownHostException " + uri);*/ null
        case e: ConnectTimeoutException => /*println("[collectPages] ConnectTimeoutException " + uri);*/ null
        case e: ClassCastException => /*println("[collectPages] ClassCastException " + uri);*/ null
        case e: RuntimeException => /*println("[RuntimeException] ClassCastException " + e.getMessage + " " + uri);*/ null
      }
      if (mainPage != null) {
        val future = Future[PageDirectoryManifest] {
          val conferenceDomainName = mainPage.getUrl.getHost
          FileHelper.saveHtmlPage(mainPage, outDirForWebPage + Conf.SLASH + INDEX)
          val subpageNodes = mainPage.getByXPath("//a").toList.asInstanceOf[List[HtmlAnchor]]
          val manifestBuilder = PageDirectoryManifest.getManifestBuilder()
          PageDirectoryManifest.addToBuilder(manifestBuilder, (uri ->(Set(INDEX), INDEX)))

          subpageNodes.foreach {
            case subpageNode =>
              if (subpageNode.getAttributes.getNamedItem(NetHelper.HREF_ATTRIBUTE) != null) {
                try {
                  val subpageLabel = subpageNode.getTextContent
                  val subPageUrl = mainPage.getFullyQualifiedUrl(subpageNode.getHrefAttribute()) //catch malformedURLException
                  try {
                    if (
                      conferenceDomainName == subPageUrl.getHost &&
                        premilinaryValidUri(subPageUrl.toString) &&
                        !fillManifestIfItContainsUri(manifestBuilder, subPageUrl.toString.replaceAll("#.*", ""), subpageLabel)
                    ) {
                      val response: HtmlPage = subpageNode.click()
                      if (response != null) {
                        if (subPageUrl.toString != response.getUrl.toString) {
                          //println("not exact urls before and after loading anchor " + subPageUrl + " | " + response.getUrl)
                        }

                        FileHelper.saveHtmlPage(response, outDirForWebPage + Conf.SLASH + subpageIter + NetHelper.HTML_SUFFIX)
                        PageDirectoryManifest.addToBuilder(manifestBuilder, (response.getUrl.toString ->(Set(subpageLabel), subpageIter + NetHelper.HTML_SUFFIX)))
                        subpageIter += 1
                      }
                    }

                  } catch {
                    case e: RuntimeException => /*println("[collectPages] RuntimeException for page " + subPageUrl)*/ //; e.printStackTrace()
                    case e: ClassCastException =>/* println("[collectPages] ClassCastException for page " + subPageUrl)*/ //; e.printStackTrace()
                    case e: DOMException => /*println("[PageDirectoryCollector.collectPages] DomException")*/
                  }

                } catch {
                  case e: MalformedURLException =>{
                    //e.printStackTrace()
                   // println("[collectPages] MalformedURLException")
                  }
                  case e: URISyntaxException =>
                    //e.printStackTrace()
                  case e: IllegalArgumentException =>
                    //e.printStackTrace()
                }
              }
          }
          val resManifest = PageDirectoryManifest.build(manifestBuilder)
          saveManifestInXml(outDirForWebPage + Conf.SLASH + MANIFEST + ".xml", resManifest)
          addToAlreadyScrapped(outDir, uri, iter)

          resManifest
        }
        Await.result(future, Duration.Inf)
        println("Created Manifest "+outDirForWebPage)

      }
    }
  }

  def collectPages(data: Set[ConferenceData], outDir: String) {
    val alreadyScrapped = readAlreadyScrappedPages(outDir)

    data.foldLeft(0) {
      case (iter, conferenceData) =>
        if (
          !alreadyScrapped.contains(conferenceData.uri) &&
            premilinaryValidUri(conferenceData.uri) &&
            iter >= 0
        ) {
          val nextDirNo = getNextDirNumber(outDir)
          val outDirForWebPage = outDir + Conf.SLASH + nextDirNo
          (new File(outDirForWebPage)).mkdirs()
          WikicfpDatasource.saveConference(conferenceData, outDirForWebPage)
          var subpageIter = 1

          //        val conferenceDomainName = NetHelper.getRootUri(conferenceData.uri)
          //        val response = httpClient.syncDownload(NetHelper.composeUri(conferenceDomainName, conferenceData.uri))
          //        val mainPage = domParser.cleanAndConvertToDocument(response)
          println(iter + " loading main: " + conferenceData.uri)
          val mainPage = try {
            if ("http://www.springer.com/computer/swe/journal/11334" == conferenceData.uri) {
              throw new RuntimeException("Scrapper couldn't effort this page. Ommiting.")
            }
            httpClient.getPageOmmitScriptsWaitForJs(conferenceData.uri)
          } catch {
            case e: ClientProtocolException => println("[collectPages] ClientProtocolException " + conferenceData.uri); null
            case e: SSLProtocolException => println("[collectPages] SSLProtocolException " + conferenceData.uri); null
            case e: SSLHandshakeException => println("[collectPages] SSLHandshakeException " + conferenceData.uri); null
            case e: UnknownHostException => println("[collectPages] UnknownHostException " + conferenceData.uri); null
            case e: ConnectTimeoutException => println("[collectPages] ConnectTimeoutException " + conferenceData.uri); null
            case e: ClassCastException => println("[collectPages] ClassCastException " + conferenceData.uri); null
            case e: RuntimeException => println("[RuntimeException] ClassCastException " + e.getMessage + " " + conferenceData.uri); null
          }
          if (mainPage != null) {
            val conferenceDomainName = mainPage.getUrl.getHost
            //        FileHelper.saveDocumentOnDisk(mainPage, outDirForWebPage + Conf.SLASH + INDEX)
            FileHelper.saveHtmlPage(mainPage, outDirForWebPage + Conf.SLASH + INDEX)
            //        val subpageNodes = (new DOMXPath("//a")).selectNodes(mainPage).toList.asInstanceOf[List[Node]]
            val subpageNodes = mainPage.getByXPath("//a").toList.asInstanceOf[List[HtmlAnchor]]
            val manifestBuilder = PageDirectoryManifest.getManifestBuilder()
            PageDirectoryManifest.addToBuilder(manifestBuilder, (conferenceData.uri ->(Set(INDEX), INDEX)))

            subpageNodes.foreach {
              case subpageNode =>
                if (subpageNode.getAttributes.getNamedItem(NetHelper.HREF_ATTRIBUTE) != null) {
                  try {
                    //              val relativeSubPageUri = subpageNode.getAttributes.getNamedItem(NetHelper.HREF_ATTRIBUTE).getNodeValue
                    //              if (premilinaryValidUri(relativeSubPageUri)) {
                    val subpageLabel = subpageNode.getTextContent
                    //                val subpageDomainName = NetHelper.getRootUri(relativeSubPageUri) match {
                    //                  case null | "" => conferenceDomainName
                    //                  case smth => smth
                    //                }
                    //                val fullSubpageUri = NetHelper.composeUri(conferenceDomainName, relativeSubPageUri)
                    //                println("subpage: " + fullSubpageUri + " ||| from " + conferenceDomainName + " |and| " + relativeSubPageUri)

                    //                if (! fillManifestIfItContainsUri(manifestBuilder, fullSubpageUri, subpageLabel)) {
                    //                  if (conferenceDomainName == subpageDomainName ||
                    //                      relativeSubPageUri.startsWith(Conf.SLASH)) {
                    //                    val response = httpClient.syncDownload(fullSubpageUri)
                    val subPageUrl = mainPage.getFullyQualifiedUrl(subpageNode.getHrefAttribute())
                    try {
                      if (
                        conferenceDomainName == subPageUrl.getHost &&
                          premilinaryValidUri(subPageUrl.toString) &&
                          !fillManifestIfItContainsUri(manifestBuilder, subPageUrl.toString.replaceAll("#.*", ""), subpageLabel)
                      ) {
                        val response: HtmlPage = subpageNode.click()
                        if (response != null) {
                          if (subPageUrl.toString != response.getUrl.toString) {
                            println("not exact urls before and after loading anchor " + subPageUrl + " | " + response.getUrl)
                          }

                          //                        FileHelper.saveDocumentOnDisk(domParser.cleanAndConvertToDocument(response),
                          //                          outDirForWebPage + Conf.SLASH + subpageIter + NetHelper.HTML_SUFFIX)

                          FileHelper.saveHtmlPage(response, outDirForWebPage + Conf.SLASH + subpageIter + NetHelper.HTML_SUFFIX)
                          PageDirectoryManifest.addToBuilder(manifestBuilder, (response.getUrl.toString ->(Set(subpageLabel), subpageIter + NetHelper.HTML_SUFFIX)))
                          subpageIter += 1
                        }
                      }
                      //                  }
                    } catch {
                      case e: RuntimeException => println("[collectPages] RuntimeException for page " + subPageUrl) //; e.printStackTrace()
                      case e: ClassCastException => println("[collectPages] ClassCastException for page " + subPageUrl) //; e.printStackTrace()
                      case e: DOMException => println("[PageDirectoryCollector.collectPages] DomException")
                    }
                    //                }
                    //              }
                  } catch {
                    case e: MalformedURLException =>
                      e.printStackTrace()
                    case e: URISyntaxException =>
                      e.printStackTrace()
                    case e: IllegalArgumentException =>
                      e.printStackTrace()
                  }
                }
            }
            val resManifest = PageDirectoryManifest.build(manifestBuilder)
            //            FileHelper.saveBinary(resManifest)(outDirForWebPage + Conf.SLASH + MANIFEST)
            saveManifestInXml(outDirForWebPage + Conf.SLASH + MANIFEST + ".xml", resManifest)
            addToAlreadyScrapped(outDir, conferenceData.uri, nextDirNo)
          }
        }
        iter + 1

    }

  }

  def addIndexToManifestIfNotAdded(documentDirectory: String, totalPages: Int) = {
    for (i <- 0 to totalPages - 1) {
      val documentPath = documentDirectory + "/" + i
      if (FileHelper.fileExists(documentPath + Conf.SLASH + Conf.MANIFEST_INDEX_ENTRY)) {
        val manifest = readPageManifestFromXml(documentPath)
        if(manifest!=null) {
          var containsIndex = false
          val builder = PageDirectoryManifest.getManifestBuilder()
          manifest.result().foreach {
            manifestEntry =>
              builder += manifestEntry
              if (PageDirectoryManifest.getFileNameOnDrive(manifestEntry) == Conf.MANIFEST_INDEX_ENTRY) {
                containsIndex = true
              }
          }
          if (!containsIndex && FileHelper.fileExists(documentPath + Conf.SLASH + Conf.MANIFEST_INDEX_ENTRY)) {
            println("Adding index for " + i)
            PageDirectoryManifest.addToBuilder(builder, ("hand added" ->(Set(Conf.MANIFEST_INDEX_ENTRY), Conf.MANIFEST_INDEX_ENTRY)))
            saveManifestInXml(documentPath + Conf.SLASH + MANIFEST + ".xml", PageDirectoryManifest.build(builder))
          }
        }
      }
    }
  }

  def removeNotAnnotatedPages(documentDirectory: String, totalPages: Int) = {
    val indexFileName = "0.html"
    var directoryDeleted = 0
    val fileIterator = iterFactory.buildIter(documentDirectory, 0, totalPages)
    fileIterator.foreach {
      case (conferenceData, manifestEntry, document, documentPath) =>
        val annotationOccuredNo = ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.values.foldLeft(0) {
          case (occurrences, annotation) =>
            occurrences + (document.select(annotation).toList match {
              case Nil => 0
              case _ => 1
            })
        }
        if (annotationOccuredNo == 0 && manifestEntry._2._2 == indexFileName) {
          directoryDeleted += 1
          println("Usuwam " + directoryDeleted + " folder " + documentPath)
          FileHelper.deleteDirectory(documentPath)
        } else if (annotationOccuredNo == 0) {
          val res = FileUtils.deleteQuietly(new File(documentPath + "/" + manifestEntry._2._2))
          if (!res) {
            println("file " + documentPath + "/" + manifestEntry._2._2 + " couldnt be removed!")
          }
        }
    }
  }

  def deleteWebPageResources(documentsDirectory: String, totalPages: Int) = {
    for (i <- 0 to totalPages - 1) {
      for (j <- 0 to 1000) {
        val documentPath = documentsDirectory + "/" + i + "/" + j
        FileHelper.deleteDirectory(documentPath)
      }
    }
  }

  def saveManifestsInXml(documentsDirectory: String, totalPages: Int) = {
    try {
      for (i <- 0 to totalPages - 1) {
        val manifest = this.readPageManifest(documentsDirectory + Conf.SLASH + i)
        this.saveManifestInXml(documentsDirectory + Conf.SLASH + i + Conf.SLASH + MANIFEST + ".xml", manifest)
        val data = WikicfpDatasource.loadConference(documentsDirectory + Conf.SLASH + i)
        WikicfpDatasource.saveConference(data, documentsDirectory + Conf.SLASH + i)
      }
    } catch {
      case e: FileNotFoundException =>
    }
  }

  private def premilinaryValidUri(relativeSubPageUri: String): Boolean = {
    relativeSubPageUri.nonEmpty &&
      (relativeSubPageUri.size == 1 || relativeSubPageUri.charAt(1) != ':') &&
      !relativeSubPageUri.endsWith(".pdf") &&
      !relativeSubPageUri.endsWith(".doc") &&
      !relativeSubPageUri.endsWith(".zip") &&
      !relativeSubPageUri.contains("mailto")
  }

  def readPageManifest(mainDir: String) = {
    val fileIn = new FileInputStream(mainDir + Conf.SLASH + MANIFEST)

    val in = new ObjectInputStream(fileIn);
    val pageManifest = in.readObject().asInstanceOf[PageDirectoryManifest];
    in.close();
    fileIn.close();

    pageManifest
  }

  def readPageManifestFromXml(mainDir: String): PageDirectoryManifest = {
    var node: Elem = null
    try {
      node = xml.XML.loadFile(mainDir + Conf.SLASH + MANIFEST + ".xml")
    }
    catch{
      case e: FileNotFoundException => println("No data for seached manifest!")
        return null
    }
    val entries = node \ "entry"
    val manifestBuilder = PageDirectoryManifest.getManifestBuilder()
    for (entry <- entries) {
      val webAddress = (entry \ "address").text.trim
      val labels = entry \ "labels"
      var labelSet = Set[String]()
      for (label <- labels \ "label") {
        labelSet = labelSet + label.text.trim
      }
      val filePath = (entry \ "path").text.trim

      PageDirectoryManifest.addToBuilder(manifestBuilder, webAddress ->(labelSet, filePath))
    }
    PageDirectoryManifest.build(manifestBuilder)
  }



  private def manifestToXml(m: PageDirectoryManifest) = {
    <manifest>
      {for ((webAddress, (labels, filePath)) <- m.result())
      yield
        <entry>
          <address>
            {webAddress}
          </address>
          <labels>
            {for (label <- labels)
            yield
              <label>
                {label}
              </label>}
          </labels>
          <path>
            {filePath}
          </path>
        </entry>}
    </manifest>
  }

  def saveManifestInXml(filePath: String, manifest: PageDirectoryManifest) = {
    scala.xml.XML.save(filePath, this.manifestToXml(manifest), "UTF-8", true, null)
  }

  private def fillManifestIfItContainsUri(manifest: PageDirectoryManifest.Builder, subpageUri: String, label: String): Boolean = {
    manifest.result().foreach {
      case (uri: String, (labels: Set[String], localUri: String)) =>
        if (subpageUri == uri) {
          PageDirectoryManifest.addToBuilder(manifest, (uri ->(labels + label, localUri)))
          return true
        }
    }
    false
  }

  private def readAlreadyScrappedPages(outDir: String) = {
    if (!FileHelper.fileExists(outDir + SCRAPPED_FILE_NAME)) {
      Set[String]()
    } else {
      val helper = new FileHelper(new File(outDir + SCRAPPED_FILE_NAME))
      val builder = new mutable.SetBuilder[String, Set[String]](Set[String]())
      var isItConferenceUrl = false
      helper.foreachLine {
        conferenceUrl => if (isItConferenceUrl) builder += conferenceUrl; isItConferenceUrl = !isItConferenceUrl
      }
      builder.result()
    }
  }

  private def addToAlreadyScrapped(outDir: String, conferenceUri: String, dirNo: Int) = {
    FileHelper.appendFile(outDir + SCRAPPED_FILE_NAME, dirNo + "\n" + conferenceUri + "\n")
  }

  private def getNextDirNumber(outDir: String) = {
    if (FileHelper.fileExists(outDir + SCRAPPED_FILE_NAME)) {
      val helper = new FileHelper(new File(outDir + SCRAPPED_FILE_NAME))
      var isItDirNo = true
      var previousDirNo = -1
      var nextDirNo = -1
      helper.foreachLine {
        row =>
          if (isItDirNo) {
            previousDirNo = nextDirNo
            nextDirNo = row.toInt
            if (previousDirNo + 1 < nextDirNo) {
              for (i <- previousDirNo + 1 to nextDirNo - 1) {
                if (FileHelper.fileExists(outDir + "/" + i)) {
                  // FileUtils.deleteDirectory(new File(outDir + "/" + i))
                }
              }
            }
          }
          isItDirNo = !isItDirNo
      }
      nextDirNo += 1
      if (FileHelper.fileExists(outDir + "/" + nextDirNo)) {
        nextDirNo += 1
        //FileUtils.deleteDirectory(new File(outDir + "/" + nextDirNo))
      }
      nextDirNo
    } else {
      0
    }
  }
}
