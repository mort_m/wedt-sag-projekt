package pl.edu.pw.ii.entityminer.mining.train

import java.io.File

import cc.factorie.app.nlp
import cc.factorie.app.nlp.{Document, Token}
import cc.factorie.model.TemplateModel
import de.bwaldvogel.liblinear._
import pl.edu.pw.ii.entityminer.mining.factorie.FactorieInferencer
import pl.edu.pw.ii.entityminer.mining.factorie.TreeLabelCrfModel.TokenFeatures
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.{ConferenceLabelDomain, LabeledTreeNerTag}

import scala.collection.mutable.ListBuffer

/**
 * Created by Raphael Hazan on 7/19/2014.
 */
class SvmLiblinearTrainer extends FactorieInferencer {
  private var model: Model = _

  override def inference(document: Document, unusedModel: TemplateModel, year: String): Document = {
    for (token <- document.tokens) {
      val nodes: Array[Feature] = wordToNode(token)
      val probabilities = new Array[Double](ConferenceLabelDomain.size)
      Linear.predictProbability(model, nodes, probabilities)
      val bestChoice = Linear.predict(model, nodes)

      //      val bestChoice = probabilities.zipWithIndex.maxBy(_._1)._2
      token.attr[LabeledTreeNerTag] := bestChoice.toInt
    }
    document
  }

  def train(trainSet: Seq[nlp.Document]) = {
    println("SVM Liblinear train started")

    val documentNodes = new ListBuffer[Array[Feature]]()
    val documentLabels = new ListBuffer[Double]()
    var featuresNo = trainSet.head.tokens.head.attr[TokenFeatures].value.asArray.size

    var count = 0
    for (document <- trainSet) {
      count += 1
      if (count % 30 == 0) println("svm data " + count)

      for (token <- document.tokens) {

        val nodes: Array[Feature] = wordToNode(token)
        documentNodes += nodes

        documentLabels += token.attr[LabeledTreeNerTag].target.intValue
      }
    }

    val problem = new Problem()
    problem.l = documentNodes.size
    problem.n = featuresNo
    problem.x = documentNodes.toArray
    problem.y = documentLabels.toArray

    val solver = SolverType.L2R_LR // -s 0
    val C = 1 // cost of constraints violation
    val eps = 0.01 // stopping criteria

    val parameter = new Parameter(solver, C, eps)
    model = Linear.train(problem, parameter)
    val modelFile = new File("data/model/liblinear.txt")
    model.save(modelFile)
  }

  def wordToNode(token: Token): Array[Feature] = {
    //    val ones = token.attr[TokenFeatures].value.asArray.zipWithIndex.filter(d => (d._1 - 1.0).abs < 0.01)
    val ones = token.attr[TokenFeatures].value.asArray.zipWithIndex.filter(d => d._1 == 1.0)
    val nodes = new Array[Feature](ones.size)
    var i = 0
    for (featureOne <- ones) {
      val node = new FeatureNode(featureOne._2 + 1, featureOne._1)
      nodes(i) = node
      i += 1
    }
    nodes
  }

  def loadModel() = {
    val modelFile = new File("data/model/liblinear.txt")
    model = Model.load(modelFile)
  }
}
