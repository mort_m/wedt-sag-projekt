package pl.edu.pw.ii.entityminer.mining.nlp

import org.springframework.stereotype.Service
import org.jsoup.nodes.Document
import pl.edu.pw.ii.entityminer.database.model.token.WebToken

/**
 * Created by Raphael Hazan on 17.01.14.
 */
trait WebPageTokenizer {
  def tokenize(document: Document): List[WebToken]
}
