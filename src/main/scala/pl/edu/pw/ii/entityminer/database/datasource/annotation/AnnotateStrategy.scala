package pl.edu.pw.ii.entityminer.database.datasource.annotation

import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import org.jsoup.nodes.TextNode

/**
 * Created by Raphael Hazan on 1/9/14.
 */
trait AnnotateStrategy {
  val annotationTagName: String

  /**
   *
   * @param tokens
   * @param seq
   * @return (occurence tokens, rest tokens not analyzed yet)
   */
  def findFirstOccurence(tokens: List[WebToken], seq: List[String]): (List[WebToken], List[WebToken])

  /**
   *
   * @param conferenceData
   * @return list of different annotation values in simple string list of tokens notion
   */
  def getValueTokens(conferenceData: ConferenceData): List[List[String]]

  def tokenize(text: String, node: TextNode): List[WebToken]
}
