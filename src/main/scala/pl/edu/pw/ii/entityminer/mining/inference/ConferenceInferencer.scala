package pl.edu.pw.ii.entityminer.mining.inference

import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData

/**
 * Created by Raphael Hazan on 7/9/2014.
 */

/**
 * Conference data extractor.
 */
trait ConferenceInferencer {
  def inference(mainPageUri: String): ConferenceData
}
