package pl.edu.pw.ii.entityminer.mining.train

import cc.factorie.app.nlp
import cc.factorie.app.nlp.{Document, Token}
import cc.factorie.model.TemplateModel
import libsvm._
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import pl.edu.pw.ii.entityminer.mining.factorie.FactorieInferencer
import pl.edu.pw.ii.entityminer.mining.factorie.TreeLabelCrfModel.{TokenFeaturesDomain, TokenFeatures}
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.{ConferenceLabelDomain, LabeledTreeNerTag}

import scala.collection.mutable.ListBuffer

/**
 * Created by Raphael Hazan on 7/19/2014.
 */
class SvmRbfTrainer extends FactorieInferencer {
  private var model: svm_model = _

  override def inference(document: Document, unusedModel: TemplateModel, year: String): Document = {
    for (token <- document.tokens) {
      val nodes: Array[svm_node] = wordToNode(token)
      val probabilities = new Array[Double](ConferenceLabelDomain.size)
//      svm.svm_predict_probability(model, nodes, probabilities)
      val bestChoice = svm.svm_predict(model, nodes)

//      val bestChoice = probabilities.zipWithIndex.maxBy(_._1)._2
      token.attr[LabeledTreeNerTag] := bestChoice.toInt
    }
    document
  }

  def train(trainSet: Seq[nlp.Document]) = {
    println("SVM RBF train started")

    val documentNodes = new ListBuffer[Array[svm_node]]()
    val documentLabels = new ListBuffer[Double]()

    var count = 0
    for (document <- trainSet) {
      count += 1
      if (count % 30 == 0) println("svm data " + count)

      for (token <- document.tokens) {

        val nodes: Array[svm_node] = wordToNode(token)
        documentNodes += nodes

        documentLabels += token.attr[LabeledTreeNerTag].target.intValue
      }
    }
    val problem = new svm_problem()
    problem.l = documentNodes.size
    problem.x = new Array[Array[svm_node]](problem.l)
    var i = 0
    for (node <- documentNodes) {
      problem.x(i) = node
      i += 1
    }
    problem.y = new Array[Double](problem.l)
    i = 0
    for (label <- documentLabels) {
      problem.y(i) = label
      i += 1
    }

    val param = new svm_parameter()
    param.svm_type = svm_parameter.C_SVC
    param.kernel_type = svm_parameter.RBF
    param.degree = 3
    param.gamma = 1 //// TokenFeaturesDomain.dimensionSize
    param.coef0 = 0
    param.nu = 0.5
    param.cache_size = 100
    param.C = 1000
    param.eps = 1e-3
    param.p = 0.1
    param.shrinking = 1
    param.probability = 0
    param.nr_weight = 0
    param.weight_label = new Array[Int](0)
    param.weight = new Array[Double](0)

//    addWeight(param, ConferenceHtmlAnnotation.NAME, 200.0)
//    addWeight(param, ConferenceHtmlAnnotation.ABBREVIATION, 200.0)
//    addWeight(param, ConferenceHtmlAnnotation.WHERE, 200.0)
//    addWeight(param, ConferenceHtmlAnnotation.WHEN, 200.0)

    val crossValidation = false
    val nr_fold = 4

    if (crossValidation) {
      crossValidate(problem, param, nr_fold)
    }
    model = svm.svm_train(problem, param)
    println("svm rbf training done")
    svm.svm_save_model("data/model/svm_model", model)
  }

  def loadModel() = {
    model = svm.svm_load_model("data/model/svm_model")
  }

  def wordToNode(token: Token): Array[svm_node] = {
    //    val ones = token.attr[TokenFeatures].value.asArray.zipWithIndex.filter(d => (d._1 - 1.0).abs < 0.01)
    val ones = token.attr[TokenFeatures].value.asArray.zipWithIndex.filter(d => d._1 == 1.0)
    val nodes = new Array[svm_node](ones.size)
    var i = 0
    for (featureOne <- ones) {
      val node = new svm_node()
      node.index = featureOne._2 + 1;
      node.value = featureOne._1
      nodes(i) = node
      i += 1
    }
    nodes
  }

  def addWeight(param: svm_parameter, weightLabelStr: String, weight: Double) = {
    val weightLabel = ConferenceLabelDomain.index(weightLabelStr)
    param.nr_weight += 1
      val old = param.weight_label
      param.weight_label = new Array[Int](param.nr_weight)
      System.arraycopy(old,0,param.weight_label,0,param.nr_weight-1)

      val old2 = param.weight;
      param.weight = new Array[Double](param.nr_weight)
      System.arraycopy(old2,0,param.weight,0,param.nr_weight-1);

    param.weight_label(param.nr_weight - 1) = weightLabel
    param.weight(param.nr_weight - 1) = weight
  }

  def crossValidate(prob: svm_problem, param: svm_parameter, nr_fold: Int) = {
    var i = 0
    var total_correct = 0
    var total_error = 0.0
    var sumv = 0.0
    var sumy = 0.0
    var sumvv = 0.0
    var sumyy = 0.0
    var sumvy = 0.0
    val target = new Array[Double](prob.l)

    svm.svm_cross_validation(prob, param, nr_fold, target)
    if (param.svm_type == svm_parameter.EPSILON_SVR ||
      param.svm_type == svm_parameter.NU_SVR) {
      for (i <- 0 to prob.l - 1) {
        val y = prob.y(i)
        val v = target(i)
        total_error += (v - y) * (v - y)
        sumv += v
        sumy += y
        sumvv += v * v
        sumyy += y * y
        sumvy += v * y
      }
      System.out.print("Cross Validation Mean squared error = " + total_error / prob.l + "\n")
      System.out.print("Cross Validation Squared correlation coefficient = " +
        ((prob.l * sumvy - sumv * sumy) * (prob.l * sumvy - sumv * sumy)) /
          ((prob.l * sumvv - sumv * sumv) * (prob.l * sumyy - sumy * sumy)) + "\n"
      )
    } else {
      for (i <- 0 to prob.l - 1) {
        if (target(i) == prob.y(i)) {
          total_correct += 1
        }
      }
      System.out.print("Cross Validation Accuracy = " + 100.0 * total_correct / prob.l + "%\n");
    }
  }
}
