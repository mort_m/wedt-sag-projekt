package pl.edu.pw.ii.entityminer.mining.token.feature.discover

import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import scala.collection.mutable.ListBuffer
import pl.edu.pw.ii.entityminer.crawler.HtmlConsts
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation

/**
 * Created by Raphael Hazan on 2/26/14.
 */
class NlpFeatureDiscover extends FeatureDiscover {
  override def getPrefixes(): Set[String] = Set(Conf.PREFIX_FEAT_FIRST_IN_SENTENCE)

  override def discoverFeature(webToken: WebToken): List[String] = {
    (if (webToken.sentenceEntityIndex == 0)
      List(Conf.PREFIX_FEAT_FIRST_IN_SENTENCE + "true")
    else
      Nil
      ) ::: (
      Nil
      )
  }

  override def discoverFeature(entity: List[WebToken]): List[String] = discoverFeature(entity.head)
}
