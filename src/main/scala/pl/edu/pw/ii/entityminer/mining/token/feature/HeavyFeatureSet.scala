package pl.edu.pw.ii.entityminer.mining.token.feature

import pl.edu.pw.ii.entityminer.database.model.token.WebToken

/**
 * Created by Raphael Hazan on 28.12.13.
 */
case class HeavyFeatureSet(
                            entity: List[WebToken],
                            var labelss: List[String], //TODO unvar this, TODO typo
                            var features: List[String],
                            connections: scala.collection.mutable.Set[WebToken] = scala.collection.mutable.Set[WebToken](),
                            children: List[HeavyFeatureSet] = Nil
                            ) {

}
