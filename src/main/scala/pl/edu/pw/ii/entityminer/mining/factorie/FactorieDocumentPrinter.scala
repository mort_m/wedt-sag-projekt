package pl.edu.pw.ii.entityminer.mining.factorie

import cc.factorie.app.nlp.Document
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.{SkipTagWW, ConferenceLabelDomain, TreeNerTag, LabeledTreeNerTag}
import cc.factorie.optimize.{Trainer, LikelihoodExample}
import cc.factorie.infer.InferByBPChain
import cc.factorie.variable.HammingObjective
import cc.factorie.model.{DotTemplateWithStatistics2, DotTemplateWithStatistics1}
import cc.factorie._
import pl.edu.pw.ii.entityminer.mining.factorie.TreeLabelCrfModel.{TokenFeaturesDomain, TokenFeatures}
import org.springframework.stereotype.Service

/**
 * Created by Raphael Hazan on 12/31/13.
 */
@Service
class FactorieDocumentPrinter {
  private val strBuffer = new StringBuilder

  def printData(trainTestData: Seq[Document]) = {
    for (tags <- trainTestData.map(_.tokens.toSeq.map(_.attr[LabeledTreeNerTag]))) {
      println("Tokens for next DOCUMENT")

      var indentation = 0
      var parentStack = collection.mutable.Stack[LabeledTreeNerTag]()
      var prevNode: Option[LabeledTreeNerTag] = None

      for (tag <- tags) {
        val currParent = if (tag.token.attr[TreeTag].parent == null) None else Some(tag.token.attr[TreeTag].parent.attr[LabeledTreeNerTag])
        val lastParent: Option[LabeledTreeNerTag] = if (parentStack.isEmpty) None else Some(parentStack.top)
        currParent match {
          case None =>
            lastParent match {
              case None =>
              case Some(beforeParent) =>
                parentStack.pop
                indentation -= 1
            }
          case Some(parent) =>
            lastParent match {
              case None =>
                if (prevNode.nonEmpty && prevNode.get != parent) {
                  this.printTag(tag, indentation)
                  throw new Exception("bad tree structure!")
                }
                parentStack.push(currParent.get)
                indentation += 1
              case Some(beforeParent) =>
                if (beforeParent != parent) {
                  val tmp = parentStack.pop
                  if (parentStack.nonEmpty && parentStack.top == parent) {
                    indentation -= 1
                  } else {
                    parentStack.push(tmp)
                    parentStack.push(parent)
                    indentation += 1
                  }
                }
            }
        }
        this.printTag(tag, indentation)
        prevNode = Some(tag)
      }
    }
  }

  private def printTag(tag: LabeledTreeNerTag, indentation: Int) = {
    strBuffer.clear()
    for (i <- 0 to indentation - 1) {
      strBuffer append "  >"
    }
    strBuffer append tag.token.toString()
    println(strBuffer)
    if (indentation != 0 && indentation != 1 && indentation != 2) {
      throw new Exception("bad indentation depth!")
    }
  }

  def printChainData(trainTestData: Seq[Document]) = {
    for (tokens <- trainTestData.map(_.tokens)) {//.toSeq.map(_.attr[LabeledTreeNerTag]))) {
      println("Tokens for next DOCUMENT")

      for (token <- tokens) {
//        val prefix = if (token.attr[SkipTag] != null && token.attr[SkipTag].nextToken.isDefined) "[[[" else ""
//        val postfix = if (token.attr[SkipTag] != null && token.attr[SkipTag].prevToken.isDefined) "]]]" else ""
//        print(" next " + prefix + token.string + postfix)
        //TODO print with indexes of tokens
      }
    }
  }
}
