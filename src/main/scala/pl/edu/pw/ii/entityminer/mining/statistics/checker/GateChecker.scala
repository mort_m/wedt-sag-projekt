package pl.edu.pw.ii.entityminer.mining.statistics.checker

import org.springframework.context.support.{ClassPathXmlApplicationContext, AbstractApplicationContext}
import pl.edu.pw.ii.entityminer.{Conf, RootFacade}
import pl.edu.pw.ii.entityminer.mining.token.feature.{DomTokenizer, FactorieLinearCrfFeatureCreator}
import pl.edu.pw.ii.entityminer.mining.token.feature.discover.GateFeatureDiscover
import pl.edu.pw.ii.entityminer.mining.statistics.ScrappedWebPageIteratorFactory
import org.jsoup.nodes.TextNode

/**
 * Created by Raphael Hazan on 2014-05-16.
 */
object GateChecker {
  def main(args: Array[String]) = {
    val applicationContext: AbstractApplicationContext = new ClassPathXmlApplicationContext("META-INF/spring/module-context.xml")
    val rootFacade = applicationContext.getBean("RootFacade", classOf[RootFacade])
    val domTokenizer = rootFacade.featureCreator.asInstanceOf[FactorieLinearCrfFeatureCreator].featureTokenizer.asInstanceOf[DomTokenizer]
    val gateDiscover = domTokenizer.gateFeatureDiscover.asInstanceOf[GateFeatureDiscover]
    val iterFactory = applicationContext.getBean(classOf[ScrappedWebPageIteratorFactory])
    val tokenizer = domTokenizer.annotateStrategy

    val fileIterator = iterFactory.buildIter(Conf.DATA_STORAGE_ANNO, 0, 3333)
    fileIterator.foreach { case (conferenceData, manifestEntry, document, documentPath) =>
      if (conferenceData.place != "N/A]") {

        val tokens = tokenizer.tokenize(conferenceData.place.replaceAll("\\s+", " "), new TextNode(conferenceData.place, ""))
        val entities = domTokenizer.divideToEntitiesSimple(tokens).flatten

        val locationInGazetter = entities.foldLeft(false) {
          case (foundLocation, entity) =>
            if (!foundLocation) {
              gateDiscover.discoverFeature(entity).contains("LOC=location")
            } else {
              foundLocation
            }
        }
        if (!locationInGazetter) {
          println("location not in Gazetter: " + conferenceData.place)
        }
      }
    }
    println("done")
  }
}
