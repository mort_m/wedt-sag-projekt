package pl.edu.pw.ii.entityminer.mining.factorie

import cc.factorie._
import cc.factorie.infer.BP

// The base library: variables, factors

import cc.factorie.la._

// Linear algebra

import cc.factorie.optimize._

// Gradient-based optimization and training
// Declare random variable types

object ChainNER1 {

//  object TokenDomain extends CategoricalDomain[String]
//
//  class Token(val str: String, parentPosition: Int) extends CategoricalVariable(str) {
//    def getParentPosition() = parentPosition
//
//    def domain = TokenDomain
//  }
//
//  object LabelDomain extends CategoricalDomain[String]
//
//  class Label(str: String, val token: Token) extends LabeledCategoricalVariable(str) {
//    def domain = LabelDomain
//  }
//
//  class LabelSeq extends scala.collection.mutable.ArrayBuffer[Label]
//
//  def main(args: Array[String]) = {
//    // Create random variables from data
////    val data = List("See/V Spot/N run/V", "Spot/N is/V a/DET big/J dog/N", "He/N is/V fast/J jlj/O")
//    val trainData = List("a/1/0 b/2/-1 c/2/-2 d/3/-3 a/1/0",
//      "a/1/0 b/2/-1 c/3/0 d/3/0",
//      "b/2/0 a/1/0 c/3/0 d/3/0",
//      "a/1/0 c/2/-1 b/2/0",
//      "d/3/0 b/1/0 c/3/0 b/2/0",
//      "a/1/0 b/2/0 c/3/0 d/3/0",
//      "c/3/0 a/1/0 b/2/0 c/3/0")
//    val labelSequences = for (sentence <- trainData) yield new LabelSeq ++= sentence.split("\\s+").map(s => {
//      val a = s.split("/"); new Label(a(1), new Token(a(0), a(2).toInt))
//    })
//    // Define a model
//    val model = new Model with Parameters {
//      // Two families of factors, where factor scores are dot-products of sufficient statistics and weightsSet (which will be trained below)
//      val markov = new DotFamilyWithStatistics2[Label, Label] {
//        val weights = Weights(new DenseTensor2(LabelDomain.size, LabelDomain.size))
//      }
//      val observ = new DotFamilyWithStatistics2[Label, Token] {
//        val weights = Weights(new DenseTensor2(LabelDomain.size, TokenDomain.size))
//      }
//
//      // Given some variables, return the collection of factors that neighbor them.
//      override def factors(labels: Iterable[Var]) = labels match {
//        case labels: LabelSeq => labels.map(label => new observ.Factor(label, label.token)) ++ labels.sliding(2).map(window => new markov.Factor(window.head, window.last))
//      }
//
//      override def factors(v: Var) = throw new Error("This model does not implement unrolling from a single variable.")
//    }
//
//    val treeModel = new Model with Parameters {
//      // Two families of factors, where factor scores are dot-products of sufficient statistics and weightsSet (which will be trained below)
//      val markov = new DotFamilyWithStatistics2[Label, Label] {
//        val weights = Weights(new DenseTensor2(LabelDomain.size, LabelDomain.size))
//      }
//      val observ = new DotFamilyWithStatistics2[Label, Token] {
//        val weights = Weights(new DenseTensor2(LabelDomain.size, TokenDomain.size))
//      }
//      val parentv = new DotFamilyWithStatistics2[Label, Label] {
//        val weights = Weights(new DenseTensor2(LabelDomain.size, LabelDomain.size))
//      }
//
//      // Given some variables, return the collection of factors that neighbor them.
//      override def factors(labels: Iterable[Var]) = labels match {
//        case labels: LabelSeq =>
//          println("licze ")
//          labels.map(label =>
//          new observ.Factor(label, label.token)) ++ labels.sliding(2).map(window => new markov.Factor(window.head, window.last))// ++
////          {
////            var i = 0
////            labels.foldLeft(List[parentv.Factor]()) {
////              case (result, label) =>
////                i += 1
////                if (label.token.getParentPosition() != 0) {
////                  println ("nowy factor " + labels(i - 1 + label.token.getParentPosition()).token.str + " parent of " + label.token.str)
////                  new parentv.Factor(labels(i - 1 + label.token.getParentPosition()), label) :: result
////                } else {
////                  result
////                }
////            }
////          }
//      }
//
//      override def factors(v: Var) = throw new Error("This model does not implement unrolling from a single variable.")
//    }
//
//    // Learn parameters
//    val trainer = new BatchTrainer(model.parameters, new ConjugateGradient)
//    trainer.trainFromExamples(labelSequences.map(labels => new LikelihoodExample(labels, model, InferByBPChainSum)))
//    // Inference on the same data.  We could let FACTORIE choose the inference method,
//    // but here instead we specify that is should use max-product belief propagation specialized to a linear chain
//
////    val testData = List("Its/N nice/J to/DET see/N")
////    val testData = List("See/V Spot/N See/V Spot/O jljk/N")
//    val testData = List("a/1/0 b/2/-1 c/2/-2")
//    val testSeqs = for (sentence <- testData) yield new LabelSeq ++= sentence.split("\\s+").map(s => {
//      val a = s.split("/"); new Label(a(1), new Token(a(0), a(2).toInt))
//    })
//
//    testSeqs.foreach(labels => BP.inferChainMax(labels, model))
//    // Print the learned parameters on the Markov factors.
//    println(model.markov.weights)
//    // Print the inferred tags
//    testSeqs.foreach(_.foreach(l => println("Token: " + l.token.value + " Label: " + l.value)))
//  }
}