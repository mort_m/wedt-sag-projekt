package pl.edu.pw.ii.entityminer.mining.nlp


/**
 * Created by Raphael Hazan on 1/23/14.
 */
object TimeName extends Enumeration {
  type TimeName = Value
  val Mon, Tue, Wed, Thu, Fri, Sat, Sun = Value
  val Jan, Mar, May, Jul, Sep, Nov, Feb, Apr, Jun, Aug, Oct, Dec = Value
  val days = Set(Mon, Tue, Wed, Thu, Fri, Sat, Sun)
  val months = Set(Jan, Mar, May, Jul, Sep, Nov, Feb, Apr, Jun, Aug, Oct, Dec)

  private val timeAliases = {
    val map = collection.mutable.Map[String, TimeName]()
    map put ("mon", Mon)
    map put ("monday", Mon)
    map put ("m", Mon)
    map put ("tue", Tue)
    map put ("tuesday", Tue)
    map put ("t", Tue)
    map put ("wed", Wed)
    map put ("wednesday", Wed)
    map put ("w", Wed)
    map put ("thu", Thu)
    map put ("thursday", Thu)
    map put ("th", Thu)
    map put ("fri", Fri)
    map put ("friday", Fri)
    map put ("f", Fri)
    map put ("sat", Sat)
    map put ("saturday", Sat)
    map put ("s", Sat)
    map put ("sun", Sun)
    map put ("sunday", Sun)
    map put ("sn", Sun)

    map put ("jan", Jan)
    map put ("january", Jan)
    map put ("feb", Feb)
    map put ("february", Feb)
    map put ("mar", Mar)
    map put ("mach", Mar)
    map put ("march", Mar)
    map put ("apr", Apr)
    map put ("april", Apr)
    map put ("may", May)
    map put ("jun", Jun)
    map put ("june", Jun)
    map put ("jul", Jul)
    map put ("july", Jul)
    map put ("aug", Aug)
    map put ("august", Aug)
    map put ("sep", Sep)
    map put ("sept", Sep)
    map put ("september", Sep)
    map put ("oct", Oct)
    map put ("october", Oct)
    map put ("nov", Nov)
    map put ("november", Nov)
    map put ("dec", Dec)
    map put ("december", Dec)
    map.toMap
  }

  def isItDay(str: String) = {
    whatTimeIsIt(str) match {
      case None => false
      case Some(time) => days.contains(time)
    }
  }

  def isItMonth(str: String) = {
    whatTimeIsIt(str) match {
      case None => false
      case Some(time) => months.contains(time)
    }
  }

  def whatTimeIsIt(dayStr: String): Option[TimeName] = {
    timeAliases.get(dayStr.trim.toLowerCase())
  }
  
  def getTimeName(str: String): String = {
    if (str.matches("\\d+.*")) {
      str.takeWhile(_.isDigit)
    } else {
      whatTimeIsIt(str) match {
        case None => str
        case Some(timeName) => timeName.toString
      }
    }
  }
}
