package pl.edu.pw.ii.entityminer.mining.train

import cc.factorie.app.nlp
import cc.factorie.app.classify.backend.{LinearMulticlassClassifier, SVMMulticlassTrainer}
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.{LabeledTreeNerTag, ConferenceLabelDomain}
import scala.collection.mutable.ListBuffer
import cc.factorie.la.Tensor1
import pl.edu.pw.ii.entityminer.mining.factorie.TreeLabelCrfModel.TokenFeatures
import cc.factorie.app.nlp.Sentence
import cc.factorie.app.nlp.parse.TransitionBasedParser
import java.io.{BufferedInputStream, BufferedOutputStream, File}
import cc.factorie.util.BinarySerializer
import cc.factorie.la

/**
 * Created by Raphael Hazan on 5/13/2014.
 */
class SvmLinearTrainer(inputDimension: Int) {
  implicit private val random = new scala.util.Random(0)

  val trainer = new SVMMulticlassTrainer(1)

  val model = new LinearMulticlassClassifier(ConferenceLabelDomain.size,
    inputDimension)

  def train(trainSet: Seq[nlp.Document]) = {

//    def testSingle(c: TransitionBasedParser, ss: Seq[Sentence], extraText: String = ""): Unit = {
//      if (ss.nonEmpty) {
//        println(extraText + " " + c.testString(ss))
//      }
//    }
//    def testAll(c: TransitionBasedParser, extraText: String = ""): Unit = {
//      println("\n")
//      testSingle(c, sentences, "Train " + extraText)
//      testSingle(c, devSentences, "Dev " + extraText)
//      testSingle(c, testSentences, "Test " + extraText)
//    }
    def evaluate(cls: LinearMulticlassClassifier) {
      println(cls.weights.value.toSeq.count(x => x == 0).toFloat / cls.weights.value.length + " sparsity")
//      testAll(c, "iteration ")
    }

    val labels = new ListBuffer[Int]()
    val features = new ListBuffer[Tensor1]()
    val weights = new ListBuffer[Double]()
    for (document <- trainSet) {
      labels ++= document.tokens.map(_.attr[LabeledTreeNerTag].target.intValue).toSeq
      features ++= document.tokens.map(_.attr[TokenFeatures].value).toSeq
      weights ++= document.tokens.map(v => 1.0).toSeq
    }

    trainer.baseTrain(model, labels.result(), features.result, weights.result, evaluate)
    this
  }

  def saveModel(filePath: String) = {
    serialize(new java.io.File(filePath))
  }

  def loadModel(filePath: String) = {
    deserialize(new java.io.File(filePath)).asInstanceOf[LinearMulticlassClassifier]
  }

  private def serialize(file: File): Unit = {
    if (file.getParentFile ne null) file.getParentFile.mkdirs()
    serialize(new java.io.FileOutputStream(file))
  }

  private def deserialize(file: File): LinearMulticlassClassifier = {
    require(file.exists(), "Trying to load non-existent file: '" + file)
    deserialize(new java.io.FileInputStream(file))
  }

  private def serialize(stream: java.io.OutputStream): Unit = {
    import cc.factorie.util.CubbieConversions._
    // Sparsify the evidence weights
    import scala.language.reflectiveCalls

    //    val labelDomain = ConferenceLabelDomain
    //    val featuresDomain =

    //    val sparseEvidenceWeights = new la.DenseLayeredTensor2(featuresDomain.dimensionDomain.size, labelDomain.size, new la.SparseIndexedTensor1(_))
    //    model.weights.value.foreachElement((i, v) => if (v != 0.0) sparseEvidenceWeights +=(i, v))
    //    model.weights.set(sparseEvidenceWeights)
    val dstream = new java.io.DataOutputStream(new BufferedOutputStream(stream))
    //    BinarySerializer.serialize(featuresDomain.dimensionDomain, dstream)
    //    BinarySerializer.serialize(labelDomain, dstream)
    BinarySerializer.serialize(model, dstream)
    dstream.close()
  }

  def deserialize(stream: java.io.InputStream): LinearMulticlassClassifier = {
    import cc.factorie.util.CubbieConversions._
    // Get ready to read sparse evidence weights
    val dstream = new java.io.DataInputStream(new BufferedInputStream(stream))
    //    BinarySerializer.deserialize(featuresDomain.dimensionDomain, dstream)
    //    BinarySerializer.deserialize(labelDomain, dstream)
    import scala.language.reflectiveCalls
    //    model.weights.set(new la.DenseLayeredTensor2(featuresDomain.dimensionDomain.size, ConferenceLabelDomain.size, new la.SparseIndexedTensor1(_)))
//    model.weights.set(new la.DenseLayeredTensor2(model.featureSize, ConferenceLabelDomain.size, new la.SparseIndexedTensor1(_)))
    BinarySerializer.deserialize(model, dstream)
    println("TransitionBasedParser model parameters oneNorm " + model.parameters.oneNorm)
    dstream.close()
    model
  }
}