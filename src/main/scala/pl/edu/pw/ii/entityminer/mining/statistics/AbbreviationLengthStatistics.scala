package pl.edu.pw.ii.entityminer.mining.statistics

import org.jsoup.nodes.Document
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryManifest._
import scala.Some
import pl.edu.pw.ii.entityminer.database.datasource.annotation.{ConferenceAbbreviationAnnotator, ConferenceHtmlAnnotation}
import org.springframework.beans.factory.annotation.Autowired
import pl.edu.pw.ii.entityminer.mining.nlp.WebPageTokenizer

/**
 * Created by Raphael Hazan on 17.01.14.
 */
case class AbbreviationLengthStatistics(lengthToNumber: scala.collection.mutable.Map[Int, Int]) extends DataStatistics {
  val CODE: String = "abbr length"

  override def accumulate(other: DataStatistics): Unit = {
    for ((otherLen, otherNumber) <- other.asInstanceOf[AbbreviationLengthStatistics].lengthToNumber) {
      lengthToNumber.get(otherLen) match {
        case None => lengthToNumber put(otherLen, otherNumber)
        case Some(thisNumber) => lengthToNumber put(otherLen, thisNumber + otherNumber)
      }
    }
  }
}

class AbbreviationLengthStatisticsComputer extends StatisticsComputer {

  @Autowired
  private var webPageTokenizer: WebPageTokenizer = _

  def computeStatistics(conferenceData: ConferenceData, document: Document, manifestEntry: ManifestEntry, documentPath: String): DataStatistics = {
    val tokens = webPageTokenizer.tokenize(document)
    val resultMap = collection.mutable.Map[Int, Int]()

    val abbreviationAnnotator = new ConferenceAbbreviationAnnotator
    val abbrs = abbreviationAnnotator.getValueTokens(conferenceData).head
    if (abbrs.nonEmpty) {
      resultMap put(abbrs.head.size, 1)
    }
    return new AbbreviationLengthStatistics(resultMap)
  }
}