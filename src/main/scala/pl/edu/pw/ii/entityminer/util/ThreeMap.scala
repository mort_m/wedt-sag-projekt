package pl.edu.pw.ii.entityminer.util

/**
 * Created by Raphael Hazan on 5/11/2014.
 */
class ThreeMap[K1, K2, V] {
  val wrapped = new scala.collection.mutable.HashMap[(K1, K2), V]

  def put(k1: K1)(k2: K2)(v: V) = wrapped.put((k1, k2), v)

  def update(k1: K1, k2: K2, v: V, f: (V, V) => V) = {
    wrapped.get((k1, k2)) match {
      case None =>
        put(k1)(k2)(v)
      case Some(thisV) =>
        put(k1)(k2)(f(thisV, v))
    }
  }

  def containsKey(k1: K1)(k2: K2) = wrapped.contains((k1, k2))

  def containsFirstKey(k1: K1) = wrapped.filterKeys(_._1 == k1).nonEmpty

  def immutableMap = wrapped.toMap

  def byFirstKey(k1: K1) = wrapped.filter(_._1._1 == k1).map(x => x._1._2 -> x._2).toMap

  def apply(k1: K1)(k2: K2) = wrapped((k1, k2))

  override def toString: String = "ThreeMap: " + wrapped.toString
}