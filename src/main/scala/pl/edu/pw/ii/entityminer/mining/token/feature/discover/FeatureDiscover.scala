package pl.edu.pw.ii.entityminer.mining.token.feature.discover

import pl.edu.pw.ii.entityminer.database.model.token.WebToken

/**
 * Created by Raphael Hazan on 2/26/14.
 */
trait FeatureDiscover {

  def getPrefixes(): Set[String]

  def discoverFeature(webToken: WebToken): List[String]
  def discoverFeature(entity: List[WebToken]): List[String]

  protected def discoverFeaturesLocallyAndSum(entity: List[WebToken]): List[String] = {
    entity.foldLeft(Set[String]()) {
      case (set, webToken) =>
        set ++ discoverFeature(webToken)
    }.toList
  }
}
