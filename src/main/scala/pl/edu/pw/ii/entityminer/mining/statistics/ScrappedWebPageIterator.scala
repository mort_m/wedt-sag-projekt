package pl.edu.pw.ii.entityminer.mining.statistics

import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.{PageDirectoryCollector, PageDirectoryManifest}
import org.jsoup.nodes.Document
import pl.edu.pw.ii.entityminer.util.FileHelper
import pl.edu.pw.ii.entityminer.database.datasource.wikicfp.WikicfpDatasource
import pl.edu.pw.ii.entityminer.crawler.dom.DomParser
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryManifest.ManifestEntry
import java.io.FileNotFoundException
import pl.edu.pw.ii.entityminer.Conf

/**
 * Created by Raphael Hazan on 17.01.14.
 */
class ScrappedWebPageIterator(pageDirectoryCollector: PageDirectoryCollector, domParser: DomParser,
                              documentsDirectory: String, pagesStart: Int, pagesNo: Int) {

  def foreach[T](f: (ConferenceData, ManifestEntry, Document, String) => T) = {
    for (i <- pagesStart to pagesStart + pagesNo - 1) {
      if (FileHelper.fileExists(documentsDirectory + "/" + i)) {
        val conferenceDirectory = documentsDirectory + "/" + i
        val manifest = pageDirectoryCollector.readPageManifestFromXml(conferenceDirectory)
        val conferenceData = WikicfpDatasource.loadConferenceFromXml(conferenceDirectory)
        if (conferenceData.names != null && manifest !=null) {
          manifest.result().foreach {
            manifestEntry =>
              try {
                if ( ! Conf.COMPUTE_STATS_ONLY_FOR_INDEX || PageDirectoryManifest.getFileNameOnDrive(manifestEntry) == Conf.MANIFEST_INDEX_ENTRY) {
                  val document = domParser.cleanAndConvertToDocument(conferenceDirectory + "/" + manifestEntry._2._2)
                  f(conferenceData, manifestEntry, document, documentsDirectory + "/" + i)
                }
              } catch {
                case e: FileNotFoundException =>
              }
          }
        }
      }
    }
  }
}
