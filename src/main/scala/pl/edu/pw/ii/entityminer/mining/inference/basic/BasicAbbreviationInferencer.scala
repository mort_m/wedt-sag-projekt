package pl.edu.pw.ii.entityminer.mining.inference.basic

import cc.factorie.app.nlp
import pl.edu.pw.ii.entityminer.mining.token.feature.discover.TokenTypeFeatureDiscover
import pl.edu.pw.ii.entityminer.mining.factorie.TreeLabelCrfModel.TokenFeatures
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.{ConferenceLabelDomain, LabeledTreeNerTag}
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP

/**
 * Created by Raphael Hazan on 5/12/2014.
 */

class BasicAbbreviationInferencer extends BasicInferencer {

  def inference(document: nlp.Document): nlp.Document = {
    for (token <- document.tokens) {
      val features = token.attr[TokenFeatures]
      if (features.activeCategories.contains(TokenTypeFeatureDiscover.FEAT_UPTERM) && token.string.size > 3) {
        token.attr[LabeledTreeNerTag] := ConferenceLabelDomain.value(ANNO_TO_LABEL_MAP.get(ConferenceHtmlAnnotation.ABBREVIATION).get)
        return document
      }
    }
    
    document
  }
}
