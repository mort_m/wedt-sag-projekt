package pl.edu.pw.ii.entityminer.database.service

import org.springframework.data.mongodb.core.{MongoTemplate, MongoOperations}
import org.springframework.beans.factory.annotation.Autowired
import pl.edu.pw.ii.entityminer.database.model.{DataWebSource, Conference}

/**
 * Created with IntelliJ IDEA.
 * User: ralpher
 * Date: 9/30/13
 * Time: 9:31 PM
 * To change this template use File | Settings | File Templates.
 */
class MongoAccessService {
  @Autowired
  private var mongoTemplate : MongoTemplate = _;

  def doSmth() = {
    println("res:")
    val conference = Conference("Warsjava",  "http://warsjava.com", DataWebSource("http://ospartner.pl"),
    "IT",
    "2013-10-03",
    "Warsaw",
    "2013-10-03",
    "2013-10-03",
    "2013-10-03")
    mongoTemplate.insert(conference)
    println(mongoTemplate.findAll(classOf[Conference]))
  }
}
