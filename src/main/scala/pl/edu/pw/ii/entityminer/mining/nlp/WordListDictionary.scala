package pl.edu.pw.ii.entityminer.mining.nlp

import pl.edu.pw.ii.entityminer.util.FileHelper
import org.apache.commons.io.IOUtils

/**
 * Created by Raphael Hazan on 4/12/2014.
 */
object WordListDictionary {
  lazy val wordList = {
    val stream = FileHelper.getResourceStream("agid_word_list_infl.txt")
    val string = IOUtils.toString(stream, "UTF-8");
    val result = string.split("\\s+").map(_.filter(_.isLetterOrDigit)).filter(_.nonEmpty)
    result.toSet
  }
}
