package pl.edu.pw.ii.entityminer.database.model.token

import org.jsoup.nodes.TextNode
import cc.factorie.app.nlp.Token
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation

/**
 * Created by ralpher on 26.12.13.
 */
case class WebToken(text: String, offsetStart: Int, offsetEnd: Int, node: TextNode, section: Int = 0) {
  private var sequenceNo_ : Int = _
  private var paragraphNo_ : Int = _
  private var tokenNo_ : Int = _

  private var nlpToken_ : Token = _
  private var gateText_ : Option[gate.Document] = None
  private var prevToken_ : WebToken = _
  private var nextToken_ : WebToken = _
  private var sentence_ : List[List[WebToken]] = Nil
  private var sentencePos_ : Int = -1

  private val connections_ : scala.collection.mutable.Set[WebToken] = scala.collection.mutable.Set[WebToken]()

  def connections = connections_

  def addConnection(webToken: WebToken) = {
    connections_.add(webToken)
    connections_
  }

  def hasNextInSentence = this.sentenceEntityIndex < this.sentence.size - 1

  def sentenceEntityIndex: Int = {
    var i = 0
    for (entity <- sentence) {
      for (token <- entity) {
        if (token.sentenceTextPos == this.sentenceTextPos) {
          return i
        }
      }
      i +=1
    }
    i
  }

  def tokenNo = tokenNo_

  def tokenNo_=(number: Int) = tokenNo_ = number

  def sequenceNo = sequenceNo_

  def sequenceNo_=(number: Int) = sequenceNo_ = number

  def paragraphNo = paragraphNo_

  def paragraphNo_=(number: Int) = paragraphNo_ = number

  def tokenEntity = sentence(sentenceEntityIndex)

  def sentenceTextPos = sentencePos_
  
  def sentenceTextPos_=(position: Int) = sentencePos_ = position

  def sentence = sentence_

  def sentenceIntoText = sentence.flatten.foldLeft[String]("") { case (sum, token) => sum + " " + token.originalText }.drop(1)

  def sentence_=(webTokens: List[List[WebToken]]) = sentence_ = webTokens

  def originalText = this.node.text().substring(offsetStart, offsetEnd)

  def parentName = this.node.parent().nodeName()

  def nlpToken = this.nlpToken_

  def nlpToken_=(token: Token) = this.nlpToken_ = token

  def prevToken = this.prevToken_

  def prevToken_=(prevToken: WebToken) = this.prevToken_ = prevToken

  def nextToken = this.nextToken_

  def nextToken_=(nextToken: WebToken) = this.nextToken_ = nextToken

  def gateText = this.gateText_

  def gateText_=(doc: gate.Document) = this.gateText_ = Some(doc)

  def getLabel = {
    this.node.parent match {
      case null => ConferenceHtmlAnnotation.OTHER_LABEL
      case parent =>
        ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.get(parent.nodeName()) match {
          case None => ConferenceHtmlAnnotation.OTHER_LABEL
          case Some(label) => label
        }
    }
  }

  override def toString: String = "WebToken(text=" + text + ", offset= " + offsetStart + " " + offsetEnd + ")"//FIXME offset start i end moga byc zle liczone!!
}
