package pl.edu.pw.ii.entityminer.mining.token.feature

import cc.factorie.app.nlp
import cc.factorie.app.nlp._
import org.jsoup.nodes.TextNode
import pl.edu.pw.ii.entityminer.database.datasource.annotation.{AnnotateStrategy, ConferenceDateAnnotator, ConferenceHtmlAnnotation}
import pl.edu.pw.ii.entityminer.mining.model.Skip2CrfModel

import scala.collection.mutable.ListBuffer
import pl.edu.pw.ii.entityminer.crawler.dom.DomParser
import pl.edu.pw.ii.entityminer.mining.token.feature.discover._
import pl.edu.pw.ii.entityminer.database.model.token.WebToken

import scala.Some
import pl.edu.pw.ii.entityminer.util.DocumentHelper
import pl.edu.pw.ii.entityminer.mining.nlp.TimeName
import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryManifest.ManifestEntry

/**
 * Created by Raphael Hazan on 1/21/14.
 */
trait SequenceFeatureTokenizer extends FeatureTokenizer {

  protected var domParser: DomParser
  protected var annotateStrategy: AnnotateStrategy
  protected var gateFeatureDiscover: FeatureDiscover
  protected var paragraphDivisor: ParagraphDivisor

  def tokenizeFromFiles(manifestEntries: List[(String, ManifestEntry)], singleEntities: Boolean): List[HeavyFeatureSet]

  def tokenizeDocuments(documents: List[(ManifestEntry, org.jsoup.nodes.Document)], singleEntities: Boolean): List[HeavyFeatureSet] = {
    annotateStrategy = new ConferenceDateAnnotator
    val featureBuffer = new ListBuffer[String]()
    paragraphDivisor = new ParagraphDivisor()
    val featureDiscovers = List(
      //new RealClassDiscover,
      //gateFeatureDiscover,

     // new ContainerFeatureDiscover,
    //  new ParagraphNoFeatureDiscover(featureBuffer),

      new TokenTypeFeatureDiscover,
      new TableDateFeatureDiscover(featureBuffer, domParser, annotateStrategy)//TODO comment maybe not for dates
   //   new PosFeatureDiscover(featureBuffer),
   //   new WordFeatureDiscover(featureBuffer),
   //   new WordShapeFeatureDiscover(featureBuffer),

      //below order independent discovers
    //  new NeighborFeatureDiscover(domParser),
   //   new ShortWordDiscover(featureBuffer),
  //    new NotInWordListDiscover(featureBuffer)
    //  new LinkFeatureDiscover,
     // new NlpFeatureDiscover,
     // new HighlightFeatureDiscover//,
    )
    annotateStrategy = new ConferenceDateAnnotator()
    val nlpDocument = new nlp.Document("").setName("tempName")
    nlpDocument.annotators(classOf[Token]) = UnknownDocumentAnnotator.getClass // register that we have token boundaries
    nlpDocument.annotators(classOf[Sentence]) = UnknownDocumentAnnotator.getClass // register that we have sentence boundaries
    val sentence = new Sentence(nlpDocument)
    //println("Paragraph divisor of "+documents.size+" documents.")
    val paragraphDocs = documents.
      foldLeft(new ListBuffer[(Set[String], List[List[TextNode]])]) {
        case (collected, (manifestEntry, document)) =>
          collected += ((manifestEntry._2._1, paragraphDivisor.divideToParagraphs(document)))
    }
    val paragraphWebTokens = new ListBuffer[List[WebToken]]()
    val webTokenSeq = ListBuffer[WebToken]()
    var prevWebToken: Option[WebToken] = None
    var currentLinkLabel = Set[String]()
    for ((manifestEntry, paragraphs) <- paragraphDocs) {//TODO add link features
      currentLinkLabel = manifestEntry
      for (paragraph <- paragraphs) {
        webTokenSeq.clear()
        for (node <- paragraph) {
          val text = node.text()
          val tmpWebTokens = annotateStrategy.tokenize(text, node.asInstanceOf[TextNode])
          tmpWebTokens.foreach {
            webToken =>
              prevWebToken match {
                case None =>
                case Some(prevToken) =>
                  webToken.prevToken = prevToken
                  webToken.prevToken.nextToken = webToken
              }
              prevWebToken = Some(webToken)

              webToken.nlpToken = new Token(sentence, webToken.originalText.toLowerCase())
              webTokenSeq += webToken
          }
        }
        paragraphWebTokens += webTokenSeq.result()
      }
    }
    DocumentAnnotatorPipeline(DocumentAnnotatorPipeline.defaultDocumentAnnotationMap, Nil, List(classOf[pos.PennPosTag])).process(nlpDocument)
    val entities = paragraphWebTokens.map(divideToEntitiesSimple(_))
    this.enumerateTokens(entities)

//    test -------------------------------------------------------------
//    val testConnectionFeatures = new Skip2CrfModel
//    for (entity <- entities.flatten.flatten) {
//      if (entity.size > 1) {
//        val label = getEntityLabel(entity)
//        val otherLabelSize = entity.count(_.getLabel != label)
//        if (label == "ABBRE" && otherLabelSize > 0) {
//          println("!!! " + entity.foldLeft(""){_ + " " + _.originalText})
//        }
//        SequenceFeatureTokenizer.tableMap.get(label) match {
//          case None =>
//            SequenceFeatureTokenizer.tableMap += (label -> scala.collection.mutable.Map((otherLabelSize, 1)))
//          case Some(coverages) =>
//            coverages.get(otherLabelSize) match {
//              case None =>
//                coverages += ((otherLabelSize, 1))
//              case Some(occurances) =>
//                coverages += ((otherLabelSize, occurances + 1))
//            }
//        }
//      }
//    }
//    ------------------------------------------------------------------

    entities.flatten.flatten.flatMap {
      entity =>
        if (singleEntities) {
          entity.foldLeft(List[HeavyFeatureSet]()) {
            case (container, token) =>
              featureBuffer.clear
              featureDiscovers.foreach(featureBuffer ++= _.discoverFeature(token))
              val label = token.getLabel

              if (featureBuffer.nonEmpty) {
                val heavySet = HeavyFeatureSet(List(token), List(label), featureBuffer.result, entity.head.connections)
                container :+ heavySet
              } else {
                container
              }
          }
        } else {
          val tokenLabels = entity.groupBy(_.getLabel)
          if (tokenLabels.size > 2 || (tokenLabels.size == 2 && !tokenLabels.keySet.contains(ConferenceHtmlAnnotation.OTHER_LABEL))) {
            println("for entity " + entity.map(_.originalText) + "many lbls: " + tokenLabels)
          }
          featureBuffer.clear
          featureDiscovers.foreach(featureBuffer ++= _.discoverFeature(entity))
          val label = getEntityLabel(entity)

          if (featureBuffer.nonEmpty) {
            val heavySet = HeavyFeatureSet(entity, List(label), featureBuffer.result, entity.head.connections)
            List(heavySet)
          } else {
            Nil
          }
        }
    }.filterNot(_ == null).result

    //TODO daty sa jeszcze zle oznaczane, np. 1 Jun 2014, jesli 1 jest zle, a Jun 2014 dobrze to dwa ostatnie zostana zaznaczone: niech wszystkie czynniki musza byc
  }

  private def enumerateTokens(document: ListBuffer[List[List[List[WebToken]]]]) = {
    var paragraphNo = 0
    var sentenceNo = 0
    var tokenNo = 0
    for (paragraph <- document) {
      paragraphNo += 1
      for (sentence <- paragraph) {
        sentenceNo += 1
        for (entity <- sentence) {
          for (token <- entity) {
            tokenNo += 1
            token.tokenNo = tokenNo
            token.sequenceNo = sentenceNo
            token.paragraphNo = paragraphNo
          }
        }
      }
    }
  }

  /**
   * Takes the most frequent label, but not other if possible
    *
    * @param entity
   * @return
   */
  private def getEntityLabel(entity: List[WebToken]): String = {
    val possibleLabels = entity.map(_.getLabel)

    val longDiffs = entity.filter(a => a.text.size > 4 && a.getLabel == ConferenceHtmlAnnotation.OTHER_LABEL)
//    if (longDiffs.nonEmpty && possibleLabels.toSet.size > 1) {
//      println("sygnatura " + longDiffs.map(_.text) + " | " + entity.map(_.text))
//    }

    if (possibleLabels.toSet.size == 1) {
      possibleLabels.head
    } else {
      possibleLabels.filterNot(_.toUpperCase == ConferenceHtmlAnnotation.OTHER_LABEL).groupBy(identity).maxBy(_._2.size)._1
    }
  }

  def divideToEntitiesSimple(tokens: List[WebToken]) = {
    val nlpDocument = new nlp.Document("").setName("tempName")
    nlpDocument.annotators(classOf[Token]) = UnknownDocumentAnnotator.getClass // register that we have token boundaries
    DocumentHelper.intoText(tokens, nlpDocument)
    DocumentAnnotatorPipeline(DocumentAnnotatorPipeline.defaultDocumentAnnotationMap, Nil, List(classOf[Sentence])).process(nlpDocument)

    val maxGap = 1
    val NameType = 0
    val DateType = 1
    val AbbrType = 2
    val OtherType = 3

    val breakingDateChains = List(".", "!", "?", "(", "[", ")", "]", ";")
    val breakingNameChains = breakingDateChains ++ List(",", "-", ":", "to", "&", "TO", "/", "at")

    var leadingChain = List[WebToken]()
    var awaitingChain = List[WebToken]()
    var currGap = 0
    var currType = OtherType

    val result = new ListBuffer[List[List[WebToken]]]()
    val resultSentence = new ListBuffer[List[WebToken]]()

    for (sentence <- nlpDocument.sentences) {
      //      println(sentence)
      var tokenPosition = 0
      currType = OtherType
      for (token <- sentence.tokens) {
        val webToken = token.attr[WebToken]
        webToken.sentenceTextPos = tokenPosition
        tokenPosition += webToken.originalText.size + 1//TODO moze skroty niech beda tylko pojedyncze

        if (token.string.trim.nonEmpty) {
          //TODO check if words like 'on' should be chain breakers for name entities
          //          some abbreviation maybe
          if (token.string.filter(_.isUpper).size > 1) {
            handleSpecialWord(token, webToken, AbbrType)
          }
          //          some date maybe, order is important because i.e. November would be classified to both classes
          else if ((token.string.filter(_.isDigit).nonEmpty && token.string.filterNot(_.isDigit).size <= 3) ||
            TimeName.whatTimeIsIt(token.string).isDefined) {

            handleSpecialWord(token, webToken, DateType)
          }
          //          some name maybe
          else if (token.string.head.isUpper) {
            handleSpecialWord(token, webToken, NameType)
          }
          //          punctuation
          else if (breakingNameChains.contains(token.string)) {
            if (currType == OtherType) {
              currGap = 0
              resultSentence += List(webToken)
            } else if (currType == NameType || currType == AbbrType) {
              currGap = 0
              currType = OtherType
              resultSentence += leadingChain
              awaitingChain.foreach(resultSentence += List(_))
              resultSentence += List(webToken)

              leadingChain = Nil
              awaitingChain = Nil
            } else {
              // Date
              handleOtherWord(token, webToken)
            }
          }
          //          other type, not important, counted as one
          else {
            handleOtherWord(token, webToken)
          }
        }
      }
      if (leadingChain.nonEmpty) {
        resultSentence += leadingChain
        leadingChain = Nil
      }
      if (awaitingChain.nonEmpty) {
        awaitingChain.foreach(resultSentence += List(_))
        awaitingChain = Nil
      }
      if (resultSentence.nonEmpty) {
        resultSentence.result.flatten.foreach(_.sentence = resultSentence.result)
        result += resultSentence.result
        resultSentence.clear
      }
    }

    def handleSpecialWord(t: Token, wt: WebToken, specialType: Int) = {
      currGap = maxGap
      if (currType == OtherType) {
        if (leadingChain.nonEmpty) throw new UnsupportedOperationException("laeding chain shuold be empty 1 " + t.string + " " + t.position)
        leadingChain = leadingChain :+ wt

      } else if (currType == specialType) {
        leadingChain = leadingChain ++ awaitingChain :+ wt
        awaitingChain = Nil
 
      } else {
        // other special types
        resultSentence += leadingChain
        awaitingChain.foreach(resultSentence += List(_))

        leadingChain = List(wt)
        awaitingChain = Nil
      }
      currType = specialType
    }

    def handleOtherWord(t: Token, wt: WebToken) = {
      if (currGap > 0 && currType != OtherType) {
        awaitingChain = awaitingChain :+ wt
        currGap -= 1
      } else if (currGap == 0) {
        if (currType != OtherType) {
          resultSentence += leadingChain
          awaitingChain.foreach(resultSentence += List(_))

          currType = OtherType
          leadingChain = Nil
          awaitingChain = Nil
        }
        resultSentence += List(wt)
      } else {
        throw new UnsupportedOperationException("something wrong " + t.string + " " + t.position)
      }
    }

    result.result
  }
}

object SequenceFeatureTokenizer {
  val tableMap = scala.collection.mutable.Map[String, scala.collection.mutable.Map[Int, Int]]()
}
