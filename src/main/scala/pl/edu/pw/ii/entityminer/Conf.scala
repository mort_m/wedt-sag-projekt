package pl.edu.pw.ii.entityminer

/**
 * Created with IntelliJ IDEA.
 * User: Rafael Hazan
 * Date: 4/21/13
 * Time: 10:34 PM
 */
object Conf {

  val testVal = -1
  val maxSearchedGoogleAnswers = 5

  val SKIP_CHAIN = true
  val LINEAR_MODEL = true
  val IOB_MODE = false
  val STOP_WORDS_MODE = false
  val SINGLE_ENTITIES = false
  val COMPUTE_STATS_ONLY_FOR_INDEX = true
  val STEM = true

  val SLASH = "/"

  val DATA_OUTPUT = "data/output"

  val DATA_STORAGE = "data/pagestorage"
  val DATA_STORAGE_ANNO = "data/pagestorage_anno"
  val FEATURES_STORAGE_ENTITY = "data/featurestorage"
  val FEATURES_STORAGE_SINGLE = "data/featurestorage_single"
  val FEATURES_STORAGE_STOP_WORDS = "data/featurestorage_stop"
  val FEATURES_STORAGE_IOB = "data/featurestorage_iob"
  val DATA_STOP_WORDS_FILE = "data/stopwords.txt"

  val LINEAR_MODEL_PATH = "data/model/factorie_linear_crf"
  val LINEAR_MODEL_IOB_PATH = "data/model/factorie_linear_crf_iob"
  val SVM_MODEL_PATH = "data/model/svmLinear"

  val WIKICFP_CONFERENCE_START_PAGE_CPOMPUTER_SC = "http://www.wikicfp.com/cfp/call?conference=computer%20science&skip=1"
  val WIKICFP_CONFERENCE_START_PAGE_AI = "http://www.wikicfp.com/cfp/call?conference=artificial%20intelligence&page=22"

  val MANIFEST_INDEX_ENTRY = "0.html"
  val FEATURED_LINKS = Set(MANIFEST_INDEX_ENTRY, "important dates", "home", "call for papers")

  val FEAT_TOKEN_NO = 2
  val FEAT_SENTENCE_NO = 1
  val FEAT_PARAGRAPH_NO = 0
  val FEAT_CONNS = 3
  val FEAT_WORD = 4
  val FEAT_CLASS = 5
  val FEATURES = 6
  val FEAT_DATA_ORDER = List(FEAT_TOKEN_NO, FEAT_SENTENCE_NO, FEAT_PARAGRAPH_NO, FEAT_CONNS, FEAT_WORD, FEAT_CLASS, FEATURES).sorted

  val PREFIX_FEAT_REAL_CLASS = "X="
  val PREFIX_FEAT_LOCATION = "LOC="
  val PREFIX_FEAT_YEAR = "YEAR="
  val PREFIX_FEAT_CONTAINER = "P="
  val PREFIX_FEAT_PARAGRAPH = "PNO="
  val PREFIX_FEAT_TOKEN_TYPE = "T="
  val PREFIX_FEAT_TABLE_TEXT = "WDN="
  val PREFIX_FEAT_POS = "POS="
  val PREFIX_FEAT_WORD = "W="
  val PREFIX_FEAT_SHAPE = "SHAPE="
  val PREFIX_FEAT_NEIGHBOR_NEXT_1 = "WT+1="
  val PREFIX_FEAT_NEIGHBOR_PREV_1 = "WT-1="
  val PREFIX_FEAT_SHORT = "SHORT="
  val PREFIX_FEAT_DICT = "DICT="
  val PREFIX_FEAT_HIGHLIGHT = "HLT="
  val PREFIX_FEAT_FIRST_IN_SENTENCE = "FST-SENT="
  val UsedFeatures = Set(
    PREFIX_FEAT_REAL_CLASS
    ,PREFIX_FEAT_LOCATION
    ,PREFIX_FEAT_YEAR
    ,PREFIX_FEAT_CONTAINER
    ,PREFIX_FEAT_PARAGRAPH
    ,PREFIX_FEAT_TOKEN_TYPE
    ,PREFIX_FEAT_TABLE_TEXT
    ,PREFIX_FEAT_POS
    ,PREFIX_FEAT_WORD
    ,PREFIX_FEAT_SHAPE
    ,PREFIX_FEAT_NEIGHBOR_NEXT_1
    ,PREFIX_FEAT_NEIGHBOR_PREV_1
    ,PREFIX_FEAT_SHORT
    ,PREFIX_FEAT_DICT
    ,PREFIX_FEAT_HIGHLIGHT
    ,PREFIX_FEAT_FIRST_IN_SENTENCE
  )

  def getFeatureStorage = if (IOB_MODE) FEATURES_STORAGE_IOB else if (SINGLE_ENTITIES) FEATURES_STORAGE_SINGLE else FEATURES_STORAGE_ENTITY
  def getFeatureStoragePrepared =  if (Conf.STOP_WORDS_MODE || Conf.STEM) Conf.FEATURES_STORAGE_STOP_WORDS else if (Conf.IOB_MODE) Conf.FEATURES_STORAGE_IOB else if (Conf.SINGLE_ENTITIES) Conf.FEATURES_STORAGE_SINGLE else Conf.FEATURES_STORAGE_ENTITY
}