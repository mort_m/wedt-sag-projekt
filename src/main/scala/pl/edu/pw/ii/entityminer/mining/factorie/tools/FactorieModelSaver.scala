package pl.edu.pw.ii.entityminer.mining.factorie.tools

import cc.factorie.util.BinarySerializer
import pl.edu.pw.ii.entityminer.mining.factorie.TreeLabelCrfModel.TokenFeaturesDomain
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.ConferenceLabelDomain
import cc.factorie.model.Parameters
import java.io.File
import org.springframework.stereotype.Service
import pl.edu.pw.ii.entityminer.util.FileHelper

/**
 * Created by Raphael Hazan on 3/28/2014.
 */
@Service
class FactorieModelSaver {
  def serialize(sourceModel: Parameters, filePath: String) = {
    BinarySerializer.serialize(ConferenceLabelDomain, sourceModel, new File(filePath), gzip = true)
    BinarySerializer.serialize(TokenFeaturesDomain, new File(filePath + "_feat"), gzip = true)
  }

  def deserialize(filePath: String, targetModel: Parameters) = {
    BinarySerializer.deserialize(ConferenceLabelDomain, targetModel, new File(filePath), gzip = true)
    if (FileHelper.fileExists(filePath + "_feat"))
      BinarySerializer.deserialize(TokenFeaturesDomain, new File(filePath + "_feat"), gzip = true)
    targetModel
  }
}
