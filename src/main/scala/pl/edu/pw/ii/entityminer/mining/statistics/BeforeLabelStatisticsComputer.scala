package pl.edu.pw.ii.entityminer.mining.statistics

import org.jsoup.nodes.Document
import org.springframework.beans.factory.annotation.Autowired
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.mining.nlp.WebPageTokenizer
import scala.collection.mutable
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryManifest._
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import weka.core.stemmers.SnowballStemmer

/**
 * Created by Raphael Hazan on 17.01.14.
 */
class BeforeLabelStatisticsComputer extends StatisticsComputer {

  private val firstToken = "FIRST_TOKEN"
  private val numberOfWordsBefore = 5

  @Autowired
  private var webPageTokenizer: WebPageTokenizer = _

  def computeStatistics(conferenceData: ConferenceData, document: Document, manifestEntry: ManifestEntry, documentPath: String): MapToListOccursDataStatistics = {
    val stemmer = new SnowballStemmer()

    val labelToWordsBefore = mutable.Map[String, mutable.Map[List[String], Int]]()
    val tokens = webPageTokenizer.tokenize(document)
    val tokenBuffer = mutable.Queue[WebToken]()
    tokenBuffer += new WebToken(firstToken, 0, 0, null)
    var lastLabel: String = ""
    val interLabelGap = 3
    var lastLabelIndex = interLabelGap + 1
    for (token <- tokens) {
      lastLabelIndex += 1
      if (ConferenceHtmlAnnotation.ALL_ANNOTATIONS.contains(token.parentName)) {
        if (lastLabelIndex > interLabelGap || lastLabel != token.parentName) {
          val priorTokens = tokenBuffer.takeRight(numberOfWordsBefore).toList.map(str => stemmer.stem(str.text))
          if (labelToWordsBefore.contains(token.parentName)) {
            if (labelToWordsBefore(token.parentName).contains(priorTokens)) {
              labelToWordsBefore(token.parentName).put(priorTokens, labelToWordsBefore(token.parentName)(priorTokens) + 1)
            } else {
              labelToWordsBefore(token.parentName).put(priorTokens, 1)
            }
          } else {
            labelToWordsBefore.put(token.parentName, mutable.Map((priorTokens, 1)))
          }
        }
        lastLabelIndex = 0
        lastLabel = token.parentName
      }
      tokenBuffer += token
    }
    new BeforeLabelStatistics(collection.mutable.Map() ++ labelToWordsBefore.mapValues(collection.mutable.Map() ++ _.mapValues(_ => 1)))
  }
}

case class BeforeLabelStatistics(
                                  labelToStat: collection.mutable.Map[String, collection.mutable.Map[List[String], Int]]) extends MapToListOccursDataStatistics {
  val CODE: String = "before label"
}