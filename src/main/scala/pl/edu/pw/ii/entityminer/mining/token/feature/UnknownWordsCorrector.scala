package pl.edu.pw.ii.entityminer.mining.token.feature

import pl.edu.pw.ii.entityminer.util.FileHelper
import org.springframework.stereotype.Service
import cc.factorie.app.nlp.Document
import pl.edu.pw.ii.entityminer.mining.factorie.TreeLabelCrfModel.TokenFeatures

/**
 * Created by Raphael Hazan on 2/21/14.
 */
@Service
class UnknownWordsCorrector extends FeatureCorrector {
  override def correctFeatures(dataDirectory: String, firstTestPageNo: Int): Unit = {
    val bagOfWords = collection.mutable.Set[String]()
    for (i <- 1 to firstTestPageNo - 1) {
      for (j <- 0 to 1000) {
        val fileName = dataDirectory + "/" + i + "." + j + ".txt"
        if (FileHelper.fileExists(fileName)) {
          io.Source.fromFile(fileName).getLines().foreach { line =>
            val fields = line.split("\\s+")
            val word = fields(0)
            bagOfWords.add(word.toLowerCase())
          }
        }
      }
    }
    val lineBuffer = new StringBuilder()
    for (i <- firstTestPageNo to 9999) {
      for (j <- 0 to 1000) {
        lineBuffer.clear()
        val fileName = dataDirectory + "/" + i + "." + j + ".txt"
        if (FileHelper.fileExists(fileName)) {
          io.Source.fromFile(fileName).getLines().foreach { line =>
            val fields = line.split("\\s+")
            val word = fields(0)
            val wordFeature = fields(3)
            if (bagOfWords.contains(word.toLowerCase())) {
              lineBuffer.append(line + '\n')
            } else {
              lineBuffer.append(fields.filterNot{field => field == wordFeature}.reduceLeft((cumulated, field) => cumulated + " " + field) + '\n')
            }
          }
          val writer = FileHelper.getFileWriter(fileName)
          writer.write(lineBuffer.result())
          writer.close()
        }
      }
    }
  }

  override def correctFeatures(trainDocs: Seq[Document], testDocs: Seq[Document]): Unit = {
    val bagOfWords = collection.mutable.Set[String]()
    for (doc <- trainDocs) {
      for (token <- doc.tokens) {
        bagOfWords.add(token.string)
      }
    }
    for (doc <- testDocs) {
      for (token <- doc.tokens) {
        if ( ! bagOfWords.contains(token.string)) {
//          token.attr[TokenFeatures] = token.attr[TokenFeatures].
        }
      }
    }
  }
}
