package pl.edu.pw.ii.entityminer.mining.token.feature

import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryManifest.ManifestEntry


/**
 * Created by Raphael Hazan on 1/21/14.
 */
trait FeatureTokenizer {
  val stopWords = List("&amp;", "and", "for", "of", "on", "amp")

  def tokenizeFromFiles(manifestEntries: List[(String, ManifestEntry)], singleEntities: Boolean): List[HeavyFeatureSet]

  def tokenizeDocuments(documents: List[(ManifestEntry, org.jsoup.nodes.Document)], singleEntities: Boolean): List[HeavyFeatureSet]
}