package pl.edu.pw.ii.entityminer.mining.statistics.checker

import org.springframework.context.support.{ClassPathXmlApplicationContext, AbstractApplicationContext}
import pl.edu.pw.ii.entityminer.{Conf, RootFacade}
import pl.edu.pw.ii.entityminer.mining.token.feature.{DomTokenizer, FactorieLinearCrfFeatureCreator}
import pl.edu.pw.ii.entityminer.mining.token.feature.discover.GateFeatureDiscover
import pl.edu.pw.ii.entityminer.mining.statistics.ScrappedWebPageIteratorFactory
import org.jsoup.nodes.TextNode
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import scala.collection.JavaConversions._

/**
 * Created by Raphael Hazan on 2014-05-17.
 */
object DatesChecker {
  def main(args: Array[String]) = {
    val applicationContext: AbstractApplicationContext = new ClassPathXmlApplicationContext("META-INF/spring/module-context.xml")
    val rootFacade = applicationContext.getBean("RootFacade", classOf[RootFacade])
    val domTokenizer = rootFacade.featureCreator.asInstanceOf[FactorieLinearCrfFeatureCreator].featureTokenizer.asInstanceOf[DomTokenizer]
    val iterFactory = applicationContext.getBean(classOf[ScrappedWebPageIteratorFactory])
    val tokenizer = domTokenizer.annotateStrategy

    val fileIterator = iterFactory.buildIter(Conf.DATA_STORAGE_ANNO, 0, 3333)
    fileIterator.foreach { case (conferenceData, manifestEntry, document, documentPath) =>

      val annotation =
        ConferenceHtmlAnnotation.NOTIFICATION
//        ConferenceHtmlAnnotation.SUBMISSION
//        ConferenceHtmlAnnotation.FINAL_VERSION

      document.select(annotation).toList match {
        case Nil =>
//          document.select("")
        case annotationNode =>
          val annotatedDate = annotationNode.head.child(0).text()
      }

    }
    println("done")
  }
}
