package pl.edu.pw.ii.entityminer.mining.factorie.treecrf

import cc.factorie.app.nlp._
import cc.factorie.app.nlp.ner._
import cc.factorie.variable.{CategoricalDomain, CategoricalLabeling}
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation

/**
 * Created with IntelliJ IDEA.
 * User: ralpher
 * Date: 12/7/13
 * Time: 12:11 AM
 * To change this template use File | Settings | File Templates.
 */
class LabeledTreeNerTag(token: Token, initialCategory: String) extends TreeNerTag(token, initialCategory) with CategoricalLabeling[String]

class TreeNerTag(token:Token, initialCategory:String) extends NerTag(token, initialCategory) { def domain = ConferenceLabelDomain }

object ConferenceLabelDomain extends CategoricalDomain[String] {
  this ++= Vector(ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.values:_*)
  freeze()
}