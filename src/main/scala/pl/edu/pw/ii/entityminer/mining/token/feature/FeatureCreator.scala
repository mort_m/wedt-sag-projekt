package pl.edu.pw.ii.entityminer.mining.token.feature


/**
 * Created by ralpher on 28.12.13.
 */
trait FeatureCreator {
  def findAndSaveStopWords(fromDirectory: String, destinyDirectory: String): Unit

  def createFeaturesAndSave(documentsDirectory: String, totalPages: Int, featuresDirectory: String, singleEntities: Boolean): Unit

  def convertFeaturesToIOB(featuresDirectory: String, destinyDirectory: String, totalPages: Int): Unit
}
