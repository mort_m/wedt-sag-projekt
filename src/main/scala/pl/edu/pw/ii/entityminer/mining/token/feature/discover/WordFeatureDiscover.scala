package pl.edu.pw.ii.entityminer.mining.token.feature.discover

import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import scala.collection.mutable.ListBuffer
import weka.core.stemmers.SnowballStemmer

/**
 * Created by Raphael Hazan on 2/26/14.
 */
class WordFeatureDiscover(featureBuffer: ListBuffer[String]) extends FeatureDiscover{
  private val stem = false
  private val stemmer = new SnowballStemmer()

  override def discoverFeature(entity: List[WebToken]): List[String] = discoverFeaturesLocallyAndSum(entity)

  override def getPrefixes(): Set[String] = Set(Conf.PREFIX_FEAT_WORD)

  override def discoverFeature(webToken: WebToken): List[String] = {
    if (featureBuffer.contains(TokenTypeFeatureDiscover.FEAT_STANDARD) ||
      featureBuffer.contains(TokenTypeFeatureDiscover.FEAT_TERM) ||
      featureBuffer.contains(TokenTypeFeatureDiscover.FEAT_LONG_TERM) ||
      featureBuffer.contains(TokenTypeFeatureDiscover.FEAT_UPTERM)) {
      if (stem) {
        List(WordFeatureDiscover.LABEL_FEAT + stemmer.stem(webToken.text.toLowerCase()))
      } else {
        List(WordFeatureDiscover.LABEL_FEAT + webToken.text.toLowerCase())
      }
    } else {
      Nil
    }
  }
}

object WordFeatureDiscover {
  val LABEL_FEAT = Conf.PREFIX_FEAT_WORD
}