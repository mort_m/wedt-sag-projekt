package pl.edu.pw.ii.entityminer.mining.factorie

import cc.factorie.model._
import cc.factorie._
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf._
import pl.edu.pw.ii.entityminer.mining.factorie.TreeLabelCrfModel.{TokenFeaturesDomain, TokenFeatures}
import scala.collection.mutable.ListBuffer
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.SkipTagWW
import cc.factorie.variable.{BooleanDomain, BooleanValue}

/**
 * Created by Raphael Hazan on 3/28/2014.
 */
class LinearCrf extends TemplateModel with Parameters {
  addTemplates(
    // Bias term on each individual label
    new DotTemplateWithStatistics1[TreeNerTag] {
      val weights = Weights(new la.DenseTensor1(ConferenceLabelDomain.size))
    },
    // Factor between label and observed token
    new DotTemplateWithStatistics2[TreeNerTag, TokenFeatures] {
      val weights = Weights(new la.DenseTensor2(ConferenceLabelDomain.size, TokenFeaturesDomain.dimensionSize))

      def unroll1(label: TreeNerTag) = Factor(label, label.token.attr[TokenFeatures])

      def unroll2(tf: TokenFeatures) = Factor(tf.token.attr[TreeNerTag], tf)
    }
    , new DotTemplateWithStatistics2[TreeNerTag, TreeNerTag] {
      val weights = Weights(new la.DenseTensor2(ConferenceLabelDomain.size, ConferenceLabelDomain.size))

      def unroll1(label: TreeNerTag) = {
        (if (label.token.hasPrev) List(Factor(label.token.prev.attr[TreeNerTag], label)) else Nil) :::
          (if (label.token.hasNext) List(Factor(label, label.token.next.attr[TreeNerTag])) else Nil)
      }

      def unroll2(label: TreeNerTag) = {
        (if (label.token.hasPrev) List(Factor(label.token.prev.attr[TreeNerTag], label)) else Nil) :::
          (if (label.token.hasNext) List(Factor(label, label.token.next.attr[TreeNerTag])) else Nil)
      }
    }
    ,
    new DotTemplateWithStatistics2[TreeNerTag, TreeNerTag] {
      val weights = Weights(new la.DenseTensor2(ConferenceLabelDomain.size, ConferenceLabelDomain.size))
      def unroll1(label: TreeNerTag) = factorsForAllConnections(label)
      def unroll2(label: TreeNerTag) = factorsForAllConnections(label)

      def factorsForAllConnections(label: TreeNerTag) = {
        if (label.token.attr[SkipTagWW] != null && label.token.attr[SkipTagWW].connections.nonEmpty) {
          //          if (label.token.attr[SkipTag].connections.size == 1) {
          //            println((label.token.attr[SkipTag].connections.size + 1) + " xXx " + label.token.document.name + " " + label.token.toString() + " " + label.token.attr[SkipTag].connections.map(_.toString()))
          //          }
          label.token.attr[SkipTagWW].connections.foldLeft(new ListBuffer[Factor]()) {
            case (buffer, destToken) =>
              buffer += Factor(label, destToken.attr[TreeNerTag])
          }.result
        } else {
          Nil
        }
      }
    }
//  ,
//    new DotTemplateWithStatistics2[TreeNerTag, TreeNerTag] {
////    new Template2[TreeNerTag, TreeNerTag] with TensorFamily1[BooleanValue] {
//      val weights = Weights(new la.DenseTensor2(ConferenceLabelDomain.size, ConferenceLabelDomain.size))
//      def unroll1(label: TreeNerTag) = {
//        val r = factorsForAllConnections(label)
////        if (r.nonEmpty) {
////          println("xx " + label.token.attr[TokenFeatures].toString())
////        }
//        r
//      }
//      def unroll2(label: TreeNerTag) = factorsForAllConnections(label)
//
//      def factorsForAllConnections(label: TreeNerTag) = {
//        if (label.token.attr[SkipTagNA] != null && label.token.attr[SkipTagNA].connections.nonEmpty) {
//
////          if (!(Set(ConferenceHtmlAnnotation.NAME, ConferenceHtmlAnnotation.ABBREVIATION).contains(label.token.attr[LabeledTreeNerTag].target.categoryValue) &&
////            Set(ConferenceHtmlAnnotation.NAME, ConferenceHtmlAnnotation.ABBREVIATION).contains(label.token.attr[SkipTagNA].connections.head.attr[LabeledTreeNerTag].target.categoryValue)) &&
////            !(label.token.attr[LabeledTreeNerTag].target.categoryValue == ConferenceHtmlAnnotation.OTHER_LABEL &&
////              label.token.attr[SkipTagNA].connections.head.attr[LabeledTreeNerTag].target.categoryValue == ConferenceHtmlAnnotation.OTHER_LABEL)) {
////            println("COS nie tak!!! " + label.token.attr[TokenFeatures].toString() + "|" + label.token.attr[SkipTagNA].connections.head.attr[TokenFeatures].toString())
////            println("cd.. " + label.token.attr[LabeledTreeNerTag].target.categoryValue + "|" + label.token.attr[SkipTagNA].connections.head.attr[LabeledTreeNerTag].target.categoryValue)
////            println("cd... " + label.token.string + "|" + label.token.attr[SkipTagNA].connections.head.string)
////          }
//
//          label.token.attr[SkipTagNA].connections.foldLeft(new ListBuffer[Factor]()) {
//            case (buffer, destToken) =>
//              buffer += Factor(label, destToken.attr[TreeNerTag])
//          }.result
//        } else {
//          Nil
//        }
//      }
//    }
//    ,
//    new DotTemplateWithStatistics2[TreeNerTag, TreeNerTag] {
//      val weights = Weights(new la.DenseTensor2(ConferenceLabelDomain.size, ConferenceLabelDomain.size))
//      def unroll1(label: TreeNerTag) = factorsForAllConnections(label)
//      def unroll2(label: TreeNerTag) = factorsForAllConnections(label)
//
//      def factorsForAllConnections(label: TreeNerTag) = {
//        if (label.token.attr[SkipTagAA] != null && label.token.attr[SkipTagAA].connections.nonEmpty) {
//
////          if (label.token.attr[LabeledTreeNerTag].target.categoryValue == ConferenceHtmlAnnotation.ABBREVIATION) {
////            for (conn <- label.token.attr[SkipTagAA].connections) {
////              if (conn.attr[LabeledTreeNerTag].target.categoryValue != ConferenceHtmlAnnotation.ABBREVIATION) {
////                println("ABBR cos nie tak!!! " + label.token.attr[TokenFeatures].toString() + "|" + conn.attr[TokenFeatures].toString())
////                println("cd.. " + label.token.attr[LabeledTreeNerTag].target.categoryValue + "|" + conn.attr[LabeledTreeNerTag].target.categoryValue)
////                println("cd... " + label.token.string + "|" + conn.string)
////                println("cd.... " + label.token.document.name + "|" + conn.document.name)
////              }
////            }
////          }
////          if (label.token.attr[LabeledTreeNerTag].target.categoryValue == ConferenceHtmlAnnotation.OTHER_LABEL) {
////            for (conn <- label.token.attr[SkipTagAA].connections) {
////              if (conn.attr[LabeledTreeNerTag].target.categoryValue != ConferenceHtmlAnnotation.OTHER_LABEL) {
////                println("ABBR cos nie tak!!! " + label.token.attr[TokenFeatures].toString() + "|" + conn.attr[TokenFeatures].toString())
////                println("cd.. " + label.token.attr[LabeledTreeNerTag].target.categoryValue + "|" + conn.attr[LabeledTreeNerTag].target.categoryValue)
////                println("cd... " + label.token.string + "|" + conn.string)
////                println("cd.... " + label.token.document.name + "|" + conn.document.name)
////              }
////            }
////          }
//
//          label.token.attr[SkipTagAA].connections.foldLeft(new ListBuffer[Factor]()) {
//            case (buffer, destToken) =>
//              buffer += Factor(label, destToken.attr[TreeNerTag])
//          }.result
//        } else {
//          Nil
//        }
//      }
//    }

  )
}