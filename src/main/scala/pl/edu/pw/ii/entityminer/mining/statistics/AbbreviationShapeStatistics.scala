package pl.edu.pw.ii.entityminer.mining.statistics

import org.jsoup.nodes.Document
import org.springframework.beans.factory.annotation.Autowired
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceAbbreviationAnnotator
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryManifest._
import pl.edu.pw.ii.entityminer.mining.nlp.WebPageTokenizer

/**
 * Created by Raphael Hazan on 17.01.14.
 */
case class AbbreviationShapeStatistics(lengthToNumber: scala.collection.mutable.Map[String, Int]) extends DataStatistics {
  val CODE: String = "abbr shape"

  override def accumulate(other: DataStatistics): Unit = {
    for ((otherLen, otherNumber) <- other.asInstanceOf[AbbreviationShapeStatistics].lengthToNumber) {
      lengthToNumber.get(otherLen) match {
        case None => lengthToNumber put(otherLen, otherNumber)
        case Some(thisNumber) => lengthToNumber put(otherLen, thisNumber + otherNumber)
      }
    }
  }
}

class AbbreviationShapeStatisticsComputer extends StatisticsComputer {

  @Autowired
  private var webPageTokenizer: WebPageTokenizer = _

  def computeStatistics(conferenceData: ConferenceData, document: Document, manifestEntry: ManifestEntry, documentPath: String): DataStatistics = {
    val tokens = webPageTokenizer.tokenize(document)
    val resultMap = collection.mutable.Map[String, Int]()

    val abbr = conferenceData.nameAbbreviation
    resultMap put(cc.factorie.app.strings.stringShape(abbr, 2), 1)
    println(cc.factorie.app.strings.stringShape(abbr, 2) + " " + abbr)
    return new AbbreviationShapeStatistics(resultMap)
  }
}