package pl.edu.pw.ii.entityminer.crawler.dom

import org.w3c.dom.{Node, Document}

/**
 * Created by Raphael Hazan on 28.12.13.
 */
trait DomIter {
  val document: Document

  def next(): Option[Node]
}
