package pl.edu.pw.ii.entityminer.database.datasource.annotation

import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.util.FileHelper
import pl.edu.pw.ii.entityminer.database.datasource.wikicfp.WikicfpDatasource
import org.springframework.beans.factory.annotation.{Qualifier, Autowired}
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryCollector
import pl.edu.pw.ii.entityminer.crawler.static.HttpClient
import org.springframework.stereotype.Service
import scala.collection.mutable.ListBuffer
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import pl.edu.pw.ii.entityminer.crawler.dom.DomParser
import org.jsoup.nodes.{Node, TextNode, Element, Document}
import org.jsoup.parser.Tag
import pl.edu.pw.ii.entityminer.mining.token.feature.{FeatureTokenizer, StopWords}
import javax.annotation.Resource
import collection.JavaConversions._
import java.io.{File, FileNotFoundException}
import pl.edu.pw.ii.entityminer.Conf

/**
 * Created by ralpher on 23.12.13.
 */
@Service("WebPageAnnotator")
class DomAnnotator extends WebPageAnnotator {
  @Autowired
  private var pageDirectoryCollector: PageDirectoryCollector = _

  @Autowired
  private var domParser: DomParser = _

  @Resource
  private var javaAnnotators: java.util.List[AnnotateStrategy] = _

  @Autowired
  @Qualifier("BoilerplateFeatureTokenizer")
  private var featureTokenizer: FeatureTokenizer = _

  def annotatePage(document: Document, conferenceData: ConferenceData) = {
    var anythingChanged = false
    val annotators = javaAnnotators.toList

    annotators.foreach {
      annotator =>
        val conferenceTokens = annotator.getValueTokens(conferenceData)

        conferenceTokens.foreach {
          conferenceTokensSeq =>

            var loop = true
            val iter = domParser.getDfsIterator(document)

            var drawnNodes = new ListBuffer[TextNode]()
            var textNodePairs = List[(String, TextNode)]()

            while (loop) {
              iter.next() match {
                case None => loop = false
                  val anyNodeChanged = annotateNodes(annotator, drawnNodes, conferenceTokensSeq)
                  if (!anythingChanged) {
                    anythingChanged = anyNodeChanged
                  }

                case Some(node) =>
                  if (node.isInstanceOf[TextNode]) {
                    if (node.asInstanceOf[TextNode].text.trim.nonEmpty) {
                      drawnNodes += node.asInstanceOf[TextNode]//TODO span niech tez znajduja
                      textNodePairs = (node.asInstanceOf[TextNode].text, node.asInstanceOf[TextNode]) :: textNodePairs
                    }

                  } else if (StopWords.continousTextTags.contains(node.nodeName())) {
                    // do nothing, text continue
                  } else {
                    val anyNodeChanged = annotateNodes(annotator, drawnNodes, conferenceTokensSeq)
                    if (!anythingChanged) {
                      anythingChanged = anyNodeChanged
                    }
                  }
              }
            }
        }
    }
    anythingChanged
  }


  def annotateNodes(annotator: AnnotateStrategy, drawnNodes: ListBuffer[TextNode], conferenceTokensSeq: List[String]) = {
    var anythingChanged = false
    if (drawnNodes.nonEmpty) {
      val allTokens = tokenize(annotator, drawnNodes.result)
      val occurences = new ListBuffer[WebToken]()
      annotateAllOccurences(annotator, allTokens, conferenceTokensSeq, occurences)
      if (occurences.nonEmpty/* && annotator.annotationTagName == ConferenceHtmlAnnotation.NAME*/) println("found occur for " + annotator.annotationTagName + ", value " + occurences)
      val occursInTokens = occurences.distinct.groupBy(_.node)
      var leadingNode: Option[Node] = None
      var firstNode: Option[Node] = None
      for ((textNode, tokens) <- occursInTokens) {

//        drawnNodes.foreach { // uncomment if you want paragraph tags
//          textNode =>
//            val possibleParagraph = domParser.findParentParagraph(textNode)
//            possibleParagraph match {
//              case None =>
//              case Some(paragraph) =>
//                if (!ConferenceHtmlAnnotation.ALL_PARENT_ANNOTS.contains(paragraph.parent.nodeName)) {
//                  val tmp = new Element(Tag.valueOf(ConferenceHtmlAnnotation.INFOMATIONAL_PARAGRAPH), "")
//                  paragraph.replaceWith(tmp)
//                  tmp.appendChild(paragraph)
//                }
//            }
//        }

        leadingNode = None
        firstNode = None
        if (annotator.annotationTagName != textNode.parent().nodeName()) {
          var lastPosition = 0
          tokens.foreach {
            token =>
              if (token.offsetStart > lastPosition) {
                val tmp = new TextNode(token.node.text().substring(lastPosition, token.offsetStart), "")
                leadingNode = leadingNode match {
                  case Some(node) =>
                    node.after(tmp)
                    Some(tmp)
                  case None =>
                    textNode.after(tmp)
                    firstNode = Some(tmp)
                    firstNode
                }
              }
              val tmp = new Element(Tag.valueOf(annotator.annotationTagName), "")
              tmp.text(token.originalText)
              leadingNode = leadingNode match {
                case Some(node) =>
                  node.after(tmp)
                  Some(tmp)
                case None =>
                  textNode.after(tmp)
                  firstNode = Some(tmp)
                  firstNode
              }
              lastPosition = token.offsetEnd
          }
          if (lastPosition < textNode.text.size) {
            val tmp = new TextNode(textNode.text.substring(tokens.last.offsetEnd), "")
            leadingNode.get.after(tmp)
          }
          textNode.replaceWith(firstNode.get)
          anythingChanged = true
        }
      }
      drawnNodes.clear()
    }
    anythingChanged
  }

  def annotateConferencesWithLabels(sourceDocDirectory: String, destinyDocDirectory: String, totalPages: Int): Unit = {
    if (Conf.testVal < 0) {
      FileHelper.copyFolder(new File(sourceDocDirectory), new File(destinyDocDirectory))
    } else {
      FileHelper.copyFolder(new File(sourceDocDirectory + "/" + Conf.testVal), new File(destinyDocDirectory + "/" + Conf.testVal))
    }
    try {
      for (i <- 0 to totalPages - 1) {
        if (FileHelper.fileExists(sourceDocDirectory + "/" + i)
          && (Conf.testVal < 0 || i == Conf.testVal)) {
          println(i + " ----------------------------------------------------")
          val conferenceDirectory = sourceDocDirectory + "/" + i
          val destinyConferenceDirectory = destinyDocDirectory + "/" + i
          val manifest = pageDirectoryCollector.readPageManifestFromXml(conferenceDirectory)
          val conferenceData = WikicfpDatasource.loadConferenceFromXml(conferenceDirectory)
          if (conferenceData.names != null && manifest!=null) {
            manifest.result().foreach {
              manifestEntry =>
                if (! Conf.COMPUTE_STATS_ONLY_FOR_INDEX || manifestEntry._2._2 == Conf.MANIFEST_INDEX_ENTRY) {
                  println(i + " " + manifestEntry._1 + " for document " + manifestEntry._2._2)// + " " + manifestEntry._2._1)
                  val document = domParser.cleanAndConvertToDocument(conferenceDirectory + "/" + manifestEntry._2._2)
                  //              println(document.title())
                  //              println(document.select("c1_name"))
                  val anythingChanged = annotatePage(document, conferenceData)
                  if (anythingChanged) {
                    FileHelper.saveDocumentOnDisk(document, destinyConferenceDirectory + "/" + manifestEntry._2._2)
                  } else {
                    println("NOT CHANGED: " + i + " " + manifestEntry._1 + " for document " + manifestEntry._2._2 + " " + manifestEntry._2._1)
                  }
                }
            }
          }
        }
      }
    } catch {
      case e: FileNotFoundException =>
    }
  }

  private def tokenize(annotator: AnnotateStrategy, nodes: List[TextNode]): List[WebToken] = {
    val buffer = new ListBuffer[WebToken]()
    nodes.foreach(node => buffer ++= annotator.tokenize(node.text(), node))
    buffer.result
  }

  private def annotateAllOccurences(annotator: AnnotateStrategy, tokens: List[WebToken], conferenceTokensSeq: List[String], allOccurs: ListBuffer[WebToken]): Unit = {
    annotator.findFirstOccurence(tokens, conferenceTokensSeq) match {
      case (Nil, Nil) => // nothing
      case (Nil, tailTokens) => // nothing found do nothing
      case (occurence, Nil) => // found on end of token sequence
        allOccurs ++= occurence
      case (occurence, tailTokens) => // foun in middle of token sequence
        allOccurs ++= occurence
        annotateAllOccurences(annotator, tailTokens, conferenceTokensSeq, allOccurs)
    }
  }
}
