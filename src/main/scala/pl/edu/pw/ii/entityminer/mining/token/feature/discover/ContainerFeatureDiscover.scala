package pl.edu.pw.ii.entityminer.mining.token.feature.discover

import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.crawler.dom.DomParser
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import scala.collection.mutable.ListBuffer

/**
 * Created by Raphael Hazan on 2/26/14.
 */
class ContainerFeatureDiscover extends FeatureDiscover {

  private val domParser = new DomParser

  override def getPrefixes(): Set[String] = Set(Conf.PREFIX_FEAT_CONTAINER)

  override def discoverFeature(webToken: WebToken): List[String] = {
    domParser.findParentParagraph(webToken.node) match {
      case None => Nil
      case Some(parent) =>
        List(ContainerFeatureDiscover.LABEL_FEAT + parent.nodeName())
    }
  }

  override def discoverFeature(entity: List[WebToken]): List[String] = discoverFeature(entity.head)
}

object ContainerFeatureDiscover {
  val LABEL_FEAT = Conf.PREFIX_FEAT_CONTAINER
}