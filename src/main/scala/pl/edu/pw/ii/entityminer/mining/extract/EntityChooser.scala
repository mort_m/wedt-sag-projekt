package pl.edu.pw.ii.entityminer.mining.extract

import cc.factorie.app.nlp
import cc.factorie.model.TemplateModel

/**
 * Created by Raphael Hazan on 3/29/2014.
 */
trait EntityChooser {
  val operatingLabel: String

  /**
   * @param inferredSeqs theese sequences are ordered in mining order.
   * @return
   */
  def chooseBestEntity(inferredSeqs: scala.List[scala.Seq[nlp.Token]], model: TemplateModel): Option[scala.Seq[String]]

  def chooseBestEntity(inferredSeqs: scala.List[scala.Seq[nlp.Token]], model: TemplateModel, label: String): Option[scala.Seq[String]] = {
    if (inferredSeqs.isEmpty) {
      None
    } else if (operatingLabel == label) {
      if (model == null) {
        Some(inferredSeqs.head.map(_.string.trim))
      } else {
       // println("ENTITY: "+inferredSeqs)
        chooseBestEntity(inferredSeqs.filterNot(_.isEmpty), model)
      }
    } else {
      None
    }
  }
}
