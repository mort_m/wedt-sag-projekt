package pl.edu.pw.ii.entityminer.mining.token.feature.discover

import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.crawler.dom.DomParser
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import org.jsoup.nodes.Node

/**
 * Created by Raphael Hazan on 2/26/14.
 */
//TODO make this dependent also from sentences (maybe)
//TODO add post word features
class NeighborFeatureDiscover(domParser: DomParser) extends FeatureDiscover {
  private val featureDiscover = new TokenTypeFeatureDiscover
  var prev1Token: List[WebToken] = Nil
  var prev1Parent: List[Node] = Nil

  override def getPrefixes(): Set[String] = Set(Conf.PREFIX_FEAT_NEIGHBOR_PREV_1, Conf.PREFIX_FEAT_NEIGHBOR_NEXT_1)

  override def discoverFeature(entity: List[WebToken]): List[String] = {
    var result: List[String] = Nil
    //    uncomment if you want paragraph boundaries instead of sentence boundaries
    //    domParser.findParentParagraph(entity.head.node) match {
    //      case None =>
    //      case Some(currParent) =>
    //        prev1Parent match {
    //          case Nil =>
    //            prev1Parent = List(currParent)
    //            prev1Token = entity
    //          case prevParent :: Nil if prevParent == currParent =>
    //            result = List("WT-1=" + featureDiscover.discoverFeature(prev1Token).head)
    //            prev1Token = entity
    //          case prevParent :: Nil =>
    //            prev1Parent = List(currParent)
    //            prev1Token = entity
    //        }
    //    }

    if (entity.head.sentenceEntityIndex > 0 && entity.head.sentenceEntityIndex < entity.head.sentence.size - 1) {
      result = List(Conf.PREFIX_FEAT_NEIGHBOR_PREV_1 +
        featureDiscover.discoverFeature(entity.head.sentence(entity.head.sentenceEntityIndex - 1)).head)
//      if (entity.head.sentence(entity.head.sentenceEntityIndex - 1).size == 1) {
//       result = "W-1=" + entity.head.sentence(entity.head.sentenceEntityIndex - 1).head.text :: result
//      }
    }

    if (entity.last.sentenceEntityIndex < entity.head.sentence.size - 1) {
      result = result ::: List(Conf.PREFIX_FEAT_NEIGHBOR_NEXT_1 +
        featureDiscover.discoverFeature(entity.head.sentence(entity.last.sentenceEntityIndex + 1)).head)
//      if (entity.head.sentence(entity.head.sentenceEntityIndex + 1).size == 1) {
//        result = "W+1=" + entity.head.sentence(entity.last.sentenceEntityIndex + 1).head.text :: result
//      }
    }

    result
  }

  def discoverFeature(webToken: WebToken) = discoverFeature(List(webToken))
}
