package pl.edu.pw.ii.entityminer.mining.statistics.checker

import pl.edu.pw.ii.entityminer.util.FileHelper
import scala.util.control.Breaks._
import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.database.datasource.annotation.{ConferenceAbbreviationAnnotator, ConferenceNameAnnotator, ConferenceHtmlAnnotation}
import org.springframework.context.support.{ClassPathXmlApplicationContext, AbstractApplicationContext}
import pl.edu.pw.ii.entityminer.database.datasource.wikicfp.WikicfpDatasource

/**
 * Created by Raphael Hazan on 2014-05-17.
 */
object NameChecker {
  var applicationContext: AbstractApplicationContext = _
  val nameAnnotator = new ConferenceNameAnnotator
  val abbrAnnotator = new ConferenceAbbreviationAnnotator

  def main(args: Array[String]) = {
    applicationContext = new ClassPathXmlApplicationContext("META-INF/spring/module-context.xml")

    //    checkFoundNamesDifferenciesWithReal
    //    checkAbbreviationsGeneratedFromNames
    checkNameAppearance

    println("done")
  }

  private def checkNameAppearance {
    for (i <- 0 to 1001) {
      val conferenceData = WikicfpDatasource.loadConferenceFromXml(Conf.DATA_STORAGE + Conf.SLASH + i)

      val name = conferenceData.names.foldLeft("") { case (full, part) => full + " " + part}.trim
      val nameChanged = nameAnnotator.getValueTokens(conferenceData).flatten
      if (
//        nameChanged.size <= 2
        nameChanged.contains("international") || nameChanged.contains("conference")
//        true
//        (name.split(')').filter(_.nonEmpty).size > 1 && name.split(')').last.trim.nonEmpty)
      ) {
        //        || name.contains("-")) {

        println(i + " " + name + " ====> " + nameChanged)
      }
    }
  }

  private def checkAbbreviationsGeneratedFromNames {
    var nGood = 0
    var n = 0
    for (i <- 0 to 1001) {
      val conferenceData = WikicfpDatasource.loadConferenceFromXml(Conf.DATA_STORAGE + Conf.SLASH + i)
      val generatedAbbreviation = generateAbbreviation(conferenceData.names.head)
      abbrAnnotator.getValueTokens(conferenceData) match {
        case (abbr :: Nil) :: Nil =>
          if (generatedAbbreviation != abbr) {
            n += 1
            println(nGood + "|" + n + " " + conferenceData.names.head + "||| " + generatedAbbreviation + " " + abbr)
          } else {
            nGood += 1
          }
        case _ =>
      }
    }
  }

  private def generateAbbreviation(name: String) = {
    name.split("\\s+").foldLeft("") {
      case (abbr, namePart) =>
        val obfuscated = namePart.replaceAll("[-().,]", "").replaceAll("\\d+", "").toLowerCase.trim
        if (obfuscated.size > 3) {
          abbr :+ obfuscated.head
        } else {
          abbr
        }
    }
  }

  private def checkFoundNamesDifferenciesWithReal {
    for (i <- 0 to 1001) {
      if (FileHelper.fileExists(Conf.FEATURES_STORAGE_ENTITY + "/" + i + ".0.txt")) {
        breakable {
          for (j <- 0 to 1000) {
            val fileName = Conf.FEATURES_STORAGE_ENTITY + "/" + i + "." + j + ".txt"
            if (FileHelper.fileExists(fileName)) {
              fromSource(i, io.Source.fromFile(fileName))
            }
          }
        }
      }
    }
  }

  private def fromSource(fileIndex: Int, source: io.Source): Unit = {
    source.getLines().foreach {
      sample =>
        val fields = sample.split("\\s+")
        val wordSet = fields(1).split("\\|").toList.filterNot(_.isEmpty).map(_.toLowerCase())
        val ner = fields(2)
        if (ner == ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP(ConferenceHtmlAnnotation.NAME)) {
          val conferenceData = WikicfpDatasource.loadConferenceFromXml(Conf.DATA_STORAGE + Conf.SLASH + fileIndex)
          val dataSet = nameAnnotator.getValueTokens(conferenceData).flatten.map(_.trim)
          if (wordSet.diff(dataSet).filterNot(str =>
            Set("").
              contains(str.trim)).
            filterNot(_.size < 4).
            filterNot(_.filter(_.isDigit).nonEmpty).
            nonEmpty) {

            println(fileIndex + " roznica imion " + wordSet + " &~ " + conferenceData.names + ", abbr=" + conferenceData.nameAbbreviation + " = " + wordSet.diff(dataSet))
          }
        }
    }
  }

  /**
  Method findBestLongForm takes as input a short-form and a long-
form candidate (a list of words) and returns the best long-form
that matches the short-form, or null if no match is found.
    * */
  private def findBestLongForm(shortForm: String, longForm: String): String = {
    var sIndex: Int = -1 // The index on the short form
    var lIndex: Int = -1 // The index on the long form
    var currChar: Char = '0' // The current character to match

    sIndex = shortForm.length() - 1; // Set sIndex at the end of the
    // short form
    lIndex = longForm.length() - 1; // Set lIndex at the end of the
    // long form
    while (sIndex >= 0) {
      // Scan the short form starting
      // from end to start
      // Store the next character to match. Ignore case
      currChar = Character.toLowerCase(shortForm.charAt(sIndex));
      // ignore non alphanumeric characters
      if (!Character.isLetterOrDigit(currChar)) {

      } else {
        // Decrease lIndex while current character in the long form
        // does not match the current character in the short form.
        // If the current character is the first character in the
        // short form, decrement lIndex until a matching character
        // is found at the beginning of a word in the long form.
        while (
          ((lIndex >= 0) &&
            (Character.toLowerCase(longForm.charAt(lIndex)) != currChar))
            ||
            ((sIndex == 0) && (lIndex > 0) &&
              (Character.isLetterOrDigit(longForm.charAt(lIndex - 1)))))
          lIndex -= 1
        // If no match was found in the long form for the current
        // character, return null (no match).
        if (lIndex < 0)
          return null;
        // A match was found for the current character. Move to the
        // next character in the long form.
        lIndex -= 1
      }
      sIndex -= 1
    }
    // Find the beginning of the first word (in case the first
    // character matches the beginning of a hyphenated word).
    lIndex = longForm.lastIndexOf(" ", lIndex) + 1;
    // Return the best long form, the substring of the original
    // long form, starting from lIndex up to the end of the original
    // long form.
    return longForm.substring(lIndex);
  }

}
