package pl.edu.pw.ii.entityminer.mining.train

import cc.factorie.app.nlp
import cc.factorie.app.classify.backend.LinearMulticlassClassifier
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.{ConferenceLabelDomain, LabeledTreeNerTag}
import pl.edu.pw.ii.entityminer.mining.factorie.TreeLabelCrfModel.TokenFeatures
import cc.factorie.app.nlp.parse.TransitionBasedParser
import java.io.{BufferedInputStream, BufferedOutputStream, File}
import cc.factorie.la
import cc.factorie.util.BinarySerializer

/**
 * Created by Raphael Hazan on 5/13/2014.
 */
class SvmLinearClassifier(model: LinearMulticlassClassifier) {
  def inference(document: nlp.Document) = {
    for (token <- document.tokens) {
      val predictResultIndex = model.predict(token.attr[TokenFeatures].value).toArray.zipWithIndex.maxBy(_._1)._2
      token.attr[LabeledTreeNerTag] := predictResultIndex
    }
  }
}
