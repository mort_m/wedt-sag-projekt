package pl.edu.pw.ii.entityminer.mining.token.feature.discover

import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import pl.edu.pw.ii.entityminer.mining.nlp.TimeName

/**
 * Created by Raphael Hazan on 2/26/14.
 */
class TokenTypeFeatureDiscover extends FeatureDiscover {

  override def discoverFeature(webToken: WebToken): List[String] = {//TODO cecha z dlukoscia entity
//    println("|" + webToken.text + "|")
     List (webToken.originalText match {
      case "," | "." | "!" |"?" | ";" => TokenTypeFeatureDiscover.FEAT_PUNCT
      case text if text.filter(_.isDigit).nonEmpty => TokenTypeFeatureDiscover.FEAT_NUMBER
      case text if isUpperKind(text) => TokenTypeFeatureDiscover.FEAT_UPTERM
      case text if text.charAt(0).isUpper => TokenTypeFeatureDiscover.FEAT_TERM
      case text if (text.contains("@") | text.contains("www") | text.contains("http") | text.filter(_.isDigit).nonEmpty) => TokenTypeFeatureDiscover.FEAT_SPECIAL
      case _ => TokenTypeFeatureDiscover.FEAT_STANDARD
    })
  }

  override def discoverFeature(entity: List[WebToken]): List[String] = {
    if (entity.size == 1) {
      discoverFeature(entity.head)
    } else  if (isItDate(entity.map(_.originalText))) {
      List(TokenTypeFeatureDiscover.FEAT_DATE)
    } else if (entity.size > 3) {
      List(TokenTypeFeatureDiscover.FEAT_LONG_TERM)
    } else if (entity.filter(_.text.filter(_.isDigit).nonEmpty).nonEmpty) {
      List(TokenTypeFeatureDiscover.FEAT_NUMBER) //TODO dodac gdzies ceche rd <- jest czesto przed nazwa konferencji
    } else {
      List(TokenTypeFeatureDiscover.FEAT_TERM)
    }
  }

  def isUpperKind(text: String) = {
    WordShapeFeatureDiscover.Prefix + cc.factorie.app.strings.stringShape(text, 2) match {
      case WordShapeFeatureDiscover.ShapeAA => true
      case WordShapeFeatureDiscover.ShapeAaaAaa => true
      case WordShapeFeatureDiscover.ShapeAaaAA => true
      case WordShapeFeatureDiscover.ShapeAA1AA => true
      case WordShapeFeatureDiscover.ShapeAAaa => true
      case WordShapeFeatureDiscover.ShapeAaAA => true
      case WordShapeFeatureDiscover.ShapeAAa => true
      case WordShapeFeatureDiscover.ShapeAAaAA => true

      case _ => false
    }
  }

  override def getPrefixes(): Set[String] = Set(Conf.PREFIX_FEAT_TOKEN_TYPE)

  private def isItDate(tokens: List[String]) = {
    var result = false

    var dayOccuredNo = 0//TODO tokenizowanie dat na pojedyncze daty i daty podwojne (trwajace od do)
    var monthOccuredNo = 0
    var dayOrMonthOccuredNo = 0
    var yearOccuredNo = 0
    var dateMemberNo = 0

    def dateCondtionsFulfilled: Boolean = {
      if (dateMemberNo >= 3 && yearOccuredNo > 0 && (
        dayOccuredNo > 0 && monthOccuredNo > 0 ||
          dayOrMonthOccuredNo > 1 ||
          dayOrMonthOccuredNo > 0 && (dayOccuredNo > 0 || monthOccuredNo > 0)
        )) {

        result = true
      }
      result
    }
    for (token <- tokens) {
      if (TimeName.isItDay(token)) {
        dateMemberNo += 1
        dayOccuredNo += 1
      } else if (TimeName.isItMonth(token)) {
        dateMemberNo += 1
        monthOccuredNo += 1
      } else {
        try {
          val number = token.filter(_.isDigit).toInt
          if (number > 2000 && number < 2050) {
            dateMemberNo += 1
            yearOccuredNo += 1
          } else if (number > 0 && number <= 12) {
            dateMemberNo += 1
            dayOrMonthOccuredNo += 1
          } else if (number > 12 && number <= 31) {
            dateMemberNo += 1
            dayOccuredNo += 1
          }
        } catch {
          case _ : java.lang.NumberFormatException =>
        }
      }
      result = dateCondtionsFulfilled
    }
    result
  }
}

object TokenTypeFeatureDiscover {
  val LABEL_FEAT = Conf.PREFIX_FEAT_TOKEN_TYPE
  val FEAT_SPECIAL = LABEL_FEAT + "SPEC"
  val FEAT_NUMBER = LABEL_FEAT + "NUMBER"
  val FEAT_STANDARD = LABEL_FEAT + "STANDARD"
  val FEAT_TERM = LABEL_FEAT + "TERM"
  val FEAT_LONG_TERM = LABEL_FEAT + "L-TERM"
  val FEAT_UPTERM = LABEL_FEAT + "UPTERM"
  val FEAT_PUNCT = LABEL_FEAT + "PUNCT"

  val FEAT_DATE = LABEL_FEAT + "DATE"
}