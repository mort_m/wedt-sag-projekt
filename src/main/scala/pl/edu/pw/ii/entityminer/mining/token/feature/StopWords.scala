package pl.edu.pw.ii.entityminer.mining.token.feature

import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation

/**
 * Created by Raphael Hazan on 1/13/14.
 */
object StopWords {
  val StopWords = List("&amp;", "and", "for", "of", "on")

  val continousTextTags = Set("font", "b", "br", "a", "strong",
    ConferenceHtmlAnnotation.ABBREVIATION,
    ConferenceHtmlAnnotation.FINAL_VERSION,
    ConferenceHtmlAnnotation.NAME,
    ConferenceHtmlAnnotation.NOTIFICATION,
    ConferenceHtmlAnnotation.SUBMISSION,
    ConferenceHtmlAnnotation.WHEN,
    ConferenceHtmlAnnotation.WHERE)

  /// Feature constants
  val FeatureNA = "N/A"
  val FeaturePosNA = "XX"
  val FeatureWord = "W="
  val FeatureParagraph = "PAR="
  val FeatureShape = "SHAPE="
}
