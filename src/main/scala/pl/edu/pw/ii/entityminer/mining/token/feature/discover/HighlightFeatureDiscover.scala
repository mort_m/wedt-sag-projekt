package pl.edu.pw.ii.entityminer.mining.token.feature.discover

import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import scala.collection.mutable.ListBuffer
import pl.edu.pw.ii.entityminer.crawler.HtmlConsts
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation

/**
 * Created by Raphael Hazan on 2/26/14.
 */
class HighlightFeatureDiscover extends FeatureDiscover {
  override def discoverFeature(entity: List[WebToken]): List[String] = {
    discoverFeaturesLocallyAndSum(entity)
  }

  override def getPrefixes(): Set[String] = Set(Conf.PREFIX_FEAT_HIGHLIGHT)

  override def discoverFeature(webToken: WebToken): List[String] = {
    var parent = webToken.node.parent()
    while (ConferenceHtmlAnnotation.ALL_ANNOTATIONS.contains(parent.nodeName())) {
      parent = parent.parent()
    }
     if (HtmlConsts.TextHighlighters.contains(parent.nodeName())) {
      List(HighlightFeatureDiscover.LABEL_FEAT + parent.nodeName())
    } else {
      Nil
    }
  }
}

object HighlightFeatureDiscover {
  val LABEL_FEAT = Conf.PREFIX_FEAT_HIGHLIGHT
}