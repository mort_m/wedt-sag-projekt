package pl.edu.pw.ii.entityminer.crawler.dom

import pl.edu.pw.ii.entityminer.crawler.HtmlContentCleaner
import org.springframework.stereotype.Service
import org.jsoup.nodes.{Node, Document}

/**
 * Created by Raphael Hazan on 28.12.13.
 */
@Service
class DomParser extends HtmlContentCleaner {
  private val maximalParentDeph = 5
  //TOOD check if div should stay
  private val acceptedParagraphTagRegexes = Set("^p$", "^div$", "^title$", "^tr$", "^li$", "^h\\d$")

  def getDfsIterator(document: Node) = {
    new DfsJsoupIter(document)
  }

  def findParentParagraph(node: Node): Option[Node] = {
    var currentParent: Node = node.parent()
    var currentDeph = 1
    while (currentParent != null) {
      if (acceptedParagraphTagRegexes.count(currentParent.nodeName().matches(_)) > 0) {
        return Some(currentParent)
      }
      if (currentDeph >= maximalParentDeph) {
        return None
      }
      currentParent = currentParent.parent()
      currentDeph += 1
    }
    None
  }
}
