package pl.edu.pw.ii.entityminer.mining.statistics

import org.jsoup.nodes.Document
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import scala.collection.mutable
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryManifest._
import scala.collection.JavaConversions._
import pl.edu.pw.ii.entityminer.util.CollectionHelper


/**
 * Created by Raphael Hazan on 17.01.14.
 */
class LinkLabelStatisticsComputer extends StatisticsComputer {

  def computeStatistics(conferenceData: ConferenceData, document: Document, manifestEntry: ManifestEntry, documentPath: String): MapToListOccursDataStatistics = {
    val resultMap = collection.mutable.Map[String, collection.mutable.Map[List[String], Int]]()
    manifestEntry._2._1.map(_.toLowerCase.trim.replaceAll("\\s+", " ")).foreach {
      linkLabel =>
        val linkMap = collection.mutable.Map[List[String], Int](List(linkLabel) -> 1)
        CollectionHelper.updateMap(resultMap, (linkLabel, linkMap), (a: collection.mutable.Map[List[String], Int], b: collection.mutable.Map[List[String], Int]) =>
          collection.mutable.Map[List[String], Int](a.keys.head -> (a.values.head + b.values.head))
        )
    }
    new LinkLabelStatistics(resultMap)
  }
}

case class LinkLabelStatistics(labelToStat: collection.mutable.Map[String, collection.mutable.Map[List[String], Int]]) extends MapToListOccursDataStatistics {
  val CODE: String = "link label"
}