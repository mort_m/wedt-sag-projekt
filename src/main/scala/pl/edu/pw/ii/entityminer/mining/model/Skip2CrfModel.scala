package pl.edu.pw.ii.entityminer.mining.model

import cc.factorie.app.nlp.Token
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.{SkipTagAA, LabeledTreeNerTag, SkipTagNA}
import pl.edu.pw.ii.entityminer.mining.token.feature.HeavyFeatureSet
import pl.edu.pw.ii.entityminer.mining.token.feature.discover.TokenTypeFeatureDiscover

import scala.collection.mutable.ListBuffer

/**
 * Created by Raphael Hazan on 6/29/2014.
 */
class Skip2CrfModel {

  val tokenTypeDiscover = new TokenTypeFeatureDiscover

  var currentSpecialTokenIndex2 = 0
  val queueMaxGap2 = 2
  var prevSpecialTokenIndex2 = -queueMaxGap2 - 2
  var prevSkipToken2: Option[HeavyFeatureSet] = None
  var nameTurn = true
  var prevSentenceNo2 = -1

  def checkForConnections(entity: List[WebToken], heavySet: HeavyFeatureSet) = {
    val featureSet = new ListBuffer[String]()
    featureSet ++= tokenTypeDiscover.discoverFeature(entity)

    val word = entity.foldLeft("")(_ + "|" + _.originalText)
    val sentenceNo = entity.head.sequenceNo

    if (featureSet.contains(TokenTypeFeatureDiscover.FEAT_LONG_TERM) ||
      featureSet.contains(TokenTypeFeatureDiscover.FEAT_UPTERM)) {

      if (entity.head.getLabel == ConferenceHtmlAnnotation.NAME) {
        heavySet.labelss = List(ConferenceHtmlAnnotation.OTHER_LABEL)
      }

      val currentGap2 = currentSpecialTokenIndex2 - prevSpecialTokenIndex2 - 1
      if (featureSet.contains(TokenTypeFeatureDiscover.FEAT_LONG_TERM)) {
        prevSkipToken2 = Some(heavySet)
        nameTurn = false
        prevSentenceNo2 = sentenceNo
        prevSpecialTokenIndex2 = currentSpecialTokenIndex2

      } else if (featureSet.contains(TokenTypeFeatureDiscover.FEAT_UPTERM) && !nameTurn
        && currentGap2 <= queueMaxGap2 && sentenceNo == prevSentenceNo2) {

        if (entity.head.getLabel == ConferenceHtmlAnnotation.ABBREVIATION) {
          prevSkipToken2.get.labelss = List(ConferenceHtmlAnnotation.NAME)
          heavySet.labelss = List(ConferenceHtmlAnnotation.ABBREVIATION)
        }

        nameTurn = true
      }
    }

    currentSpecialTokenIndex2 += 1
  }
}
