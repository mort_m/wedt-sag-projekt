package pl.edu.pw.ii.entityminer.mining.token.feature

import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.{Qualifier, Autowired}
import pl.edu.pw.ii.entityminer.crawler.BoilerplateRemover
import pl.edu.pw.ii.entityminer.crawler.dom.DomParser
import pl.edu.pw.ii.entityminer.database.datasource.annotation.AnnotateStrategy
import pl.edu.pw.ii.entityminer.mining.token.feature.discover.{FeatureDiscover, GateFeatureDiscover}

/**
 * Created by Raphael Hazan on 1/21/14.
 */
@Service("BoilerplateFeatureTokenizer")
class BoilerplateSequenceFeatureTokenizer extends SequenceFeatureTokenizer with BoilerplateFeatureTokenizer {

  @Autowired
  var domParser: DomParser = _

  @Autowired
  var boilerplateRemover: BoilerplateRemover = _

  @Autowired
  @Qualifier("ConferenceDateAnnotator")//TODO separate annotation strategy with tokenizer
  var annotateStrategy: AnnotateStrategy = _

  @Autowired
  @Qualifier("GateFeatureDiscover")
  var gateFeatureDiscover: FeatureDiscover = _

  @Autowired
  @Qualifier("ParagraphDivisor")
  var paragraphDivisor: ParagraphDivisor = _
}
