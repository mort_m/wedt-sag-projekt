package pl.edu.pw.ii.entityminer.database.datasource.annotation

import pl.edu.pw.ii.entityminer.database.model.token.WebToken
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.mining.token.feature.StopWords
import org.jsoup.nodes.TextNode
import scala.collection.mutable.ListBuffer
import org.springframework.stereotype.Service
import pl.edu.pw.ii.entityminer.database.datasource.wikicfp.WikicfpConsts
import pl.edu.pw.ii.entityminer.mining.nlp.TimeName

/**
 * Created by Raphael Hazan on 1/13/14.
 */
class ConferenceWhereAnnotator extends AnnotateStrategy with AllSeparatelyTokenizer {
  val annotationTagName: String = ConferenceHtmlAnnotation.WHERE
  val maxGapBetweenNextKeywords = 15
  val maxTokenExtension = 3

  def getValueTokens(conferenceData: ConferenceData): List[List[String]] = {
    val result = if (WikicfpConsts.NA == conferenceData.place || conferenceData.place == null) {
      Nil
    } else {
      List(
        conferenceData.place.toLowerCase.replaceAll("[().\\[\\]-]", " ").split("[\\s+,]").
          filter(_.length > 1).toList
      )
    }
    result
  }

  def findFirstOccurence(tokens: List[WebToken], seq: List[String]): (List[WebToken], List[WebToken]) = {
    findFirstOccurence(tokens, seq, 0, Nil)
  }

  private def findFirstOccurence(tokens: List[WebToken], seq: List[String], currentGap: Int,
                                 matchedPart: List[WebToken]): (List[WebToken], List[WebToken]) = {
    tokens match {
      case Nil =>
        (Nil, Nil)
      case token :: rest if seq.filter(seqToken => token.text.contains(seqToken) && token.text.size <= seqToken.size + maxTokenExtension).nonEmpty =>
        val result = findFirstOccurence(rest, seq, maxGapBetweenNextKeywords, matchedPart :+ token)
        if (result._1.isEmpty) {
          val res2 = findFirstOccurence(rest, seq, currentGap - 1, matchedPart)
          if (res2._1.isEmpty) {
            (matchedPart :+ token, rest)
          } else if (res2._1.isEmpty && matchedPart.isEmpty) {
            findFirstOccurence(rest, seq, 0, Nil)
          } else {
            res2
          }
        } else {
          (result._1, rest)
        }
      case token :: rest if currentGap > 0 =>
        findFirstOccurence(rest, seq, currentGap - 1, matchedPart)
      case token :: rest if currentGap <= 0 =>
        if (matchedPart.isEmpty) {
          findFirstOccurence(rest, seq, 0, Nil)
        } else {
          (Nil, Nil)
        }
    }
  }

}
