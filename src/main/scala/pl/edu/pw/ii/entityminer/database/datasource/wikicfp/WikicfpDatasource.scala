package pl.edu.pw.ii.entityminer.database.datasource.wikicfp

import pl.edu.pw.ii.entityminer.crawler.static.{HttpClient}
import pl.edu.pw.ii.entityminer
import org.jaxen.dom.DOMXPath
import scala.collection.JavaConversions.iterableAsScalaIterable
import scala.collection.mutable
import pl.edu.pw.ii.entityminer.database.datasource.{ConferenceData, WebDatasource}
import org.w3c.dom.Node
import pl.edu.pw.ii.entityminer.util.FileHelper
import java.io.{File, ObjectInputStream, FileInputStream}
import pl.edu.pw.ii.entityminer.{DOMBuilder, Conf}
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryManifest
import org.springframework.stereotype.Service
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.{OutputKeys, TransformerFactory}
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult
import org.springframework.beans.factory.annotation.Autowired
import pl.edu.pw.ii.entityminer.crawler.dom.DomParser

/**
 * Created with IntelliJ IDEA.
 * User: Rafael Hazan
 * Date: 7/1/13
 * Time: 9:10 PM
 */
@Service
class WikicfpDatasource extends WebDatasource {

  @Autowired
  private var domParser: DomParser = _

  @Autowired
  private var httpClient: HttpClient = _

  val DOMAIN_URL = "http://www.wikicfp.com/"
//    val BASE_URL = DOMAIN_URL + "cfp/call?conference=artificial%20intelligence&page="
//    val BASE_URL = DOMAIN_URL + "cfp/call?conference=machine%20learning&page="
//  val BASE_URL = DOMAIN_URL + "cfp/call?conference=computer%20science&page="
//  val BASE_URL = DOMAIN_URL + "cfp/call?conference=NLP&page="
    val BASE_URL = DOMAIN_URL + "cfp/call?conference=communications&page="

  val CONFERENCE_SUBPAGES_XPATH: String = "/HTML/body/div[4]/center/form/table/tbody/tr[3]/td/table/tbody/tr/td/a/@href"
  val CONFERENCE_LINK_XPATH: String = "/HTML/body/div[4]/center/table/tbody/tr[3]/td/a/text()"
  val NAME_XPATH = "/HTML/body/div[4]/center/table/tbody/tr[2]/td/h2/span/span/text()"
  val WHEN_XPATH = "/HTML/body/div[4]/center/table/tbody/tr[5]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/text()"
  val WHERE_XPATH = "/HTML/body/div[4]/center/table/tbody/tr[5]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td/text()"
  val SUBMISSION_DEADLINE_XPATH = "/HTML/body/div[4]/center/table/tbody/tr[5]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td/span/span[3]/text()"
  val NOTIFICATION_DUE_XPATH = "/HTML/body/div[4]/center/table/tbody/tr[5]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[4]/td/span/span[3]/text()"
  val FINAl_VERSION_DUE_XPATH = "/HTML/body/div[4]/center/table/tbody/tr[5]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[5]/td/span/span[3]/text()"

  var startPage: Int = 0
  var pageIndex: Int = 0

  def setUp() {
    startPage = 1
    pageIndex = 1
  }

  def nextPage() = {
    val response = httpClient.syncDownload(BASE_URL + pageIndex)
    pageIndex += 1

    val document = domParser.cleanAndConvertToDomDocument(response)
    //    FileHelper.saveDocumentOnDisk(document, "data/test/nextPageSample.html")
    val xpathExpr: DOMXPath = new DOMXPath(CONFERENCE_SUBPAGES_XPATH)
    val subpages1 = xpathExpr.selectNodes(document)
    val subpages = subpages1.toSet.map {
      node: Any =>
        DOMAIN_URL + node.toString.drop(7).dropRight(1)
    } // remove 7 chars: href="/ and frol right "
    println(subpages)
    subpages
  }

  def nextConferencePages(conferenceSubpages: Set[String]): Set[ConferenceData] = {
    conferenceSubpages.foldLeft(new mutable.SetBuilder[ConferenceData, Set[ConferenceData]](Set[ConferenceData]())) {
      case (aggregator, conferenceSubpageUri) => //TODO typ przechowujacy dane o stronie
        val response = httpClient.syncDownload(conferenceSubpageUri)

        val document = domParser.cleanAndConvertToDomDocument(response)
        val uriXpathExpr: DOMXPath = new DOMXPath(CONFERENCE_LINK_XPATH)
        val nameXpathExpr: DOMXPath = new DOMXPath(NAME_XPATH)
        val whenXpathExpr: DOMXPath = new DOMXPath(WHEN_XPATH)
        val whereXpathExpr: DOMXPath = new DOMXPath(WHERE_XPATH)
        val submissionXpathExpr: DOMXPath = new DOMXPath(SUBMISSION_DEADLINE_XPATH)
        val notificationXpathExpr: DOMXPath = new DOMXPath(NOTIFICATION_DUE_XPATH)
        val finalVersionXpathExpr: DOMXPath = new DOMXPath(FINAl_VERSION_DUE_XPATH)
        try {
          val conferenceUri = (uriXpathExpr.selectNodes(document)).toList.asInstanceOf[List[Node]].head.getNodeValue

          println(conferenceUri)

          val conferenceData = new ConferenceData
          conferenceData.uri = conferenceUri
          var tmp = ""
          tmp = nameXpathExpr.selectNodes(document).toList.headOption.getOrElse("").toString.
            drop(8).
            replaceAll("]+", "")
          if (!tmp.isEmpty) {
            val abbreviationAndName = tmp.split(":").map(_.trim).toList
            if (abbreviationAndName.size > 1) {
              conferenceData.nameAbbreviation = abbreviationAndName.head
            }
            conferenceData.names = List(abbreviationAndName.last)
          }
          tmp = whenXpathExpr.selectNodes(document).toList.headOption.getOrElse("").toString.drop(8).replaceAll("]+", "").trim
          if (!tmp.isEmpty) {
            conferenceData.date = tmp
          }
          tmp = whereXpathExpr.selectNodes(document).toList.headOption.getOrElse("").toString.drop(8).replaceAll("]`+", "").trim
          if (!tmp.isEmpty) {
            conferenceData.place = tmp
          }
          tmp = submissionXpathExpr.selectNodes(document).toList.headOption.getOrElse("").toString.drop(8).replaceAll("]+", "").trim
          if (!tmp.isEmpty) {
            conferenceData.paperSubmissionDate = tmp
          }
          tmp = notificationXpathExpr.selectNodes(document).toList.headOption.getOrElse("").toString.drop(8).replaceAll("]+", "").trim
          if (!tmp.isEmpty) {
            conferenceData.paperNotificationDate = tmp
          }
          tmp = finalVersionXpathExpr.selectNodes(document).toList.headOption.getOrElse("").toString.drop(8).replaceAll("]+", "").trim
          if (!tmp.isEmpty) {
            conferenceData.paperFinalVersionDate = tmp
          }

          aggregator += conferenceData
        } catch {
          case e: NoSuchElementException => println("[nextConferencePages] NoSuchElementException " + conferenceSubpageUri)
        }

        aggregator
    }.result()
  }
}

object WikicfpDatasource {
  val COFERENCE_DATA_DEFAULT_FILE_NAME = "conferenceData"

  def saveConference(data: ConferenceData, directory: String, filename: String = COFERENCE_DATA_DEFAULT_FILE_NAME) {
    val filePath = directory + Conf.SLASH + filename
    FileHelper.saveBinary(data)(filePath)
    scala.xml.XML.save(filePath + ".xml", this.conferenceDataToXml(data), "UTF-8", true, null)
  }

  def loadConference(directory: String, filename: String = COFERENCE_DATA_DEFAULT_FILE_NAME): ConferenceData = {
    val fileIn = new FileInputStream(directory + Conf.SLASH + filename)
    val in = new ObjectInputStream(fileIn);
    val data = in.readObject().asInstanceOf[ConferenceData];
    in.close();
    fileIn.close();
    data
  }

  def loadConferenceFromXml(directory: String, filename: String = COFERENCE_DATA_DEFAULT_FILE_NAME + ".xml"): ConferenceData = {
    val filePath = directory + Conf.SLASH + filename
    val node = xml.XML.loadFile(filePath)
    val conference = new ConferenceData()

    conference.acceptanceDate = (node \ "acceptance").text.trim match {
      case txt if txt.isEmpty => null
      case txt => txt
    }
    conference.cameraReadyDate = (node \ "camera").text.trim match {
      case txt if txt.isEmpty => null
      case txt => txt
    }
    conference.committee = (node \ "committee").text.trim match {
      case txt if txt.isEmpty => null
      case txt => txt
    }
    conference.date = (node \ "date").text.trim match {
      case txt if txt.isEmpty => null
      case txt => txt
    }
    conference.nameAbbreviation = (node \ "abbreviation").text.trim match {
      case txt if txt.isEmpty => null
      case txt => txt
    }
    val names = node \ "names"
    var namesList = List[String]()
    for (name <- names) {
      namesList = (name \ "name").text.trim :: namesList
    }
    conference.names = namesList
    conference.paperFinalVersionDate = (node \ "final").text.trim match {
      case txt if txt.isEmpty => null
      case txt => txt
    }
    conference.paperNotificationDate = (node \ "notification").text.trim match {
      case txt if txt.isEmpty => null
      case txt => txt
    }
    conference.paperSubmissionAbstract = (node \ "submissionAbstract").text.trim match {
      case txt if txt.isEmpty => null
      case txt => txt
    }
    conference.paperSubmissionDate = (node \ "submission").text.trim match {
      case txt if txt.isEmpty => null
      case txt => txt
    }
    conference.place = (node \ "place").text.trim match {
      case txt if txt.isEmpty => null
      case txt => txt
    }
    conference.registrationDate = (node \ "registration").text.trim match {
      case txt if txt.isEmpty => null
      case txt => txt
    }
    conference.uri = (node \ "uri").text.trim match {
      case txt if txt.isEmpty => null
      case txt => txt
    }
    conference
  }


  private def conferenceDataToXml(c: ConferenceData) = {
    <conference>
      <acceptance>
        {c.acceptanceDate}
      </acceptance>
      <camera>
        {c.cameraReadyDate}
      </camera>
      <committee>
        {c.committee}
      </committee>
      <date>
        {c.date}
      </date>
      <abbreviation>
        {c.nameAbbreviation}
      </abbreviation>
      <names>
        {for (name <- c.names) yield <name>
        {name}
      </name>}
      </names>
      <final>
        {c.paperFinalVersionDate}
      </final>
      <notification>
        {c.paperNotificationDate}
      </notification>
      <submissionAbstract>
        {c.paperSubmissionAbstract}
      </submissionAbstract>
      <submission>
        {c.paperSubmissionDate}
      </submission>
      <place>
        {c.place}
      </place>
      <registration>
        {c.registrationDate}
      </registration>
      <uri>
        {c.uri}
      </uri>
    </conference>
  }
}