package wedt.sag.extension

import java.util.logging.Level

import akka.actor.{ActorSystem, Props}
import org.apache.commons.logging.LogFactory
import org.springframework.context.support.{AbstractApplicationContext, ClassPathXmlApplicationContext}
import wedt.sag.extension.Actors.Supervisor
import wedt.sag.extension.Actors.Supervisor.NewConference

import scala.io.Source
import scala.util.control.Breaks._

/**
 * Created by Marta on 2016-05-21.
 */
object Main extends App{

  println("Type name and year of the conference (ex icaisc 2016) to get results. Press q to quit.")

  val system = ActorSystem.create("app")
  val supervisor = system.actorOf(Props[Supervisor], name = "supervisor")

  for (ln <- Source.stdin.getLines) {
    println(ln)
    val msg = NewConference(ln.toLowerCase)

    if(ln.equals("q")){
      break()
    }
    supervisor ! msg
  }

}
