package wedt.sag.extension.Actors

import java.util.concurrent.{Executors, TimeUnit}

import akka.actor.{Actor, Props}
import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.crawler.HtmlContentCleaner
import pl.edu.pw.ii.entityminer.crawler.dynamicpage.HtmlUnitSpider
import pl.edu.pw.ii.entityminer.crawler.static.{PageAsyncHandler, WebpageAsyncHttpClient, WrappingHandler}
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.database.datasource.pagestorage.PageDirectoryCollector
import pl.edu.pw.ii.entityminer.mining.inference.CrfConferenceInferencer
import pl.edu.pw.ii.entityminer.mining.token.feature.FactorieLinearCrfFeatureCreator
import pl.edu.pw.ii.entityminer.web.WebPage
import wedt.sag.extension.Actors.Supervisor.{NewConference, PageCollectorAnswer, PageCollectorQuestion, WebCrawlerAnswer}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}



/**
  * Created by Marta on 2016-05-27.
  */
class WebCrawler extends Actor {

  def time[R](block: => R): Long = {
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val t1 = System.nanoTime()
    val elapsedTime = t1 - t0
    val minutes = TimeUnit.MINUTES.convert(elapsedTime, TimeUnit.NANOSECONDS)
    val seconds = TimeUnit.SECONDS.convert(elapsedTime, TimeUnit.NANOSECONDS)
    println("Elapsed time: " + minutes + "min " + seconds + "s " + elapsedTime + "ns")
    return t1 - t0
  }


  override def receive =
    new Receive() {
      implicit val ec = ExecutionContext
        .fromExecutor(Executors.newFixedThreadPool(100))
      var searchedName = ""
      override def isDefinedAt(x: Any): Boolean = true

      override def apply(v1: Any): Unit = {
        v1 match {
          case msg: NewConference =>
            Console.println("WebCrawler received " + v1)
            val msg1: String = v1.asInstanceOf[NewConference].msg

            searchedName = msg1.replaceAll(" |\\s", "+")
            val fileName = msg1.replaceAll(" |\\s", "")
            def client = new WebpageAsyncHttpClient
            var urlsToCheck = mutable.MutableList[String]()

            /**
              * Google searching for links
              */
            def searchGoogle(searchedName: String): String = {
              val urlLink = "https://www.google.com/search?q=" + searchedName
              client.syncDownload(urlLink, new WrappingHandler(urlLink, new PageAsyncHandler {
                override def onNextDownloadedPage(webPage: WebPage): Unit = {
                  urlsToCheck ++= webPage.burls
                  urlsToCheck = urlsToCheck.distinct
                  var urlsToCheckTmp = mutable.MutableList[String]()
                  urlsToCheck.foreach(url =>
                    if (!url.contains("http://webcache") && !url.contains("twitter") && url.contains("http") && !url.contains("youtube")) {
                      urlsToCheckTmp += url
                    }
                  )
                  urlsToCheck = urlsToCheckTmp
                }

                override def onThrowable(p1: Throwable, url: String): Unit = {
                  println("NOT DOWNLOADING PAGE " + p1)
                }
              }))
            }

            time {
              val future = Future[String] {
                searchGoogle(searchedName)
              }
              Await.result(future, Duration.Inf)
              println("After google searching")
            }
            urlsToCheck=urlsToCheck.dropRight(urlsToCheck.length-Conf.maxSearchedGoogleAnswers)

            /**
              * Downloading pages
              */
            time {
              val futures = new ListBuffer[Future[Unit]]
              for (i: Int <- 0 to urlsToCheck.length) {
                val pageDirectoryCollector = new PageDirectoryCollector
                pageDirectoryCollector.httpClient = new HtmlUnitSpider

                val future2 = Future {
                  try {
                    pageDirectoryCollector.collectPages(urlsToCheck.get(i).get, i, CrfConferenceInferencer.HtmlDir + "/" + fileName)
                  }
                  catch {
                    case e: Exception => println(e.getMessage)
                  }
                }
                futures += future2
              }
              Await.result(Future.sequence(futures), Duration.Inf)
              println("All pages downloaded!")
            }

            val pageDirectoryCollector = new PageDirectoryCollector
            pageDirectoryCollector.addIndexToManifestIfNotAdded(CrfConferenceInferencer.HtmlDir + "/" + fileName,1000)

            sender() ! WebCrawlerAnswer(msg.msg)
        }
      }
    }

}

