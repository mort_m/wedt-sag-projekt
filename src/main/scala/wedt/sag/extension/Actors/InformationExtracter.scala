package wedt.sag.extension.Actors

import java.util.concurrent.{Executors, TimeUnit}

import akka.actor.Actor
import cc.factorie._
import cc.factorie.app.nlp
import cc.factorie.app.nlp.{Document, Token}
import cc.factorie.model.Parameters
import pl.edu.pw.ii.entityminer
import pl.edu.pw.ii.entityminer.Conf
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.database.datasource.annotation.ConferenceHtmlAnnotation
import pl.edu.pw.ii.entityminer.mining.extract.{NotificationEntityChooser, SubmissionEntityChooser, _}
import pl.edu.pw.ii.entityminer.mining.factorie._
import pl.edu.pw.ii.entityminer.mining.factorie.TreeLabelCrfModel.TokenFeaturesDomain
import pl.edu.pw.ii.entityminer.mining.factorie.tools.FactorieModelSaver
import pl.edu.pw.ii.entityminer.mining.factorie.treecrf.LabeledTreeNerTag
import pl.edu.pw.ii.entityminer.mining.inference.CrfConferenceInferencer
import pl.edu.pw.ii.entityminer.mining.token.feature.{FactorieLinearCrfFeatureCreator, FeatureCreator}
import pl.edu.pw.ii.entityminer.mining.token.feature.FeatureCreator
import wedt.sag.extension.Actors.Supervisor.{InformationExtractorAnswer, WebCrawlerAnswer}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
/**
  * Created by Marta on 2016-05-27.
  */
class InformationExtracter extends Actor {
  private var model: TemplateModel with Parameters = _
  private var modelSaver: FactorieModelSaver = new FactorieModelSaver()
  val modelFilePath = "factorie_linear_crf"
  private var dataSetReader: DataSetReader =  new DataSetReader()
  private var inferencer: FactorieInferencer = new OptimizeChainFactorieInferencer()
  private def featureStorage = ""
  private val entityChoosers =
    List(
      new AbbreviationEntityChooser,
      new NameEntityChooser,
      new WhenEntityChooser,
      new WhereEntityChooser,
      new FinalVersionEntityChooser,
      new NotificationEntityChooser,
      new SubmissionEntityChooser
    )
  var finalFeatureStorage: String = ""


  def time[R](block: => R): Long = {
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val t1 = System.nanoTime()
    val elapsedTime = t1 - t0
    val minutes = TimeUnit.MINUTES.convert(elapsedTime, TimeUnit.NANOSECONDS)
    val seconds = TimeUnit.SECONDS.convert(elapsedTime, TimeUnit.NANOSECONDS)
    println("Elapsed time: " + minutes + "min " + seconds + "s " + elapsedTime + "ns")
    return t1 - t0
  }


  private def findAllOccurs(document: nlp.Document, categoryValueFunction: Token => String) = {
    val result = scala.collection.mutable.Map[String, List[Seq[Token]]]()
    var isSpecialSeq = false
    var currentLabel = ""
    var currentSeq: Seq[nlp.Token] = Nil
    for (token <- document.tokens) {
      val labelNoIob = ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.convertNoIop(categoryValueFunction(token))
      if (labelNoIob != ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP(ConferenceHtmlAnnotation.OTHER_LABEL)) {
        isSpecialSeq = true

        if (currentLabel != labelNoIob && currentLabel.nonEmpty) {
          result put(currentLabel, result.getOrElse(currentLabel, Nil) :+ currentSeq)
          currentSeq = Nil
        }
        currentLabel = labelNoIob

        currentSeq = currentSeq :+ token
      } else if (isSpecialSeq == true) {
        result put(currentLabel, result.getOrElse(currentLabel, Nil) :+ currentSeq)
        isSpecialSeq = false
        currentLabel = ""
        currentSeq = Nil
      }
    }
    if (isSpecialSeq == true) {
      result put(currentLabel, result.getOrElse(currentLabel, Nil) :+ currentSeq)
    }
    result
  }
  override def receive =
    new Receive() {
      implicit val ec = ExecutionContext
        .fromExecutor(Executors.newFixedThreadPool(100))
      override def isDefinedAt(x: Any): Boolean = true

      override def apply(v1: Any): Unit = {
        Console.println("InformationExtractor received " + v1)
        val fileName: String = v1.asInstanceOf[WebCrawlerAnswer].msg.replaceAll(" |\\s", "")
        val year = parseYear(v1.asInstanceOf[WebCrawlerAnswer].msg)

        val featureCreator = new FactorieLinearCrfFeatureCreator()

        /**
          * Creating and saving features and tokens
          */
        time {
          println("Creating and saving features...")
          featureCreator.createFeaturesAndSave("./"+CrfConferenceInferencer.HtmlDir + "/" + fileName, 1000, "./"+CrfConferenceInferencer.FeaturesDir + "/" + fileName, Conf.SINGLE_ENTITIES)
          println("Features saved!")
        }

        finalFeatureStorage = "./"+CrfConferenceInferencer.FeaturesDir+"/"+fileName

        if (model == null) {
          model = new LinearCrf
          modelSaver.deserialize("./"+Conf.LINEAR_MODEL_PATH, model)
        }
        TokenFeaturesDomain.freeze()
        println("Reading data sets for CRF..")
        val doc = dataSetReader.readDataSestForLinearCrf(finalFeatureStorage, 0, 1000, 0)._2.head
        println("Data sets readed!")

        /**
          * Inference with CRF and Viterbi
          */
        time {
          println("Inferencing with model...")
          val future = Future[Document] {
            inferencer.inference(doc, model, year.toString)
          }
          Await.result(future, Duration.Inf)
          println("Inferenced with model!")
        }

        val inferredOccur = findAllOccurs(doc, thisCategoryFunction) //tylko formatowanie ladne zapewnia

        val result = new ConferenceData()
        for (label <- ConferenceHtmlAnnotation.ANNO_TO_LABEL_MAP.valuesNoIob) {
          inferredOccur.get(label) match {
            case None =>
            case Some(inferredSeqs) =>
              entityChoosers.filter(_.operatingLabel == label).head.chooseBestEntity(inferredSeqs, model, label) match {
                case None =>
                case Some(information) =>
                  //println("label: "+label)
                  //println("Information: "+information)
                  result.setInformationByLabel(label, information)
              }
          }
        }

        result

        val msg = InformationExtractorAnswer("Answering after question "+v1.asInstanceOf[WebCrawlerAnswer].msg+"\n" + result)
        sender() ! msg
      }
    }
  private def thisCategoryFunction: (Token) => String = {
    _.attr[LabeledTreeNerTag].categoryValue
  }

  def parseYear(toSplit: String): Int = {
    val splitted = toSplit.split(" ")
    var year = 0
    splitted.foreach(split => {
      var int = 0
      try {
        int = Integer.decode(split)
      } catch {
        case e: Exception =>
      }
      if (int != 0 && int > 2000) {
        year = int
        println("YEAR: " + year)
      }
    })
    year
  }
}
