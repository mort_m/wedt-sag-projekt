package wedt.sag.extension.Actors

import akka.actor.{Actor, Props}
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import pl.edu.pw.ii.entityminer.web.WebPage
import wedt.sag.extension.Actors.Supervisor.{InformationExtractorAnswer, NewConference, WebCrawlerAnswer}

/**
  * Created by Marta on 2016-05-27.
  */
object Supervisor {

  case class PageCollectorQuestion(confData: Option[ConferenceData], iter: Int, searchedName: String)
  case class PageCollectorAnswer(confData: Option[ConferenceData], iter: Int, searchedName: String)

  case class WebCrawlerAnswer(msg: String)

  case class InformationExtractorAnswer(msg: String)

  case class NewConference(msg: String)

}

class Supervisor extends Actor {

  override def receive =
    new Receive() {
      override def isDefinedAt(x: Any): Boolean = true

      override def apply(v1: Any): Unit = {
        v1 match {
          case msg : NewConference =>
            println("Searching for new conference")
            val crawler = context.actorOf(Props[WebCrawler])
            crawler ! msg

          case msg: WebCrawlerAnswer =>
            println("Extracting informations about "+msg.msg)
            val extractor = context.actorOf(Props[InformationExtracter])
            extractor ! msg

          case msg : InformationExtractorAnswer =>
            println(msg)

          case _ =>

        }
      }
    }

}
