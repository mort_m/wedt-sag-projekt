package pl.edu.pw.ii.entityminer.mining.token.feature.discover

import org.scalatest.matchers.ShouldMatchers
import org.scalatest.FlatSpec
import org.springframework.context.support.{ClassPathXmlApplicationContext, AbstractApplicationContext}
import pl.edu.pw.ii.entityminer.{StringTestObjects, RootFacade}
import java.io.ByteArrayInputStream
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import org.jsoup.Jsoup
import org.jsoup.nodes.{TextNode, Document}
import pl.edu.pw.ii.entityminer.crawler.dom.{DfsJsoupIter, DomParser}
import pl.edu.pw.ii.entityminer.mining.token.feature.{DomTokenizer, FactorieLinearCrfFeatureCreator}

/**
  * Created with IntelliJ IDEA.
  * User: Rafael Hazan
  * Date: 7/2/13
  * Time: 11:46 PM
  */
class GateDiscoverSpec extends FlatSpec with ShouldMatchers {

   "Gate" should "find all entities." in {
     val applicationContext: AbstractApplicationContext = new ClassPathXmlApplicationContext("META-INF/spring/module-context.xml")
     val rootFacade = applicationContext.getBean("RootFacade", classOf[RootFacade])
     val annotator = rootFacade.annotator

     val domTokenizer = rootFacade.featureCreator.asInstanceOf[FactorieLinearCrfFeatureCreator].featureTokenizer.asInstanceOf[DomTokenizer]
     val gateDiscover = domTokenizer.gateFeatureDiscover.asInstanceOf[GateFeatureDiscover]
     val domParser = domTokenizer.domParser
     val tokenizer = domTokenizer.annotateStrategy

//     val someText = StringTestObjects.page1Text
     val someText = StringTestObjects.page2Text
     val tokens = tokenizer.tokenize(someText, new TextNode(someText, ""))

     val entities = domTokenizer.divideToEntitiesSimple(tokens).flatten
     val buffer = new StringBuilder()
     for (entity <- entities) {
       buffer.clear()
       for (word <- entity) {
         buffer append (word.originalText + ", ")
       }
       println(buffer.result())
     }
     assert(tokens.size == entities.flatten.size)
//     gateDiscover.divideToEntities(tokens)
   }

  "Simple entity divider" should "divide to one class entities." in {
    val applicationContext: AbstractApplicationContext = new ClassPathXmlApplicationContext("META-INF/spring/module-context.xml")
    val rootFacade = applicationContext.getBean("RootFacade", classOf[RootFacade])
    val annotator = rootFacade.annotator

    val domTokenizer = rootFacade.featureCreator.asInstanceOf[FactorieLinearCrfFeatureCreator].featureTokenizer.asInstanceOf[DomTokenizer]
    val gateDiscover = domTokenizer.gateFeatureDiscover.asInstanceOf[GateFeatureDiscover]
    val domParser = domTokenizer.domParser
    val tokenizer = domTokenizer.annotateStrategy






    val someText = StringTestObjects.page1Text
    val tokens = tokenizer.tokenize(someText, new TextNode(someText, ""))

    val entities = domTokenizer.divideToEntitiesSimple(tokens).flatten
    val buffer = new StringBuilder()
    for (entity <- entities) {
      buffer.clear()
      for (word <- entity) {
        buffer append (word.originalText + ", ")
      }
      println(buffer.result())
    }
  }
 }
