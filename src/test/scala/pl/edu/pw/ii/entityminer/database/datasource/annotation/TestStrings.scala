package pl.edu.pw.ii.entityminer.database.datasource.annotation

/**
 * Created by Raphael Hazan on 2/8/14.
 */
object TestStrings {
  val page1 = """<?xml version="1.0" encoding="UTF-8"?>
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta name="title" content="acity" />
        <meta name="description" content="Advances in Computing and Information technology" />
        <meta name="author" content="Airccse" />
        <meta name="copyright" content="First International Conference on Computational Science and Engineering (CSE-2013)" />
        <meta name="publisher" content="http://aicty.org/" />
        <meta name="language" content="Many" />
        <meta name="robots" content="index, follow" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="http://aicty.org/ - Computational Science" />
        <meta name="audience" content="all" />
        <meta name="distribution" content="global" />
        <meta name="imagetoolbar" content="no" />
        <meta property="og:title" content="First International Conference on Computational Science and Engineering (CSE-2013)" />
        <meta property="og:site_name" content="http://aicty.org/" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="http://aicty.org/index.html" />
        <meta property="og:image" content="http://aicty.org/images" />
        <meta property="og:description" content="CSE-2013 focuses on Complex systems, information and computation using mathematics and engineering techniques. This conference will act as a major forum for the presentation of innovative ideas, approaches, developments, and research projects in the area of Computation theory and applications. It will also serve to facilitate the exchange of information between researchers and industry professionals to discuss the latest issues and advancement in the area of advanced Computation and its applications will be covered during the conference " />
        <title>
          AIFL - 2014    </title>
        <link rel="stylesheet" type="text/css" media="all" href="0/style.css" />
      </head>
      <body>
        <div id="page">
          <div id="wapper">
            <div class="top">
              <a href="index.html"> <h1> </h1> </a>
            </div>
            <c2_info_paragraph>
              <div class="venu">
                <h4 align="right">
                  <c1_when>
                    April
                  </c1_when>
                  <c1_when>
                    4
                  </c1_when>
                  <c1_when>
                    ~
                  </c1_when>
                  <c1_when>
                    5
                  </c1_when>
                  <c1_when>
                    ,
                  </c1_when>
                  <c1_when>
                    2014
                  </c1_when>,
                  <c1_where>
                    Dubai
                  </c1_where>,
                  <c1_where>
                    UAE
                  </c1_where> </h4>
              </div>
            </c2_info_paragraph>
            <div class="clear">
            </div>
            <div class="sidebar">
              <div id="style1">
                <ul>
                  <li class="odd"> <a href="index.html"> Home </a> </li>
                  <li class="even"> <a href="papersub.html"> Paper Submission </a> </li>
                  <li class="odd"> <a href="workshop.html"> Workshops </a> <br /> </li>
                  <li class="even"> <a href="committee.html"> Committee Members </a> </li>
                  <li class="odd"> <a href="acceptedpaper.html"> Accepted Papers </a> </li>
                  <li class="even"> <a href="venue.html"> Venue &amp; Visa </a> </li>
                  <li class="odd"> <a href="contact.html"> Contact Us </a> </li>
                </ul>
              </div>
              <div class="style121">
                <h3 align="center"> Proceedings by </h3>
                <p align="center"> <a href="http://airccse.org/cscp.html" target="blank"> </a> </p>
                <center>
                  <a href="http://airccse.org/cscp.html" target="blank"> <img height="50" width="120" src="0/Final.jpg" /> </a>
                </center>
                <p> </p>
                <h3 align="center"> Sponsored by </h3>
                <p align="center"> <a href="http://airccj.org/csecfp/cfp/index.php"> </a> </p>
                <center>
                  <a href="http://airccj.org/csecfp/cfp/index.php"> <img height="60" alt="" width="50" src="0/cscp.gif" /> </a>
                </center>
                <p> </p>
                <h3 align="center"> Courtesy </h3>
                <h6 align="center"> <img height="49" width="104" src="0/docoloc.JPG" /> </h6>
                <h3 align="center"> Technically Sponsored by </h3>
                <p align="center"> <img height="67" alt="" width="143" src="0/CSITC.gif" /> </p>
              </div>
              <div class="style121">
                <h3 align="center"> Proceedings </h3>
                <p style="font-size:12px; color:#F00; padding:0px 5px 0px 5px; " align="justify"> The Registration fee is 250 USD. Hard copy of the proceedings will be distributed during the Conference. The softcopy will be available on <a href="http://airccj.org/csecfp/library/index.php"> AIRCC Digital Library </a> </p>
              </div>
              <div class="style121">
                <h3 align="center"> Other Conferences </h3>
                <p align="center"> <a href="http://aicty.org/cse14/index.html"> CSE - 2014 </a> <br /> <a href="http://airccj.org/2014/cics/index.html"> CICS - 2014 </a> <br /> <a href="http://airccj.org/2014/dbdm/index.html"> DBDM - 2014 </a> <br /> <a href="http://airccj.org/2014/scom/index.html"> SCOM - 2014 </a> <br /> </p>
                <h3 align="center"> Past Conference </h3>
                <p align="center"> <a href="http://airccj.org/2013/aifl/index.html"> AIFL - 2013 </a> <br /> </p>
              </div>
            </div>
            <div class="mainbar">
              <table class="br1">
                <tbody align="left">
                  <tr>
                    <td width="36%"> <h3> Call for Papers </h3> </td>
                    <td width="43%"></td>
                    <td width="21%"> <h3></h3> </td>
                  </tr>
                </tbody>
              </table>
              <p align="justify"> This Conference provides a forum for researchers who address this issue and to present their work in a peer-reviewed forum. Authors are solicited to contribute to the conference by submitting articles that illustrate research results, projects, surveying works and industrial experiences that describe significant advances in the following areas, but are not limited to these topics only </p>
              <h3 class="br1"> Artificial Intelligence </h3>
              <p align="justify"> Authors are solicited to contribute to this conference by submitting articles that illustrate research results, projects, surveying works and industrial experiences that describe significant advances in the areas of Artificial Intelligence &amp; applications. Topics of interest include, but are not limited to, the following: </p>
              <table border="0" cellpadding="0" width="80%">
                <tbody align="left">
                  <tr>
                    <td width="40%" valign="top">
                      <ul class="list_style">
                        <li> AI Algorithms </li>
                        <li> Artificial Intelligence tools &amp; Applications </li>
                        <li> Automatic Control </li>
                        <li> Bioinformatics </li>
                        <li> Natural Language Processing </li>
                        <li> CAD Design &amp; Testing </li>
                        <li> Computer Vision and Speech Understanding </li>
                        <li> Data Mining and Machine Learning Tools </li>
                        <li> Fuzzy Logic </li>
                        <li> Heuristic and AI Planning Strategies and Tools </li>
                        <li> Computational Theories of Learning </li>
                        <li> Hybrid Intelligent Systems </li>
                        <li> Information Retrieval </li>
                        <li> Intelligent System Architectures </li>
                        <li> Knowledge Representation </li>
                      </ul> </td>
                    <td width="40%" valign="top">
                      <ul class="list_style">
                        <li> Knowledge-based Systems </li>
                        <li> Mechatronics </li>
                        <li> Multimedia &amp; Cognitive Informatics </li>
                        <li> Neural Networks </li>
                        <li> Parallel Processing </li>
                        <li> Pattern Recognition </li>
                        <li> Pervasive computing and ambient intelligence </li>
                        <li> Programming Languages </li>
                        <li> Reasoning and Evolution </li>
                        <li> Recent Trends and Developments </li>
                        <li> Robotics </li>
                        <li> Semantic Web Techniques and Technologies </li>
                        <li> Soft computing theory and applications </li>
                        <li> Software &amp; Hardware Architectures </li>
                        <li> Web Intelligence Applications &amp; Search </li>
                      </ul> </td>
                  </tr>
                </tbody>
              </table>
              <h3 class="br1"> Fuzzy Logic </h3>
              <p align="justify"> Topics to be discussed in this forum includes (but are not limited to) the following: </p>
              <table border="0" cellpadding="0" width="80%">
                <tbody align="left">
                  <tr>
                    <td width="40%" valign="top">
                      <ul class="list_style">
                        <li> Fuzzy logic techniques &amp; Algorithm </li>
                        <li> Fuzzy mathematics </li>
                        <li> Fuzzy measure and integral </li>
                        <li> Type 2 fuzzy Logic </li>
                        <li> Possibility theory and imprecise probability </li>
                        <li> Fuzzy database </li>
                        <li> Fuzzy Clustering </li>
                        <li> Fuzzy decision support system </li>
                        <li> Fuzzy expert system </li>
                        <li> Fuzzy mathematical programming </li>
                        <li> Fuzzy decision making and decision support systems </li>
                        <li> Fuzzy modeling and fuzzy control of biotechnological processes </li>
                        <li> Fuzzy neural systems, neuro-fuzzy systems </li>
                        <li> Fuzzy systems modeling and identification </li>
                      </ul> </td>
                    <td width="40%" valign="top">
                      <ul class="list_style">
                        <li> Fuzzy pattern recognition </li>
                        <li> Fuzzy process control </li>
                        <li> Fuzzy reasoning system </li>
                        <li> Fuzzy-rule based system </li>
                        <li> Fuzzy System in Multimedia and web-based applications </li>
                        <li> Fuzzy system applications in computer vision </li>
                        <li> Hybrid fuzzy systems (fuzzy-neuro-evolutionary-rough) </li>
                        <li> Fuzzy system applications in e-commerce </li>
                        <li> Fuzzy sets in bioinformatics </li>
                        <li> Fuzzy system applications in human-machine interface </li>
                        <li> Fuzzy system applications in robotics </li>
                        <li> Fuzzy system applications in system and control engineering </li>
                      </ul> </td>
                  </tr>
                </tbody>
              </table>
              <h3 class="br1"> Paper submission </h3>
              <p align="justify"> Authors are invited to submit papers through the conference <strong> <a href="https://www.easychair.org/conferences/?conf=aifl2014"> Submission system </a> </strong> by January 18, 2014. Submissions must be original and should not have been published previously or be under consideration for publication while being evaluated for this conference. The proceedings of the conference will be published by <a href="http://airccse.org/cscp.html"> <strong> Computer Science Conference Proceedings </strong> </a> in <a href="http://airccse.org/cscp.html"> <strong> Computer Science &amp; Information Technology (CS &amp; IT) </strong> </a> series (Confirmed). <br /> <br /> Selected papers from AI &amp; FL - 2014, after further revisions, will be published in the special issue of the following journals </p>
              <ul style="line-height:22px; font-size:14px; font-family:Arial; list-style:none">
                <li> <a href="http://airccse.org/journal/ijaia/ijaia.html"> International Journal of Artificial Intelligence &amp; Applications ( IJAIA ) </a> </li>
                <li> <a href="http://airccse.org/journal/ijfls/ijfls.html"> International Journal of Fuzzy Logic Systems( IJFLS ) </a> </li>
                <li> <a href="http://airccse.org/journal/ijscai/index.html"> International Journal on Soft Computing, Artificial Intelligence and Applications ( IJSCAI ) </a> </li>
              </ul>
              <h3 class="br1"> Important Dates </h3>
              <ul style="font-family:Arial, Helvetica, sans-serif; line-height:20px; font-size:12px; font-weight:bold;">
                <li> Submission Deadline : January 18, 2014 </li>
                <li> Authors Notification : <b> March 10, 2014 </b> <br /> </li>
                <li> Final Manuscript Due : <b> March 20, 2014 </b> </li>
              </ul>
            </div>
            <div class="clear">
            </div>
            <div class="footer">
              <h6 align="center"> All Rights Reserved @ AIFL 2014 </h6>
            </div>
            <div class="clear">
            </div>
          </div>
          <div class="clear">
          </div>
          <p> </p>
        </div>
        <!-- text below generated by server. PLEASE REMOVE -->
        <!-- Counter/Statistics data collection code -->
        <img style="display:none" src="0/visit.gif" border="0" />
        <noscript>
        </noscript>
      </body>
    </html>"""
}