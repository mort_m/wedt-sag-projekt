package pl.edu.pw.ii.entityminer.database.datasource.pagestorage

import org.scalatest.matchers.ShouldMatchers
import org.scalatest.FlatSpec
import pl.edu.pw.ii.entityminer.database.datasource.wikicfp.WikicfpDatasource

/**
  * Created with IntelliJ IDEA.
  * User: Rafael Hazan
  * Date: 7/2/13
  * Time: 11:46 PM
  */
class ManifestSpec extends FlatSpec with ShouldMatchers {

  "It" should "read manifest." in {
    val pageSaver = new PageDirectoryCollector
    val manifest = pageSaver.readPageManifest("data/test/pagestorage/0")
    println(manifest.result())
  }

  "It" should "save in xml and read manifest." in {
    val pageSaver = new PageDirectoryCollector
    val manifest = pageSaver.readPageManifest("data/test/pagestorage/100")
//    save
    pageSaver.saveManifestInXml("data/test/pagestorage/100/pageManifest.xml", manifest)

    val manifest2 = pageSaver.readPageManifestFromXml("data/test/pagestorage/100")

    for ((webAddress, (labels, filePath)) <- manifest2.result()) {
      println("--------------------------------------")
      println(webAddress)
      println(labels.map(_.trim))
      println(filePath)
    }

    assert(manifest.result.size == manifest2.result.size)
  }
 }
