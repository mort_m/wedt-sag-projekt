package pl.edu.pw.ii.entityminer.database.datasource.wikicfp

import org.scalatest.matchers.ShouldMatchers
import org.scalatest.FlatSpec
import org.springframework.context.support.{ClassPathXmlApplicationContext, AbstractApplicationContext}
import pl.edu.pw.ii.entityminer.RootFacade
import java.io.ByteArrayInputStream
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import org.jsoup.Jsoup
import org.jsoup.nodes.{TextNode, Document}
import pl.edu.pw.ii.entityminer.crawler.dom.{DfsJsoupIter, DomParser}

/**
 * Created with IntelliJ IDEA.
 * User: Rafael Hazan
 * Date: 7/2/13
 * Time: 11:46 PM
 */
class ConferenceDataSpec extends FlatSpec with ShouldMatchers {

  val applicationContext: AbstractApplicationContext = new ClassPathXmlApplicationContext("META-INF/spring/module-context.xml")
  val rootFacade = applicationContext.getBean("RootFacade", classOf[RootFacade])

  "Conferece data" should "be saved and loaded." in {
    val dir = "data/test/pagestorage/100"
    val data = WikicfpDatasource.loadConference(dir)

    WikicfpDatasource.saveConference(data, dir)
    val data2 = WikicfpDatasource.loadConferenceFromXml(dir)

    assert(data == data2)
  }
}
