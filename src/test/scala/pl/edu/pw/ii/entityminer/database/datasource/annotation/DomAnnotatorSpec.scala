package pl.edu.pw.ii.entityminer.database.datasource.annotation

import org.scalatest.matchers.ShouldMatchers
import org.scalatest.FlatSpec
import org.springframework.context.support.{ClassPathXmlApplicationContext, AbstractApplicationContext}
import pl.edu.pw.ii.entityminer.RootFacade
import javax.xml.parsers.DocumentBuilderFactory
import java.io.{ByteArrayInputStream, StringReader}
import org.xml.sax.InputSource
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import org.jsoup.Jsoup
import org.jsoup.nodes.{TextNode, Node, Element, Document}
import org.jsoup.select.NodeVisitor
import pl.edu.pw.ii.entityminer.crawler.dom.{DfsJsoupIter, DomParser}

/**
  * Created with IntelliJ IDEA.
  * User: Rafael Hazan
  * Date: 7/2/13
  * Time: 11:46 PM
  */
class DomAnnotatorSpec extends FlatSpec with ShouldMatchers {

   "Conferece web pages" should "be properly annotated." in {
     val applicationContext: AbstractApplicationContext = new ClassPathXmlApplicationContext("META-INF/spring/module-context.xml")
     val rootFacade = applicationContext.getBean("RootFacade", classOf[RootFacade])
     val annotator = rootFacade.annotator

//     val db = DocumentBuilderFactory.newInstance().newDocumentBuilder()
//     val is = new InputSource()
//     is.setCharacterStream(new StringReader(xmlString))
     val xmlString = "<root>1111<node1>222222</node1>333333 aaa bbb 55555</root>"
     val is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

//     val doc = db.parse(is)
     val doc = Jsoup.parse(is, "UTF-8", "")


//     doc.traverse(new NodeVisitor() {
//       def head(node: Node, depth: Int) {
//         println("Entering tag: " + node.nodeName());
//         if (node.isInstanceOf[TextNode]) {
//           println("text " + node.asInstanceOf[TextNode].text())
//         }
//       }
//       def tail(node: Node, depth: Int) {
//       }
//     });

     val data = new ConferenceData()
     data.names = List("aaa bbb")
     println("before")
     printDocument(doc)
     annotator.annotatePage(doc, data)
     println("after")
     printDocument(doc)
   }

  private def printDocument(document: Document) = {
    val parser = new DomParser
    val iter = parser.getDfsIterator(document)
    printNode(iter, 0)
  }

  private def printNode(iter: DfsJsoupIter, depth: Int): Unit = {
    iter.next() match {
      case None =>
      case Some(node) =>
        println(node.nodeName())
        if (node.isInstanceOf[TextNode]) {
          println("text " + node.asInstanceOf[TextNode].text() + " with parent " + node.parent().nodeName())
        }
        printNode(iter, 0)
    }

//    println(depth + "node: " + element.nodeName() + " | " + element.ownText())
//    for (i <- 0 to (element.children().size - 1).toInt) {
//      printNode(element.child(i.toInt), depth + 1)
//    }
  }
 }
