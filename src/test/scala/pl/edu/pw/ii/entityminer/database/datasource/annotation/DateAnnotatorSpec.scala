package pl.edu.pw.ii.entityminer.database.datasource.annotation

import org.scalatest.matchers.ShouldMatchers
import org.scalatest.FlatSpec
import org.springframework.context.support.{ClassPathXmlApplicationContext, AbstractApplicationContext}
import pl.edu.pw.ii.entityminer.RootFacade
import java.io.ByteArrayInputStream
import pl.edu.pw.ii.entityminer.database.datasource.ConferenceData
import org.jsoup.Jsoup
import org.jsoup.nodes.{TextNode, Document}
import pl.edu.pw.ii.entityminer.crawler.dom.{DfsJsoupIter, DomParser}
import pl.edu.pw.ii.entityminer.database.model.token.WebToken

/**
 * Created with IntelliJ IDEA.
 * User: Rafael Hazan
 * Date: 7/2/13
 * Time: 11:46 PM
 */
class DateAnnotatorSpec extends FlatSpec with ShouldMatchers {

  val applicationContext: AbstractApplicationContext = new ClassPathXmlApplicationContext("META-INF/spring/module-context.xml")
  val rootFacade = applicationContext.getBean("RootFacade", classOf[RootFacade])

  "Date on conferece web pages" should "be not annotated." in {
    val validDateSeq = List("Jul", "23", "2014", "Jul", "26", "2014")
    val annotator = new ConferenceDateAnnotator
    val tokens = annotator.tokenize("July 26 - 31", null)
    val (found, notAnalyzedYet) = annotator.findFirstOccurence(tokens, validDateSeq)
    assert(found.isEmpty)
  }

  "Date on conferece web pages" should "be properly annotated." in {
    val annotator = rootFacade.annotator

    val xmlString = "<root>1111<node1>222222</node1>333333 aaa 10 mar to Sep. 12 55555</root>"
    val is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"))

    val doc = Jsoup.parse(is, "UTF-8", "")


    val data = new ConferenceData()
    data.date = "10 Martch 12 Sep."
    data.paperSubmissionDate = "xxxxxx"
    data.nameAbbreviation = "xxxxxxxx"
    println("before")
    printDocument(doc)
    annotator.annotatePage(doc, data)
    println("after")
    printDocument(doc)
  }

  private def printDocument(document: Document) = {
    val parser = new DomParser
    val iter = parser.getDfsIterator(document)
    printNode(iter, 0)
  }

  private def printNode(iter: DfsJsoupIter, depth: Int): Unit = {
    iter.next() match {
      case None =>
      case Some(node) =>
        println(node.nodeName())
        if (node.isInstanceOf[TextNode]) {
          println("text " + node.asInstanceOf[TextNode].text() + " with parent " + node.parent().nodeName())
        }
        printNode(iter, 0)
    }

    //    println(depth + "node: " + element.nodeName() + " | " + element.ownText())
    //    for (i <- 0 to (element.children().size - 1).toInt) {
    //      printNode(element.child(i.toInt), depth + 1)
    //    }
  }
}
