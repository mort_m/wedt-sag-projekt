package pl.edu.pw.ii.entityminer

/**
 * Created by Raphael Hazan on 4/23/2014.
 */
object StringTestObjects {

  val page1Text = """You are invited to participate in The International Conference on Artificial Intelligence and Pattern Recognition (AIPR2014) that
                    |will be held in Asia Pacific University of Technology and Innovation (APU), Kuala Lumpur, Malaysia on November 17-19, 2014 as
                    |part of The Third World Congress on Computing and Information Technology (WCIT) . The event will be held over three days,
                    |with presentations delivered by researchers from the international community, including presentations from keynote speakers and state-of-the-art lectures.
                    |Welcome to our Big Shit Conference LOLO""".
  stripMargin

  val page2Text =
    """All papers of EACCI 2014 will be published in the Volume of International Journal of Computer
      |Theory and Engineering (IJCTE) (ISSN: 1793-8201), and will be indexed by Electronic Journals Library ,
      |EBSCO, Engineering & Technology Digital Library, Google Scholar, INSPEC, Ulrich's Periodicals Directory, Crossref, ProQuest,
      |WorldCat, and EI ( INSPEC, IET ). """.stripMargin

  val page1 =
    """
      |<html><head><title>AIPR2014 International Conference in Malaysia | SDIWC Conferences</title></head>
      |<body>
      |You are invited to participate in The International Conference on Artificial Intelligence and Pattern Recognition (AIPR2014) that
      |will be held in Asia Pacific University of Technology and Innovation (APU), Kuala Lumpur, Malaysia on November 17-19, 2014 as
      |part of The Third World Congress on Computing and Information Technology (WCIT) . The event will be held over three days,
      |with presentations delivered by researchers from the international community, including presentations from keynote speakers and state-of-the-art lectures.
      |</body></html>
    """.stripMargin
}
