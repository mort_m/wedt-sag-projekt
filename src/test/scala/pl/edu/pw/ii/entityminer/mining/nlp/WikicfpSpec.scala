package pl.edu.pw.ii.entityminer.mining.nlp

import org.scalatest.matchers.ShouldMatchers
import org.scalatest.FlatSpec
import cc.factorie.app.nlp.pos.{PennPosTag, OntonotesForwardPosTagger}
import cc.factorie.app.nlp
import cc.factorie.app.nlp._
import cc.factorie.app.nlp.pos

/**
 * Created with IntelliJ IDEA.
 * User: Rafael Hazan
 * Date: 7/2/13
 * Time: 11:46 PM
 */
class PosTaggerSpec extends FlatSpec with ShouldMatchers {

  "Post tagger" should "tag document" in {
    val doc = new nlp.Document("").setName("tempName")
    doc.annotators(classOf[Token]) = UnknownDocumentAnnotator.getClass // register that we have token boundaries
    doc.annotators(classOf[Sentence]) = UnknownDocumentAnnotator.getClass // register that we have sentence boundaries
    val sentence = new Sentence(doc)
    List("10th", "merecenary", "horse", "jumps", "over", "people", "10").foreach {
      word =>
        val token = new Token(sentence, word)
    }

//    val doc = new Document("Education is the most powerful weapon which you can use to change the world.")
    DocumentAnnotatorPipeline(DocumentAnnotatorPipeline.defaultDocumentAnnotationMap, Nil, List(classOf[pos.PennPosTag])).process(doc)
    for (token <- doc.tokens)
      println("%-10s %-5s".format(token.string, token.attr[PennPosTag].categoryValue))

//    println(sentence)
  }
}
