package pl.edu.pw.ii.entityminer.mining.token.feature

import org.scalatest.matchers.ShouldMatchers
import collection.JavaConversions._
import org.scalatest.FlatSpec
import java.net.URL
import de.l3s.boilerpipe.extractors._
import java.io.{PrintWriter, InputStreamReader, File, FileInputStream}
import de.l3s.boilerpipe.sax.{HTMLHighlighter, BoilerpipeSAXInput}
import org.xml.sax.InputSource
import org.apache.tika.io.IOUtils
import de.l3s.boilerpipe.document.TextDocument

/**
 * Created with IntelliJ IDEA.
 * User: Rafael Hazan
 * Date: 7/2/13
 * Time: 11:46 PM
 */
class BoilerplateRemovalSpec extends FlatSpec with ShouldMatchers {

  "Boilerplate" should "be removed" in {
    val file = new File("data/test/boilerplate/0.html");
    val inputStream = new FileInputStream(file);
    val reader = new InputStreamReader(inputStream, "UTF-8");
    val text = KeepEverythingExtractor.INSTANCE.getText(reader)
    println(text)
    assert(text.nonEmpty)
  }

  "Web page" should "be converted to textDocument" in {
    val file = new File("data/test/boilerplate/0.html");
    val inputStream = new FileInputStream(file);
    val source = new InputSource(new InputStreamReader(inputStream, "UTF-8"));
    val textDoc = new BoilerpipeSAXInput(source).getTextDocument()
    textDoc.getTextBlocks.toList.foreach {
      block =>
        println("content:")
        println(block.getText)
    }
    assert(textDoc.getTextBlocks.toList.nonEmpty)
  }

  "Boilerplate" should "clean document and preserve html" in {
    val file = new File("data/test/boilerplate/0.html");
    val inputStream = new FileInputStream(file);
    val source = new InputSource(new InputStreamReader(inputStream, "UTF-8"));

    val str = IOUtils.toString(inputStream, "UTF-8");
//    println(str)

    val extractor = CommonExtractors.KEEP_EVERYTHING_EXTRACTOR;
    //    val hh = HTMLHighlighter.newHighlightingInstance();
    val hh = HTMLHighlighter.newExtractingInstance()

    val textDoc = new BoilerpipeSAXInput(source).getTextDocument()
//    println(hh.process(new URL("http://paginas.fe.up.pt/~mash14/index.html"), extractor));
    println(hh.process(file.toURI.toURL, extractor));
//        println(hh.process(new TextDocument(Nil), str))
  }
}
