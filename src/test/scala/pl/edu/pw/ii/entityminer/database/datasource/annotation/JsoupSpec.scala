package pl.edu.pw.ii.entityminer.database.datasource.annotation

import org.scalatest.matchers.ShouldMatchers
import org.scalatest.FlatSpec
import org.springframework.context.support.{ClassPathXmlApplicationContext, AbstractApplicationContext}
import pl.edu.pw.ii.entityminer.RootFacade
import java.io.ByteArrayInputStream
import org.jsoup.Jsoup
import org.jsoup.nodes.{Node, TextNode, Document}
import pl.edu.pw.ii.entityminer.crawler.dom.{DfsJsoupIter, DomParser}
import org.jsoup.select.NodeVisitor
import org.jsoup.parser.Parser

/**
 * Created with IntelliJ IDEA.
 * User: Rafael Hazan
 * Date: 7/2/13
 * Time: 11:46 PM
 */
class JsoupSpec extends FlatSpec with ShouldMatchers {

  "Conferece web pages" should "be properly annotated." in {
    val applicationContext: AbstractApplicationContext = new ClassPathXmlApplicationContext("META-INF/spring/module-context.xml")
    val rootFacade = applicationContext.getBean("RootFacade", classOf[RootFacade])
    val annotator = rootFacade.annotator

    //     val db = DocumentBuilderFactory.newInstance().newDocumentBuilder()
    //     val is = new InputSource()
    //     is.setCharacterStream(new StringReader(xmlString))
    val xmlString = "<html><head><title>a1 <span>t1</span> a2</title></head><span>t1</span>1111<node1>222222</node1>333333 aaa bbb 55555</html>"
//    val is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));
val is = new ByteArrayInputStream(TestStrings.page1.getBytes("UTF-8"));
    val domParser = new DomParser

    val doc = Jsoup.parse(TestStrings.page1, "", Parser.xmlParser())
//    printDocumentWithJsoupIter(doc)
    printDocument(doc)
  }

  private def printDocumentWithJsoupIter(document: Document) = {
    document.traverse(new NodeVisitor {
      def tail(node: Node, depth: Int): Unit = {
        println("tail " + node.nodeName())
      }

      def head(node: Node, depth: Int): Unit = {
        println("head " + node.nodeName())
        if (node.isInstanceOf[TextNode]) {
          println("text " + node.asInstanceOf[TextNode].text() + " with parent " + node.parent().nodeName())
        }
      }
    })
  }

  private def printDocument(document: Document) = {
    val parser = new DomParser
    val iter = parser.getDfsIterator(document)
    printNode(iter, 0)
  }

  private def printNode(iter: DfsJsoupIter, depth: Int): Unit = {
    iter.next() match {
      case None =>
      case Some(node) =>
        println(node.nodeName())
        if (node.isInstanceOf[TextNode]) {
          println("text " + node.asInstanceOf[TextNode].text() + " with parent " + node.parent().nodeName())
        }
        printNode(iter, 0)
    }
  }
}
