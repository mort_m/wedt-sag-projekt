package pl.edu.pw.ii.entityminer.util

import org.scalatest.matchers.ShouldMatchers
import org.scalatest.FlatSpec

/**
 * Created with IntelliJ IDEA.
 * User: Rafael Hazan
 * Date: 7/2/13
 * Time: 11:46 PM
 */
class NetHelperSpec extends FlatSpec with ShouldMatchers {

  "Net helper" should "proprerly merge uri parts" in {
    val base = "www.xxxx.com/house/room/desk"
    val relative = "../cellar/shaft"
    NetHelper.composeUri(base, relative) should equal("www.xxxx.com/house/cellar/shaft")
  }
}
