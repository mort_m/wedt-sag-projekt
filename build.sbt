import com.github.retronym.SbtOneJar

name := "eduentitymine-proc"

scalaVersion := "2.11.7"

exportJars := true

SbtOneJar.oneJarSettings

resolvers += "Sonatype Nexus Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

resolvers += "SpringSource Repository" at "http://repo.springsource.org/release"

resolvers += "Spring Maven Release Repository" at "http://repo.springsource.org/libs-release"

resolvers += "Local Maven Repository" at "file:/c:/Users/ralpher/.m2/repository"

resolvers += "IESL Releases" at "https://dev-iesl.cs.umass.edu/nexus/content/groups/public"

resolvers += "boilerpipe" at "http://boilerpipe.googlecode.com/svn/repo/"

resolvers += "dev.davidsoergel.com releases" at "http://dev.davidsoergel.com/nexus/content/repositories/releases"

resolvers += "dev.davidsoergel.com snapshots" at "http://dev.davidsoergel.com/nexus/content/repositories/snapshots"

resolvers += "IESL Release" at "http://dev-iesl.cs.umass.edu/nexus/content/groups/public"

libraryDependencies ++= Seq(
  "org.mongodb" % "mongo-java-driver" % "2.11.3",
  "org.springframework.data" % "spring-data-mongodb" % "1.3.3.RELEASE",
  "com.ning" % "async-http-client" % "1.7.22",
  "joda-time" % "joda-time" % "2.3",
  "org.joda" % "joda-convert" % "1.5",
  "nz.ac.waikato.cms.weka" % "weka-stable" % "3.6.10",
  "uk.ac.gate" % "gate-core" % "8.0",
  "cc.mallet" % "mallet" % "2.0.7",
  "tw.edu.ntu.csie" % "libsvm" % "3.17",
  "commons-logging" % "commons-logging" % "1.1.3",
  "net.sourceforge.htmlunit" % "htmlunit" % "2.15",
  "jaxen" % "jaxen" % "1.1.4",
  "net.sourceforge.htmlcleaner" % "htmlcleaner" % "2.7",
  "org.jsoup" % "jsoup" % "1.7.3",
  "cc.factorie" % "factorie_2.11" % "1.2",
  "cc.factorie.app.nlp" % "pos" % "1.0-RC5" intransitive(),
  "org.scalatest" % "scalatest_2.10" % "1.9.2" % "test",
  "de.l3s.boilerpipe" % "boilerpipe" % "1.1.0",
  "com.github.rholder" % "snowball-stemmer" % "1.3.0.581.1",
  "org.jfree" % "jfreechart" % "1.0.17",
  "tw.edu.ntu.csie" % "libsvm" % "3.17",
  "de.bwaldvogel" % "liblinear" % "1.94"
)

libraryDependencies ++= Seq("spring-beans", "spring-tx", "spring-core", "spring-context", "spring-aop", "spring-expression") map ("org.springframework" % _ % "4.0.0.RELEASE")